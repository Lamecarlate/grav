/* global THEME_URL */

// heavily inspired by https://mxb.dev/blog/color-theme-switcher/
class ThemeSwitcher {
  constructor() {
    this.themes = { DARK: 'dark', LIGHT: 'light' };
    this.activeTheme = document.documentElement.dataset.theme;
    this.hasLocalStorage = typeof Storage !== 'undefined';
  }

  init() {
    this.createButton().then(() => {
      if (this.activeTheme === undefined) {
        this.activeTheme = window.matchMedia('(prefers-color-scheme: dark)')
          .matches
          ? this.themes.DARK
          : this.themes.LIGHT;
      }
      this.setIcon();
      this.setTitle();

      this.switcher.addEventListener('click', () => {
        this.setTheme(this.getOppositeTheme(this.activeTheme));
        this.setIcon();
        this.setTitle();

        document.body.dispatchEvent(new Event('theme-changed'));
      });
    });
  }

  async createButton() {
    const wrapper = document.querySelector('.theme-switcher-wrapper');
    const button = document.createElement('button');
    button.classList.add('theme-switcher');

    this.setAttributes(button, {
      type: 'button',
      id: 'theme-switcher',
      'data-title-dark': wrapper.getAttribute('data-title-dark'),
      'data-title-light': wrapper.getAttribute('data-title-light')
    });

    button.appendChild(await this.getIcon('sun-filled.svg', 'light'));
    button.appendChild(await this.getIcon('moon-filled.svg', 'dark'));

    wrapper.appendChild(button);

    this.switcher = button;
  }

  setTheme(theme) {
    this.activeTheme = theme;
    document.documentElement.setAttribute('data-theme', theme);

    if (this.hasLocalStorage) {
      window.localStorage.setItem('theme', theme);
    }
  }

  setIcon() {
    this.switcher
      .querySelector(`.theme-${this.activeTheme}`)
      .classList.add('hidden');
    this.switcher
      .querySelector(`.theme-${this.getOppositeTheme(this.activeTheme)}`)
      .classList.remove('hidden');
  }

  setTitle() {
    const title = this.switcher.dataset[
      this.toCamelCase(`title-${this.getOppositeTheme(this.activeTheme)}`)
    ];

    this.switcher.setAttribute('title', title);
    this.switcher.setAttribute('aria-label', title);
  }

  toCamelCase(string) {
    return string.replace(/-([a-z])/g, function (g) {
      return g[1].toUpperCase();
    });
  }

  getOppositeTheme(theme) {
    return theme === this.themes.LIGHT ? this.themes.DARK : this.themes.LIGHT;
  }

  setAttributes(el, attrs) {
    Object.keys(attrs).forEach(key => el.setAttribute(key, attrs[key]));
  }

  async getIcon(fileName, themeName) {
    const text = await fetch(
      THEME_URL + '/dist/icons/' + fileName
    ).then(response => response.text());

    let element = document.createElement("span");
    element.innerHTML = text;
    element.firstChild.classList.add(`theme-${themeName}`,'icon-svg');
    element.firstChild.setAttribute('aria-hidden', true);

    return element.firstChild;
  }
}

export default ThemeSwitcher;
