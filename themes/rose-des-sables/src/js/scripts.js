import '../scss/style.scss';

import ThemeSwitcher from './theme-switcher';
import './icons';

(function (window, undef) {
  'use strict';

  document.querySelector('html').classList.remove('no-js');
  document.querySelector('html').classList.add('has-js');

  // this whole thing only makes sense if custom properties are supported -
  // so let's check for that before initializing our switcher.
  if (window.CSS && window.CSS.supports('color', 'var(--fake-var)')) {
    const themeSwitcher = new ThemeSwitcher();
    themeSwitcher.init();
  }

})(window);

