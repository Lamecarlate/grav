<?php

namespace Grav\Theme;

use Grav\Common\Theme;

class RoseDesSables extends Theme
{
    public static function getSubscribedEvents()
    {
        return [
            'onThemeInitialized' => ['onThemeInitialized', 0],
            'onTwigLoader' => ['onTwigLoader', 0],
            'onPagesInitialized' => ['onPagesInitialized', 0],
            'onTwigExtensions' => ['onTwigExtensions', 0],
        ];
    }

    public function onThemeInitialized()
    {
        if ($this->isAdmin()) {
            $this->active = false;

            return;
        }

        $this->enable([
            'onOutputGenerated' => ['onOutputGenerated', 0],
        ]);
    }

    public function onTwigLoader()
    {
    }

    public function onPagesInitialized()
    {
    }

    public function onTwigExtensions()
    {
    }

    public function onOutputGenerated()
    {
        // Get raw content
        $raw = $this->grav->output;

        // If raw content contains "<feed", it's a feed (RSS or Atom), or a json, we do not want to modify it.
        if (strpos($raw, '<feed') !== false || strpos($raw, '<rss') !== false || strpos($raw, '{') === 0) {
            return;
        }

        $dom = new \DOMDocument('1.0', 'utf-8');

        // Because SVG & other new tags are not recognized.
        libxml_use_internal_errors(true);
        $dom->loadHTML($raw);
        libxml_use_internal_errors(false);

        $dom = $this->overrideFootnotes($dom);

        $raw = $dom->saveHTML();

        // Write back the modified html.
        $this->grav->output = $raw;
    }

    private function overrideFootnotes(\DOMDocument $dom): \DOMDocument
    {
        $xpath = new \DOMXpath($dom);
        $footnotes = $xpath->query('//div[@class="footnotes"]');

        foreach ($footnotes as $footnote) {
            $horizontalLines = $xpath->query('./hr', $footnote);
            foreach ($horizontalLines as $hr) {
                $footnote->removeChild($hr);
            }

            $links = $xpath->query('./ol/li/p/a[@rev]', $footnote);
            foreach ($links as $link) {
                $link->nodeValue = '^';
                $link->setAttribute('title', 'Retour au texte');
            }
        }

        return $dom;
    }
}
