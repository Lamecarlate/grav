const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const LiveReloadPlugin = require('webpack-livereload-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = {
  mode: 'development',
  watch: true,
  entry: './src/js/scripts.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'scripts.js'
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'style.css',
      chunkFilename: '[id].css',
    }),
    new LiveReloadPlugin({
      appendScriptTag: true,
    }),
    new StyleLintPlugin({
      syntax: 'scss',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.(sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: (loader) => [require('autoprefixer')()],
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
        type: 'javascript/auto',
      },
      {
        test: /\.js/,
        use: ['eslint-loader'],
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          'file-loader?context=assets/&name=icons/[name].[ext]&publicPath=build/',
          {
            loader: 'image-webpack-loader',
            options: {
              svgo: {
                plugins: [
                  {
                    removeViewBox: false,
                    removeDimensions: true
                  },
                ],
              },
            },
          },
        ],
      },
    ],
  },
};
