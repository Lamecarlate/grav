<?php


class Twig_Extensions_Extension_Recipe extends Twig_Extension
{

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        $filters = array(
            new Twig_SimpleFilter('machine_time', array($this, 'display_machine_time')),
            new Twig_SimpleFilter('time_values', array($this, 'process_time')),
        );

        return $filters;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $functions = array(

        );

        return $functions;
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'recipe';
    }

    /**
    * Render machine time from any string representing a duration.
    *
    * @param string $value
    */
    public function display_machine_time ($value) {
        $values = $this->process_time($value);

        // Now that we have hours and minutes we can display them as machine time.
        $formatted_value = "PT";

        if ($values[0] !== 0) {
            $formatted_value .= $values[0]."H";
        }

        $formatted_value .= $values[1]."M";

        return $formatted_value;
    }

    public function process_time ($value) {
        preg_match_all('/([0-9])*([0-9])/', $value, $out);

        // If only a number was given, we consider it was minutes
        if ($value === (int)$out[0][0] || $value === $out[0][0]." minutes" || $value === $out[0][0]."min") {
            $return = array(0, $out[0][0]);
        } else {
            $return = $out[0];
        }

        return $return;
    }
}
