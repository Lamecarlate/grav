<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;
use RocketTheme\Toolbox\Event\Event;

/**
 * Class CustomTwigExtensionsPlugin
 * @package Grav\Plugin
 */
class CustomTwigExtensionsPlugin extends Plugin
{
    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0]
        ];
    }

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized()
    {
        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {
            return;
        }

        // Enable the main event we are interested in
        $this->enable([
            'onTwigExtensions' => ['onTwigExtensions', -100],
        ]);
    }

    /**
     * Do some work for this event, full details of events can be found
     * on the learn site: http://learn.getgrav.org/plugins/event-hooks
     *
     * @param Event $e
     */
    public function onPageContentRaw(Event $e)
    {

    }

    public function onTwigExtensions()
    {
        require_once(__DIR__ . '/Icon.php');
        $this->grav['twig']->twig->addExtension(new \Twig_Extensions_Extension_Icon());

        require_once(__DIR__ . '/File.php');
        $this->grav['twig']->twig->addExtension(new \Twig_Extensions_Extension_File());

        require_once(__DIR__ . '/Text.php');
        $this->grav['twig']->twig->addExtension(new \Twig_Extensions_Extension_Text());

        require_once(__DIR__ . '/Recipe.php');
        $this->grav['twig']->twig->addExtension(new \Twig_Extensions_Extension_Recipe());
    }
}
