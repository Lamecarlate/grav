<?php


class Twig_Extensions_Extension_File extends Twig_Extension
{

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        $filters = array(
            //  new Twig_SimpleFilter(),
        );

        return $filters;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $functions = array(
             new Twig_SimpleFunction('get_file', array($this, 'get_file')),
        );

        return $functions;
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'file';
    }

    /**
     * Returns the contents of a file to the template.
     * @source: https://github.com/kgilden/KGStaticBundle/blob/master/Twig/StaticExtension.php
     *
     * @param string $path    A logical path to the file (e.g '@AcmeFooBundle:Foo:resource.txt').
     *
     * @return string         The contents of a file.
     */
    public function get_file($path)
    {
        return file_get_contents($path);
    }
}
