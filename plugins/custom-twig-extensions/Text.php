<?php


class Twig_Extensions_Extension_Text extends Twig_Extension
{

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        $filters = array(
            new Twig_SimpleFilter('nl2p', array($this, 'nl2p')),
        );

        return $filters;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $functions = array(

        );

        return $functions;
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'text';
    }

    public function nl2p($value) {
      $paragraphs = '';

      foreach (explode("\n", $value) as $line) {
          if(trim($line)) {
              $paragraphs .= '<p>' . $line . '</p>';
          }
      }

      return $paragraphs;
    }
}
