<?php

use Grav\Common\Utils;
use Grav\Common\Twig\TwigExtension as GravTwigExtension;

class Twig_Extensions_Extension_Icon extends Twig_Extension
{

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        $filters = array(
            //  new Twig_SimpleFilter(),
        );

        return $filters;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $functions = array(
             new Twig_SimpleFunction('get_icon', array($this, 'get_icon')),
             new Twig_SimpleFunction('svg_image', [$this, 'customSvgImageFunction']),
        );

        return $functions;
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'icon';
    }

    public function get_icon($id, $role = "presentation") {
        return
        '<svg class="icon-svg icon-' . $id . '" role="' . $role . '" width="16" height="16">
            <use xlink:href="#icon-' . $id . '" />
        </svg>';
    }

    /**
     * Returns the content of an SVG image and adds extra classes as needed
     *
     * @param string $path
     * @param string|null $classes
     * @return string|string[]|null
     */
    public static function customSvgImageFunction($path, $classes = null, $attributes = null)
    {
        $attributesString = '';
        if(!empty($attributes)) {
            foreach($attributes as $attribute) {
                foreach($attribute as $name => $value) {
                    $attributesString .= $name . '="' . $value . '" ';
                }
            }
        }

        $svg = GravTwigExtension::svgImageFunction($path, $classes, true);
        $svg = str_replace('<svg ', "<svg aria-hidden='true' ", $svg);
        $svg = str_replace('<svg ', "<svg ". $attributesString, $svg);

        return $svg;
    }
}
