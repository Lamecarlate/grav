---
title: Contact
menu: Contact
body_classes: 'page--contact'
published: false
form:
    name: contact-me
    fields:
        -
            name: name
            id: contact-me-name
            label: Nom
            autocomplete: on
            type: text
            validate:
                required: true

        -
            name: email
            id: contact-me-email
            label: Email
            type: email
            validate:
                rule: email
                required: true

        -
            name: message
            id: contact-me-message
            label: Message
            size: long
            type: textarea
            validate:
                required: true
        -
            name: url
            type: honeypot

    buttons:
        -
            type: submit
            value: Envoyer le message
            classes:

    process:
        - email:
            from: "{{ config.plugins.email.from }}"
            to:
              - "{{ config.plugins.email.from }}"
              - "{{ form.value.email }}"
            subject: "[Feedback] {{ form.value.name|e }}"
            body: "{% include 'forms/data.html.twig' %}"
        - save:
            fileprefix: feedback-
            dateformat: Ymd-His-u
            extension: txt
            body: "{% include 'forms/data.txt.twig' %}"
        - message: Merci, votre message est parti !
        - display: thankyou
---

# Contact

Vous pouvez me contacter à travers ce formulaire, je vous répondrai au plus vite :)
