---
meta-création: 2024-09-17 21:35
meta-modification: 2024-09-17 21:35
stringified-date: "20240917.213527"
title: Liens de septembre 2024 (partie 2)
template: blog-item
date: 2024-09-17 21:35
update_date: 
taxonomy:
  type: article
  category:
    - liens
  tag:
    - mignonneries
    - webdesign
    - jeu vidéo
    - lecture
    - sociologie
extra_content:
  items: "@self.modular"
highlight:
  enabled: false
---

Du bon webdesign, des jolies bêtes, de la mythologie égyptienne, de la linguistique enthousiaste, de la création, de la balade en forêt.

===

[Kit pour un site web d'urgence](https://mxb.dev/blog/emergency-website-kit/) (en anglais).

---

[Drhydne hérisson - Champidragon](https://www.artfol.co/a/BtAMLpI) (image sans texte alternatif, la description suit).

C'est un petit animal imaginaire enroulé autour d'une souche couverte de mousse, un mélange de dragon et de champignon, blanc et très duveteux, avec une expression surprise de voir une coccinelle à quelques centimètres de son nez. 

---

J'ai déjà parlé de _Godslave_, en [octobre dernier](../2023/20231031.liens-octobre-partie-2/Liens%20d'octobre%20(partie%202).md). La dernière page en date faisait référence à une vieille réplique, donc, heu, je me relis tout le webcomic. Ce faisant, je repense à ces dessins qui avaient fleuri sur le net fut un temps, les ["Stick Gods" d'Inonibird](https://inonibird.tumblr.com/stick-gods). Et de fil en aiguille je découvre la déesse [Mafdet](https://en.wikipedia.org/wiki/Mafdet), guérisseuse qui châtie les criminels et a une forme de guépard 😻 (ou de panthère). Inonibird la représente plutôt avec la tête d'un caracal, pour bien la distinguer des autres déesses félines, et je trouve que ça va !

Quel est votre niveau en Microsoft Excel ? [« Oui. »](https://www.youtube.com/watch?v=iCeOEQVUWZ0) (vidéo avec du son, pas de paroles, 6 minutes). Donc. Heu. C'est quelqu'un qui a fait une sorte de Doom-like dans Excel. Juste avec des formules dans les cases. Enfin, « juste ». C'est à la fois génial et terrifiant, je trouve. Via l'Amoureux.

> On dit pas "Le château miaulera" mais "Castle va nya"

Source : [Marud](https://social.marud.fr/notes/9ygt5yivepv11pe6)

[Why we should go back to runes](https://www.youtube.com/watch?v=4npuVmGxXuk) (vidéo en anglais, avec du son et des sous-titres, 20 minutes). Une chouette vidéo sur les runes norroises et le fait qu'on pourrait simplifier l'écriture de l'anglais en les utilisant de nouveau. (ne pas lire les commentaires récents, comme la vidéo mentionne les nazis, ça attire certaines populations qui se sentent obligées de défendre leurs idoles) Via [Augier](https://diaspodon.fr/@AugierLe42e/113168862733892326).

[The forgotten history of how automakers invented the crime of "jaywalking"](https://www.vox.com/2015/1/15/7551873/jaywalking-history) (en anglais). C'est déprimant : comment les fabricants et vendeurs d'automobiles ont transformé les rues, en ridiculisant le fait de traverser puis en le faisant devenir un crime.

[Les plaids c'est bien](https://piaille.fr/@Lesdondons/113231723707593430) (gif animé, gardez votre souris sur l'image ou cliquez dessus).

[Éclipse de tissu](https://universeodon.com/@Celie/113216056940207524) (image avec description et texte alternatif en anglais, voici ma traduction : « Ma femme fait des dessus-de-lit, voilà son Éclipse, suspendu à notre réfrigérateur ; un design de dessus-de-lit en tissu avec des volutes colorées qui imitent une éclipse solaire. »

[Lever la tête en forêt](https://mstdn.social/@lifewithtrees/113256327455419327) (image avec description et texte alternatif en anglais, voici ma traduction : « Chaque feuille est une fleur ; rouges, verts et jaunes de la canopée forestière. » 

---

Petite fournée, malgré la liste qui s'allonge dans mon groupe d'onglets « Articles en cours » 😬 les journées ont été bien remplies (notamment par une petite semaine à Paris pour Paris Web, je vais essayer de faire une restitution, mais bon, j'ai échoué l'an dernier, et l'année d'avant…).
