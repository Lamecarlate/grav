---
meta-création: "2024-07-25 07:43"  
meta-modification: "2024-07-28 09:37:35"  

stringified-date: "20240725.074302"

title: "Ma synesthésie"  
template: "blog-item"  

date: "2024-07-28 07:43"  
update_date: ""

taxonomy:
    type: article
    category:
        - personnel
    tag:
        - sciences
        - linguistique
        - art

# plugin Highlight
highlight:
    enabled: false

---

Rien à voir avec le poème de Rimbaud, il n'y a guère que le O bleu qui corresponde !

===

Depuis longtemps, j'associe des lettres et des sons à des couleurs. C'est léger, c'est seulement certaines lettres, c'est « juste » une couche supplémentaire, uniquement dans le creux de mon cerveau, je ne **vois** pas les lettres de ces couleurs-là. Cela ne m'occasionne pas de blocage de lecture comme ça peut le faire pour d'autres. Je sais qu'il y  a des gens qui ont eu de gros soucis pour apprendre, parce que les lettres n'avaient pas la bonne couleur… estimons-nous heureuse.

Bref, je vois des couleurs dans ma tête quand je pense à des lettres ou des sons, et ça va bien.

## Le O et le U

Pour moi, le O (tant la lettre que le son) est bleu. Un bleu plutôt foncé, à la teinte peu précise. Le U, plus mince, plus pointu, est rouge. Un rouge standard, pas très loin du magenta quand même. Généralement je les pense ensemble, et le OU (le son, plutôt que les lettres) est violet. Un violet profond, lumineux, avec souvent une petite tache rouge.

J'ai essayé de représenter en image ces couleurs. Ce n'est pas exactement ça, mais je n'ai pas l'impression de pouvoir faire mieux. Après tout, ce sont des couleurs dans ma tête, je ne connais pas leur code hexadécimal 😸 (mais j'aurais adoré).

![Représentation de la description plus haut du violet avec un centre rouge dégradé.](ou.png)

Le OU est tout petit, il est rond et je peux l'imaginer moelleux. Ça a sûrement un rapport avec le fait que j'appelle tous les animaux avec des surnoms contenant ce son : doudou, rourou, loute, pougnou (et pougnoute), bouchon, noune (qui vient de ma maman). (et un peu les petits enfants mais pas trop fort, je suis déjà bizarre sans que je parle, alors si j'en rajoute…)
## Le I

Le I est large, brillant, jaune, comme un sourire. L'image dessous ne lui rend pas grand hommage mais je n'ai pas réussi à trouver la bonne teinte. C'est censé être un jaune doré, comme les pétales des boutons d'or. 

![Représentation de la description plus haut, un jaune uni.](i.png)

## Le È

Du plus loin que je me souvienne, I et OU sont là. Mais je peux à peu près situer l'arrivée du È dans le temps. C'était lors d'une répétition de chorale, peut-être en 2019. La cheffe nous scandait un texte que nous allions travailler.

Et elle répète "è è è". Là, une illumination. C'est vert. C'est vert pâle, vert amande je dirais. J'ai dû rester bouche bée quelques secondes. Et ça s'est ajouté à la liste, l'augmentant de 33% d'un coup.

![Représentation de la description plus haut, un vert pâle uni.](è.png)

---

Je sais que plusieurs des associations que je fais sont aussi sociétales, linguistiques ou « logiques » (le OU qui est petit, le I large comme un sourire), mais c'est mêlé dans ma tête aux couleurs, c'est comme ça.

Voilà, billet rapide pour décrire ce phénomène très léger mais présent chez moi. J'ignore pourquoi ça a mis tant de temps à être écrit.