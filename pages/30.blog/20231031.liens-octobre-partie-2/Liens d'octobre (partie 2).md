---
meta-création: "2023-10-23 12:20"  
meta-modification: "2023-10-23 12:22"   

stringified-date: "20231001.115515"

title: "Liens d'octobre (partie 2)"  
template: "blog-item"  

date: "2023-11-04 7:55"  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - humour
        - informatique
        - mignonnerie
        - linguistique

# plugin Highlight
highlight:
    enabled: false

---

Fournée d'octobre, deuxième ! Webcomics encore, un logiciel pour enregistrer son écran en vidéo (oui, c'est un besoin de niche), ce qui manque à la langue anglaise, un guide sur les écureuils, comment bien jouer à Ouno, un chat fantôme, des citrouilles et des tulipes. 

===

[Edgar Allan Poe, dans Crow Time](https://www.webtoons.com/en/canvas/crow-time/edgar-allan-poe/viewer?title_no=693372&episode_no=113). Il serait temps que le monde sache à quel point _Crow Time_ est bieeeeeen.

Profitons-en pour présenter quelques uns de mes webcomics préférés, allez ! J'en lis beaucoup. Genre : **beaucoup**.

![FreshRSS-webcomics](FreshRSS-webcomics.mp4) 

- [Godslave](https://www.godslavecomic.com/comic/chapter-one) : Edith va au musée d'égyptologie, elle libère par hasard un dieu enfermé, des péripéties s'ensuivent (en anglais).
- [Wilde Life](https://www.wildelifecomic.com/comic/1) : Oscar, fuyant on ne sait quoi, arrive à Podunk, petite ville de l'Oklahoma, "a real ghost town" (en anglais).
- [Castoff](https://www.castoff-comic.com/comic/1) : Vector, reclus dans sa librairie, se fait kidnapper et commence un long voyage pour rentrer (en anglais).
- [Sleepless Domain](https://www.sleeplessdomain.com/comic/chapter-1-cover) : une ville enfermée derrière une barrière magique est envahie chaque nuit par des monstres, donc tout un système de magical girls a été mis en place (marketing inclus) ; on suit les aventures de l'équipe Alchemist (en anglais).
- Oh, et évidemment [Daughter of the lilies](https://www.daughterofthelilies.com/dotl/part-1-a-girl-with-no-face) : Thistle est une mage dans un monde médiéval-fantastique, avec des pouvoirs étranges et une anxiété à toute épreuve ; il faut dire que bien souvent elle quitte les villages en courant poursuivie par une foule armée de fourches. Ne lisez rien autour, surtout pas les commentaires ou les pages récentes, l'identité du personnage principal doit rester un mystère tant que vous n'êtes pas arrivé⋅e au bon moment de l'histoire (en anglais).
- [Pepper & Carrot](https://www.peppercarrot.com/fr/) : les aventures de la petite sorcière Pepper (et de son chat Carrot) ; à noter que c'est une BD libre, open-source et à la traduction collaborative (filez des sous à l'auteur svp).
- [Crow Time](https://www.webtoons.com/en/canvas/crow-time/welcome-to-crow-time/viewer?title_no=693372&episode_no=1), mentionné plus haut : ce sont des historiettes mettant en scène des corbeaux, des pigeons (biblically accurate), des elfes et autres personnages (en anglais)(je lis beaucoup plus en anglais qu'en français).
- [Namesake](https://www.namesakecomic.com/comic/the-journey-begins), de la même artiste que Crow Time : Emma découvre que notre monde est lié à bien d'autres, décrits dans les contes de fée, et qu'elle est elle-même une "namesake", une éponyme, capable d'ouvrir des portails vers ces mondes ; mais elle arrive à Oz, où tout le monde attend une Dorothy (en anglais).

Je découvre le logiciel _[Simple Screen Recorder](https://www.maartenbaert.be/simplescreenrecorder/)_, qui est… eh bien, simple, et permet d'enregistrer en vidéo tout ou partie de son écran (pour Linux). Je m'en suis servie pour la micro-vidéo plus haut, la liste animée des webcomics dans mon lecteur de flux RSS (FressRSS, c'est trop bien, utilisez-le !). 

[Deux mètres de mystère absolu](https://sacripanne.net/post/2023/10/23/Deux-m%C3%A8tres-de-myst%C3%A8re-absolu) : j'aime la plume de Sacrip'Anne. Je lui envie sa capacité à voir les jolies choses du monde et à en écrire des histoires.

[All that we share](https://www.youtube.com/watch?v=jD8tjhVO1Tc) : vidéo avec du son en anglais non sous-titré (et danois sous-titré en anglais). C'est une publicité, mais comme certaines pubs, rares, elle est touchante, elle tape juste là sous le nerf-qui-fait-pleurer. On se met dans des cases (ou la société, les parents les ami⋅es l'école le travail) : et si on voyait d'autres cases… ou aucune. C'est beau, et les gens qui ont participé à cette vidéo sont courageux et courageuses (si tant est que ça ne soit pas complètement scripté, mais laissez-moi rêver).

[Un guide pour les chiens sur les écureuils](https://masto.ai/@cybeardjm/111299044867383054) (en anglais). Accurate/20, woofld recommend. ([Page Instagram de l'artiste](https://www.instagram.com/doodleforfood/))

J'adore les "Linguistic files" de Tom Scott, c'est super intéressant ! Ici, quatre caractéristiques qui manquent (selon lui) à l'anglais et qui se retrouvent dans d'autres langues : [Fantastic Features We Don't Have In The English Language](https://www.youtube.com/watch?v=QYlVJlmjLEc) (vidéo avec du son, en anglais avec sous-titres). Que répondriez-vous à la dernière question ?

Je veux aller vivre au Japon pour prendre [ce train-chat](https://masto.allrite.at/@allrite/111298583996067527) tous les jours.

[Belle image de foudre sur un arbre](https://web.archive.org/web/20231027124402/https://nitter.poast.org/womensart1/status/1716706921804083312#m) (le lien est une version archivée d'un front-end alternatif à Twitter, parce que je n'ai plus aucune confiance en Twitter et que je n'ai pas réussi à sauver la page dans la Web Archive… c'est dire).

[C'est une tulipe, sisisi](https://oisaur.com/@hellgy/111302528026743301).

[Ouno, le jeu](https://www.youtube.com/watch?v=Defpcu0MBPM) (vidéo avec du son) : c'est… heu… c'est Bon ben voilà en pleine forme, qui passe du rire au glauque facilement.

[L'a beugné la gravité, l'était impécab](https://mamot.fr/@Kitishin/111216308529455795).

[Boo'lean](https://infosec.exchange/@i0null/111307475269741345) (la source originelle semble être [Kaz Miyamo](https://twitter.com/38mo1/status/1320004943542009857)) : comment expliquer les portes logiques avec une citrouille.

[Cuddle button](https://www.reddit.com/r/cats/comments/17f7llf/does_your_cat_have_this_button_to_activate_the/) (vidéo avec du son) : <span role="img" aria-label="Sourire radieux avec plein de cœurs">🥰</span>.

[Le chat fantôme](https://www.youtube.com/watch?v=9NAJvQ8o9Jk) (vidéo avec du son, en français sans sous-titres). 

> Cette vidéo a été filmée le 1<sup>er</sup> novembre 2016. Je n'ai plus jamais revu le chat fantôme après ça.

[Peut-on rire de tout ? sur Arte](https://www.arte.tv/fr/videos/109818-011-A/peut-on-rire-de-tout/).

[Mieux encore que les maids cafes !](https://mendeddrum.org/@tigerfort/111335136160992053) (en anglais)

[Searching for humanity in Fortnite, par Pop Detective](https://www.youtube.com/watch?v=SArtgoMMDNg) : une intéressante expérience sociale, se faire des ami⋅es dans le jeu vidéo de type Battle Royale Fornite (vidéo avec du son, en anglais, sans sous-titres hélas).

--- 

Le mois prochain sera possiblement moins fourni : novembre, c'est le NaNoWriMo, et j'ai des histoires sur le feu (je vais essayer de produire au moins quelques nouvelles, et peut-être revenir sur mon projet « Punks à écureuils », une discussion fin septembre dans un bar après Paris Web m'en ayant redonné le goût).