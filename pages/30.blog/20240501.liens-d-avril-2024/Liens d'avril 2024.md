---
meta-création: "2024-04-05 08:05"
meta-modification: "2024-04-05 08:05"

stringified-date: "20240405.080518"

title: "Liens d'avril"
template: "blog-item"

date: "2024-05-01 17:00"
publish_date: ""
published: true # si besoin de date de publi + force publi

taxonomy:
  type: article
  category:
    - liens
  tag:
    - mignonneries
    - linguistique
    - jeux vidéo
    - humour
    - sciences
    - cuisine
    - jeux de rôle

# plugin Highlight
highlight:
  enabled: false
---

Ce mois-ci, un seul article, je m'étais trop décalée pour poster autour du 15, j'ai pris pas mal de temps pour faire une belle transcription (vous verrez) et pis zut hein c'est mon carnet. Nan mais oh.

(j'aime bien faire des transcriptions de BD mais c'est loooooong ohlala)

Du thé, des trucs mignons, de l'astronomie et des jeux de mots, un peu d'horreur et d'informatique.

===

[Disability aids](https://chaos.social/@madrabbit/112224814858843681) (en anglais).

[Une petite salamandre](https://piaille.fr/@teol03/112225887075339126).

[De l'art accidentel](https://nitter.poast.org/JamesLucasIT/status/1775938441106067739#m) : des marques de rouille, de cailloux, l'eau sur la vitre, la réflexion de la lumière, la météo, bref, tout un tas de photos où les conditions aléatoires créent de l'art. Pas de alt, malheureusement : je vous emmène sur Nitter, un front-end alternatif pour Twitter, puisque Twitter ne permet plus de voir les fils sans avoir de compte, et Nitter ne récupère pas les alt des images 😓

[Des chats qui marchent](https://diaspodon.fr/@ayoli/111809487035567266) (vidéo avec du son non-essentiel à la compréhension, une demi-minute).

Le [simulateur de thé de Terry Cavanaugh](https://downpour.games/~terry/a-proper-cup-of-tea) est hilarant, il faut vraiment que vous l'essayiez ! C'est en anglais, utilisable à la souris et au clavier. Via [chris martens](https://hci.social/@chrisamaphone/112180416885907926).

[Un chat roux qui miaugraule, mruuuke et maraouuule](https://www.reddit.com/r/cats/comments/1bx7pgz/can_anyone_understand_orangese/) (vidéo avec du son, pas de voix humaine, 1 minute).

[L'accent québécois, ça vient d'où](https://www.youtube.com/watch?v=CQ46BbbLRrk) (vidéo avec du son, pas de sous-titres français… mais des sous-titres anglais et espagnols, une demi-heure environ).

[Une hermine](https://pixelfed.social/p/nhoizey/681685785012323721).

[Pourquoi les jeux vidéo sont dominés par les flingues](https://www.youtube.com/watch?v=nnWUvnP_e8Y) (vidéo avec du son, des sous-titres, 40 minutes, attention : il y a beaucoup de sang et de créatures virtuelles qui meurent violemment) : partez pas, partez pas, ce n'est pas une énième panique morale sur les jeux vidéo qui rendent nos z'enfants violents, non non, bien au contraire. Excellente analyse sur les rapports entre les flingues (ici définis par « tout objet visant à envoyer des projectiles dont la finalité est d'endommager l'intégrité physique d'une entité sentiente ») et les jeux vidéos. Le passage sur le partenariat entre des éditeurs de jeu et des fabricants d'armes, afin d'avoir des détails physiques pour un réalisme de plus en plus poussé, et surtout le droit d'utiliser les vrais noms des armes, est glaçant.

[Homak Gun Safe Opened With Orange Juice Bottle](https://www.youtube.com/watch?v=Chu4mvEUc5I) (vidéo avec du son, pas de sous-titres, 2 minutes) : comment ouvrir un coffre-fort pour arme à feu (donc vendu comme quelque chose de sécurisé) de la marque Homak… avec un bout de plastique. C'est une honte de commercialiser ça, vu la facilité d'ouverture. Via [sebsauvage](https://sebsauvage.net/links/?2-EWsQ).

---

En ce moment, je relis les archives du blog de Boulet. [Les bisounours sont-ils carnivores ?](https://bouletcorp.com/notes/2010/03/23) (pas de texte alternatif, je mets une transcription plus bas), c'est une tranche de vie de Boulet en dédicace qui trouve que les clowns c'est quand même un peu malsain, et la femme en face de lui n'est pas d'accord. Elle, elle ne déteste personne !

Transcription personnelle :

Case 1 : Boulet, assis à une petite table de dédicace dans une grande salle à haut plafond, regarde la foule derrière lui. Il y a un gars en costume vert sur des échasses.

Case 2 : Il dit « J'ai jamais trop capté le coup des types sur des échasses, là… ». Un homme dans la file d'attente répond, peu assuré, « Heu… Je sais pas, c'est joli ? ».

Case 3 : Boulet rétorque « Bah chais pas : ils se promènent juste comme ça. Et des fois ils te regardent alors toi t'es là : Haha ! Super ! Hooo, t'es vachement haut ! » Il a le pouce levé et un sourire crispé.

Case 4 : il continue « C'est comme les clowns. Quand j'étais petit, ça me mettait super mal à l'aise. » Une femme, possiblement la copine de l'homme mentionné plus tôt, s'étonne « Les clowns ? »

Case 5 : c'est un flash-back. On voit un clown avec une perruque verte très touffue, un maquillage blanc et un nez rouge, dans un costume blanc et rouge avec une fraise. À l'arrière-plan, un Boulet enfant tremble de peur, les yeux écarquillés. Boulet (en voix off) reprend « Bah oui. Au départ tu vois un bonhomme multicolore qui fait le con alors c'est rigolo… Mais après il s'approche de toi et tu le vois de près… »

Case 6 : un gros plan sur le visage du clown, avec des flèches pour montrer que les cheveux sont faux, qu'il a des cernes, une haleine d'adulte. « Tu vois le maquillage gras, la sueur qui perle, les poils de barbe : tu réalises que c'est un _adulte_… et un adulte qui se comporte en cinglé, je trouvais ça super malsain. »

Case 7 : la femme, souriante, répond « Ha ? Moi j'aime bien tout ces trucs, là… les échasses, les clowns. C'est chouette, je trouve ! ». Boulet, pas convaincu « Ha ouais ? »

Case 8 : la femme continue « Oui, ça met de la bonne humeur, c'est joli. Ça fait “fête”… Bref, c'est pas malsain du tout ! » Boulet « La vache ! Tu es sérieuse ? »

Case 9: Boulet, ébahi, continue « Hooo ! Un Bisounours ! C'est donc à ça que vous ressemblez sous votre costume ! C'est dingue ! » Elle s'en défend « Non mais je… » L'homme la taquine « Haha tu peux y aller, c'est madame Gentille, elle aime tout le monde ! »

Case 10 : Boulet « Allez ! Sincèrement ? Il y a bien un truc qui t'énerve! Les ados ? Les coiffeurs ? Les taxis ? » Elle, commençant à rougir, « Hiiii non ! Il n'y a personne que je déteste ! »

Case 11 : Boulet, n'y croyant pas, « Incroyable ! Mais tu fais quel boulot pour être aussi gentille ? Nonne ? Marchande de glaces ? Miss France ? ». Elle répond « Beh non. Je suis libraire. »

Case 12 : Boulet éclate de rire « Mouhahahahahahaha ! Une libraire qui ne déteste personne : c'est ça ! ». Elle, un peu vexée : « bah quoi ? »

Case 13 : Boulet, avec un éclairage par en dessous, qui le rend sérieux et effrayant « Je connais bien ta profession. Je sais que, plus que nulle autre, elle est rongée par la colère et l'amertume. »

Case 14 : elle se défend « Mais enfin non ! Pas du tout ! ». Boulet se remet à dessiner :  « Bon, bon, j'ai rien dit. »

Case 15 : grand silence.

Case 16 : Boulet, sans lever les yeux « J'ai commandé ce livre il y a une semaine : appelez-moi le responsable. » Elle répond, se prenant au jeu, calmement « Je veux bien, monsieur, mais ce n'est pas lui non plus qui conduit le camion, il ne va pas le sortir sa poche par magie ! »

Case 17 : Boulet, imperturbable, continue « Est-ce que qu'il va bientôt y avoir un nouveau Tolkien ? » La femme, commençant déjà à fulminer « Il est mort il y a presque 40 ans ! Pourquoi pas un nouveau Jules Verne ? »

Case 18 : Boulet, hors-champ, « J'ai plus le ticket de caisse de ce bouquin, mais je ne l'ai pas aimé. Je voudrais l'échanger. » La libraire, montrant les dents « Haha mais oui : et après on pourrait aller au rayon informatique, je vous offrirai une Game Cube ! »

Case 19 : Boulet, décidément sans pitié « Bonjour, je cherche le dernier Lévy. Vous l'avez ? » Elle « Hahahaha vous n'avez pas remarqué les 450 exemplaires de cette merde à l'entrée du magasin ? »

Case 20 : « Je cherche un livre. Je ne sais plus le titre ni l'auteur, mais il a une couverture rouge. » (il se marre, l'enflure) La femme, hors champ, on voit juste sa main recroquevillée de colère, crie « Haha ce livre ! Mais bien sûr, c'est le seul qui est rouge ! »

Case 21 : Boulet, ayant fini sa dédicace, tend le livre à la femme, en assénant un coup supplémentaire « Ya Google sur votre ordinateur ? je voudrais vérifier un bouquin… » Elle, rouge de fureur, les yeux révulsés « Oui pis j'vous laisse les clefs ! Comme ça vous regarderez tranquillement vos mails ! » Le compagnon de la dame la prend doucement par l'épaule, tend la main pour prendre le livre et dit « (à Boulet) Merci… (à elle) Allez viens ! »

Case 22 : Boulet, pas rassasié pour un sou, les regardant partir, « J'ai acheté trois livres, je peux avoir une réduction ? Faites un geste commercial, quoi ! » Elle, se retournant à peine « Et le prix fixe des livres, tu connais ? Tu vas le voir, mon geste commercial ! » Son compagnon rit un peu (peut-être nerveusement à ce stade).

Case 23 : le prochain lecteur dans la file regarde avec effarement. « Hé bé… » Boulet, un sourire mauvais, dit « C'est presque trop facile avec les libraires. Je sais tout de leurs plaies vives. Je connais la noirceur qui pourrit leur âme ! » En hors champ, la libraire hurle « Qu'est-ce t'as, toi là-haut ? Tu veux du Werber, c'est ça ? », et on entend « Ha je… Vous allez me faire tomber ! Lâchez ça ! »

---

[Ça a changé, le WWF](https://aus.social/@dgar/112285996330296870).

[Empilement félin](https://eldritch.cafe/@Lisoo/112286302198089564).

Le clip de [Land of confusion](https://www.youtube.com/watch?v=TlBIa8z_Mts), de Genesis (vidéo avec du son, et des sous-titres fort bien faits) est assez spécial. Je connaissais la chanson sans l'avoir associée à Genesis, du coup j'ai été un brin surprise. Si vous trouvez un air de ressemblance des personnages avec les Guignols de l'info, c'est normal : ce sont ici les marionnettes de <i lang="en">Spitting Image</i>, une émission satirique britannique, qui a fortement inspiré la création des Guignols. Via [Laird Fumble](https://piaille.fr/@Laird_Fumble/112292221673816976).

Grâce à [Moutmout](https://framapiaf.org/@Moutmout/112291240594950809) (lisez tout le fil, il est très chouette), je découvre que [Galilée présentait ses travaux sous forme d'anagrammes](https://petiteshistoiresdessciences.com/2017/08/24/galilee-et-les-anagrammes/). OK, c'est amusant, mais gars, c'est relou quand même… surtout quand ça induit les copains en erreur.

[Cooking for my love](https://www.youtube.com/watch?v=vFX2aCc9ltA) (vidéo avec du son, en anglais, sans sous-titres hélas, 20 minutes) : décidément j'aime beaucoup les vidéos de Gaz Oakley, elles sont toutes calmes, il présente bien les étapes de préparation et de cuisson, on sent qu'il aime ce qu'il fait et c'est un espace tout doux, vert, brun et cuivré. Il manque juste les odeurs et le goût, parce que là, vraiment, les raviolis géants à l'ail des ours et champignons, je veux.

[Bel essai littéraire](https://kevinvuilleumier.net/2024/04/lindicible/) chez Kevin Vuilleumier.

[La part de la honte](https://grisebouille.net/la-part-de-la-honte/) sur Grisebouille : prendre la dernière part de gâteau, c'est difficile.

[The better of two sci-fi franchises](https://www.moneycontrol.com/europe/?url=https://www.moneycontrol.com/news/trends/disturbingly-clever-new-york-times-crossword-tricks-star-wars-star-trek-fans-8058871.html) (en anglais) : un magnifique puzzle de mots croisés (ici on a juste un extrait), dont la solution officielle était "Star Wars" _et_ "Star Trek", et les quatre mots verticaux permettant d'avoir "wars" ou "trek" avaient eux aussi deux solutions, parfaitement valides. J'adore.

[Long alts](https://adrianroselli.com/2024/04/long-alt.html) (en anglais) : je plaide un peu coupable. Rien que dans certains articles récents de liens, j'ai mis des tartines de texte dans les alts de plusieurs images, parce que je voulais touuuut décrire. C'est une chose que je vais travailler. Mais au moins je ne mets pas d'url ou de tags dans mes alts, ouf.

[The kettle](https://mastodon.social/@Teryl_Pacieco/112325233970855099) (en anglais).

[Une lentille pour voir en pixel-art](https://mastodon.social/@Lopinel/112330655022803801) : c'est une petite vidéo, sans son, qui montre une lentille carrée posée sur un socle et une personne pose une peluche Kirby derrière, qui se retrouve carrelée, comme faite en gros pixels, les yeux devenant juste deux carrés, un bleu et un blanc (pour le reflet).

Je regarde une vidéo de Feldup (un vidéaste que j'ai découvert récemment) et il mentionne cyriak et quelques unes de ses vidéos. Ça m'a donné envie de revoir [Welcome to Kitty City](https://www.youtube.com/watch?v=jX3iLfcMDCw) (vidéo avec du son, pas de sous-titres car pas de paroles). L'est-elle pas mignonne, cette boule de poils ? Non. NON. Elle est pas mignonne la boule de poils. Attention : ce sont des images qui peuvent être dérangeantes. cyriak est spécialisé dans les trucs bizarres, et notamment les animaux qui se déforment et se multiplient. Ce n'est pas gore à proprement parler, il n'y a pas de sang ou de chair, mais… c'est étrange, quoi. La musique, qu'il compose lui-même, est parfaitement adaptée, c'est de l'électronique avec des chromatismes, des répétitions lancinantes avec des modulations bien zarb' couplées à l'animation (du boulot normal de musique pour vidéo, quoi, mais hé, c'est très bien fait).

[30 heures, si les poissons pouvaient pleurer](https://www.youtube.com/watch?v=sJ4jx5J2NtQ) (vidéo avec du son et des sous-titres incrustés mais mouvants, 5 minutes) : une chanson sur la quantité astronomique d'animaux aquatiques qui sont tués chaque année pour être mangés (ainsi que les dégâts collatéraux). J'espère que cette chanson aura le succès qu'elle mérite : c'est une mélodie jolie et attirante, un peu rock, et le texte est vraiment fort. Et via les commentaires de la chanson, j'ai regardé [Anthropomorphisme](https://www.youtube.com/watch?v=tQVxybyHBww) de la même chaîne, c'était plutôt intéressant (vidéo avec du son, sans sous-titres, 2 minutes).

[My house walkthrough, de nana825763](https://www.youtube.com/watch?v=qWXnt2Z2D1E) (vidéo avec du son et des sous-titres japonais et anglais incrustés, 12 minutes) : brrrr. Vidéo recommandée par Feldup. C'est de l'horreur insidieuse. Pas de jump scares, non, c'est bien plus intelligent que ça. C'est filmé « à l'épaule », au cœur de la nuit, on sait juste qu'il y a un typhon au Japon et que le vent a réveillé l'auteur, alors il nous fait visiter sa maison. Attention : de l'ambiance glauque, de la répétition bizarre, et du rouge dont il ne faudra pas se poser la question de la provenance. La caméra ne s'attarde jamais sur les détails, à peine le temps de se dire « mais attends c'est pas un peu bizarre ce truc, là ? » qu'on est déjà en train de voir autre chose. À ne pas regarder en faisant autre chose, ça n'aurait aucun intérêt.

[Le jeu de la vie, par ego](https://www.youtube.com/watch?v=eMn43As24Bo) (vidéo avec du son, avec sous-titres, 30 minutes environ) : woah, c'est cool, le jeu de la vie de Conway. Je connaissais les bases, mais j'étais loin de me douter que des gens s'amusaient à construire des structures pour découvrir de quoi elles sont capables, c'est incroyable. Le petit glider que l'on suit dans notre voyage m'a irrésistiblement fait penser au Ship of the Imagination de Carl Sagan, dans son émission Cosmos.

[Firefox est meilleur que Chrome mais tout le monde s'en fout](https://www.youtube.com/watch?v=o8xjrFj5Xh8) (vidéo avec du son, sans sous-titres, 9 minutes) : ce titre est un peu provocateur, mais, oui, Firefox est meilleur, moins gourmand, et avec plus de fonctionnalités de protection de la vie privée que Chrome. Mais Chrome a de meilleurs moyens financiers… Pour ma part, c'est Firefox depuis la 1.5 (avec des petits passages à d'autres navigateurs, genre Maxthon (oui je suis vieille laissez-moi), Opera, ou Vivaldi, mais je suis toujours revenue au renard de feu), à la vie à la mort. Le truc qui me manque vraiment dans Firefox, par contre, et qu'ont les navigateurs sur base Chromium, c'est la possibilité de changer de profil à la volée. Ahhhh, ça, j'en rêve, pouvoir enlever toutes les extensions d'aide au développement (Clockwork, Wave, Tanaguru Webext, HTML Validator, etc) du profil principal pour les mettre seulement dans le profil de dév…

[The greatest/dumbest moment of my career](https://mastodon.me.uk/@pikesley/111007434026223446). Dans ce pouet il y a un lien vers une très courte vidéo, en anglais, hélas sans sous-titres. L'histoire, en quelques mots : l'auteur avait fait une modification du code sur lequel il travaillait, dans le navigateur, sans s'apercevoir que son extension "Cloud to Arse" était activée et avait remplacé, comme son nom l'indique, toutes les occurrences de "cloud" par "arse", dans le formulaire qu'il remplissait. Cassant donc joyeusement les URLs vers cloudbees, qui faisait tourner les builds automatisés.

[Très potichat](https://social.goose.rodeo/@cats/112335909776999136).

Quand tu cherches un nom pour ton équipe de ~~bras cassés~~ de personnages de jeu de rôle, c'est chouette de tomber sur [une table comme ça](https://ludosphere.fr/@Whidou/112353823910254457). Via [Eric](https://tabletop.social/@e_eric/112359637787845414).

---

Les chats (enfin surtout un) muent, et je viens de récolter sur le canapé et les peluches à peu près de quoi en fabriquer un troisième. Vive le printemps !
