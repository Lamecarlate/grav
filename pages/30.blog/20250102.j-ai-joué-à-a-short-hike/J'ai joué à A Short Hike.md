---
meta-création: 2024-12-31 16:10
meta-modification: 2024-12-31 16:10
stringified-date: "20241231.161007"
title: J'ai joué à A Short Hike
template: blog-item
date: "2025-01-02T20:30"
update_date: ""
taxonomy:
  type: article
  tag:
    - jeux-vidéo
    - 100DaysToOffload
highlight:
  enabled: false
---

_A Short Hike_ est un joli petit jeu en 3D, où Claire, une ado oiselle, passe une journée sur une île.

===

En deux mots : pipou et pipou.

Vraiment.

![Capture du jeu, Claire, une oiselle anthropomorphe au milieu d'un champ de fleurs, la lumière est vive.](a-short-hike_champ-de-fleurs.webp){.figure-media--wide}

## L'histoire

Le générique de début montre le trajet vers le ferry, la nuit. Puis Claire se réveille dans une petite cabane, le matin, vers 15h (c'est une ado, OK ? c'est normal). On ne capte pas bien sur cette petite île, et Claire attend un appel important sur son téléphone. Donc sa tante May lui suggère d'aller faire une petite balade (clin d'œil clin d'œil, c'est le nom du jeu), vers le sommet du mont Hawk Peak, où peut-être qu'elle captera un peu de réseau. Et Claire y va, en ronchonnant. Mais bon, ya que ça à faire ici, t'façon.

![Claire dit à un enfant-chien : « toi aussi tu vas à Hawk Peak ? ». Les deux sont près d'une cascade.](a-short-hike_toi-aussi.webp){.figure-media--wide}

## Le visuel et la musique

L'ambiance est toute colorée, la lumière est belle, et elle varie suivant les endroits : il y a un coin où il pleut, par exemple, mais la plupart du temps, il y a un beau soleil. Ça donne envie d'aller glander sur cette île…

![Claire est en train de descendre une cascade à la nage.](a-short-hike_2024-12-27_10-00-38--p.webp){.figure-media--wide}

![Claire est sur un promontoire devant un bras de rivière avec des nénuphars en fleur.](a-short-hike_2025-01-01_12-28-31--p.webp){.figure-media--wide}

![Claire plane au milieu des nuages.](a-short-hike_2025-01-01_12-05-39--p.webp "Quand on est dans ou au-dessus des nuages, le paysage dessous est fait de lignes plus ou moins visibles."){.figure-media--wide}

![Claire continue de planer, weeeeeeeee ! En-dessous d'elle, la mer et un bateau à quai.](a-short-hike_2025-01-01_12-05-43--p.webp){.figure-media--wide}

![Un paysage pluvieux vu à travers des jumelles.](a-short-hike_2025-01-01_12-10-32--p.webp "Il y a des jumelles sur pied à plusieurs endroits de l'île, et ça permet d'avoir une idée des alentours… et trouver des trésors !"){.figure-media--wide}

![Un paysage lumineux et venteux vu à travers des jumelles.](a-short-hike_2024-12-27_09-35-10--p.webp){.figure-media--wide}

La musique est plutôt douce, elle souligne les moments et les endroits. Quand on approche de Hawking Peak, elle se fait plus présente, plus forte, elle donne de l'énergie ! Elle est composée par Mark Sparling, et vous pouvez [l'écouter sur Bandcamp](https://marksparling.bandcamp.com/album/a-short-hike-original-soundtrack).

## La mécanique de jeu

On se balade tranquillement, en marchant, sautant, escaladant ou planant avec ses propres ailes.

La difficulté principale, c'est l'escalade et la gestion de son endurance. En effet, pour atteindre Hawk Peak, il va falloir escalader, et ça, au début, on ne sait pas faire. On a une endurance de pioupioute citadine qui ne sort pas, et c'est bien normal. Pour l'augmenter, on va acheter, ou trouver, des plumes d'or, qui permettent de « se sentir plus léger quand on en a dans les poches ». Écoutez je juge pas, le but du jeu n'est pas de grinder pendant des heures pour augmenter des points d'endurance, j'aime bien ce mécanisme.

Et pour trouver des plumes d'or, on va faire le tour de l'île, sans vraiment de guide, juste en suivant les chemins, plutôt bien balisés, et en parlant à tout le monde. Parler, ça sert à avoir un peu plus d'infos sur cette île, et obtenir des quêtes secondaires. 

Ou bien jouer au stickball. Vous ne connaissez pas ? C'est normal, c'est un jeu inventé, où on se fait des passes avec un ballon en lui tapant dessus avec un bâton. Le but n'est pas de gagner contre l'autre mais bien de faire le plus de passes possibles. Et ça résume bien l'esprit du jeu : _A Short Hike_, c'est pas toi contre les autres, ni même contre le monde, c'est toi qui arrives à faire quelque chose que tu n'arrivais pas à faire, avec tes forces et l'aide indirecte des autres. J'avais dit que c'était pipou, hein.

Oh, et parlez à tout le monde. Vraiment. Et ne partez pas du principe que les gens sont méchants.

On peut pêcher, aussi. Ça fait partie des multiples petites choses non-nécessaires, mais c'est marrant, on gagne même un journal de pêche.

![Claire lève les bras en signe de victoire. Il y a un panneau qui indique qu'elle a pêché un gros brochet (92,2cm).](a-short-hike_gros-brochet.webp){.figure-media--wide}

On me dit que les poissons et crustacés albinos sont rares.

![Bulle de texte qui dit « C'est le rarissime brochet albinos ?!? ».](a-short-hike_brochet-albinos--p.webp){.figure-media--wide}
   
Moi : *\*pêche 6 langoustines albinos en 5 minutes*\*

On peut aussi faire du bateau, sauter sur des fleurs, descendre une cascade, planer au-dessus d'une source d'air chaud, en fait c'est incroyable la diversité des activités.

Il y a des succès, mais pas de notifications (youpi !). Je les ai découverts uniquement parce que je fouillais dans les paramètres. Comme ça, pour les complétionnistes, il y a de quoi faire, et ça ne dérange pas les autres.

## Paramètres

Il y a une sauvegarde automatisée, et trois emplacements de sauvegarde.

Comme dans beaucoup de jeux (de bons jeux), on peut changer les assignations de touche du clavier, et aussi des contrôles de manette.

On peut choisir la taille des pixels des polygones, c'est une attention touchante : ultra-rétro avec des gros pixels, ou moderne, plus lissé.

![Claire, de dos, entre des arbres et l'entrée de la cabane, les pixels sont gros, carrés et très visibles.](a-short-hike-pixel-gros-et-craquant--p.webp "Pixel : gros et craquant")

![Claire, de dos, entre des arbres et l'entrée de la cabane, les pixels sont moins visibles, c'est encore un peu rétro.](a-short-hike-pixel-petit-et-savoureux--p.webp "Pixel : petit et savoureux")

![Claire, de dos, entre des arbres et l'entrée de la cabane, tout est en polygones assez nets.](a-short-hike-pixel-minimum--p.webp "Pixel : minimum")

## Verdict

J'ai vraiment bien aimé. C'était doux, il y avait juuuste la bonne quantité de difficulté, et l'arrivée au sommet est… pfou. Je n'en dis rien, je résiste fort à vous mettre des captures  parce qu'elles sont très très belles mais je préfère vous laisser y jouer.

Fini en 2 heures 20 pour ma part, sans avoir trouvé tous les trésors, ni toutes les plumes d'or. J'y reviendrai probablement un de ces jours pour le platiner, comme on dit. Pour l'instant, j'ai fini l'histoire, j'ai un peu chouigné parce que c'est mignon, ça me va.

_A short hike_ a été réalisé par [Adam Robinson-Yu](https://mastodon.gamedev.place/@adamgryu), et disponible sur GOG, itch.io, Steam, Epic, Switch, Playstation 4 et Xbox One. Voir le [site officiel du jeu](https://ashorthike.com/).

![Claire est penchée sur un enfant-écureuil couché sur la plage, qui soupire « Hashtag pire papa du monde ! ».](a-short-hike_hashtag--p.webp "Que cet enfant est dramatique. Non je ne donnerai pas plus de contexte."){.figure-media--wide}
