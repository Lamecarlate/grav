---
title: Mi-Inktober

date: 2021-03-20 15:23
taxonomy:
  type: article
  category:
    - création
  tag:
    - défi
    - peinture numérique
thumbnail: thumbnail.jpg
thumbnail_orientation: landscape
---

En octobre dernier, j'ai participé à Inktober, suite <del>au lobbying</del> <del>à l'insistance malsaine</del> à l'impulsion chaleureuse de plusieurs de mes ami⋅es 😁

===

[Inktober](https://inktober.com/), c'est un défi de production quotidienne d'œuvres graphiques pendant le mois d'octobre. Ainsi est proposée une série de mots ou de concepts, un par jour pendant le mois d'octobre, mais on n'est pas obligé⋅e de les suivre, c'est juste une aide. 

Les premiers jours ont été prolifiques, alors que ça faisait littéralement des années que je n'avais pas touché ma tablette graphique. Et puis, le mois avançant, c'est devenu plus difficile, journées longues et fatigue ont pris leur dû, et moi du retard ; j'ai fini par cesser d'essayer de le rattraper, en ayant dessiné à peu près la moitié de la quantité attendue. Fin octobre, j'étais très déçue de moi-même, mais avec du recul, eh bien tant pis, j'ai déjà fait pas mal, et je ferai mieux la prochaine fois !

![La liste des "prompts", ou amorces de 2020](2020promptlist.jpg)

J'ai aimé suivre la liste, ou la détourner, via des jeux de mots ou bien des sens alternatifs. Certains dessins ont émergé très vite : fish, wisp, blade, teeth ont été des évidences. D'autres m'ont donné plus de fil à retordre : bulky a vu trois itérations, et j'ai bien galéré pour dune, car il m'a fallu trouver une version moins évidente 😅

![Un poisson bleu clair, avec des nageoires et la queue bleu sombre, dans un environnement bleu, il y a une lueur verte en bas, qui se répercute sur le vente et le bout de la queue du poisson.](01-fish.png "Jour 1 : Fish")

![Sur un fond rouge sombre, un mug olive contenant du thé, une volute (wisp) de vapeur sort du thé, et se transforme en will-o'-the-wisp (une petite lueur bleutée entourée de sortes de flammes bleues)](02-wisp.png "Jour 2 : Wisp")

J'ai fait un petit jeu de mot, parce qu'on ne se refait pas, hein. Pas très satisfaite du mug, que je trouve plat, j'ai pas réussi à lui donner du volume, mais je n'ai pas voulu insister, j'essaie de ne pas me dégoûter en allant trop dans le détail. (en vrai, ceci est la version refaite, l'ombrage du mug était vraiment pété) 

Pour info, le jeu de mot était sur les acceptions de wisp : cela veut dire « a small thin or twisted bunch, piece, or amount of something », j'y voyais aussi le [will-o'-wisp](https://en.wikipedia.org/wiki/Will-o%27-the-wisp) et donc j'ai imaginé un feu follet naissant de la volute de la vapeur au-dessus du thé.

![Une très haute montagne verte, avec plusieurs pics - deux sur trois sont enneigés. Au flanc de ladite montagne, un petit village dont les maisons ont des murs jaunes et des toits rouges, il y a un petit château en surplomb. Dans le fond, d'autres montagnes, lointaines, dans les tons bleus. ](03-bulky.png "Jour 3 : Bulky")

Ce fut… compliqué. Après une idée non commencée (une gamine dans un énorme pull, irréalisable dans l'état actuel de mes compétences) et une idée abandonnée à mi-parcours (un cerf avec des groooos bois, mais il avait une tête à dire « kill me please »), j'ai changé de direction, et ai pensé à un tout petit village à flanc d'une grande montagne, volumineuse, imposante - bulky.

![L'espace, un fond noir, des étoiles scintillantes, et au centre, la Terre (&quot;pale blue dot&quot; :D) et la Lune. À partir de la Terre, des cercles concentriques pâles, qui s'élancent vers l'extérieur.](04-radio.png "Jour 4 : Radio")

Je suis satisfaite du résultat - et j'aime bien mon idée, qui m'est venue [la veille], j'avais hâte de la réaliser !

![Trois fois la même épée, en crayonné, en ligne claire, et en couleurs. C'est une épée à long manche doré un peu asymétrique, la lame est argentée et tranchante d'un seul côté,  et trois gemmes en cabochon sont serties dans le manche, une violette, une verte et une jaune.](05-blade.png "Jour 5 : Blade")

Je l'attendais avec impatience, parce que les épées et moi c'est une grande histoire 🥰 et j'aime beaucoup en dessiner, en inventer (paradoxal pour une pacifiste tendance bisounourse comme moi). Ici j'ai voulu faire un design « visible » en laissant les étapes de construction. Plus : les gemmes en cabochon c'est trop cool (merci Dune 2 de m'avoir appris la méthode).

![Une maison-gland plantée dans le sol dans une forêt, il y a une porte ronde à la "trou de hobbit", et une fenêtre ronde par laquelle on voit un écureuil roux avec un bol fumant dans les pattes.](06-rodent.png "Jour 6 : Rodent (rongeur)")

J'avais envie de faire un écureuil, et il serait dans sa maison-gland à manger de la soupe tranquillou. Cette fois j'ai laissé le crayonné (recoloré légèrement), je tente plein de styles différents, déjà pour expérimenter et pour trouver / retrouver ce qui me plaît dans la peinture numérique. (et un peu aussi la flemme de faire une ligne propre, je me donne une contrainte de temps pour Inktober sinon je n'y arriverai jamais)

![Une femme vue de dos, en habit noir, ses cheveux sont très longs, blonds pâles, et ils laissent transparaître une couche de cheveux colorés en arc-en-ciel. Une mèche arc-en-ciel s'échappe sur la droite.](07-fancy.png "Jour 7 : Fancy")

Je me suis inspirée de la suite de photos de la coiffeuse @lucie d'il y a quelques jours (à l'époque) : [https://mastodon.opportunis.me/@lucie/104988633281730814](https://mastodon.opportunis.me/@lucie/104988633281730814).

Si ça c'est pas fancy je mange mon chapeau 😁

![Un ver des sables (de l'univers de Dune, de Frank Herbert), la gueule ouverte, on voit une rangée de dents sur chacune de ses trois lèvres. Il sort du sable, au milieu de dunes beiges.](08-teeth.png "Jour 8 : Teeth")

Le thème du jour a été assez évident pour moi ! (et je suis très fière du rendu)

![Un crayon rouge est en train de tomber du haut d'une table. On est du point du vue du crayon, la table est haute et loin. Au-dessus, la tête d'un chat dépasse, le regard brillant et vissé sur le crayon.](09-throw.png "Jour 9 : Throw")

En retard, parce que le thème m'inspirait peu - ou bien des idées irréalisables - voilà le jour 9 : Throw. (toute ressemblance avec une situation ou un chaton réels est bien entendu fortuite)

![Une plage, la mer froide et grise, une petite fille hâlée rousse, habillée de vert, fait sortir une toute petite lumière magique de sa main.](10-hope.png "Jour 10 : Hope")

Toute ressemblance avec ma photo de profil (sur Mastodon et un peu partout) est absolument pas du tout fortuite. « Hope », « espoir », c'est ce qui caractérise l'ambiance de l'image originelle : une petite personne incapable de magie comme les autres de son peuple et qui en souffre, et elle va s'entraîner au bord de l'eau. Ici, je l'ai redessinée, pour son message originel et pour l'espoir personnel de pouvoir de nouveau dessiner comme avant, et de continuer son histoire.

![Trois coprins chevelus (champignons beiges craquelées de bruns) sur fond de verdure. L'ensemble n'est pas très photoréaliste.](11-disgusting.png "Jour 11 : Disgusting")

Pendant très longtemps j'ai détesté les champignons, mais viscéralement. Et un soir d'automne, on se baladait avec ma mère, on est passées devant la piscine et elle m'a obligée à passer par un trou dans la clôture pour aller cueillir ces choses honnies qui poussaient sur la pelouse, dedans. Trauma d'enfance 😛 Maintenant, les champignons, ça va. Mais les coprins chevelus, c'est toujours beurk gluant dégoûtant.

![Un bord de piscine, du sol marbré en haut, l'eau en bas. Il y a une flaque d'eau près du bord de la piscine.](12-slippery.png "Jour 12 : Slippery")

Pas grand chose à raconter ici, juste une envie de dessiner du décor, tout en restant dans le thème.

![De grands champs de blé vallonnés sous un ciel bleu pâle.](13-dune.png "Jour 13 : Dune")

Vu que j'avais déjà traité « le » cas au jour 8 (teeth), j'ai dû innover 😁

![Un petit garçon noir souriant, il a des cheveux verts pâles et est délicieusement enveloppé dans une énorme couverture bleue.](14-armor.png "Jour 14 : Armor")

J'avoue, j'ai hésité. J'avais fait un chouette design d'épée au jour 5, et j'aurais pu lui faire un compagnon. Pis j'ai voulu jouer sur les mots ; et quoi de plus protecteur qu'une couverture ou une couette (ou un plaid 😉) ces temps-ci ? La source est une photo stock, j'ai croqué par dessus et reproduit environ les plis et replis, coins et recoins de la couverture.

L'image du jour 14 a été postée sur les rézosocios le 20 octobre. Le jour 15 (« outpost ») m'a mise sur les rotules, je n'arrivais pas à matérialiser les idées que j'avais, ça me frustrait et me stressait et au bout d'un moment j'ai laissé filer : inutile de me mettre la rate au court-bouillon pour un défi non-obligatoire. Comme je l'ai dit plus haut, la prochaine fois, je ferai mieux ! Je pense notamment que je prévoierai les dessins en amont plutôt que de les découvrir chaque matin, au moins en notant mes idées et en faisant un croquis rapide.

D'une manière générale, c'était une belle expérience ! J'ai testé plein de styles différents, comme je le remarque au jour 6, c'était vivifiant.

Et le mois suivant, les mêmes ami⋅es m'ont entraînée dans le <abbr title="National Novel Writing Month">NaNoWriMo</abbr>, mais c'est une autre histoire…
