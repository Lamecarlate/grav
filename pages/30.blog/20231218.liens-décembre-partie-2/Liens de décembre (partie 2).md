---
meta-création: "2023-12-18 17:55"  
meta-modification: "2024-01-01 11:40:11"  

stringified-date: "20231218.175528"

title: "Liens de décembre (partie 2)"  
template: "blog-item"  

date: "2024-01-01 11:55"  
publish_date: "" 
published: true # si besoin de date de publi + force publi  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - cinéma
        - cuisine
        - musique
        - mathématiques
        - sciences
        - mignonnerie
        - histoire
        - humour
        - artisanat
        - 

# plugin Highlight
highlight:
    enabled: false

---

Vu le nombre de tags, c'est éclectique cette fois-ci. Encore plus que d'habitude. Du poil (chat, mouton, papillon), de la cuisine (historique, scientifique, architecturale), de la scieeeence (maths (partez pas !), physique, technologie), des jolies musiques. Et joyeuses fêtes !

===

[Le lien entre Le père Noël et Meg Ryan](https://www.youtube.com/watch?v=KQl6R2m_zcU) (vidéo avec du son et sous-titres) : les vidéos de Calmos sont toujours d'une grande qualité, et celle-ci n'y fait pas exception. À travers anecdotes, acteurices et scénaristes en commun, on part du _Père Noël est une ordure_ pour arriver à _Quand Harry rencontre Sally_ et c'est très chouette.

[A History of Ketchup](https://www.youtube.com/watch?v=iWlqxGQXZx8) (vidéo en anglais avec du son et des sous-titres) : où j'apprends que le ketchup « originel » ne comportait pas de tomates, mais était plutôt proche du  nuoc-mâm.

Je découvre que le compositeur [Danny Elfman](https://en.wikipedia.org/wiki/Danny_Elfman), célèbre pour les bandes originales des films de Tim Burton, est aussi le chanteur et compositeur du groupe new wave Oingo Boingo, surtout connu en France pour la chanson _Weird Science_, générique de la série _Code Lisa_. Voilà voilà. Vous faites ce que vous voulez de cette information.

[Un article sur les fenêtres dans la Rome antique, mais…](https://mastodon.art/@Chloeg/111620626442103902). Mais, quoi. Le lien mène à un pouet sur le fediverse, en anglais, qui ensuite envoie vers l'article en question, en anglais toujours. C'est… déprimant. Le web écrit par des bots, c'est nul.

[How we made a cute lion in candy](https://www.youtube.com/watch?v=VlJUZQkEZpg) (vidéo avec du son mais pas de paroles, et le son n'est pas nécessaire à la compréhension) : une explication en images de comment une entreprise de bonbons fabrique des sucres d'orge (la version en cylindre plat) avec des décors à l'intérieur. Un peu comme les [millefiori](https://fr.wikipedia.org/wiki/Millefiori) (en verre), ou les [maki](https://fr.wikipedia.org/wiki/Makizushi) (en nourriture) 😄 Le principe : coller entre elles des baguettes de sucre coloré, afin de former une image quand on coupera le cylindre dans l'autre sens. Via [Yann Orpheus](https://yannorpheus.com/blog/post/en-bref-et-en-vrac-29).

La différence entre deux carrés successifs est un nombre impair, et la liste de ces différences est… la liste des nombres impairs. 🤯 La preuve en tableau plus bas.

Le tableau suivant est fait avec LibreOffice Calc. La première ligne contient des 0 et je l'ai faite à la main. La seconde ligne contient un 1 dans la première cellule, et puis j'ai calculé le carré dans la seconde cellule (`A2*A2`), et dans la troisième ligne, j'ai mis la différence entre la seconde cellule de la seconde et de la première ligne (`B2-B1`). Ensuite les formules ont été simplement répétées, Calc changeant tout seul les noms des cellules, bref, vous connaissez sûrement.

| Nombre | Carré | Différence | 
| ------ | ----- | ---------- |
| 0      | 0     |            |
| 1      | 1     | 1          |
| 2      | 4     | 3          |
| 3      | 9     | 5          |
| 4      | 16    | 7          |
| 5      | 25    | 9          |
| 6      | 36    | 11         |
| 7      | 49    | 13         |
| 8      | 64    | 15         |
| 9      | 81    | 17         |
| 10     | 100   | 19         |
| 11     | 121   | 21         |
| 12     | 144   | 23         |
| 13     | 169   | 25         |
| 14     | 196   | 27         |
| 15     | 225   | 29         |
| 16     | 256   | 31         |
| 17     | 289   | 33         |
| 18     | 324   | 35         |
| 19     | 361   | 37         |
| 20     | 400   | 39         |
| 21     | 441   | 41         |
| 22     | 484   | 43         |
| 23     | 529   | 45         |
| 24     | 576   | 47         |

Source : [Odd sur SBMC](https://www.smbc-comics.com/comic/odd) (comics en anglais… sans texte alternatif hélas, l'auteur n'en met jamais).

[Des ânes mignons](https://union.place/@inquiline/111356055042660867) (oui c'est un pléonasme) : une photo d'ânes sauvages dans le marais de sel du [Rann de Kursh](https://fr.wikipedia.org/wiki/Rann_de_Kutch) entre l'Inde et le Pakistan. (et non, il n'y a pas d'âne à deux têtes, c'est une illusion)

[Admirez le chat](https://oisaur.com/@hellgy/111557545245519321).

[Animation de rubik's cube urbain](https://mastodon.gamedev.place/@martincrownover/111608238725746711) (vidéo sans son, en boucle).

[Un martin-pêcheur en action](https://mastodon.social/@wonderofscience/110978074289703473) (vidéo sans son) : que c'est beau ! Le pouet a un lien vers une vidéo avec son, sans sous-titres, qui dit en substance « les martins-pêcheurs c'est encore mieux quand c'est filmé au ralenti », et ensuite on voit des martins-pêcheurs au ralenti. Je suis impressionnée par le fait que ces oiseaux arrivent à chopper des poissons en tenant compte de la réfraction de l'eau.

[Rendre vie aux instruments médiévaux : parcours et recherches d’un luthier de vièles et violes](https://phonotheque.hypotheses.org/26520) (article avec des parties sonores sans transcription) : article-interview d'Hubert Dufour, luthier. C'est mon papa 🥲 et c'est trop chouette. (oui je suis partiale)

![Un papillon blanc tout poilu de profil sur une feuille morte marron, sur fond jaune-marron flou, un sol de forêt probablement.](Artace_cribrarius.jpeg)

Plus de photos de cette si jolie bête : [Artace cribrarius, par Beardy Star Stuff](https://social.coop/@dennyhenke/111477184656223787) 😻

Ohlalala mais j'avais oublié de poster ça ici, où avais-je la tête :

![Une carte médiévale d’une grosse saucisse contenant la Terre avec le soleil et tout. Légende : verament menons vie en une saulcisse](saucisse.jpeg "C'est bien vrai, ça."){.figure-media--wide}

Source (incluant le texte d'ambiance) : [La théorie de la terre plate c’est dépassé… (par Lyz)](https://pipou.academy/@MissLyzzie/111456820441367859).

[Micro-histoire de Noël, avec des drones](https://mastodon.social/@Teryl_Pacieco/111638850433182518) (en anglais).

[Why Japanese Calligraphy Ink Is So Expensive](https://www.youtube.com/watch?v=GSuFSYY-X9w) (vidéo avec du son, en anglais, sous-titres en anglais) : belle plongée dans la fabrication artisanale de l'encre à calligraphie au Japon. 

[Snowfall - Crow Time](https://www.webtoons.com/en/canvas/crow-time/snowfall/viewer?title_no=693372&episode_no=122) (en anglais, webcomic sans texte alternatif) : pour fêter Yuletide, il y a du monde !

[The Surprising Genius of Sewing Machines](https://www.youtube.com/watch?v=RQYuyHNLPTQ) (vidéo avec du son, en anglais, sous-titres en anglais) : une belle explication du fonctionnement des différentes machines à coudre à travers les âges (avec gros prototypes en bois et mousse 😻).

[Le film perdu](https://medium.com/@f_descraques/le-film-mystere-32f855175694) de François Descraques. Je ne vous en dis rien, il faut découvrir par soi-même (longue histoire écrite, dix épisodes).

[Tobie Lolness](https://www.france.tv/enfants/neuf-douze-ans/tobie-lolness/) (vidéo avec du son, et des sous-titres assez descriptifs) : c'est une adaptation animée des romans du même nom, en série d'épisodes d'une vingtaine de minutes. Jolie fable écologique, roman initiatique d'un tout petit, très petit garçon (un millimètre et demi, mais c'est normal pour son peuple). 

Vraiment très, très joli. Bien dessiné, avec un rendu crayonné sympathique, bien animé, sans les tics d'animation cheapos qu'on peut trouver parfois. Bien doublé également, ce qui joue vraiment beaucoup (un peu inégal cependant, surtout pour les méchants). Je suis ravie de voir qu'il y a encore de belles œuvres d'art faites pour les enfants, qui ne souscrivent pas à la paresse du « C'est pour les enfants ». Les lichens gigantesques sont merveilleux à voir 😻 

Bien sûr, ça n'échappe pas à quelques clichés un peu nuls… Le grand méchant est un type obèse et ses sbires sont des brutes très bêtes. Ça ne nuit pas à la qualité globale, maiiiis comme toujours on aurait aimé s'en passer.

Pour créer les utopies on doit d'abord les imaginer :

![Le chien du meme "This is fine" mais iel est dans une pièce aux murs couverts de bibliothèques, il y a des livres par terre, un feu ronronne dans la cheminée et un petit chat noir fait de même dans le giron du chien.](Pasted%20image%2020231227135216.png)

Dessin : [penpanoply](https://www.tumblr.com/penpanoply/736065812783366144?source=share). Via [myrmepropagandist](https://sauropods.win/@futurebird/111647399327265082).

[Sauvetage de chat](https://www.youtube.com/watch?v=8r_TfB1ikNc) (vidéo avec du son, sans sous-titres, je reproduis plus bas les paroles dans un élément dépliable pour garder la surprise) : mouhhhh un petit chat coincé (on suppose) sous un pont à Poitiers.

<details>
<summary>Paroles de la vidéo</summary>
(le pompier, dans l'eau, en riant) Ça s'est pas passé comme prévu !
</details>

[Charnelangue](https://agate.blue/2023/12/08/Charnelangue.html) : une nouvelle d'Agate Blue, sur le soin, le fait de grandir et l'autonomie corporelle. C'est beau et contemplatif, et ça décrit un monde eutopique[^1] qui donne envie.

[The Mob](https://mastodon.world/@jeffowski/111654193465391233) (jolie photo avec texte explicatif en anglais).

[Danse pendulaire](https://c.im/@etcetera/111663235742654109) (vidéo avec du son mais sans paroles, ça peut se regarder sans) : une série de pendules colorés à la longueur de fil parfaitement calibrée, à qui l'on donne une énergie cinétique de départ et hop. Je vous laisse voir ça, c'est un peu hypnotisant.

Le pain d'épices de Noël, chez certaines personnes, c'est pris très, très au sérieux : [le télescope VLA (Very Large Array) en pain d'épices](https://astrodon.social/@ClaireLamman/111641687702997854).

La transhumance des moutons vers les pâturages (d'hiver ou d'été) se fait souvent sur de longues trajectoires, et pendant plusieurs jours. Les agneaux nouveaux-nés ne peuvent pas marcher si longtemps, donc il faut trouver des solutions. En Italie, les bergers et bergères utilisent des ânes et des mules pour transporter les petits. C'est [beaucoup trop choupi](https://www.youtube.com/watch?v=T7GMnSEyoEM) (vidéo avec du son sans paroles, le texte en anglais est inscrit sur l'image directement).

(et j'en profite pour apprendre l'étymologie de « transhumance » : en latin, <i>trans</i> veut dire « au-delà » et <i>humus</i>, « terre, pays »)

Pain d'épices encore, mais cette fois [en forme de la cathédrale de Nidaros](https://icosahedron.website/@bitbear/111660281096617740), en Norvège. Ya du niveau.

Le projet Becorns de David M. Bird est génial : ce sont de petites créatures faites de glands, brindilles et autres objets glanés, et David M. Bird les met en scène pour les photographier. Souvent avec des animaux sauvages. En plus de poster les vidéos et photos résultant de ces travaux, il montre son processus, par exemple avec le [Birdbath challenge](https://www.youtube.com/watch?v=NNSNQVWPfQA) (vidéo avec du son, en anglais, avec sous-titres en anglais).

[La version "Steamboat Willie" de Mickey Mouse va entrer dans le domaine public](https://www.youtube.com/watch?v=Rq_BM8_yyQM) (vidéo avec du son, en anglais, sans sous-titres) : segment très intéressant sur le sujet, sans trop de jugement, et rappelant que les grands classiques de Disney sont des adaptations de contes de fée du domaine public (et aussi que c'est comme ça que l'art, la culture évolue, en prenant et en remaniant). Via [Meeea](https://piaille.fr/@Meeea/111674353417608148).

---

Et c'est la fin de cet article, et de l'année. (bon évidemment je ne poste pas le 31 au soir, j'ai une vie)(il paraît)(sortez-moi)

À bientôt !



  [^1]: ce terme est en train de remplacer « utopique » dans le langage courant, car utopie et utopique, qui avaient le sens d'endroit (ou de société) idéal, rêvé, recherché, sont peu à peu devenus synonymes de endroits (ou de sociétés) inaccessibles…