---
title: "Le dragon qui était un monde"
template: "blog-item"

date: "2022-06-04 08:34"
publish_date: "2022-06-05 12:34"

taxonomy:
    type: article
    category:
        - création
    tag:
        - défi
        - peinture numérique

thumbnail: "skin_guardian_f_dragon_world-no-dragon.png"

highlight:
	enabled: false

---

Depuis quelques mois, je joue à _<span lang="en">Flight Rising</span>_, parce qu'élever des dragons c'est rigolo. Pour l'événement <span lang="en">Greenskeeper Gathering</span>, j'ai créé une <span lang="en">skin</span> pour dragon, basée sur une carte que j'ai créée pour l'occasion.

===

Voilà la carte en question :

![Une carte dessinée avec des continents, des forêts, des montagnes](skin_guardian_f_dragon_world-no-dragon.png){.media--wide .media--align-center}

Elle paraît normale, malgré quelques points qui trahissent la présence d'une gigantesque bête : une ombre étrange derrière les montagnes du nord-ouest, et une plaine fort découpée au sud-est.

En effet :

![Un dragon, la tête tournée vers la droite, les continents suivent assez paresseusement ses formes](skin_guardian_f_dragon_world_750.png){.media--wide .media--align-center}

Je tiens à préciser que je n'ai pas dessiné le dragon ni les ombres. Le site fournit des fichiers psd pour chaque race et sexe de dragons, et il est demandé de ne rien dessiner qui dépasse, ni de toucher aux ombres. Donc, ça claque, mais c'est surtout l'œuvre des designers de _<span lang="en">Flight Rising</span>_.

(et argl, sur fond sombre je vois des couleurs qui dépassent, je corrigerai – peut-être)

Peinture réalisée avec GIMP, carte dessinée à la mimine et au jugé 😛, et les décors viennent des brosses créées par StarRaven [Sketchy Cartography Brushes](https://www.deviantart.com/starraven/art/Sketchy-Cartography-Brushes-198264358).

Je vous invite également à aller voir le sujet sur le forum, il y a de très, très belles œuvres : [Greenskeeper Gathering 2022 - Submission](https://www1.flightrising.com/forums/skin/3133202).