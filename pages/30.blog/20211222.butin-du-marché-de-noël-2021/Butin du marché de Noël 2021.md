---
title: Butin du Marché de Noël 2021

date: "2021-12-23 09:00"
template: blog-item

taxonomy:
    type: article
    category:
        - humeur
    tag:
        - Noël
        - achats
        - victuailles

# image principale
header_image_file: 20211222-butin-marche-de-noel-2021-ensemble.jpg
header_image_alt: "Mon plan de travail couvert du butin : des boîtes de tisanes, de biscuits, du savon, des pâtes à tartiner"

# plugin Highlight
highlight:
	enabled: false

---

Petit passage au marché de Noël de Grenoble avec l'Amoureux. 

===

Nous avons trouvé tout ce que nous cherchions (à l'exception de mitouffles pour mes petites pattes froides, mais je tomberai bien dessus un jour).

Sur la photo ci-dessus, de gauche à droite et de haut en bas :
- de la tisane à l'érable (un classique chez nous depuis près de 10 ans, on saute dessus quand on en trouve)
- du thé à l'érable (pas goûté encore)
- de la tisane aux bleuets (myrtilles pour les français 😄)
- un pain d'épices aux pommes et cannelle
- des savons des [Affranchis](https://www.les-affranchis.bio/), je ne connais que de nom, mais c'est du savon de qualité et local (on peut même aller chercher les commandes au labo si on habite dans le coin)
![Deux savons de 100g dans un coffret de carton, le premier est au miel et aux noix, le second au pain d'épices](20211222-butin-marche-de-noel-2021-savons.jpg)
- des biscuits à l'érable (même remarque que pour la tisane)
- du sirop d'érable en boîte (ça se sent qu'on aime ça ? ça se sent que j'avais mis "La cabane à Mario" dans ma liste à voir pour le marché avant même d'y venir ?)
- un stollen aaaaaaaaah enfin, ça fait des années que je me dis "allez cette année j'en achète, non j'en fais, ah j'ai la flemme, oh les stocks sont épuisés", et c'est nul 
![Gros plan du stollen dans son emballage, on voit surtout la couche monstrueuse de sucre glace au-dessus et quelques raisins secs](20211222-butin-marche-de-noel-2021-stollen-aaaaaah.jpg)
- cinq (5) pots de pâtes à tartiner de chez Mathilde (oui je sais j'aurais pu les prendre au magasin, mais c'est ça aussi le marché de Noël, prendre des trucs sur un coup de tête et ne pas regretter parce que le chocolat c'est bon)
![Une tour formée de cinq pots de pâtes à tartiner avec différents parfums : lait & cacahuète, lait & noix, chocolat au lait pétillant, chocolat noir de Noël aux épices, chocolat noir façon truffe](20211222-butin-marche-de-noel-2021-tartinades.jpg)
- du beurre d'érable
- des sucres d'orge

Ces deux derniers éléments étaient encore au fond du sac quand j'ai pris la photo, et j'avais déjà tout démonté, donc hop photo à l'arrache :

![À peu près la même chose que la première photo, avec au centre un petit pot de beurre d'érable et un sucre d'orge blanc rayé de bleu](20211222-butin-marche-de-noel-2021-ensemble2.jpg "Le butin final"){.figure-media--wide}

Voilà, c'est tout, vous pouvez retrouver une activité normale.
