---
title: Carnet
blog_url: carnet
slug: carnet
id: notebook
menu: Carnet

sitemap:
  changefreq: weekly
  priority: 1.03

content:
  items: "@self.children"
  order:
    by: date
    dir: desc
  # limit: 10
  # pagination: true

extra_content:
  items: "@self.modules"

# simplesearch:
#     enabled: true
#     route: "@self"
#     filters:
#          - "@self"
#     filter_combinator: and

feed:
  title: lamecarlate.net | Carnet

highlight:
  enabled: false

pagination: true
#archives:
#  enabled: true
#  filters:
#    - @page: '/blog'
#  filter_combinator: and

short_notes_description: "Notules : petites notes, petits brouillons, humeur et pensées nécessitant de sortir mais pas forcément sous la forme d'un article construit."
---

Pour être honnête, je blogue peu. La ligne éditoriale est, en gros : des [créations](../carnet/category:création) (dessin, bijouterie, écriture), du [développement web](../carnet/category:développement) (à ce jour, un retour de conf, un script bash et un peu de CSS), des [critiques de bouquins](../carnet/category:lecture), de l'humeur (ça dépend™)…

Vous pouvez naviguer via la [liste des taxonomies du carnet](../carnet/taxonomie) (catégories et tags).
