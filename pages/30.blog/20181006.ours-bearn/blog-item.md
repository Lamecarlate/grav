---
title: L'ours dans le Béarn, plaidoyer d'une bergère

date: 2018-10-06 18:00
publish_date: 2018-10-06 18:00
published: true
taxonomy:
    type: article
    category:
        - nature
    tag:
        - ours
        - Béarn
        - environnement
header_image_file: 15515155.jpg
header_image_caption: 'Élise Thébault rassemble son troupeau pour la nuit, le 17 juillet 2018 à Etsaut (Pyrénées-Atlantiques). (THOMAS BAÏETTO / FRANCEINFO)'
author: 'Lara'
---

Sur le site du Monde, un article extrêmement intéressant sur le métier de berger, sur l'ours, sa réintroduction, et pourquoi ça n'est pas un problème selon Élise Thébault, une bergère du Béarn.

===

[« Si l'ours disparaît, mon métier va mourir »](https://www.francetvinfo.fr/monde/environnement/biodiversite/grand-format-si-l-ours-disparait-mon-metier-va-mourir-le-plaidoyer-d-une-bergere-en-bearn_2863767.html) : c'est une phrase choc, mais tellement bien sortie.
