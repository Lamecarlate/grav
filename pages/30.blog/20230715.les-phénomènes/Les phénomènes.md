---
meta-création: "2023-07-15 16:53"  
meta-modification: "2023-07-15 16:53"  

stringified-date: "20230715.165301"

title: "Les phénomènes"  
template: "blog-item"  

date: "2023-07-15 16:53"  

taxonomy:
    type: article
    category:
        - écriture
    tag:
        - micro-histoire
        - SFFF
        - humour

# plugin Highlight
highlight:
    enabled: false

---

– Votre maison est hantée, me dites-vous ? demande l'exorciste.

===

– Oui ! Par exemple, si je quitte une pièce sans éteindre la lumière, une voix sépulcrale dans les murs résonne : « Hé c'est pas Versailles ici ! ». Ou alors si je dis « Je suis rentrée ! », j'entends « Enchanté Rentrée, moi c'est papa. ». Je n'en peux plus…

L'exorciste reste quelques instants silencieux.

– Écoutez. Hum… Je ne peux rien pour vous.  
– Mais…  
– Je ne traite pas les phénomènes papanormaux. Désolé.

