---
meta-création: "2023-11-04 18:29"
meta-modification: "2023-11-04 18:29"

stringified-date: "20231104.20231104182945"

date: "2023-11-04 18:29"
template: "short-note"
taxonomy:
	type: short-note

title: ""

---

En me baladant sur le fediverse comme <del>tous les jours</del> à mon habitude, j'ai vu un carambolage amusant d'images. 

Ça m'a fait penser au projet \#almost de Krysalia, qu'elle explique dans [un de ses pouets épinglés](https://mastodon.social/@Krysalia/110660565582046542). Et donc j'ai repris le hashtag pour ça :

![Un pouet avec une photo d'un vélo bleu, et dans le fond un mur avec une photo gigantesque d'un avocat coupé en deux. En dessous, un autre pouet du compte random color contrasts avec deux couleurs : un jaune pâle un peu vert, et un vert éclatant. Les couleurs font écho à la photo de l'avocat.](almost-20231104.webp)

Les pouets concernés sont : [un message d'Adrianna Tan](https://hachyderm.io/@skinnylatte/111347818513069621) et [un contraste de chez random color contrast](https://botsin.space/@randomColorContrasts/111349394868472480).