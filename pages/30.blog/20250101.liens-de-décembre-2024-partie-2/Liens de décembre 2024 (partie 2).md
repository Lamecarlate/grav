---
meta-création: 2024-12-15 14:49
meta-modification: 2025-01-01 11:40
stringified-date: "20241215.144915"
title: Liens de décembre 2024 (partie 2)
template: blog-item
date: 2025-01-01T11:49
update_date: ""
taxonomy:
  type: article
  tag:
    - accessibilité
    - mignonneries
    - politique
    - musique
    - société
    - humour
    - littérature
    - jeux-vidéo
    - 100DaysToOffload
highlight:
  enabled: false
---
Du mignon et du moins mignon. Beaucoup d'animaux non humains, et d'humains qui se comportent pire que des animaux (en tant que société). Il y a une vidéo kitsch exprès et un rouge-gorge et des grenouilles et des robots et un joli lever de soleil. Et bonne année, ou bien « bonne condition limite grégorienne arbitraire », comme le dit [Eric Meyer](https://mastodon.social/@Meyerweb/113751333696063896) !

===

Sylvie Duchateau est une experte en accessibilité numérique. Elle est également aveugle. Elle est en train de créer son entreprise, et pour ça, il lui faut créer un espace pro sur le site des impôts. Je trouvais que le site des impôts s'était amélioré avec les années… Eh bien pas partout. C'est une honte à quel point certaines actions, essentielles, sont impossibles, inaccessibles tout simplement. [Créer son espace pro sur le site des impôts](https://www.sylduch-conseil.fr/creer-espace-pro-site-impots/). Via [Julie Moynat](https://eldritch.cafe/@juliemoynat/113674497957511307).

[Séparer l’homme de l’écolo](https://www.popol-media.com/article/separer-lhomme-de-lecolo/) via [Maiwann](https://framapiaf.org/@maiwann/113674090208607942).

[Un arbre décisionnel pour l'attribut `alt`](https://www.w3.org/WAI/tutorials/images/decision-tree/fr) . Comment choisir quoi mettre dans l'attribut `alt` d'une image. Ou dans le texte alternatif, ou la description, quel que soit le nom que les différents réseaux sociaux lui donnent (oui, c'est important, mettez des alts sur toutes vos images, merci beaucoup).

[Un cailletette](https://sunny.garden/@handmade_ghost/113680161065565793) (image avec texte alternatif en anglais).

Quand j'étais au collège, je détestais les musiques à la mode, techno et dance[^1] , sauf Robert Miles. Ça, j'aimais bien. Voilà [une version orchestrale de son célèbre morceau Children](https://www.youtube.com/watch?v=RpySgCQpm5M) (vidéo avec du son, sans paroles, 5 minutes). Regardez la pêche de la cheffe d'orchestre, Sarah-Grace Williams, j'adore sa manière de danser tout en restant très concentrée sur les temps. J'apprends d'ailleurs à l'occasion que Robert Miles était italien, que c'était un nom de scène, et qu'il est décédé en 2017.

[C'est… heu… c'est pas un cochon d'Inde](https://infosec.exchange/@catsalad/113718087735617471) (vidéo sans son, avec texte alternatif en anglais). Via l'ami [Stéphane](https://piaille.fr/@notabene/113718767321304777).

[Des mamans chats qui transportent leurs petits](https://infosec.exchange/@catsalad/113711410157671465) (en anglais).

[Un chat qui apprend à la dure que le savon ne se mange pas](https://infosec.exchange/@catsalad/113726640306541259) (en anglais).

[Comment les villes chassent les SDF](https://www.youtube.com/watch?v=Dg1Rtp--XIw) (vidéo avec du son et des sous-titres, 13 minutes). Bien sûr qu'il n'y a pas de réponse facile, de solution simple, mais le fait qu'on fabrique et place du mobilier hostile aux SDF (et par extension à toute personne qui aurait besoin de s'asseoir, de se coucher, de se reposer, ne serait-ce que quelques minutes), ça me dépasse. Parallèlement j'écoute une émission des Nuits de France Culture, une rediffusion d'une émission de 1972 : [Pour en finir avec le mythe du clochard](www.radiofrance.fr/franceculture/podcasts/serie-pour-en-finir-avec-le-mythe-du-clochard) (10 épisodes de 40 minutes environ, hélas sans transcription). 

[Danger boops](https://www.youtube.com/watch?v=ANzOdRPUDxU) (vidéo avec du son, en anglais, mais il y a pour ainsi dire un seul mot : "boop", 4 minutes). Compilation de boops, c'est-à-dire de touchers de nez de chats (entre autres) avec un doigt. Ici, par Samantha Faircloth, ou Safari Sammie, dans le sanctuaire d'animaux en Floride où elle travaille. Regardez-moi ce puma qui ronronne comme si sa vie en dépendait ! Via [bhasic](https://mastodon.social/@bhasic/113329337016190798).

[Loren et les grenouilles](https://flipping.rocks/@loren/113750832893503672) (en anglais). C'est une belle histoire, un des plus beaux moments que Loren ait vécus. Une rencontre avec des grenouilles du genre <i>Pseudacris</i>, <i lang="en">chorus frogs</i> en anglais. Il y a une très jolie photo.

[C'est l'heure de s'actualiser sur France Travail](https://framapiaf.org/@tcit/113746248285978009) (vidéo avec du son, et des sous-titres incrustés et animés, comme une vieille vidéo de télé-achat des années 70, 1 minute). Il y a un texte alternatif aussi. C'est… c'est quelque chose. Un monsieur tapote sur son minitel avec un aplomb que je lui envie et le narrateur nous aguiche pour qu'on se réinscrive sur France Travail. 

[Un rouge-gorge qui fait piou](https://ecoevo.social/@BathNature/113748558691045891) (vidéo avec du son, sans paroles humaines, 18 secondes).

[La pile à lire de l'immortalité](https://zirk.us/@ChrisMayLA6/112460934560004340) (en anglais, image avec un texte alternatif). Tout le monde sait (haha) qu'on devient un fantôme si on a des choses à régler, <i lang="en">unfinished business</i> en anglais. Est-ce qu'avoir des livres sur sa PàL, Pile à Lire, ça compte ? Apparemment, pour Tom Gauld, le dessinateur de ce comics, et pour Christopher May, qui le partage, oui.

---

[En 3129, l'humanité est éteinte](https://beige.party/@Lana/112803026930245432)  (en anglais). Micro-fiction d'anticipation à la fois mignonne et terrifiante. Extrait pour vous donner envie :

<blockquote lang="en">The last LG SmartFridge is desperately emailing its last owner that they are low on orange juice. The satellites that are still left, their orbits slowly decaying over millennia, dutifully relay the message. The automated "away from office" response turns on, as it always does, notifying the refrigerator that it's owner will likely return to the office in 3-5 business days.</blockquote>

Les réponses sont très intéressantes aussi, faisant des liens avec une des histoires des _Chroniques martiennes_ de Ray Bradbury (note à moi-même : les relire), ou bien avec ce court-métrage de Dima Fedotov, [Dead Hand: Fortress](https://www.youtube.com/watch?v=xKmZnIHzldk) (en russe avec sous-titres anglais, il y a une [version tout en anglais](https://www.youtube.com/watch?v=pyMNIFZTQkg)). Il y a d'ailleurs une suite à Fortress, [Last day of war](https://www.youtube.com/watch?v=_wWISxGhorU) (en anglais avec des sous-titres en anglais). 

C'est ça que j'aime beaucoup dans mes découvertes de liens sur le fédiverse (le réseau social où je sévis) : souvent, dans les réponses, je trouve d'autres perles, que je vous partage ou non, z'êtes des grandes personnes, vous savez faire, au besoin.

---

Comme beaucoup je suppose, je pensais que le principe du canari emmené dans les mines de charbon était de mourir pour alerter les mineurs de la piètre qualité de l'air (monoxyde de carbone ou grisou). Et en fait, ce n'est pas toujours le cas. Voici un article sur un modèle de cage, le [respirateur à canari](https://blog.scienceandindustrymuseum.org.uk/canary-resuscitator/) (article en anglais, et le nom claque plus dans cette langue : <i lang="en">canary resuscitator</i>). C'est un système où la cage a une porte qui peut se refermer hermétiquement s'il y a des signes d'empoisonnement du canari, et libérer de l'oxygène à l'intérieur afin de le réveiller.

[Morning-Cap before Slumber](https://mastodon.social/@Teryl_Pacieco/113747263243463875) (en anglais). Si c'est trop cryptique, il y a une petite explication sur [le Patreon de l'auteurice](https://www.patreon.com/posts/01-october-2024-118998026).

[Les paralympiques de tous les jours](https://newsie.social/@royaards/113050173630741314) (comics sans paroles, avec un texte alternatif en anglais).

---

J'ai utilisé ces vacances pour jouer beaucoup plus aux jeux vidéo que j'ai (ou que j'achète sur un coup de tête 😅 oups) :

- GRIS : fini en trois jours, beau comme pas possible, j'ai écrit un [article sur le sujet](../j-ai-joué-à-gris/).
- A Short Hike : fini, c'était en effet court, une petite balade sympathique, qui joue avec les codes (pourquoi devrait-on aller chercher des coquillages pour cette mioche ? parce qu'elle les veut, pardi), et c'était très joli et doux. J'ai fait [un article sur ce jeu](../j-ai-joué-à-a-short-hike).
- Torment: Tides of Numenéra : on a repris, recommencé plutôt, ce jeu avec l'Amoureux, pour l'instant c'est nébuleux, très joli, un peu buggé, mais ça fourmille d'histoires.
- Hidden Paws : un petit jeu très simple d'objets cachés, ici des pelotes de laine et des chats dans un environnement de gros polygones enneigés, je l'ai fini mais n'ai pas tout trouvé à 100%, j'y reviendrai tranquillement.

J'ai aussi mis à jour mon tableur de suivi et ajouté quelques catégories dans Lutris. Pour l'état, essentiellement, « en cours », « testé rapidement », « fini / à refaire » par exemple. Et aussi pour noter les jeux qui permettent des sessions courtes, si j'ai envie de passer quinze ou trente minutes mais pas plus. Assez fière de ça !

![Capture d'un tableur très rempli de jeux, avec de la coloration conditionnelle et des tas de colonnes, c'est écrit trop petit pour lire.](suivi-jeux-vidéo-extrait.webp)

Je suis trop organisée. Au secours. 

[^1]: Je mettais un point d'honneur à être à l'encontre de toute mode, en étant possiblement tout aussi ridicule que celleux qui les suivaient sans réfléchir… et en plus je me faisais emmerder pour ça. Yay, c'était bien le collège, hein ?
