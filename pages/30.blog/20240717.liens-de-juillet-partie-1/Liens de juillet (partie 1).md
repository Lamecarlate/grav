---
meta-création: "2024-07-02 07:27"  
meta-modification: "2024-07-02 07:27"  

stringified-date: "20240702.072725"

title: "Liens de juillet (partie 1)"  
template: "blog-item"  

date: "2024-07-17T22:00"  
update_date: ""

taxonomy:
    type: article
    category:
        - liens
    tag:
        - musique
        - mignonneries
        - histoire
        - sciences
        - humour
        - informatique
        - accessibilité
        - sociologie

# plugin Highlight
highlight:
    enabled: false

---

Chats, fleurs, espace, en passant par une étude sur l'utilisabilité du point d'hyphénation, bref, tout un tas d'échelles différentes. Et des corbeaux mignons.

===

[The Concert (The Mistake Waltz)](https://www.youtube.com/watch?v=o0axUoy4wbQ) (vidéo avec du son, sans paroles, 3 minutes). Via [Vicorva](https://mastodon.art/@vicorva/112682248179578159).

[Joyeux anniversaire Ookami!](https://blog.valeriane.org/joyeux-anniversaire-ookami/)

[Purrfect harmony](https://mstdn.social/@yurnidiot/112719216045930778).

J'apprends que Gustave Rivet, qui a au moins un arrêt de tram à son nom à Grenoble, bin c'était un type très bien : [Pour­quoi autant d’en­fants trou­vés au XIX<sup>e</sup> ?](https://www.monvoisin.xyz/on-va-etre-expose-a-tous-les-chantages-et-adieu-la-gaudriole/)

[I Rode the Craziest Trains in Japan](https://www.youtube.com/watch?v=hTPIs370dPM) (vidéo en anglais, avec sous-titres, 20 minutes) : j'aime beaucoup la chaîne "Not just bikes", qui parle… de vélo mais pas seulement. Beaucoup de trains, aussi. Tchoutchou. (Pikatchoutchou ?)

La Voie lactée au-dessus du mont Etna, [photo de Gianni Tumino sur le site de la NASA](https://apod.nasa.gov/apod/ap240705.html).

![Photo de nuit, la Voie lactée très nette au-dessus de la silhouette de l'Etna rougeoyant](GianniTumino_Etna&MW_14mm_JPG_LOGO__2048pix.webp){.figure-media--wide}

[Un chat qui aime prendre des douches](https://www.youtube.com/watch?v=7wGGklAO-fg) (vidéo avec du son, des sous-titres incrustés, 3 minutes)

[The Bike](https://worldbicyclerelief.org/the-bike/) via [Joachim](https://boitam.eu/@joachim/112738846218286242) (en anglais) : c'est un beau projet, ce vélo à double chaîne plutôt qu'un dérailleur, et apparemment ça marche bien, ça fait 10 ans qu'il est commercialisé en Afrique.

<blockquote lang="en">My roommate says our house is haunted. But I’ve lived here for 300 years and not noticed anything unusual.</blockquote>

Source : [DocAtCDI](https://mastodon.social/@DocAtCDI/112737669704545090).

[Snippet 4 – Crowtime](https://www.webtoons.com/en/canvas/crow-time/snippets-4-5/viewer?title_no=693372&episode_no=150) (bd, en anglais, sans texte alternatif…). 1) je veux une couverture comme ça, 2) <i lang="en">little star sandwich</i> <span role="img" aria-label="Trois emojis successifs de visages avec des étoiles dans les yeux">🤩🤩🤩</span>.

[Je me sens visée](https://fosstodon.org/@vanillaos/112749170589287081) (image, en anglais, avec texte alternatif, blague de linuxien⋅ne). Bon en vrai non, ça ne m'est jamais arrivé à ce point, mais j'ai déjà croisé le <i lang="en">grub rescue</i> et ça n'est pas confortable. (par contre j'ai découvert au boulot que le thème par défaut dans Ubuntu 24.04 ne doit pas être désinstallé, sous peine de désinstaller aussi les paquets ubuntu-session et ubuntu-desktop-minimal… franchement quelle dépendance à la noix)

[Des iris violets](https://mastodon.art/@naturesketchbook/112755930238725463).

[Pas assez de marmottes sur le fediverse](https://social.sciences.re/@jaztrophysicist/112768614582171095).

[Et si Saturne passait à côté de la Terre](https://www.youtube.com/watch?v=nY2jv4GWUhQ) (vidéo avec du son, en anglais, sans sous-titres, 5 minutes). C'est une belle expérience de pensée, et ça fiche un peu le vertige (personnellement j'adore autant que je redoute le moment où la Terre passe dans les anneaux, ça me met un peu mal à l'aise).

[Écriture inclusive : le point d’hyphénation rend-t-il vraiment meilleure la lecture par les lecteurs d’écran ?](https://www.lalutineduweb.fr/ecriture-inclusive-point-hyphenation-accessibilite-lecteurs-ecran/) (temps de lecture estimé 14 minutes). Très bonne analyse par Julie Moynat sur les différents caractères qu'on utilise pour les abréviations inclusives et leur lisibilité par les lecteurs d'écran (vu que c'est un argument utilisé par les détracteurices de ces abréviations, gasp, mais avez-vous pensé aux aveugles enfin voyons ? alors que poster des textes dans des images sur Insta ça ne pose aucun problème, mais je m'égare).

Je retombe sur [les résultats du sondage sur les couleurs](https://blog.xkcd.com/2010/05/03/color-survey-results/) de Randall « xkcd » Munroe et c'est très drôle. Le texte est en anglais. Je me suis bien marrée au passage sur le fait que personne, même Google, ne sait écrire « fuchsia ».  

![Google qui propose de corriger « fuscia » en « fuschia »](google_fuchsia.webp)

Et je me suis dit que j'allais voir si ça avait changé, en 14 ans (oui, le sondage date de 2010). J'ai donc cherché « fuscia » sur Google. Et… bon… ça se discute.

En français, on a une proposition de correction… incorrecte 😄

![Google propose de corriger en « fushia »](fuscia-fr.webp)

En anglais, c'est bien « fuchsia » qui est proposé.

![Google propose enfin de corriger en « fuchsia »](fuscia-en.webp)

Notons que dans tous les cas, j'avais bien des résultats cohérents, notamment cette superbe image :

![Plusieurs fleurs aux couleurs extrêmement vives](ps26040-fuchsiajolliesnantes.webp){.figure-media--wide}

(source : [Plantshed](https://www.plantshed.com/))

---

Si vous aimez manger libanais et/ou arménien et que vous êtes dans le coin de Valence, foncez au Sevan, 8 rue d'Arménie. J'y ai mangé une seule fois, mais quelle fois… Le menu Exquis, ce n'est pas 20 exemplaires de 4 ou 5 mezzes, non non non, c'est littéralement 20 plats différents, et wouuuh c'est bon. Mention spéciale aux haricots géants en sauce tomate et aneth. Et leur caviar d'aubergine dépasse presque (presque) celui de notre libanais favori (Au Liban, à Grenoble, allez-y aussi).

(l'Amoureux insiste sur le « presque »)
