---
date: "2022-05-06 19:30"
template: 'short-note'
taxonomy:
    type: 'short-note'
---

Premier essai de notule : l'idée est de publier plus, plus souvent, plus facilement.

Pas encore défini de méthode qui me permette d'effectivement écrire ces notules sans friction, mais ça va venir (j'ai en tête des bidouilles à base de rsync, et déjà, je surréfléchis 😅).
