---
meta-création: "2024-11-26 21:34"  
meta-modification: "2024-11-26 21:34"  

stringified-date: "20241126.213415"

title: "Trois bonnes choses (2024-11-26)"   
template: "blog-item"  

date: "2024-11-26 21:34"  
update_date: ""

taxonomy:
    type: article
    tag:
        - personnel
        - mes chats
        - mignonneries
        - cuisine
        - writing month
        - writing month 2024

# plugin Highlight
highlight:
    enabled: false

---

Trois bonnes choses aujourd'hui, malgré une rude nuit.

===

J'arrive à avancer sur mon futur site web. Il manque juste (juste, hahaha) les images, le plugin pour ça doit être mis à jour bientôt (bientôt, hahaha). C'est un peu ubuesque de ne pas pouvoir mettre des images, mais j'utilise Eleventy, un générateur de site statique avec des côtés géniaux et d'autres… plus relous. En attendant, je polis les flux RSS (j'ai trouvé assez facilement comment en faire par étiquette, je suis ravie), et je réfléchis à comment faire une page d'accueil qui tabasse. Vous auriez des idées ?

La petite chatte Edora qui court partout et piaule pour avoir de l'attention. On joue avec elle en lui balançant une boulette de papier, ça la rend insaissable. Elle aime beaucoup sauter dans son sac de transport dans lequel on ne l'a jamais transportée, c'est sa cache de jeu. Et quand elle le fait, souvent elle le fait glisser sur plusieurs dizaines de centimètres. Weeeeeeeee !

Des patates précuites à la vapeur hier, ce soir sautées à la poêle avec force feu, et servies avec du tofu rosso et de la salade verte. J'ai probablement trop mangé, mais on n'allait pas laisser des patates, hein, ça ne se conserve pas. Hein ? Hein ?!

Je relis mon précédent billet [Trois bonnes choses (2024-11-08)](../trois-bonnes-choses-2024-11-08) et je me dis que j'aurais dû mettre du <i lang="ja">furikake</i> sur les patates.
