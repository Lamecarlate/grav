---
meta-création: "2024-01-01 11:54"  
meta-modification: "2024-01-01 11:54"  

stringified-date: "20240101.115454"

title: "Liens de janvier (partie 1)"  
template: "blog-item"  

date: "2024-01-14 11:54"  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - humour
        - mignonnerie
        - art
        - sciences
        - histoire
        - accessibilité numérique
        - validisme
        - musique 

# plugin Highlight
highlight:
    enabled: false

---

Beaucoup d'images, cette fois-ci. La page est par conséquent un peu plus lourde, j'espère que ça ira.

Des vœux, des chats, de la parentalité, du jeu vidéo, des musiques cheloues mais entraînantes avec un sacré historique, un témoignage handi très fort, du jeu de couleur, des falafels, des chats, une forêt, de beaux objets en métal, des ânes (et un rappel d'accessibilité).

===

![Blessings For The New Year; May Your Wifi Signal Be Strong; May Your Snacks Be Plentiful; May Coffee Never Burn Your Tongue; May Dogs Smile Upon You; May Cats Always Choose Your Lap (Unless You Are Allergic); May Your House-Plants Thrive; May Your Naps Be Always Refreshing; But Above All Else ...; May You Be Healthy. Happy And Unburdened By Forkery](blessings.jpg)

Dessin de Gemma Correl, trouvée chez [devopscats](https://toot.cat/@devopscats/111676766541054754).

Traduction rapide maison :

> Vœux pour la nouvelle année
> 
> - Que votre signal wifi soit fort
> - Que vous ayez plein de grignotages
> - Que le café ne brûle jamais votre langue
> - Que les chiens vous sourient
> - Que les chats choisissent toujours votre giron (sauf si vous êtes allergique)
> - Que vos plantes d'intérieur prospèrent
> - Que vos siestes soient toujours reposantes
> - Mais surtout… que vous soyez en bonne santé, heureux⋅se et sans le poids de la connerie

Osez prétendre que vous ne voulez pas enfoncer votre visage dans le bidou du petit chat au premier plan (sauf les gens allergiques, ça va, vous avez une dérogation) : 

![Photo en noir et blanc de 3 jeunes hommes marchant dans une rue pavée sous l'œil attentif de plusieurs chats intrépides.](2ab8fd57ca37ef6d.jpg)

Source : [Felizaño](https://andalucia.social/@memento/111679858388966656) (pouet en espagnol, image avec texte alternatif dans la même langue).

Le texte :

<blockquote lang="es">Foto en blanco y negro de 3 jovénes caminado por una calle empedrada bajo la mirada atenta de varios gatos intrépidos.</blockquote>

Une traduction automatique via DeepL donne ça : 

> Photo en noir et blanc de 3 jeunes femmes marchant dans une rue pavée sous l'œil attentif de plusieurs chats intrépides.

(alors, intrépides, je sais pas, moi je vois plutôt de magnifiques patapoufs, mais je respecte l'avis de l'auteurice)

Une jolie petite histoire parentale sur [la lecture](https://mastodon.social/@mcnees/110334058263166569) (et il y a [une suite](https://mastodon.social/@mcnees/110334977761354139) !). Ahlalala, les korrigans (ou les lutins)(ou les poltergeists)(ou les parents) ne reculent devant rien.

[Le franponais, c'est souvent tout mignon](https://piaille.fr/@Blanche/111680980400101412).

Alors : je ne joue pas à <i lang="en">League of Legends</i>, je connais un peu le lore autour mais sans plus (et les vidéos promotionnelles trop cools). Je ne connais absolument rien au milieu de l'e-sport. Et cette vidéo m'a vraiment touchée : [Ils ont fait trembler League of Legends en n'ayant rien](https://www.youtube.com/watch?v=c22J8d6IAVM) (vidéo en français, parfois de l'anglais ou du coréen sous-titré en français incrusté, mais pas de sous-titres français). C'est l'histoire d'une équipe de LoL composée de perdants, de ratés, de petits nouveaux, de gens en qui personne ne croyait. Oui on dirait le pitch d'un film hollywoodien sur le baseball. Mais n'empêche que c'est tout mignon, c'est très bien raconté, et ça provoque de belles émotions. Via [Augier](https://diaspodon.fr/@AugierLe42e/111240149777143486).

[Quelques mots, quelques rimes](https://eldritch.cafe/@considermycat/111688097125524513) (en anglais, vous allez comprendre très vite).

[Si 8 bits font un byte, que font 2 bits ? Et 4 ?](https://mk.absturztau.be/notes/9nmvtbznbzvsrxum)(en anglais, plein de jeux de mots, pas vraiment traduisible…). C'est tout mignon. 

[Bad Apple but it's a Fluid Simulation](https://www.youtube.com/watch?v=2Ni13dnAbSA) (vidéo avec du son, sans sous-titres, c'est une musique, via [Eric Meyer](https://mastodon.social/@Meyerweb/111692474093293842)) : le phénomène Bad Apple a encore frappé. J'adore ce morceau, vraiment, il coche plein de cases dans les trucs musicaux que j'aime (les répétitions qui confinent au bourdon, le tempo enjoué qui donne toujours l'impression d'avancer…), et la vidéo iconique est sans cesse parodiée, remaniée, réinventée. C'est génial. Cela me rappelle la vidéo de [Megapip9001](https://www.youtube.com/watch?v=6QY4ekac1_Q) (avec du son, en anglais, sous-titres anglais) qui m'a appris les origines de la musique et de la vidéo. Tiens, j'en avais parlé [en août 2023](../20230901.liens-aout/Liens%20d'août.md) apparemment 😄.

Et puisqu'on est dans les trous de lapins de chansons et vidéos magiques du web, [l'origine de CaramellDansen](https://www.youtube.com/watch?v=rI3yD3WAcz4) par jan Misali (vidéo avec du son, en anglais, avec sous-titres en anglais, via [Aaron](https://kind.social/@trialByStory/111457985845854534)). Je ne connaissais que la première itération, avec les personnages en 2D qui dansent avec les mains sur le front. Les versions en 3D veulent manger mon âme, au secours. Et toute la dernière partie avec le groupe des Caramella Girls, je découvre complètement, et ça fait bien peur (et c'est moche)(oui je suis partiale et subjective, c'est mon blog).

[Accueilli](https://cripamphibie.wordpress.com/2021/11/05/accueilli/) : article très touchant, une suite de scènes où le crip amphibie, personne trans handie, se voit accueilli, se voit reconnu comme humain, existant. Dit comme ça, j'ai l'impression d'édulcorer complètement, le mieux c'est que vous lisiez.

[Activewear](https://www.youtube.com/watch?v=CYRENWT8lz8) (vidéo avec du son, en anglais, sans sous-titres) : une chanson avec des femmes en vêtements de sport qui font tout sauf du sport. C'est simple, efficace, hilarant (une fois que vous avez fini, revenez mettre en pause sur les détournements des logos et slogans de marques de sport). Via [Pizza Roquette](https://mastodon.social/@pizzaroquette/111577994382273344).

[Hexcodle](https://hexcodle.com/) : mais c'est génial ! C'est un petit jeu avec une couleur à trouver chaque jour. La couleur est présentée dans un carré au centre de la page, il faut tenter de découvrir son code hexadécimal. Le jeu nous aide un peu en indiquant pour chaque caractère s'il doit être plus haut ou plus bas (mais il y a un mode difficile où on n'a pas cette info !). Ce n'est pas réservé aux designers et/ou dévs, mais il faut connaître l'hexadécimal (les chiffres vont de 0 à 9 puis de A à F) et savoir qu'un code de couleur est de cette forme : RRVVBB (un nombre à deux chiffres pour le rouge, un pour le vert et un pour le bleu). À partir de là, on peut deviner. Via [Stéphanie Walter](https://front-end.social/@stephaniewalter/111719272326454562).

[Entretien d'embauche de typographe](https://transmom.love/@elilla/111730272142160167) (en anglais). C'est beau. Point.

L'esthétique cottagecore de la chaîne de Gaz Oakley est vraiment chouette (j'adore les ptits bols émaillés de toutes tailles et formes, le bois omniprésent, massif ou ajouré, les plantes partouuuuut), et en plus ça donne faim : [Falafels à l'ail des ours](https://www.youtube.com/watch?v=B95TS7IoZtI) (vidéo avec du son, des paroles, en anglais gallois, sans sous-titres).

[Une carte imaginaire de qualité](https://dice.camp/@thomrey/111572302790844767) (en anglais) (et elle n'est même pas finie 😮 )

[Grew up](https://old.reddit.com/r/cats/comments/17ywa87/grew_up/) (vidéo courte avec du son, sans paroles car c'est de la musique, avec des sous-titres).

<details>
<summary>Description de la vidéo</summary>
On voit une jeune femme couchée sur un lit, avec un chaton minuscule lové dans le creux de son épaule. Texte affiché (traduit par mes soins) : Attention, ne laissez jamais votre chaton dormir de cette façon. Suite de la vidéo : la jeune femme a désormais un énorme chat adulte couché en travers de sa poitrine, d'épaule à épaule. Texte affiché : vous n'aurez plus jamais d'espace personnel.
</details>

[C'est donc un grand non, dit le chat](https://mastodon.gougere.fr/@R1Rail/111736265232299189).

[Orion reçoit un paquet](https://social.sciences.re/@GeeksAnciens/111737432646223010) (en anglais, mais c'est traduit dans le alt de l'image, et aussi dans le message suivant).

![Une peinture numérique représentant une forêt stylisée, fin d'automne ou début hiver. Les arbres sont très hauts, bleus au premier plan et de plus en plus oranges vers le fond. Au milieu, il y a un chemin, qui s'enfonce dans la forêt.](forest.jpg "Vous aussi, vous avez envie d'emprunter ce sentier ?"){.figure-media--wide}

Source : [Orbite](https://mastodon.social/@orbite/111730630930425970).

[Macron, Uber et l'Écosse, par Naïm](https://www.youtube.com/watch?v=pebkf6KdOts) (vidéo avec du son, sous-titres incrustés au milieu de l'image) : sketch un peu décousu mais assumé comme tel, de Naïm, humoriste que je découvre. C'est plutôt sympa, drôle sans être méchant, et en plus il nomme et remercie la personne qui filme. Bon, j'ai tiqué quand il a dit « je peux pas dire aux autres que l'homme de la maison c'est ma femme » mais je ne peux pas juger sur une unique phrase (qui est peut-être prononcé par le personnage incarné, rien ne dit que c'est sa vraie vie qu'il raconte). Via [JM](https://mastodon.cloud/@meylodie/111734394097032388).

![Bracelet d'or, avec deux béliers ailés se faisant face, couchés comme des sphinx, pattes en avant ; la tête est sculptée très finement et dépasse largement du bracelet, le corps est stylisé, gravé dans le bracelet](bracelet.jpg)

Source : [Archaeo-Histories](https://ohai.social/@archaeohistories/111741181756825503)

[Atchii-klzzzt](https://hackers.town/@netkitty/111745587770500665) (en anglais).

[Une photo d'un reflet](https://mastodon.social/@Sofy_Engel/111705646211324170) : je ne sais pas si c'est parce que le film est périmé (je n'y connais rien en argentique) mais les couleurs sont magiques.

[De jolis ânes qui mangent](https://zirk.us/@impermanen_/111743496307671143) (en anglais, et le texte alternatif est sympa, même s'il ajoute des informations qui auraient eu leur place dans le message lui-même… voir [le pouet de Julie Moynat sur le sujet](https://eldritch.cafe/@juliemoynat/111081765140803421)).

![Kvoch cloisonné coloré avec un profil très découpé et une poignée représentant un profil de cheval.](kvoch.jpg "Kvoch (coupelle à boire traditionnelle russe) en métal émaillé."){.figure-media--wide}

Source de l'image et du texte alternatif : [Abie](https://octodon.social/@temptoetiam/111752369511611230).

---

Voilà pour ce début de mois !
