---
meta-création: 2024-11-12 22:17
meta-modification: 2024-11-12 22:17
stringified-date: "20241112.221725"
title: La chatte au nez vert
template: blog-item
date: 2024-11-12 22:17
update_date: ""
taxonomy:
  type: article
  tag:
    - photographie
    - mignonneries
    - writing month
    - writing month 2024
highlight:
  enabled: false
subtitle: Qu'est-ce qu'Edora a encore fichu ?
---

Qu'est-ce qu'Edora a encore fichu ?

===

Bon, ok, c'est pas exactement le nez, mais « la chatte à l'arcade sourcilière et la base du nez vert » c'était trop long.

![Photo en gros plan d'une jeune chatte bicolore gris et blanc, elle a les yeux verts, et une tache verdâtre sur les poils près de l'œil droit.](IMG_20241108_232221--p.jpg)

Dans quoi s'est-elle fourrée ? Dans de l'herbe, probablement, elle en est coutumière, si l'on en croit les trillions de graines accrochées dans ses poils très régulièrement.

(la photo a quatre jours, la petite chatte est de nouveau blanche, tout va bien)
