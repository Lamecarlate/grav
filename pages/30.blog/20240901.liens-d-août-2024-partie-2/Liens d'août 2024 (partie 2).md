---
meta-création: "2024-08-18 17:23"  
meta-modification: "2024-09-01 09:49:08"  

stringified-date: "20240818.172345"

title: "Liens d'août 2024 (partie 2)"  
template: "blog-item"  

date: "2024-09-01 09:49:03"  
update_date: ""

taxonomy:
    type: article
    category:
        - liens
    tag:
        - mignonneries
        - politique
        - science
        - cuisine
        - musique
        - linguistique

# plugin Highlight
highlight:
    enabled: false

---

Des chats (que c'est étonnant), du houmous, de la chanson alsaco-coréenne, de la coopérative, de la chanson équine, un petit trajet sur une ligne de train abandonnée, de la linguistique et de la bédé avé des muscles.

===

[Broderie d'un chat en automne](https://eldritch.cafe/@Leths/112944540750967462).

[Regarde, chaton, c'est comme ça qu'on fait](https://mstdn.social/@JenniferJorgenson/112980223753797988).

Je poste essentiellement des liens vers des trucs mignons, émouvants ou fascinants. Parfois il faut faire une exception : [This child needs to be saved. From her mother, and her fans.](https://www.youtube.com/watch?v=JLHYskB-ydM) (vidéo avec du son, en anglais, sous-titres anglais incrustés, 45 minutes). C'est effrayant. Une gamine que sa mère donne en pâture au net depuis sa naissance quasiment, via des vidéos apparemment inoffensives mais de plus en plus <i lang="en">trendy</i> voire suggestives, parce que ça fait des sous. Attention : on parle de prédateurs sexuels et de pédophilie (aucun acte n'est perpétré, c'est essentiellement dans les commentaires montrés à l'écran). On ne le répétera jamais assez : ne mettez pas de photos ou de vidéos de vos enfants sur le net, parce que 1) iels n'ont pas donné leur consentement, 2) vous ne savez pas qui récupère ces données derrière et ce qui en est fait. 

[Le houmous, le plat de la discorde](https://labodessavoirs.fr/houmous-le-plat-de-la-discorde/) (épisode sonore, sans transcription, une heure). Ça parle de houmous, de politique, de science et de cuisine. Et je découvre le concept de houmousiya, un restaurant/cantine spécialisé dans le houmous à manger sur le pouce. Allez c'est mon prochain projet de vie (qui ne rentre pas du tout en collision avec ma misanthropie et mon envie de maison paumée dans la pampa, non non pas du tout). Via l'article de [Sohan Tricoire](https://sohan-tricoire.fr/focus-sur-le-houmous/).

[Stork style](https://www.youtube.com/watch?v=RY0m1TmKKjk) (vidéo avec du son, en alsacien, avec des sous-titres en français incrustés, 5 minutes). Via [Chulinetti](https://eldritch.cafe/@APDMS/113001277140584165).

["Quicker 'n a Wink" (1940)](https://www.youtube.com/watch?v=gspK_Bi0aoQ) (vidéo avec du son, en anglais, sans sous-titres, 9 minutes). Une vidéo sur les premières caméras et appareils photo permettant de prendre des images de façon extrêmement précises : une bulle de savon qui éclate, un joueur de rugby frappe dans un ballon, etc. Via [Anne_GE](https://social.sciences.re/@Anne__GE/113005517680279650).

[Avec « 1336 », les salariés qui ont fait plier la multinationale Unilever fêtent leurs 10 ans](https://lareleveetlapeste.fr/avec-1336-les-salaries-qui-ont-fait-plier-la-multinationale-unilever-fetent-leurs-10-ans/). C'est une bonne nouvelle, cette coopérative tient, et tient bon. L'histoire de Scop Ti est très chouette, ce sont des employé⋅es qui ont lutté pour racheter leur usine qui allait être délocalisée. Via [Simon D.](https://mamot.fr/@Siltaer/113006456113074538).

---

[Lullaby for a Princess](https://www.youtube.com/watch?v=i7PQ9IO-7fU) (vidéo avec du son, en anglais, des sous-titres, 7 minutes). Je pleure pas c'est toi tu pleures. Cette chanson me fait le même effet à chaque fois : je chouigne. C'est une œuvre de fan, dans l'univers de _Mon petit Poney Les amies c'est magique_, qui imagine comment et pourquoi la princesse Luna, sœur de la princesse Celestia, est devenue Nightmare Moon, l'antagoniste de la première saison. 

<blockquote lang="en" markdown="1'">
But such is the way of the limelight, it sweetly   <br />
Takes hold of the mind of its host   <br />
And that foolish pony did nothing to stop   <br />
The destruction of one who had needed her most
</blockquote>

Ces vers me fendent le cœur de manière systématique. 

---

Plaisir coupable : le [thème du film _Mortal Kombat_](https://www.youtube.com/watch?v=Sr1bLLvsbh0) (vidéo avec du son, 4 minutes) fait partie de mes petits morceaux adorés qui filent la pêche, alors que généralement je déteste la techno. Mais voilà, « les bandes son c'est pas pareil », dis-je pour me dédouaner. (oui j'écoutais aussi les musiques des logiciels de keygen quand j'étais jeune)

Tant qu'on y est à vider les vieux onglets, je retombe sur le site de Neal Agarwal qui présente comme dans un musée des artefacts de l'ancien <del>temps</del> Internet. Et notamment  ceci que je ne connaissais pas : [The Ultimate Showdown of Ultimate Destiny](https://neal.fun/internet-artifacts/ultimate-showdown) (vidéo avec du son, des sous-titres). Ma tête pendant toute la chanson c'était à peu près ça : <span aria-label="Bouche grande ouverte de stupéfaction">😮</span>.

Matt Spears a converti une mini voiture électrique en voiture sur rails, et la teste en faisant [ce trajet sur une ligne de train abandonnée](https://invidious.lebeaunuage.net/watch?v=XvbUKqZdtjY) (vidéo avec du son, en anglais, sans sous-titres, 23 minutes). C'est très beau, tranquille, il n'y a pas de musique, c'est juste les bruits du monde et sa voix, et il lui arrive quelques bricoles pas graves. Un petit voyage sympa. Via l'ami Epy.

![Photo d'un mur de maison recouvert de motifs de losanges faits de grès et de petits losanges faits de silex.](facade-silex.webp){.figure-media--wide}

Des murs avec du silex intégré, hiiii j'adore. On distingue la cassure conchoïdale du silex, ce qui fait cet aspect de creux et de bosses si caractéristique. Via [1pseudodeplus](https://piaille.fr/@1pseudodeplus/113006482786472358).

Je ne connaissais pas les arcanes des ferme-portes mais cette vidéo est passionnante : [Les ferme-portes, les mal-aimés](https://www.youtube.com/watch?v=3-Q87w8uhwg) (vidéo avec du son, en anglais, avec sous-titres en anglais, 25 minutes).

[Voilà ce que ça peut être, d'être un parent](https://social.joelle.us/@joelle/112838470072993147).

[Une chouette affiche de Shyle pour le Planning Familial](https://mastodon.social/@Shyle/113034904448098895).

Un bel exemple de néologisme, né de multiples langues entre les fuseaux horaires : [le konbarning de Crunchyroll](https://jorts.horse/@blursedtexts/112879490184036157).

[Miriam Makeba - Qongqothwane The Click Song Live, 1963](https://www.youtube.com/watch?v=n-4U2hfMpnk) (vidéo avec du son, sans sous-titres, en anglais et en xhosa, 2 minutes). Un exemple supplémentaire de la diversité des sons que peuvent produire les humains : les « clics ». Via [Docteur Under](https://social.sciences.re/@doctunder/113044254761714451).

Découverte BD du mois : [Colossale](https://www.webtoons.com/fr/comedy/colossale/list?title_no=2378). Au début je n'étais pas sûre que ça m'intéresserait, les émois d'une jeune aristo dont le seul but dans la vie c'est faire de la muscu. Et pourtant. Et pourtant ! C'est touchant (oui, je sais, ouin ouin pauvre héritière, mais dans tous milieux il y a des pressions écrasantes qui empêchent de vivre…), drôle, les personnages sont tous et toutes attachants, avec une progression dans leur histoire. Il y a des retournements de situation, des quiproquos qui ne durent pas trop longtemps (ça c'est ma némésis, je déteste les histoires où on a envie de hurler « mais parlez vouuuuuus » toutes les deux pages). Et c'est très joliment dessiné, avec quelques faces déformées par les émotions comme dans les manga, c'est parfaitement intégré.

![Capture d'une vidéo, le texte dit « Heureusement j'ai des ciseaux et une propension à ignorer les consignes de sécurité ».](scissors-safety-instructions.webp)

La capture vient de l'épisode [Hand warmers](https://www.youtube.com/watch?v=Oj0plwm_NMs) de Technology Connections (vidéo avec du son, en anglais, avec sous-titres en anglais, 20 minutes). Fait intéressant, cette vidéo parle des petites pochettes chauffantes qu'on connaît assez bien en France, qui sont réutilisables quand on les met à bouillir. Et aussi de paquets chauffants à usage unique, que personnellement je n'ai jamais vu. De base, le côté usage unique me rebute, mais, de façon un peu contre-intuitive, ça a l'air plus efficace sur le long terme. Intriguant.

---

Hier, sans comprendre exactement pourquoi, je me suis levée à 7h, j'ai nourri le fauve qui miaulait à la mort, et à 7h10 j'étais dehors pour une balade. J'avais mis dans mon sac fruits secs, jumelles, bracelet de survie avec boussole et allume-feu, et outil multifonction (pas d'eau, tiens, oups), et je… heu… ne me suis servie que de l'outil multifonction à la fin pour scier une partie d'un long bâton que j'avais trouvé pour les chats. J'ai fait une jolie balade toute simple (et avec une pente montante de 30 degrés, gni), une heure porte à porte, juste histoire de bouger un peu. C'était chouette. Aujourd'hui, mes mollets me rappellent à l'ordre. Je souffre. (ça va, hein, vous inquiétez pas)


