---
title: Micro-script pour décharger son cerveau des pensées parasites

date: '2020-12-07 18:53'
update_date: '2022-10-15 21:00'
taxonomy:
    type: article
    category:
        - développement
    tag:
        - script
        - bash
        - bouibouitage
header_image_file: butterfly-head_ArtTower_ElisaRiva.jpg
header_image_caption: "Illustration : un visage de femme stylisé de profil intégré dans une peinture d'un papillon monarque posé sur une fleur"
author: 'Lara'
---

J'ai pas mal tendance à être distraite par des pensées de type "ah oui tiens faudrait que je fasse tel truc" mais que je ne peux ou dois pas traiter maintenant. Exemples : je suis en train d'écrire un e-mail et "ah faut que je retrouve cet article" ou "ce midi, je vais utiliser la courge restante, ya quoi comme recette sympa déjà", ou bien je suis en train de travailler et "oh, faudrait que je teste cette application dont on a parlé hier" ou bien "mais du coup, les pingouins ont des genoux ou pas ?".

Donc j'ai écrit un micro-script bash qui sauve dans un fichier daté ces quelques mots, ces pensées parasites, et je lance ça via un raccourci clavier, pour libérer mon esprit et rester concentrée.

===

<div class="notice notice--warning" markdown="1">Avant de commencer : ce script a été testé sur Ubuntu Linux 18.04 et 20.04 et pas plus. Je n'ai aucune idée de si ça marche sur d'autres plateformes.</div>

C'est vraiment ultra-simple, et même austère, mais je ne voulais pas non plus y passer trop de temps 😛

En vrai, j'ai mis une matinée car : ce besoin, sur lequel j'avais déjà réfléchi sans agir, m'est revenu parce qu'en ouvrant un logiciel j'ai vu que j'avais Albert (un logiciel de lancement de trucs) d'installé sur ma machine, donc j'ai fouiné dedans, et dans les plugins, et puis j'ai recherché l'article sur le net qui m'avait fait installer Albert (indices de mon moi du passé : "écrit en français, par un mec, sur son organisation personnelle"… facile, tiens), j'ai retrouvé l'[article](https://jonathanlefevre.com/outils/prise-de-notes/ "Prendre des notes efficacement pour décharger son cerveau"), me suis rendue compte que le gars utilisait Trello pour noter ses idées, j'ai cherché si je pouvais faire de même avec [Kanboard](https://kanboard.org/) (le kanban libre que j'utilise), c'était non, et puis j'ai fini par me dire que je pouvais écrire un fichier moi-même, et donc faire un script, et ajouter un raccourci clavier.

En fait, ceci est une description parfaite de pourquoi j'ai besoin de ce système : mon cerveau saute d'idée en idée (oui, parce que du coup, pendant ma recherche, évidemment, je suis retournée voir si Kandroid, le client Kanboard pour Android, gérait désormais l'authentification SSO de Yunohost, qui est le système avec lequel j'ai installé Kanboard - et j'en ai profité pour jeter un œil à Wekan, autre logiciel de kanban, pour avoir des nouvelles des avancées, au cas où je voudrais changer).

Ce système n'a pas vocation à engranger des notes - en tout cas pour moi, c'est vraiment juste pour me vider la tête, et il faut trier tout ça dans un moment tranquille. Sinon ça sert juste à créer un dossier de fatras de plus 😁

Après cette intro dix fois plus longue que le script, voici la bête :

## Script

```
#!/usr/bin/env bash

dir="[ici le chemin vers le dossier qui contiendra les notes]"
date=$(date +"%F_%H-%M-%S")
file=$dir$date".md"

if [[ -d "$dir" ]]
then
    read text
    echo $text > "$file"
    exit 0
else
    echo "Il y a eu une erreur: le dossier $dir n'existe pas"
    exec bash
    exit 1
fi
```

Notez bien les guillemets, laissez-les où ils sont, le chemin doit être absolu. Je répète que *je n'ai aucune idée de si ça fonctionne sur un autre système que GNU/Linux*. Enregistrez le code dans un fichier où vous voulez, avec une extension .sh, et rendez ce fichier exécutable.

Vérifiez bien si le script se lance correctement avant de passer à la suite&nbsp;: le point important est vraiment le chemin vers le dossier des notes. Vous pouvez mettre des espaces, il n'y a pas besoin de les échapper à ma connaissance.

La dernière version du script fait ce test : si le dossier n'existe pas, le script affiche une erreur et s'arrête.

## Raccourci clavier

Pour lancer le script via un raccourci clavier, voilà comment faire sur Gnome 3 (testé sur 3.28.4, 3.36.4, et 42.4) :

- recherchez "Clavier" ou "Raccourcis"
- descendez tout en bas de la liste des raccourcis, choisissez "+"
- entrez le nom de votre choix
- entrez `gnome-terminal -- [le chemin absolu vers votre script]` (pas de "~" mais bien "/home/[login]")
- choisissez le raccourci de votre choix (moi c'était <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>N</kbd>)

## Utilisation

Taper le raccourci clavier va ouvrir un terminal vide, vous pouvez écrire vos pensées, taper sur Entrée, et hop, le terminal se ferme et vous retournez à votre occupation.

Vous pouvez aussi lancer le script directement, en ligne de commande ou peut-être via un raccourci graphique.

J'espère que cette micro-astuce pourra aider les gens qui comme moi ont un essaim de papillons monarques dans le cerveau (c'est plus joli que des puces comme image, hein ?).

(ah pis faudra que je pense à faire une variante pour l'utiliser sur mon téléphone *\*tape Ctrl + Alt + N\**)

## Variante

Si on veut un seul fichier, daté du jour (sans les heures), voilà une possibilité :

```
#!/usr/bin/env bash

dir="[ici le chemin vers le dossier qui contiendra les notes]"
date=$(date +"%F_%H")
file=$dir$date".md"

read text
echo " " >> "$file"
echo "------" >> "$file"
echo " " >> "$file"
echo $text >> "$file"
```

Ce code est d'[Emmanuel Clément](http://emmanuel.clement.free.fr/).

---

Crédits image : montage personnel entre une [manipulation d'image de ArtTower](https://pixabay.com/illustrations/butterfly-butterflies-monarch-3407357/) et un [portrait stylisé de ElisaRiva](https://pixabay.com/illustrations/psychology-mind-thoughts-thought-2422442/).

---

Mise à jour du <time datetime="2020-12-07 19:37">7 décembre 2020</time> : on me suggère d'utiliser `#!/usr/bin/env bash` à la place de `#!/bin/bash`, pour que ça marche sur plus de plateformes, script modifié.

Mise à jour du <time datetime="2021-03-06 18:42">16 mars 2021</time> : `touch` n'est pas nécessaire, le simple fait de faire `echo >` suffit à créer le fichier s'il n'existe pas. (merci [Lovis IX](https://pleroma.foucry.net/users/LovisIX), je n'ai pas retrouvé le pouet précis, par contre). J'ajoute également une variante qui met plusieurs notes dans un fichier daté du jour.

Mise à jour du <time datetime="2022-15-10">samedi 15 octobre 2022</time> : j'ai enlevé le lien du pouet d'Emmanuel Clément, dans lequel il avait mis la source, car il supprime régulièrement ses messages. De plus j'ai amélioré le script en testant l'existence du dossier de destination avant de faire quoi que ce soit.