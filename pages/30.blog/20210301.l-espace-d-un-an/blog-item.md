---
title: L'espace d'un an, de Becky Chambers

date: "2021-03-12 08:54"
publish_date: "2021-03-12 08:54"
published: true
taxonomy:
  type: article
  category:
	- lecture
  tag:
	- anticipation
	- SF
	- dans l'espace
author: "Lara"
header_image_file:
header_image_caption:
highlight:
    enabled: false
thumbnail: cover-l-espace-d-un-an--small.jpeg
thumbnail_orientation: portrait
---

Un vaisseau-tunnelier à l'équipage multi-espèces accueille une nouvelle recrue pendant un voyage d'un an pour un contrat. C'est doux, c'est amer, c'est du space opéra chocolat chaud.

===

![Couverture du roman "L'espace d'un an"](cover-l-espace-d-un-an--small.jpeg){.media--align-left}

Becky Chambers, *L'Espace d'un an*, L'Atalante, coll. « La Dentelle du cygne », 2016 ((en) *The Long Way to a Small, Angry Planet*, 2015), trad. Marie Surgers, 448 p. (ISBN 978-2-84172-766-7)

Si on m'avait dit que j'adorerais un roman dont la protagoniste principale fait un boulot administratif et barbant (la gestion de la paperasse, tout ça), j'aurais froncé les sourcils en signe d'incompréhension. Et puis j'aurais rigolé. Mais j'aurais eu tort.

Note : j'ai lu plusieurs critiques pour nourrir la mienne, il se peut que certaines expressions aient été reprises sans forcément m'en rendre compte, que leurs auteurices me pardonnent - et de toute façon je met les liens en fin d'article, c'est toujours intéressant d'avoir d'autres points de vue.

Le titre original est "The long way to a small, angry planet", et j'avoue que j'aurais aimé une traduction plus littérale que "L'espace d'un an", qui ne raconte qu'une partie de l'histoire. La couverture française de la Librairie L'Atalante en livre de poche (en illustration de cet article) donne le ton, à mon sens : le titre est découpé mot par mot, un par ligne, et on sent cet étirement du temps par l'espace. Ça me plaît.

> Grâce au sol, debout ;  
Grâce aux vaisseaux, vivants ;  
Par les étoiles, l'espoir.
>  
> (Proverbe exodien)

Dans un futur lointain, les Humains sont partis de la Terre qui ne pouvait plus les accueillir, formant de nouveaux peuples, séparés essentiellement entre les Exodiens, nés dans les vaisseaux gigantesques de l'exode depuis des générations, et les habitants des colonies, comme Mars. Ce faisant, ils ont rencontrés d'autres espèces, notamment les trois qui ont co-fondé l'Union Galactique : les Aandrisks, les Harmagiens, et les Aéluons. Considérés comme arriérés et pas trop dangereux, les Humains ont joint l'Union.

Le Voyageur est un vaisseau-tunnelier, c'est à dire qu'il est équipé pour percer l'infrastrate, afin de fabriquer des trous de ver et permettre des voyages rapides dans l'immensité de la galaxie. C'est un navire quelque peu vieillot, beaucoup rafistolé mais qui tient bon.

Afin de gagner un peu plus de sous, Ashby, le capitaine, accepte un contrat qui consiste à percer un tunnel depuis une petite planète peuplée de clans d'une espèce très belliqueuse qui est en train d'entrer dans l'Union. Une des raisons de l'entrée des Torémis est la quantité aux abords de leur planète de champs d'ambi, une matière dont on fait un excellent carburant. Ceci sur fond de guerre galactique, car malgré l'Union, plusieurs espèces continuent de se battre pour augmenter leur empire.

Mais si c'est le but du voyage, c'est sur ce qui se passe pendant le long trajet (un an, ce n'est pas juste un effet d'annonce, c'est vraiment le temps nécessaire pour aller vers cette petite planète hargneuse) que se concentre la narration. Sur ce qui se dit, ce qui se vit. Becky Chambers semble capter si finement les relations interpersonnelles en se camouflant derrière une science-fiction joyeuse, tendre et bordélique. Et c'est très agréable. Space opera doudou, hopepunk, solarpunk, SF feel-good : ça décrit bien ce genre. L'univers n'est pas tout doux, mais on se focalise sur un groupe, sur des gens qui se sont choisis, qui forment une famille.

> Les gens dont on se souvient, ce sont ceux qui ont décidé des frontières sur nos cartes. Personnes ne se souvient de ceux qui ont construit les routes.

Parfois décrié comme "tout le monde il est beau et gentil", il est pour moi plutôt "humain" (difficile d'utiliser ce qualificatif quand de très nombreux personnages ne sont justement pas humains, mais vous savez ce que je veux dire).

Chaque membre de l'équipage est mis en lumière à un moment ou un autre de l'histoire, au hasard des rencontres et des mondes croisés pendant ce long voyage.

La première personne rencontrée est Rosemary Harper, une jeune humaine qui s'engage comme greffière dans ce long-courrier. Elle cache son identité véritable, veut essentiellement mettre son passé derrière elle, elle crève de trouille mais veut faire du bon boulot. Son premier contact est avec Lovelace, l'IA du vaisseau que tout le monde appelle Lovey. Lovey est adorable et prévenante, et Rosemary lui rend bien : elle s'inquiète d'elle et la remercie (en lisant cette partie, je me suis dit : "je l'aime déjà, cette petite : elle parle bien aux IA").

Jenks, le tech info du vaisseau, humain, au corps décoré de tatouages, est amoureux de Lovey, et c'est réciproque. Leur relation est belle, jamais montrée comme bizarre. Les IA peuvent être conscientes (douées de conscience) ou non, mais ne sont pas considérées comme des personnes par la plupart des gens. Ni même comme des intells - terme utilisé pour décrire tout individu intelligent. Et c'est d'ailleurs tout un pan du roman : une réflexion sur le libre-arbitre, sur le fait qu'un programme puisse développer une personnalité, et aimer.

La tech méca est Kizzy Shao, humaine, elle est une espèce de clowne survoltée qui n'aime rien tant que démonter et remonter des trucs, et à la main je vous prie, les réparabots c'est tricher. Bravache, insouciante en apparence, sans peur, elle y sera confrontée.

Artis Corbin est humain aussi, il est l'alguiste, en bref il s'occupe de faire pousser les algues nécessaires pour fabriquer le carburant du Voyageur. Je trouve personnellement cette idée magnifique ! J'aime l'idée d'un vaisseau quasiment autonome pour sa propulsion de croisière. Bien sûr, il y a des réserves d'ambi, mais c'est pour les occasions spéciales. Je n'en dirai pas plus sur Corbin, son chapitre ne peut pas se dévoiler sans divulgâcher.

Le navigateur, Ohan, sont une paire sianate. Oui, "sont" : la première fois que Rosemary le rencontre, elle dit "iel" (<i>they</i> singulier) car elle ne connaît pas son genre, et on lui répond que non, c'est bien un "<i>they</i>" pluriel, car Ohan sont la somme d'un sianat et un virus, le chuchoteur, qui donne au sianat la capacité à naviguer dans l'infrastrate. Et donc Ohan sont une seule personne mais qui se nomment au pluriel. Je me demande d'ailleurs comment ça rend en anglais, puisque justement, les <i>they</i> ne sont pas distingables. Le chuchoteur est une part très importante de la culture, et de la religion, d'Ohan. En un sens, c'est peut-être eux l'alien le plus "alien" de l'équipage à nos yeux de lecteurs et lectrices humaines.

Sissix est une aandriske, à l'aspect reptilien, et c'est la pilote du Voyageur. Elle porte des plumes et se peint les écailles du corps. Son peuple est très éloigné de nous autres mammifères : les petits naissent dans des œufs et les adultes n'ont pas d'attachement pour elleux, les adultes se réunissent en famille-plume, et changeront plusieurs fois dans leur vie, et une fois vieilles et vieux, forment une famille-couvée pour élever les petits. Ça me rappelle d'ailleurs une nouvelle de _La fin de tout_, de John Scalzi, dans l'univers du _Vieil homme et la guerre_, où un personnage dit que pour son peuple, les bébés et jeunes n'ont pas "d'existence", la sélection naturelle est très importante, et primordiale même, pour n'avoir que les plus sages (pas forcément les plus forts) à l'âge adulte. En tant qu'humaine, ça me choque instinctivement, mais c'est justement ça qui est intéressant.

Cuisine et médecine, deux facettes d'une même pièce pour le Docteur Miam - dont le nom dans sa langue prend une minute à être prononcé et n'a aucun équivalent dans aucune autre. Il est un grum, ressemble à un croisement entre une loutre et un gecko avec six pattes ("mainpieds") aux yeux de Rosemary. Énorme boule de tendresse, il ne vit que pour soigner et nourrir, et il ne faut pas lui parler de son peuple ni de sa planète, c'est trop douloureux.

> — Et vous ? Je sais bien que les humains n'attachent pas beaucoup d'importance aux noms, mais le vôtre ? A-t-il un sens ?  
> — Eh bien, même si ce n'est pas ce qui a guidé le choix de mes parents, Rosemary veut dire "romarin". C'est une plante.  
> — Une plante ? Quel genre ?  
> — Rien d'extraordinaire. Une herbe aromatique, c'est tout.  
> — "Une herbe aromatique, c'est tout."  
> Les moustaches du Docteur Miam en tremblaient.  
> — "C'est tout", qu'elle nous dit !  
> — Oh-oh, souffla Sissix. Tu as prononcé le mot magique.  

Et pour mener ce monde miniature, l'humain Ashby Santoro, capitaine. Il est pacifique au point de refuser qu'il y ait des armes sur son vaisseau, et aime en secret une militaire Aéluonne.

J'ai bien conscience que je ne fais pas honneur aux personnages en les présentant si peu, et pas non plus au roman en dissertant autant sur elles et eux. Mais c'est un roman choral, ça n'est pas l'"histoire de Rosemary sur le Voyageur", mais bien celle de tous ces gens. Tous ces gens qui évoluent, grandissent, gagnent et perdent pendant ce trajet. 

> Une réaction si humaine : exprimer la tristesse par des excuses.

J'aime tellement cette citation, du docteur Miam quand il faut finalement parler de ce qui est arrivé à son peuple. Rosemary vient de dire "je suis désolée". Je ne sais pas si c'est humain, mais c'est en effet commun au français et à l'anglais ; je me demande si cela se retrouve dans d'autres langues…

---

Quelques mots rapides sur les Torémis : dire qu'iels sont belliqueux est un euphémisme, en vrai. Iels sont animé⋅es par la géométrie, une sorte de vision de leur espace qui influe, qui *est* leur mode de vie. Si deux Torémis ne sont pas d'accord, que leurs pensées ne sont pas en accord, alors il doit y avoir duel, et la pensée la plus forte sera seule survivante. Les clans s'anéantissent régulièrement. Il est impossible de faire des compromis, car ce serait dire ce que l'on ne pense pas. 

C'est une vision intéressante. Je ne la partage pas personnellement, et je pense que d'un point de vue réaliste, ça ne tient pas la route, il ne devrait plus y avoir de Torémis depuis des millénaires (vous voyez le sketch des Inconnus sur le groupe Tranxen 200, qui ne cesse d'avoir des scissions ? ben pareil mais ceux qui quittent le groupe sont tués par ceux qui restent). Mais on ne cherche pas ici le réalisme : ce n'est pas de la hard-science fiction, et ce n'est pas le propos. J'ai lu dans une des critiques, celle d'Eva, qu'on pouvait comparer les Torémis à des fascistes, pour qui toute déviation du dogme est passible de mort ; j'aime bien cette comparaison. Et sans vouloir en révéler trop, les personnages du roman sont assez d'accord.

D'une manière générale, en fait, l'Union Galactique est quand même formée de gens qui se détestent pour beaucoup et se tolèrent pour d'autres (et puis des fois on s'aime), comme dit plus l'univers n'est pas doux, et il y a son lot de connards spécistes (les Quélins sont pas mauvais dans leur genre, et étaient d'ailleurs parmi les opposants à l'entrée des Humains dans l'UG).

---

En bref, je vous conseille grandement ce roman - malgré son aspect tranche de vie, il y a certains moments-clés, que je brûle d'envie de vous raconter, parce que ça m'a touchée, parce que ça m'a surprise, parce que j'ai pleuré. Lisez, et après on en parle, sur votre site ou sur [Mastodon](https://pouet.it/@lamecarlate) 😛

Autres critiques :
- [Critique sur syfantasy](https://syfantasy.fr/critiques/lespace-dun-an-la-critique/)
- [Critique de Bifrost](https://www.belial.fr/blog/l-espace-d-un-an)
- [Article sur le blog d'Apophis](https://lecultedapophis.com/2017/04/02/lespace-dun-an-becky-chambers) (attention, divulgâche beaucoup)
- [Article chez l'Ours inculte](https://ours-inculte.fr/lespace-dun-an) (ah, space-doudou vient de là !)
- [Article chez Eva D. Serves](https://evadserves.ovh/index.php/2019/08/18/lespace-dun-an-becky-chambers) (très intéressant et plein de réflexion, mais divulgâche beaucoup)
