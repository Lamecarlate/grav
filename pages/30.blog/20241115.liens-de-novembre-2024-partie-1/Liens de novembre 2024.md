---
meta-création: 2024-10-31 09:46
meta-modification: 2024-10-31 09:46
stringified-date: "20241031.094604"
title: Liens de novembre 2024 (partie 1)
template: blog-item
date: 2024-11-15 09:46
update_date: ""
taxonomy:
  type: article
  tag:
    - liens
    - cinéma
    - féminisme
    - histoire
    - musique
    - typographie
    - bande dessinée
    - mignonnerie
    - astronomie
    - sciences
    - writing month
    - writing month 2024
highlight:
  enabled: false
subtitle: Histoire du cinéma africain, trains musicaux, range cet ego veux-tu, regarde ces beaux crayons turquoises, et ce beau champignon turquoise aussi, et la Lune 😻 !
---

Histoire du cinéma africain, trains musicaux, range cet ego veux-tu, regarde ces beaux crayons turquoises, et ce beau champignon turquoise aussi, et la Lune 😻 !

===

Que [cette page est drôle](https://web.archive.org/web/20140310190221/http://www.sewingandembroiderywarehouse.com/embtrb.htm). C'est en anglais, mais le contenu n'est pas important ici. Ce qui est amusant, c'est que la personne qui a écrit cette page (page aujourd'hui disparue, c'est pour ça que je vous emmène sur la Wayback Machine) n'a jamais fermé les balises de titre, qui se sont emboîtées. Encore. Et encore. Et comme le style par défaut d'un titre  est d'avoir une taille de texte relative, le texte a grossi. Encore. Et encore. À la fin de la page, on ne voit plus qu'une seule énorme lettre.

[L'histoire des cowboys africains au cinéma.](https://www.youtube.com/watch?v=2zdEZPqOpnY) (vidéo avec du son, sans sous-titres, 25 minutes). Via [Augier](https://diaspodon.fr/@AugierLe42e/113378604072852860).

[c'est dur pour eux](https://paulineharmange.fr/2024/10/cest-dur-pour-eux/), article-poème de Pauline Harmange. Attention, le texte est très dur, ça parle de la haine qu'Harmange reçoit presque quotidiennement depuis qu'elle a écrit le livre « Moi, les hommes, je les déteste », et aussi de l'affaire Mazan (en quelques mots si vous n'êtes pas au courant ou dans le futur, Gisèle Mazan a été endormie et offerte au viol de dizaines d'hommes par son mari pendant dix ans). 

[5 femmes dont on a volé le travail — Nota Bene](https://www.youtube.com/watch?v=0Ms4yjIqdUw) (vidéo avec du son, des sous-titres, 18 minutes). Je connaissais déjà Alice Guy (première cinéaste de fiction, qui a créé le premier studio) et Elizabeth Magie, mais je ne savais pas (ou j'avais oublié…) que Colette s'était fait piller ses premières œuvres par son mari, ce type est vraiment né avant la honte… Via [Augier](https://diaspodon.fr/@AugierLe42e/113429565004084327).

Pour le ou la ferrovipathe en vous : [Hlavní nadraží Praha - znělka 02 (Dlouhá verze)](https://www.youtube.com/watch?v=cD3QlR98--A "https://www.youtube.com/watch?v=cd3qlr98--a") (vidéo avec du son, sans paroles, 3 minutes, attention, sur la fin les images défilent vite, faites gaffe si vous êtes épileptique). Via [Erin Kissane](https://mas.to/@kissane/113432603846145925).

[my least favourite "well actually"](https://www.youtube.com/watch?v=ndR081QG1Gg) (vidéo en anglais, avec du son, des sous-titres, 5 minutes) : OUI. Mais tellement. Ici, Linus explique pourquoi il déteste qu'on lui dise « hmmmm en fait, <i>font</i> et <i>typeface</i> c'est pas la même chose, tu racontes n'importe quoi ». En français, on fait moins la distinction, on dirait « police de caractères » et « fonte », mais surtout juste « police », donc ça s'applique moins. Le problème, ce n'est pas tant que ce sont des termes différents, mais que cette différence n'a plus de sens dans notre monde actuel. Il fait également des analogies avec la tomate, et avec le zip (« je zippe ce dossier et je te l'envoie »), et c'est tout à fait adapté. J'aime beaucoup son expression "weaponized trivia", qu'on pourrait traduire par « anecdote utilisée comme une arme ». Ce que l'on peut ressortir de cette vidéo c'est : si quelqu'un utilise un terme à tort selon toi, réfléchis avant de le ou la corriger, est-ce que c'est ton ego qui parle ?

Du même auteur, [when the fonts bring down an s-tier film](https://www.youtube.com/watch?v=l3mnqhp_nb8) (vidéo en anglais, avec du son et des sous-titres, 3 minutes) : je n'en suis personnellement pas à ce niveau, même si je grince des dents en voyant les kernings pourris de certaines enseignes.

(je crois que j'ai trouvé la nouvelle chaîne de vidéo qui va aller dans mon lecteur de flux 😅)

Duuu même auteur encore, [Comic Sans just got a serious upgrade](https://www.youtube.com/watch?v=ynliPsRmmcQ) (vidéo en anglais, avec du son et des sous-titres, 22 minutes) : un historique de la fameuse police Comic Sans, pourquoi on l'aime ou la déteste, comment l'utiliser, et par quoi la remplacer, notamment par la nouvelle police Shantell Sans. Personnellement pas très fan des gens qui mettent leur nom dans le nom de leur œuvre mais hé, c'est leur création. Via [Augier](https://diaspodon.fr/@AugierLe42e/113435845654205315)

Évidemment, quand on parle de Comic Sans, je dégaine la [Comic Sans Song](https://www.youtube.com/watch?v=OBibXwwLBts) (vidéo en anglais, avec du son et des sous-titres, chanson de 4 minutes). Une de mes musiques préférées du monde. La vidéo est sortie il y a 12 ans, et il y a toujours de nouveaux commentaires, et des gens qui la découvrent. C'est si beau.

<blockquote lang="en">Comic Sans, the best font in the world<br />
If you want your designs to look they're done by little girls</blockquote>

---

![Photo de nombreux outils d'art (portes-plume, cutters, pierre à encre japonaise et bâtonnets d'encre, pinceaux, règle, crayons, etc) placés de façon harmonieuse et dense dans un espace carré.](power-tools.webp){.figure-media--wide}

Source : [My power tools](https://www.reddit.com/r/knolling/comments/1g8ier6/my_power_tools/) (en anglais).

Ohlala c'est joli, ça. Je découvre le <i lang="en">knolling</i>, qui est le fait de photographier des objets placés de façon plutôt dense et ordonnée (verticalement et horizontalement, mais pas toujours). 

La description du subreddit /r/knolling est :

<blockquote lang="en">Knolling is items laid out, often at right angles, in a highly visually pleasing way. It does not require a specific angle of photography, colour or size ordering.</blockquote>

Ce qui peut se traduire par :

> Knoller, c'est placer des objets, souvent à angle droit, d'une manière hautement plaisante visuellement. Cela ne requiert pas d'angle de photographie, d'ordonnancement par couleur ou taille. 

D'après [Wikipédia](https://en.wikipedia.org/wiki/Tom_Sachs#Knolling),  le terme aurait été inventé par Andrew Kromelow, gardien d'une entreprise de fabrication de chaises, qui avait pris l'habitude de disposer ses outils de façon perpendiculaire afin de tous les voir d'un coup. Et comme cela ressemblait aux designs des meubles de Florence Knoll, pour qui son entreprise travaillait, il a appelé sa pratique <i lang="en">knolling</i>. 

J'ai l'impression que c'est une méthode extrêmement utilitaire qui devient par la suite une tendance de jolis arrangements. Un peu comme le Bullet Journal qui est à la base d'une austérité caricaturale et qui a muté vers du très coloré, très vivant, à la fois codifié et libre, au risque d'enfouir l'efficacité sous les autocollants.

L'image plus haut me fait de l'effet. J'ai envie de me relancer dans la peinture à l'encre (j'avais un kit, étant enfant, avec une pierre à encre, un petit bol de faïence, un bâtonnet et un pinceau et tout… je crois qu'il ne me reste que le petit bol, j'ignore où est passé le reste).

---

[Spirou & Fantasio : Seltzmann](https://tapas.io/episode/3200824) : c'est une BD, un fan-art de Spirou (pas de texte alternatif, hélas), d'une cinquantaine de pages, et franchement bien fichu ! Le point de départ : Spirou est une créature dessinée qui a pris vie pour servir de groom à l'Hôtel Moustique, mais il ne semble pas satisfaire ses patrons. Que faire ? 

![Petit champignon cyan qui dépasse d'une souche d'arbre](mycena-interrupta.webp){.figure-media--wide}

Ce champignon minuscule (les <i>Mycena interrupta</i> font généralement 1 à 2 cm de long) a été photographié par [Steve Sargent](https://www.flickr.com/photos/sargimuss/26650626112/). Trouvé via [Wonder of Science](https://mastodon.social/@wonderofscience/113449910536867741).

[La lune déguisée en Saturne](https://apod.nasa.gov/apod/ap240901.html) (photo avec texte alternatif et explications en anglais). J'adore le site [Astronomy Picture of the Day](https://apod.nasa.gov), il a très peu changé depuis sa création, en, \**regarde ses notes\**, juin 1995, et il est très efficace : une photo de l'espace, ou bien des métiers de l'espace, une petite explication, des liens vers des sources, et voilà.

[Copilot ment sur sa propre documentation](https://shkspr.mobi/blog/2024/10/githubs-copilot-lies-about-its-own-documentation-so-why-would-i-trust-it-with-my-code/) (en anglais), comment pourrait-on lui faire confiance ? Je préfère le terme de « se tromper » plutôt que « mentir », car cela impliquerait une volonté d'induire en erreur, éventuellement une conscience, ce qui pour l'instant n'est pas prouvé.

---

Quinze jours, quinze articles. Comme vous l'avez peut-être remarqué, je participe au <i lang="en">Writing Month</i> 2024 (je l'ai [vaguement annoncé](https://pouet.it/@lamecarlate/113390741837640329) sur mon compte Mastodon). J'écris chaque jour, et je publie chaque jour. L'idée ce n'est pas de faire un roman en un mois, comme je l'ai tenté les années précédentes, et où je me suis brûlée les ailes (mais j'ai deux chouettes histoires qu'un jour je reprendrai), mais de faire un peu mais souvent.

Je perçois l'effet insidieux du « publier chaque jour », qui m'a poussée à faire quelques articles pas extra, parce qu'il « fallait le faire », mais dans l'ensemble, à la moitié du défi, je suis contente de 95% de mes écrits.

Certains ont été réfléchis pendant la journée ou la veille, d'autres sont venus en 15 secondes (par exemple [Nostalgie induite](../nostalgie-induite) n'était pas prévue, j'étais en train de préparer une image pour la poster, et paf, Nukumori dans les oreilles et les plans ont changé en quelques notes). C'est une belle expérience.

À la fin de ce mois, je relâcherai la contrainte. Chaque jour, c'est trop. Par contre, ça a vraiment validé le [processus de travail](../mon-processus-de-travail-pour-le-blog) dont j'avais parlé tantôt, il est vraiment efficace pour aller vite. Sauf la taxonomie, qu'il faut que j'écrive en dehors d'Obsidian, parce que j'ai activé l'affichage des « propriétés », une gestion intéressante du frontmatter, mais qui ne supporte pas encore les propriétés imbriquées, comme la taxonomie chez moi. La prochaine version de ce site (j'y travaille j'y travaille) aura un frontmatter à plat, ce sera encore plus facile.
