---
meta-création: "2024-05-08 15:49"  
meta-modification: "2024-05-08 15:49"  

stringified-date: "20240508.154932"

title: "Geek de tout"  
template: "blog-item"  

date: "2024-05-09 15:49"  
publish_date: "" 
published: true # si besoin de date de publi + force publi  

taxonomy:
    type: article
    category:
        - humeur
    tag:
        - geek
        - personnel
        - sciences

# plugin Highlight
highlight:
    enabled: false

---

En réponse à [l'article du même nom](https://nota-bene.org/Geek-de-tout) de l'ami Stéphane, moi aussi jveux jouer !

===

À la fac, j'ai essayé d'adapter la fameuse méthode de la table de 9 avec les mains à d'autres tables (c'est un échec, ça marche pour la table de 9 justement parce qu'on a 10 doigts).

Je connais par cœur beaucoup trop de sketchs de François Pérusse.

Au collège je mettais des blagues dans les marges de mes contrôles de maths… et le prof répondait (je l'ai revu dernièrement, il m'a dit que des copies comme ça, c'est une fois par carrière, je me suis liquéfiée de gêne et de fierté).

Je fais du jeu de rôle dans un groupe de nerds qui fait aussi de l'électronique de la couture de la broderie et de la peinture (et on est majoritairement devs dans le groupe 😸).

Je vois les problèmes de crénage (plus connu sous le nom de <i lang="en">kerning</i>), et comme le dit Randall Munroe dans xkcd [c'est une malédiction](https://xkcd.com/1015/).

Je passais beaucoup de temps de mes récrés au collège à déterminer si tel ou tel nombre était premier, en divisant par 13, puis 17, puis 19, puis 23, puis… (quand, en terminale, on m'a prêté une calculatrice TI-92 avec option « factorisation », qui faisait le boulot pour moi, je… ne sais plus du tout ce que j'ai pensé mais woh c'était chouette).

En parlant de TI-92, j'ai écrit plein d'histoires (en apprenant le qwerty au passage) et le prof de maths m'a prêté plusieurs fois le câble qui permettait de transférer les données vers un ordinateur, merci encore, c'était très cool.

J'ai un T-Shirt "Three creepers and moon", à la fois geek et ringard par ironie, je l'adore et je serai malheureuse quand il sera trop troué.

Parfois, j'arrive à résister à ramasser écrous et vis que je vois par terre.

Je suis bien moins productive que dans ma jeunesse, où je dessinais et j'écrivais beaucoup, et où j'ai fait un peu de musique assistée par ordinateur, aussi ; mais ça va, ça reviendra.

Et vous saviez que, lors de la scène avec le casque, l'acteur qui jouait Aragorn s'est vraiment fait mal au pied ? (pardon, cette blague est vraiment difficile à l'écrit, j'espère qu'elle n'est pas trop cryptique)

Intérêt de niche : j'ai une île de Myst en Lego, cadeau de l'Amoureux (et il s'agirait d'ailleurs de la monter, profitons des jours qui viennent).

Je vis avec deux chats (comment ça, ça ne compte pas ?).

Et vous ?

