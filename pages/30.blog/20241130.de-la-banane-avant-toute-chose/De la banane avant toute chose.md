---
meta-création: "2024-11-30 18:42"  
meta-modification: "2024-11-30 18:42"  

stringified-date: "20241130.184254"

title: "De la banane avant toute chose"  
template: "blog-item"  

date: "2024-11-30 18:42"  
update_date: ""

taxonomy:
    type: article 
    tag:
        - cuisine
        - writing month
        - writing month 2024

# plugin Highlight
highlight:
    enabled: false

---

Une chose en amenant une autre, voilà un banana bread et un petit bonus.

===

Aujourd'hui, un ami devait passer pour nous aider pour le [lave-linge](../l-affaire-du-lave-linge). Donc j'ai voulu faire un gâteau pour le goûter. Je ne savais pas encore à quel parfum, j'errais dans les rayons de mon magasin habituel. Et j'ai vu : des bananes. Un bac entier. De. Bananes. Et surtout… avec des très mûres, toutes noires et moelleuses. Parfaites pour un banana bread[^1]. J'ai donc pris des pépites de chocolat aussi[^2].

Et le saviez-tu, on peut manger les peaux des bananes. L'idéal c'est qu'elles soient bio, ou en tout cas non traitées. J'ai trouvé une recette de tartinade pour les utiliser, juste à côté de celle pour le banana bread, dans mon bouquin « Dépenser moins, manger mieux », de Mélanie Mardelay, qui tient le blog [Le cul de poule](https://leculdepoule.co). Je la reproduis ici, dans le cadre du droit de courte citation (et si ça peut vous donner envie d'acheter le livre, ou en tout cas d'aller lire son blog de cuisine végétalienne, c'est chouette).

<aside markdown="1">
## Tartinade de peaux de bananes

Pour 2 personnes  
Préparation 5 minutes  
Cuisson 20 minutes 

### Ingrédients

- 3 peaux de bananes
- 40 g de sucre en poudre
- 250 mL d'eau
- 1 cuillère à soupe de cacao en poudre non sucré

### Instructions

1. Bien laver les peaux de bananes. Couper les extrémités et hacher les peaux. Les déposer dans une casserole avec le sucre et l'eau.
2. Porter à ébullition et laisser cuire 20 minutes à feu vif, il ne doit plus rester d'eau.
3. Mixer finement avec le cacao en tartinade crémeuse.
</aside>

J'avais mal lu, j'ai mis une cuillère à café de cacao, enfin une bombée, ça compte comme une cuillère à soupe je pense 😄 Et j'ai mixé avec mon mixer plongeant, je conseille de mettre le cacao après, parce que c'est très volatile. \**tousse tousse\**

Ça réduit beaucoup, dites, les peaux de bananes. J'en avais mis quatre petites, ça correspond à peu près à trois normales, et à la fin, ça n'a rempli qu'une moitié de mon petit pot. Je me demande si on ne peut pas stocker les peaux au fur et à mesure au frigo, par exemple trois ou quatre jours.

C'est très bon de pouvoir tartiner du chocolat vaguement banané sur du gâteau à la banane avec des pépites de chocolat.

![Photo d'un gâteau doré avec deux tranches de bananes cuites sur le dessus, et d'un petit pot avec un peu de crème couleur chocolat, sur une assiette bleue.](IMG_20241130_184011--p.jpg){.figure-media--wide}

L'ami est bien passé, on a avancé sur le lave-linge mais comme il a plus de 10 ans certaines vis sont **rouillées** et l'une d'elle nous a tellement résisté (on a même essayé de la détruire en perçant, sans résultat hélas) qu'on a laissé de côté pour cette fois. Il nous faudrait une dremel pour redessiner une raie ou une croix sur la vis, afin de pouvoir l'enlever. Mais nous avons gagné beaucoup de connaissance ici : on peut y arriver, on va pouvoir changer ce joint ! Un jour.

Et finalement l'ami ne pouvait pas rester, donc, euh, on a goûté quand même au gâteau mais je l'apporterai demain à la soirée de jeu de rôle, nous faisons partie du même groupe.

[^1]: En vrai elles étaient assez jeunes à l'intérieur des peaux qui se délitaient, mais ça va quand même.
[^2]: J'ai menti, j'avais déjà prévu d'en acheter, pour des cookies et autres. (et pour mettre sur les granolas)~~(et pour manger directement par poignée depuis le pot)~~
