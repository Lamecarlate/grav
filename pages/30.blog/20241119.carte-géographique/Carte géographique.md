---
meta-création: 2024-11-19 22:21
meta-modification: 2024-11-19 22:21
stringified-date: "20241119.222126"
title: Carte géographique
template: blog-item
date: 2024-11-19 22:21
update_date: ""
taxonomy:
  type: article
  tag:
    - sciences
    - writing month
    - writing month 2024
highlight:
  enabled: false
subtitle: Via le jeu « Forêt mixte », je découvre l'existence du papillon appelé « Carte géographique ».
---
Via le jeu « Forêt mixte », je découvre l'existence du papillon appelé « Carte géographique ».

===

En regardant les cartes d'une extension de « Forêt mixte » (bon jeu de société, mangez-en), avec l'Amoureux, on trouve une carte représentant un papillon avec un nom étrange. Carte géographique. Chelou. Mauvaise traduction ? Que nenni, c'est [une vraie bête](https://fr.wikipedia.org/wiki/Carte_g%C3%A9ographique_(papillon)), et tout à fait courante en Europe, en plus. Je sais bien qu'on ne peut pas connaître tous les insectes existants mais quand même, on s'est senti⋅es un peu ignorant⋅es sur ce coup 😓

(bon, on m'aurait demandé mon avis, je l'aurais appelé Vitrail plutôt que Carte géographique, mais voilà, ce jour-là j'avais sûrement cueillette d'edelweiss dans le marais poitevin)