---
meta-création: "2024-07-29 15:17"
meta-modification: "2024-07-29 15:17"

stringified-date: "20240729.20240729151750"

date: "2024-07-29 15:17"
update_date: ""
template: "short-note"
taxonomy:
    type: short-note
    tag:
        - sciences
        - personnel

title: "On m'a menti ! (après recherches, en fait ça va)"

---

Toute ma scolarité (enfin à partir de la fac je suppose, car je me souviens peu d'avant) j'ai utilisé un L majuscule pour le symbole du litre. mL pour millilitres, cL pour centilitres, en laboratoire il me semblait que je n'aie vu que ça. J'ai conservé cette habitude pour des recettes de cuisine ou mes écrits persos.

Or il semblerait que ça soit [optionnel](https://www.bipm.org/documents/20126/41483022/SI-Brochure-9.pdf/fcf090b2-04e6-88cc-1149-c3e029ad8232) (lien vers un fichier pdf, voir la page 33, je mets une citation plus loin), et recommandé seulement s'il y a un risque de confusion avec le chiffre 1. 

> Le litre et son symbole l (en minuscule) ont été adoptés par le CIPM en 1879 (PV, 1879, 41). Le symbole L (en majuscule) a été adopté par la CGPM à sa 16<sup>e</sup> réunion (1979, Résolution 6 ; CR, 101 et Metrologia, 1980, 16, 56-57), comme alternative pour éviter le risque de confusion entre la lettre l et le chiffre un, 1.

Ensuite… il y a des pays où c'est obligatoire, comme [les États-Unis](https://www.nist.gov/pml/special-publication-330/sp-330-section-4).

Donc. J'avais commencé cette notule avec une expression de « mais on m'a menti toute ma jeunesse vazyla rembourse ma ligne temporelle » et en fait non, il y a de grandes chances que j'aie incorporé le L majuscule parce que c'est ce que j'avais sous les yeux, et que j'ai beaucoup lu en anglais.

(et ça n'aide pas que parfois les noms des produits sur les sites de vente spécialisés soient entièrement en majuscule, et qu'on y trouve donc des flacons de « 1000ML »)

(ceci était donc une notule chaotique, pfiou, ça fait du bien de ventiler sur des points ultra-techniques)
