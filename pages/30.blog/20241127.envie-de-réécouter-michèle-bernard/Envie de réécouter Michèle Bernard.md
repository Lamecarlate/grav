---
meta-création: 2024-11-27 18:50
meta-modification: 2024-11-27 18:50
stringified-date: "20241127.185053"
title: Envie de réécouter Michèle Bernard
template: blog-item
date: 2024-11-27 18:50
update_date: ""
taxonomy:
  type: article
  tag:
    - musique
    - writing month
    - writing month 2024
highlight:
  enabled: false
subtitle: Dans mes oreilles en ce moment, un podcast sur les cabarets artistiques.
---

Dans mes oreilles en ce moment, un podcast sur les cabarets artistiques.

===

Et entendre un petit bout d'accordéon sur une chanson dédiée au Lapin agile, ça m'a rappelé à quel point j'aime la chanteuse [Michèle Bernard](https://fr.wikipedia.org/wiki/Mich%C3%A8le_Bernard). Je l'écoute depuis toute gamine (mais je ne connais pas ses dernières œuvres, tiens…). <i>Les petits cailloux</i> (au fond de la Durance), c'est mon plus ancien souvenir d'elle. Je viens de relire [les paroles](https://genius.com/Michele-bernard-les-petits-cailloux-lyrics), oumf, c'est bien plus dur que je le pensais… Je l'ai écoutée trop tôt, cette chanson. C'est dur, ça parle de mort, mais aussi d'une envie trépidante de vivre, de faire la révolution. Je frissonne.

> Oh, j'aimerais bien  
> Que tu me balances très fort  
> Contre la vitrine  
> Du magasin de porcelaine  
> Ça f'ra chanter la sirène  
> J' ferais un peu de remue-ménage  
> Dans les listes de mariage  
> Ménages et vaisselle brisés  
> J'aimerais bien être un pavé  
> Dans cette mare de médiocrité  

Et <i>Maria Szusanna</i>, que j'écoutais en boucle au lycée, parce que j'adorais la musique tzigane (je parlais tantôt de mon violon, je pense que c'est une des raisons pour lesquelles je m'y étais mise, parce que tant la musique tzigane que klezmer font la part belle au crincrin, et bon sang, qu'est-ce que j'aimais ça).

Allez, pour le plaisir, je vous mets <i>Maria Szusanna</i> et <i>Qui a volé les mots ?</i>, de son album <i>Voler…</i>. <i>Qui a volé les mots ?</i> est beaucoup plus dans le registre comique, en plus d'enseigner de tonnes de mots venus de partout. En fait j'aurais envie de vous partager tout l'album, toutes sont si bien, elles font des trucs dans le cœur, de tristesse, de rage ou de rire. Je suis sûre que ça se trouve (mais achetez-lui des CD, plutôt).

<figure markdown="1">
<figcaption><i>Qui a volé les mots ?</i></figcaption>
![Qui a volé les mots ?](1_-_Qui_a_volé_les_mots.ogg)
</figure>

<figure markdown="1">
<figcaption><i>Maria Szusanna</i></figcaption>
![Maria Szusanna](2_-_Maria_Szusanna.ogg)
</figure>
