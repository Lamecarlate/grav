---
meta-création: "2024-07-18 18:55"  
meta-modification: "2024-08-01 19:49:40"  

title: "Liens de juillet (partie 2)"  
template: "blog-item"  

date: "2024-08-01 18:55"  
update_date: ""

taxonomy:
    type: article
    category:
        - liens
    tag:
        - humour
        - musique
        - science
        - mignonneries
        - couture
        - accessibilité

# plugin Highlight
highlight:
    enabled: false

---

Il fait chaud, ne sortez pas, prenez plutôt un verre d'eau (tiède) ou un thé (chaud) et une série de liens vers des chouettes recoins d'Internet. Des chats, du jeu de rôle, des larmes d'émotion et des cailloux joliment rangés. 

===

[Les nonnes Troppo — Le crapaud à grande bouche](https://www.youtube.com/watch?v=nSUndZO1DNs) (vidéo avec du son, une chanson, sans sous-titres mais [les paroles sont dans un commentaire](https://www.youtube.com/watch?v=nSUndZO1DNs&lc=UgzrAaRu0dHaoBVwwQd4AaABAg), 3 minutes). Via l'ami Panais.

La vidéo suivante n'a pas de son, c'est normal.

![Une animation en pixel-art, une minuscule île flottante avec un arbre rose et une cascade, le ciel est de nuit avec une pleine lune](66cb5785532791d4.mp4){.figure-media--wide}

Source : [A Sister's Journey](https://mastodon.gamedev.place/@asistersjourney/112797311672150135) (c'est un jeu vidéo en cours de développement, un jeu de plateforme apparemment très difficile, donc je suis contente de voir les images sans avoir besoin de jouer, vu que je suis une tanche avec des moufles huilées à la place des mains dans ce genre de jeu).

[Danser tout seul dans la rue](https://social.goose.rodeo/@cats/112787903643322858) (image avec texte alternatif généré par IA, honnêtement bien fait, en anglais).

[Vérification en temps réel](https://syzito.xyz/@fkamiah17/112784999240158614).

[Des chats, et soudain](https://jawns.club/@zorn/112813530677952781) (vidéo avec du son, sans paroles, 20 secondes chaos total).

[Quand le jeu de rôle déborde sur le monde réel](https://www.youtube.com/watch?v=NNt7Jc6l8-Y) (vidéo avec du son, des sous-titres, 20 minutes) est un documentaire sur le jeu de rôle actuel, qui s'écarte des clichés des jeunes mecs blancs dans leur cave. Un des exemples donnés est la campagne D&Drags qui a eu lieu à La Bonne Auberge pour le mois des fiertés de cette année. Le principe ? Une campagne de Donj' dont toustes les joueurs, joueuses et maîtresse du jeu sont des personnes en drag (drag queens surtout, et la caution drag king 😛). Est-ce que c'est mieux ? Est-ce que c'est moins bien ? La question n'est pas là. C'était très chouette ! Ce que j'ai trouvé chouette c'est la représentation des drags, et leur variété (Soa de Muse par exemple me paraît éloignée des clichés des drag queens en talon et ultra-maquillées, et c'est super intéressant) ; et aussi que ce sont des personnages de scène… qui jouent des personnages de jeu. La campagne elle-même était excellente, l'histoire était captivante, et la meujeu, Éva Porée, bon sang, quelle actrice, elle m'a transportée ! [D&Drags à La Bonne Auberge](https://www.youtube.com/watch?v=YMvGgvF4yPE&list=PLXd8mfrhn6IywcdI-0mTF-2__-xR97Eky) (liste de vidéo avec du son, sans sous-titres, entre 1 et 2 heures par épisode, il y en a six). Via [Jelora](https://mastodon.gargantia.fr/@jelora/112761229427192372).

[Miiiiiii](https://toot.cat/@devopscats/112827922398834883) (vidéo avec du son, sans paroles, un ptit chat très vocal, 7 secondes).

[La science des bulles](https://fediscience.org/@helenczerski/112842117094219841) (vidéo sans son).

[Un choupisson adulte](https://mastodon.opportunis.me/@ZeldAurore/112849441256330349).

[Dungeons & Dragons taught me how to write alt text](https://ericwbailey.website/published/dungeons-and-dragons-taught-me-how-to-write-alt-text/) (en anglais). Via [nicod](https://diaspodon.fr/@nicod/112830930616370621).

[Iceberger](https://joshdata.me/iceberger.html) : jeu en anglais mais les règles sont simplissimes. Dessinez une forme dans la zone de jeu, pour en faire un iceberg, qui se mettra ensuite dans sa forme la plus stable. Oui, la fameuse image de l'iceberg pointu en haut et en bas… ça ne tient pas. Via [Stéphanie Walter](https://stephaniewalter.design/blog/pixels-of-the-week-july-28-2024/).

Voilà mon dernier essai :

![Une représentation schématique d'un iceberg aux formes rondes et tarabiscotées, quatre extrémités sur huit sont émergées.](iceberg.webp)

---

![Une capture d'écran de Tumblr, la transcription suit.](desperate-cleric.webp)

Transcription et traduction personnelle :

> Un prêtre désespéré qui lance si fort tous les sorts de soin possibles pour faire revenir quelqu'un à la vie que le sol est forcé de faire pousser des plantes et des fleurs autour du corps.
> 
> Des décennies plus tard, gardé par une forêt de roses et de ronces, gît un cadavre refusant de pourrir.

Par l'utilisateurice otherwindow.

Trouvé par l'amie Flo.

---

[Spirales](https://mamot.fr/@bbecquet/112723563209352791).

[Le vieil homme à la marche des fiertés de Porto](https://beige.party/@RickiTarr/112718332889420745) (en anglais). Un vieil homme portant un drapeau portugais a demandé à faire un échange de drapeaux lors de la marche des fiertés à Porto fin juin. [La vidéo de l'échange est là](https://mastodon.online/@ePD5qRxX/112718681402644099) (vidéo avec du son, on n'entend pas les paroles, 1 minute). Je crois qu'on ne sait pas exactement ce qu'il s'est passé : il voulait participer mais n'avait pas de drapeau adapté, ou bien il avait envie/besoin de brandir un drapeau parce que jusque dans les années 70 c'était interdit, pendant la dictature, ou peut-être est-ce un message à quelqu'un… On ne sait pas, on peut imaginer plein de choses, mais pour moi toutes sont douces, parce qu'il semble vraiment très ému et heureux de montrer son grand drapeau LBGTQIA+ inclusif 🥹

J'ai déjà parlé de la chaîne Calmos en [septembre 2023](../liens-septembre-partie-2) et en [décembre 2023](../liens-décembre-partie-2) mais c'est pas ma faute s'ils font de chouettes vidéos remplies de bonnes références cinématographiques et de réflexions pertinentes sur l'humour. Ici, [La Cité de la peur et la mort de la comédie française](https://www.youtube.com/watch?v=PviZj7ogUlU) (vidéo avec du son, des sous-titres, 11 minutes), sortie il y a 5 ans mais indémodable. C'est malin j'ai envie de revoir ce film – et aussi presque tous ceux cités comme références ou inspirations (non, pas ceux à 8:52 par contre).

[The Terminotter](https://mastodon.art/@saadazim/112863621155975159) (image en anglais). Muhuhuhuhu.

[C'est de la concurrence déloyale aux entreprises de fabrication de peluches, cet oiseau](https://mamot.fr/@eidolies/112863528868643542).

![Des galets disposés en carré, classés par couleur, formant un gradient, sur le sable](e86677f2f63e0d30.jpeg) 

Œuvre de Emily Blincoe, via [Natasha Jay](https://tech.lgbt/@Natasha_Jay/112861219272685621).

[Réparer les blessures](https://h4.io/@brettezeleliquide/112575396089355796) (vidéo sans son, 30 secondes) : repriser un vêtement tricoté, ici un pull, c'est si beau.

[Le chat qui chante le blues](https://www.youtube.com/watch?v=vEXfKoXXFUo) (vidéo avec du son, sans paroles, 1 minute). Via [LivBu](https://piaille.fr/@LivBu/112870086564212178).

[Chanter en langue des signes](https://mamot.fr/@Vive_Levant/112710396447905116) (vidéo avec du son, 1 minute) : que c'est cool. Clare Edwards double en langue des signes une performance musicale, et wow, elle tabasse. Ses expressions sont très présentes et omagad elle fait de l'<i>air guitar</i> lors des passages sans paroles. 

---

Je suis en vacances. Pour trois semaines. 

Ahhhhhhh.

Pas de plage dorée pour moi, mes cocotiers sont dans la tête (cataclop cataclop), on va se reposer, ranger, coudre, jouer à des jeux vidéo, bref, des vacances, quoi.



