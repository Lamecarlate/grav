---
meta-création: "2022-10-29 07:38"
meta-modification: "2022-10-29 07:38"

stringified-date: "20221029073815"

title: "Avoir des onglets verticaux dans VSCodium/VSCode"
template: "blog-item"

date: "2022-10-30 08:38"

taxonomy:
    type: article
    category:
        - informatique
        - logiciel
    tag:
        - vscode
        - vscodium

# plugin Highlight
highlight:
	enabled: false

---

J'ai l'habitude des onglets verticaux un peu partout : nos écrans sont de plus en plus larges, pourquoi s'embarrasser d'une ligne quand on peut en faire une colonne (et y voir plus de contenu) ? De plus, accoutumée à PHPStorm au travail, et devant changer pour un temps d'outil, j'ai voulu reproduire la même apparence dans VSCodium (c'est valide pour VSCode aussi).

===

L'opération est en trois étapes :

- afficher la seconde barre latérale
- y placer la liste des fichiers ouverts ("éditeurs ouverts")
- cacher les onglets

Tout d'abord, il faut afficher la seconde barre latérale.

Pour cela, plusieurs chemins possibles : via les menus, ou via les commandes.

En passant par les menus, il faut choisir Affichage → Apparence → Barre latérale secondaire et cocher la case (en anglais, <span lang="en">View → Appearance → Show secondary side bar</span>).

![Capture d'écran de VSCodium avec le menu "Apparence" ouvert et la ligne "Barre latérale secondaire" surlignée](menu-apparence.png " "){.figure-media--wide}

Si vous préférez passer par les commandes : <kbd>Ctrl + Maj + P</kbd> pour ouvrir le menu des commandes, et chercher <span lang="en">"side bar"</span>, ou "barre latérale", afin de trouver "Activer/désactiver la visibilité de la barre latérale secondaire" (en anglais <span lang="en">View: Toggle Secondary Side Bar Visibility</span>), et valider.

Dans l'explorateur (barre latérale principale), trouver <span lang="en">"Open Editors"</span> (en anglais) ou "Éditeurs ouverts" (en français) et glisser ce bloc dans la seconde barre.

Si ce bloc n'y est pas, il peut être ajouté en cliquant sur le petit menu kefta (les trois petits ronds horizontaux, hé, ce n'est pas un burger 😛) dans l'entête de la barre latérale et le choisir dans la liste.

![Capture d'écran du petit menu susmentionné, avec la souris pointant sur "Éditeurs ouverts"](menu-editeurs-ouverts.png)

Le résultat intermédiaire :

![Capture d'écran avec la liste des fichiers ouverts dans la barre latérale secondaire](resultat-intermediaire.png " "){.figure-media--wide}

Enfin, il faut cacher les onglets, qui font désormais double emploi. Cela se passe dans les préférences, et encore une fois il y a plusieurs manières de faire. 

Accéder à l'interface des paramètres :
- Fichier → Préférences → Paramètres (ou en anglais)
- ou <kbd>Ctrl + ,</kbd>
- ou <kbd>Ctrl + Maj + P</kbd>, chercher "Préférences: Ouvrir les paramètres utilisateur" (ou <span lang="en">"Preferences: Open User Settings"</span> en anglais)

Ensuite, dans l'interface, chercher <span lang="en">"showtabs"</span>, et faire un choix dans le menu déroulant dans "<span lang="en">Workbench > Editor: Show Tabs</span>", entre "multiple" (on voit tous les onglets ouverts), "single" (on voit seulement l'onglet en cours, avec son chemin, ce qui peut avoir son intérêt) et "none" (rien du tout, la ligne est cachée).

Ou bien modifier le fichier JSON des paramètres :

- Ouvrir les préférences comme vu plus haut et cliquer sur le petit bouton "Afficher les paramètres (en JSON)" en haut à droite
- ou <kbd>Ctrl + Maj + P</kbd>, chercher "Préférences: Ouvrir les paramètres utilisateur (JSON)" (ou <span lang="en">"Preferences: Open User Settings (JSON)"</span> en anglais)

Ensuite, ajouter à la fin du fichier `"workbench.editor.showTabs": "none"` (ou `"single"`). Ne pas oublier de mettre une virgule à la fin de la ligne précédente si on ajoute la ligne à la toute fin.

Pas besoin de redémarrer, dès l'enregistrement du fichier, les onglets disparaissent 😊

Le résultat final :

![Capture d'écran de la fenêtre avec les fichiers ouverts dans la barre latérale secondaire, et plus d'onglets horizontaux](resultat-final.png " "){.figure-media--wide}

Cet article est tiré de : [Vertical tabs in Visual Studio Code](https://domysee.com/blog/vscode-vertical-tabs), avec mes propres avis et captures.

Correction du <time datetime="2023-11-11">11 novembre 2023</time> : j'avais auparavant écrit `false` comme valeur (et je parlais de la case à cocher dans l'interface graphique), mais ce n'est plus valide. Les valeurs possibles sont `"multiple"`, `"single"`  et `"none"`.