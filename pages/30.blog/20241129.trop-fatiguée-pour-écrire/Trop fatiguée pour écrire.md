---
meta-création: "2024-11-29 22:08"  
meta-modification: "2024-11-29 22:08"  

stringified-date: "20241129.220859"

title: "Trop fatiguée pour écrire"   
template: "blog-item"  

date: "2024-11-29 22:08"  
update_date: ""

taxonomy:
    type: article
    tag:
        - photographie
        - mes chats
        - mignonneries
        - writing month
        - writing month 2024

# plugin Highlight
highlight:
    enabled: false
    
subtitle: Comme je l'ai écrit le 15, je fatigue. 

---
Comme je l'ai écrit le 15, je fatigue. 

===

On dirait que ce soir je laisserais juste une jolie photo.

![Photo d'un lit en désordre avec un grand sac en toile contenant une petite chatte à l'air blasé, mais elle est assise confortablement alors ça va.](IMG_20241124_110617--p.jpg)
