---
meta-création: "2024-03-16 18:18"  
meta-modification: "2024-04-07 14:25:54 "  

stringified-date: "20240316.181809"

title: "Liens de mars (partie 2)"  
template: "blog-item"  

date: "2024-04-07 15:00"  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - musique
        - mignonnneries
        - cinéma
        - politique
        - animation
        - humour
        - histoire
        - art
        - accessibilité
        - 

# plugin Highlight
highlight:
    enabled: false

---

Oui, je poste un peu tard. Mais je n'allais pas vous poser un lapin (de mars) ! 

Est-ce que j'ai attendu exprès pour faire cette vanne ? Vous n'avez aucune preuve.

===

[That don't impress me much](https://www.youtube.com/watch?v=mqFLXayD6e8) (vidéo avec du son, de la musique, et sous-titres en anglais) : pas fan plus que ça mais la chanson était entraînante, et le texte plutôt sympa. Oué, Shania veut bien sortir du désert, mais pas avec n'importe qui ! La richesse, le statut, la beauté, ce n'est pas forcément la chose la plus importante qu'elle recherche chez un homme. Via le [Friday night music thread](https://mastodon.social/@flexghost/112102718572889412), le thème de ce vendredi était "music a Karen might like before screaming at a barista and after screaming at a barista".

[Pattounes !!!!!](https://mastodon.online/@Marshalltown1/112105739895779878)

[Vampires : de l'hémoglobine et de la politique sur les dents du mythe](https://www.youtube.com/watch?v=_VWhzSroxYw) de KaleidosPop.c (vidéo avec du son, et sous-titres, environ 1 heure). Un sacré voyage, qui montre notamment qu'il y a des créatures dites vampiriques partout dans le monde et depuis très longtemps, liées au sang, symbole de vie et de mort à la fois, mais aussi à l'accouchement (il existe des créatures issues de femmes mortes en couche, ou bien d'enfants non-baptisés). Pas mal de citations en tout genre, doublées par de nombreux et nombreuses invitées (j'ai reconnu Bolchegeek rien qu'à la voix… et à la citation de Karl Marx 😆).

[Le joli coco](https://www.youtube.com/watch?v=1gSnrlIp9XI) (vidéo avec du son, sans sous-titres (enfin si mais ils sont en anglais)) : il est rose comme une rose et il est mignon, le joli coco. Voilà. Sur un texte originel de Capucine, retravaillé, illustré et animé par Boulet, un chef-d'œuvre de 3 minutes.

[On peut mettre un peu de magie dans nos gestes](https://www.radiofrance.fr/franceinter/podcasts/alors-voila/alors-voila-de-baptiste-beaulieu-du-lundi-18-mars-2024-5682218) (épisode de podcast avec transcription, 3 minutes). Alors voilà, Baptiste Beaulieu n'écrit plus sur son blog mais il parle à la radio et c'est vachement bien aussi.

[La sécurité des mots de passe c'est important](https://tech.lgbt/@Natasha_Jay/111951426482255720) (c'est une blague avec un jeu de mot en anglais).

[Imprimer, déprimer](https://danstonchat.com/21740.html).

[Boîtes à crâne et ossuaires en Bretagne : Finistère, Côtes d’Armor](https://lalunemauve.fr/boites-a-cranes-ossuaires-bretagne-finistere-cotes-armor/) chez Marie Gilles. Je suis tombée dessus via [La boîte à crâne du turfu](https://weblog.redisdead.net/main/post/2024/03/16/La-boite-a-crane-du-turfu) de Lawrence Vagner. Je ne connaissais pas du tout le principe des boîtes à crâne, c'est très intéressant !

[Dessiner sur des encombrants pour augmenter leur chance d'être récupérés](https://www.youtube.com/watch?v=mONfMcMsDdA) (vidéo avec du son, sans sous-titres (grrr Radio France, tu pourrais faire un effort), 4 minutes environ). C'est plutôt mignon : un artiste qui se balade en ville, et qui dégaine ses marqueurs (rechargeables !!!) pour dessiner sur des panneaux de bois, des miroirs, et qui joue avec son public, en lançant ses abonné⋅es d'Instagram à la recherche de l'œuvre qu'il vient de finir. Mention spéciale à la ptite dame qui lui fait une remarque mi-bougonne mi-critique d'art sur son travail en cours, c'est plutôt choupi.

[L'addiction aux écrans](https://www.youtube.com/watch?v=IfwQ5p_5f7s) chez Paroles de chat (vidéo avec du son, sans sous-titres, environ 3 minutes). Pffffrrrrhuhuhuhuhu. J'adore que l'auteur se marre quand il joue son texte avec ses ptites voix ridicules de chat. Via [Augier](https://diaspodon.fr/@AugierLe42e/112127502479093002).

[L'accessibilité aux aveugles du Louvre](https://mamot.fr/@Serenity/112126787038663137) (vidéo avec du son et des sous-titres incrustés) : wow, le bonheur de ce jeune homme aveugle qui découvre les œuvres tactiles et le fait que beaucoup d'indications sont en Braille au Louvre, c'est beau !

[Un chiot et des canetons](https://meow.social/@Bwee/111919258471374056) (vidéo avec du son, sans paroles) : mouuuuuuh c'est mignon.

Je n'avais jamais vu la bande annonce du film [H2G2 : Le guide du voyageur galactique](https://www.youtube.com/watch?v=p631BvhesgA) (vidéo avec du son, sans sous-titres hélas). Amusant de voir qu'il y a 20 ans, les codes des trailers étaient déjà bien là, et on pouvait s'en moquer, ici avec brio.

<details markdown="1">
<summary>Ma réplique préférée de la bande-annonce</summary>
Souvent ces plans-là sont précédés de la phrase « Un monde dans lequel… » <i>(la planète explose)</i> mais… pas toujours.
</details>	

[Oh la belle broderie](https://mastodon.social/@Robynw/112135926813378118).

![Image en pixel-art, avec de grands aplats de couleur. C'est un portrait du personnage de Teal'c dans la série Stargate : SG-1. Teal'c est un homme noir, au crâne rasé, avec des lignes dorées sur le front, qui forment un serpent dans deux ovales concentriques. Il porte une armure métallique avec des lignes gravées qui s'entrecroisent. Il regarde vers nous d'un air suspicieux (il a toujours l'air suspicieux).](a351e1e58e31d260.png)

Source : [x0_000](https://mastodon.gamedev.place/@x0_000/112137119841480390).

[La médecine est-elle sexiste ?](https://invidious.fdn.fr/watch?v=cb3Ov5r4pME) (vidéo avec du son, pas de sous-titres, 15 minutes environ) : pfiou. Avec concision, Julien Menielle décrit les nombreux cas où les femmes sont considérées comme l'exception, comme s'écartant de la norme du genre humain (par exemple pour les crises cardiaques, où les symptômes sont assez différents entre hommes et femmes…), et donc moins bien traitées.

[Combat de cris de lynx](https://www.youtube.com/watch?app=desktop&v=YiHE9uCN8dc) (vidéo avec du son, sans sous-titres, mais les quelques voix humaines sont inintéressantes, ici ce qu'on veut c'est du lynx qui grogne, environ 1 minute) : c'est… étrange. On dirait presque des humains qui essaient d'imiter le chat qui feule.

[Sunshine - Don't confuse love and abuse](https://invidious.fdn.fr/watch?v=1L6HB97lbrQ) (vidéo avec du son, de la musique uniquement, 3 minutes) : oumf. Ce petit clip musical parle d'une relation romantique abusive. Impressionnant de voir comment en si peu de temps on peut y voir tous les marqueurs inquiétants : éloignement d'avec les amies, succession de crises de colère (en privé) et d'abondance de cadeaux (en public), contrôle de la nourriture et de l'habillement… Via [clochix](https://mastodon.social/@clochix/112149450552179711).

---

Les [kei trucks](https://en.wikipedia.org/wiki/Kei_truck) sont, si je comprends bien, des sortes de camionnettes à espace ouvert à l'arrière, plutôt petites, pour satisfaire des réglementations japonaises. Et des gens s'amusent à y mettre des… jardins miniatures.

![Montage de trois photos. La première montre l'arrière d'une camionnette, avec un jardin de mousse, un petit chemin de sable bordé de grosses pierres noires, allant de l'arrière vers l'avant, et terminant sur une sorte de grotte dans une mini-montagne, toujours de pierre noire. Il y a des plantes, et même un petit arbre au feuillage roux, et le tout est très humide. Sur la deuxième photo, une autre camionnette, de profil, contient un jardin de mousse d'un vert éclatant, avec une mini-montagne, et sur le bord droit, juste au bord du plateau, un banc de bois, avec une petite table (et possiblement une théière). La troisième photo présente le côté d'une camionnette, cette fois avec une pièce de maison, aux cloisons faites de planches de bois et deux des cloisons comportent des fenêtres de papier blanc. On voit l'intérieur de la pièce, il y a de nombreuses plantes en pot, ou plantées derrière une petite barrière de bois (l'image est petite, j'ai du mal à distinguer…).](c3174502321b2f7e.jpg){.figure-media--wide}

Source : [ghostzero](https://tech.lgbt/@ghostzero/112133632760681954) (en anglais).

---

[On a les mêmes abrutis des deux côtés du Rhin](https://mastodon.social/@pizzaroquette/112150282682392805).

[Atilla est un gros tigre très câlin](https://www.youtube.com/watch?v=_KucUc2MoKM) (vidéo avec du son, des sous-titres anglais très nuls incrustés mot à mot et des sous-titres français automatiques, 1 minute).

[Saying someone from a minority group is "one of the good ones" or "not like the others" is not a compliment](https://rimgo.hostux.net/gallery/m0hPhsW) (vidéo avec du son, en anglais, sous-titres anglais incrustés, une petite minute) : ou comment on peut penser faire un compliment en disant à une personne qu'elle n'est pas comme les autres de son groupe social ou ethnique et en fait on renforce le stéréotype dans sa tête. Ici, l'exemple est sur le racisme anti-noir, mais il marche pour beaucoup d'autres choses. Je pense notamment à « mais toi t'es pas comme les autres filles » (et évidemment ça marche aussi en internalisé…).

---

Les enfants et leur parler… 😊

> Ça me fait penser à ma nièce qui réclamait à ses parents des "saucisses plates" lors des barbecues. Ça leur a prit 6 mois pour comprendre que c'est des saucisses qui piquent pas, comme l'eau plate, contrairement à l'eau qui pique et les merguez ^^

Source : [abon999](https://framapiaf.org/@abon999/112171723447132414).

---

Wowowowow c'est au niveau d'un grand méchant de saga space-opera que ce travail de Manuel Matuzović : [Comment faire la page la plus inaccessible possible avec un score Lighthouse parfait](https://www.matuzo.at/blog/building-the-most-inaccessible-site-possible-with-a-perfect-lighthouse-score/) (en anglais).

[Une histoire de vélo volé… mais pour la bonne cause](https://piaille.fr/@blablux/112161267888346931).

[Une jolie photo de fleurs](https://www.bloogart.com/blooming-rainbow/).

[Une dame avec aussi bon coeur que mauvaise vue a apporté à un refuge pour animaux sauvages un pompon de bonnet qu'elle avait pris pour un bébé hérisson](https://www.cheshire-live.co.uk/news/chester-cheshire-news/woman-mistakes-bobble-baby-hedgehog-28873338) (en anglais). Via [Abie](https://octodon.social/@temptoetiam/112158430963356604), à qui j'ai pris la phrase d'intro, elle est simplement parfaite.

[Joe Wells Has A Vision For An Autistic Future](https://www.youtube.com/watch?v=1Jvx_umysik) (vidéo avec du son, en anglais, des sous-titres en anglais un poil à la ramasse sur les nombres à un moment) : un humoriste autiste dans un sketch bien amusant, sur les personnes autistes (et le fait que peu d'entre elles sont visibles et surtout audibles, on a souvent tendance à voir parler d'elles de l'extérieur), la science de l'humour et les français⋅es. Via [Chroniques autistes](https://thecanadian.social/@adelinej/112185469555374869), qui précise que cet humoriste utilise le sarcasme, qui peut être moins compris par les personnes autistes.

[La jolie relation entre un photographe et une renarde](https://france3-regions.francetvinfo.fr/grand-est/meurthe-et-moselle/nancy/temoignage-l-histoire-vraie-de-la-relation-entre-une-renarde-sauvage-et-un-photographe-animalier-2946591.html). Via [Philippe](https://veganism.social/@mascottus/112189032966635138).

[Cette page du webcomic Paranatural](https://www.paranatural.net/comic/chapter-4-page-25) (image contenant une page de bande dessinée, pas de texte alternatif) est superbe : peu importe que vous connaissiez les personnages ou non, cette technique du méta-shuriken est géniale. Je l'ai déjà lue autre part mais je n'arrive pas à mettre le doigt dessus. (Note : il va peut-être falloir que j'arrête de lire _Paranatural_ au boulot, c'est difficile de ne pas éclater de rire à chaque page)

---

Nous avons abandonné l'idée de maintenir la petite chatte convalescente encore un mois dans la maison. Après deux semaines elle a profité d'une inattention pour aller dehors et qui serions-nous pour l'en empêcher ? Des monstres ? Elle est revenue dans la journée. Bon, nous surveillons quand même, c'était juste un mois pour bien s'assurer qu'elle est toute réparée.
