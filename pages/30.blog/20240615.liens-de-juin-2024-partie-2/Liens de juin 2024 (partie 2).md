---
meta-création: "2024-06-15 22:08"  
meta-modification: "2024-06-15 22:08"  

stringified-date: "20240615.220801"

title: "Liens de juin 2024 (partie 2)"  
template: "blog-item"  

date: "2024-06-15 22:08"  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - humour
        - mignonnerie
        - cuisine

# plugin Highlight
highlight:
    enabled: false

---

Récolte légère. Chats, émotion, blagues nulles en anglais, étude sociologique sur un mème.

===

[Un cauchemar](https://mindly.social/@annecavicchi/112622175666088972) (en anglais, blague de daron, khihihihihihi)(oui je pouffe toute seule à l'écrit dans mon carnet).

[Pleurer, ça fait du bien](https://www.cryonceaweek.com/). Via [Manuel Matuzović](https://front-end.social/@matuzo/112488634915939353).

[Quel beau chat, et poli avec ça](https://www.youtube.com/watch?v=GM5j7UICB6E) (vidéo avec du son, pas de paroles, 1 minute).

[Un chien qui maîtrise la baballe](https://framapiaf.org/@Air/111711462972316649) (vidéo, 1 minute).

[Une union d'BZ](https://www.youtube.com/watch?v=yFbqdszziYs) (vidéo avec du son, des sous-titres incrustés, 1 minute) : toute ressemblance avec de l'actualité ne serait que, ouhlalala, pur hasard. Normalement, cet article sera posté après le 30 juin, vous aurez voté, hein ? Le Nouveau Front Populaire aura gagné, hein ? Hein ?

[Ne poursuivez pas vos rêves](https://ratherbemaking.games/@Javier/112475040937661807).

[Débunkons l'histoire des sciences](https://www.youtube.com/watch?v=xVUr9GYrISA) (vidéo avec du son, sans sous-titres, 30 minutes) : intéressante interview d'Antoine Houlou-Garcia, auteur de « Et la pomme ne tomba pas sur la tête de Newton », qui raconte que certains mythes scientifiques ont la peau dure, et que la science, c'est rarement une personne seule mais bien une équipe, une communauté. Via [Deirdre](https://diaspodon.fr/@Deidre/112506948300585249).

[La recette du water pudding](https://www.youtube.com/watch?v=RSRICzm6inY) (vidéo avec du son, hélas sans sous-titres, 5 minutes) : Pierre Dac et Francis Blanche en grande forme, pour présenter la recette franco-anglaise de la terrine en gelée de merle sans merle, ou water pudding. Via [Benjamin Bayart](https://mastodon.xyz/@bayartb/112675971011695581).

[Le chien béat et l'incendie – Le dessous des images](https://www.arte.tv/fr/videos/110342-058-A/le-dessous-des-images/) (vidéo avec du son, des sous-titres, 10 minutes). Via [Natouille](https://mastodon.tetaneutral.net/@Natouille/112676601403890500).