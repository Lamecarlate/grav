---
meta-création: 2024-11-07 09:06
meta-modification: 2024-11-07 09:06
stringified-date: "20241107.090609"
title: Le lendemain
template: blog-item
date: 2024-11-07 09:06
update_date: ""
taxonomy:
  type: article
  tag:
    - personnel
    - politique
    - writing month
    - writing month 2024
highlight:
  enabled: false
subtitle: Profond soupir.
---
Profond soupir.

===

C'est bateau de le dire, et beaucoup sont dans le même cas, mais : j'ai le ventre serré après les élections étatsuniennes d'hier. Oui, on sait, ça ne sera vraiment voté qu'en décembre, mais c'est plié, c'est évident, et tout le gratin des techbros félicite déjà Trump. 

Je ne comprends pas. En vrai, si, je comprends, les plus grands médias ont donné à manger du trumpisme à la petite cuillère au peuple étatsunien, sans parler des influenceurs et autres suprémacistes qui gagnent bien leur vie à sortir des horreurs en riant. Mais moi je suis encore dans la sidération.

73 millions de gens ont voté pour ce type. Parmi elleux, j'ose espère que beaucoup ne sont pas activement racistes, xénophobes, sexistes, misogynes, transphobes, et que ce sont « juste » des gens qui n'ont rien à péter de leurs prochains et ne pensent qu'à leur personne. Parmi ces personnes, à mon avis, il y en a qui sont contre un truc en particulier, pas le reste, et qui pensent que, eux, ça ira, ils et elles ne seront pas inquiétées. Cette naïveté pourrait faire sourire.

Je ne sais pas quoi dire d'autre. 

Ah, si. Je m'inquiète de la montée (de l'accession au pouvoir) de l'extrême-droite aux USA, mais n'oublions pas qu'en France, le RN est majoritaire à l'Assemblée… 

L'ami Stéphane a posté un joli texte sur le même sujet : [Tout va bien](https://nota-bene.org/Tout-va-bien). 

Prenez soin les uns des autres.
