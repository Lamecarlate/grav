---
title: Shangri-La, de Mathieu Bablet

date: '2021-01-23 10:42'
publish_date: '2021-01-23 10:42'
published: true
taxonomy:
    type: article
    category:
        - lecture
    tag:
        - BD
        - anticipation
header_image_file: shangri-la-couverture-bannière.jpg
header_image_alt: "Sur la droite, la couverture : sur fond d'espace, un astronaute au dessus d'une planète bleue avec une grande lumière ; le reste de l'image est la planète floutée"
author: 'Lara'
highlight:
    enabled: false

---

La claque. Ouvert un soir pour "lire quelques pages", j'ai en fait mangé les 220 de l'ouvrage en une seule fois. 

===

De prime abord, je n'aimais pas trop le dessin, mais c'est justement ça qui m'a tenu en haleine au début : une ligne assez dure, des couleurs tranchées et un travail excellent sur la lumière ; je voulais savoir ce qui se racontait avec ce dessin.

La première partie se passe sur une planète lointaine de la Terre, désertique, desséchée, avec un soleil au bord de l'extinction. 

![Un personnage de dos qui regarde, effrayé, vers l'arrière, sur le haut d'un canyon violet, rouge et jaune](shangri-la-paysage.jpg "Nan mais regardez moi cette lumière ! (les fesses du monsieur aussi, si vous voulez)"){.figure-media--wide}

Après quelques pages, on retourne dans le passé, quelques mois avant l'arrivée de Scott, un des protagonistes, sur cette étrange planète. Il vit dans une station spatiale en orbite autour de la Terre devenue inhabitable. Dans cette station vivent deux populations vaguement mélangées : les humains, et les animoïdes. Ces derniers sont des hybrides entre humains et animaux non-humains (chien, chat, etc), essentiellement des humains à tête animale. La station est dirigée par la corporation Tianzhu, qui loge tout le monde, embauche tout le monde et vend tout. C'est un vase clôt parfait. 

En tout cas pour Scott, qui est très content de sa vie. Enfin, satisfait. Enfin, il ne veut rien y changer. Scott est ingénieur, et il est mandaté pour enquêter sur des accidents dans diverses mini-stations, qui ont en commun une explosion extrêmement sphérique et absolument aucun⋅e survivant⋅e. Parallèllement, il est approché par des sympathisants de la "rébellion", dont son frère fait partie. Une rébellion contre les dirigeants de la station, secrète mais pas trop, qui pirate de temps en temps les écrans géants pour y afficher des messages anticapitalistes (c'est une honte).

Dans le même temps, une grande avancée scientifique est en cours : la conception d'humains nouveaux, adaptés à l'espace, afin d'aller <del>coloniser</del> explorer d'autres planètes. Et ces recherches font du bruit : entre les chercheurs qui feraient tout pour avancer, au risque de tout détruire, et les gens qui considèrent que c'est dégueulasse parce que c'est pas à ces créatures d'aller visiter l'espace mais à eux, à ceux qui étaient déjà là et qui ont le droit divin d'aller pourrir les autres planètes eux-même non mais oh, ça n'est pas de tout repos.

Bien entendu, comme dans toute histoire où on nous présente deux trames, elles sont liées - mais je n'en dis pas plus.

![Deux cases de la BD : les gens, dont Scott, se ruent dans le magasin pour acheter à tout prix](shangri-la-soldes.jpeg "Juste avant ces cases, une annonce a été faite que le magasin est soudainement à -50%, pendant quelques minutes. Vraiment de la science-fiction…"){.figure-media--wide}

Difficile de suivre Scott, héros plus proche de l'anti-héros, assez désagréable (j'avoue, j'en ai marre des protags mecs blancs grognons), mais l'histoire ne nous le donne pas en exemple. Il est agaçant, chafouin, lâche : il détourne le regard la première fois qu'il voit un collègue se faire tabasser parce que ce dernier est un animoïde - et c'est douloureux parce que beaucoup d'entre nous, moi y compris, le faisons ou l'avons fait.

Il y a une scène magnifique, où Scott rumine "[Mon frère] me fait chier, personne l'oblige à acheter, merde ! On n'a jamais été aussi libres ! Y a pas plus parfait comme système !", alors qu'il déambule dans un couloir de 2m de haut, rempli de policiers en armure, avec casques complets et fusils, et de pubs "Acheter. Utiliser. Acheter encore. Prendre un crédit. Travailler.". La narration est excellente et nous indique clairement le point de vue de l'auteur, qui n'est pas celui de Scott. 

L'œuvre est très, très actuelle dans ses interrogations, sous couvert de science-fiction : consommation à outrance, utilisation sexiste et sexualisée du corps des femmes dans la pub, haine dirigée contre les minorités, esclaves qui fabriquent les téléphones, nombreux sont les parallèles avec notre société. Ça fait mal.

Divulgâchage à propos des animoïdes : <span class="spoiler" tabindex="0">Les animoïdes sont persécutés, parce qu'ils sont différents, parce qu'ils sont inférieurs… parce qu'ils ont été conçus pour ça. Pour détourner l'attention des gens des autres problèmes.</span>

Vraiment une belle surprise. Comme dit au début, les dessins m'ont intriguée, puis happée, puis le scénario a pris le relai (les premières pages sont très contemplatives et on ne comprend pas grand chose - et je pense que c'est voulu). 

Divulgâchage d'une partie de la fin : <span class="spoiler" tabindex="0">C'est assez déprimant dans l'ensemble. On a un peu l'impression que toute tentative de changer le système pour l'améliorer est vaine. Notamment quand la meneuse de la rébellion dit clairement qu'elle veut prendre le pouvoir et le garder parce que les humains ne sont pas capables de décider seuls… </span>

Je conseille franchement.

Liens supplémentaires :
- [Page Wikipédia sur l'œuvre](https://fr.wikipedia.org/wiki/Shangri-La_(bande_dessin%C3%A9e))
- [Site de Mathieu Bablet](https://mathieubablet.ultra-book.com/)
