Vous pouvez aller voir mes anciens blogs : ils ne seront plus à mis à jour, mais je les maintiens en ligne pour les archives.

Chacun décrivait une facette de moi. Je m'étais amusée à prendre comme mascotte pour chacun un animal mythologique (j'ai gardé la Coquecigrue pour ici, symbole de la fusion)

* cuisine et gourmandise : [Basiliculinaire](https://basilic.lamecarlate.net/)
* informatique, surtout développement : [Codex sphinxial](https://archive.lamecarlate.net/informatique)
* création graphique : [Au Griffon griffonnant](https://archive.lamecarlate.net/griffon)
* et l'ancien [Coquecigrue](https://archive.lamecarlate.net/coquecigrue)

On peut aussi noter la [Manticore à lunettes](https://archive.lamecarlate.net/magic), blog dédié à Magic et à [yaomi](https://yaomi.fr), l'application web d'inventaire que j'ai développée. Ce blog-ci étant vraiment très spécifique, il ne vient pas dans le giron de Coquecigrue.

Note du <time date="2023-04-15">15 avril 2023</time> : ce carnet ne s'appelle plus Coquecigrue, il n'a plus vraiment de nom particulier. Les archives ont d'ailleurs désormais leur [page dédiée](https://archive.lamecarlate.net/).