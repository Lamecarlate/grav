---
meta-création: "2024-11-21 18:26"  
meta-modification: "2024-11-21 18:26"  

stringified-date: "20241121.182628"

title: "Bloguidien du 21 novembre 2024"  
template: "blog-item"  

date: "2024-11-21 18:26"  
update_date: ""

taxonomy:
    type: article
    tag:
        - cinéma
        - writing month
        - writing month 2024

# plugin Highlight
highlight:
    enabled: false

---
Parlons un peu de cinéma.

===

> Quel film regrettes-tu de ne pas avoir vu au cinéma ?

Je n'aime pas beaucoup le cinéma. Le lieu, s'entend. Il y a trop de gens, on ne peut pas s'échanger des théories, s'esclaffer ou mettre en pause pour aller chercher le dessert / aller aux toilettes / développer une pensée complexe sur le point de détail qu'on vient de voir là maintenant tout de suite en bas à droite de l'écran.

Je crois qu'un film que j'aurais voulu voir au cinéma, c'est _Avatar_ (celui de Cameron). Je n'ai pas aimé ce film, je l'ai trouvé insipide, au scénario trop classique, et j'ai ragé sur les créatures extraterrestres qui étaient à mon sens surtout faites d'animaux terriens avec une paire de pattes en plus et qui brillent.

Mais voilà, je l'ai vu sur un petit écran de PC. Le voir au cinéma n'aurait pas réparé le scénario, n'aurait pas amélioré la romance et le complexe du sauveur blanc, mais j'en aurais probablement pris plein les mirettes. Ça aurait pu compenser.

(un jour je parlerai de la meilleure séance cinéma de ma vie, lors de laquelle je n'ai quand même pas pu aller chercher le dessert, mais quelle expérience de voir _Star Crash_ au milieu de fans en furie)

Amorce tirée du site [Bloguidien](https://bloguidien.fr/bloguidien-du-jeudi-21-novembre-2024) pour le jeudi 21 novembre 2024.
