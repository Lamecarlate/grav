---
meta-création: 2024-11-24 17:25
meta-modification: 2024-11-24 17:25
stringified-date: "20241124.172514"
title: Quinze secondes de souris
template: blog-item
date: 2024-11-24 17:25
update_date: ""
taxonomy:
  type: article
  tag:
    - mignonneries
    - writing month
    - writing month 2024
highlight:
  enabled: false
subtitle: Aujourd'hui, à la laverie, j'ai surtout attendu, j'ai discuté un peu avec une gentille dame qui a refusé que je l'aide à plier ses draps, et surtout, surtout, j'ai vu une petite souris.
---

Aujourd'hui, à la laverie, j'ai surtout attendu, j'ai discuté un peu avec une gentille dame qui a refusé que je l'aide à plier ses draps, et surtout, surtout, j'ai vu une petite souris.

===

Elle est entrée par la porte ouverte, a longé le mur en courant à toute vitesse, fait crier une dame (je ne me moque pas, la peur panique des rongeurs c'est réel et plutôt logique, mais il faut avouer que c'était un peu comique, un stéréotype de femme qui a peur des souris, c'est tout juste si elle n'a pas grimpé sur son siège), et voyant qu'elle arrivait devant un mur, a fait promptement demi-tour et a filé par la porte (qui était toujours ouverte).

Interlude fort court mais ça m'a fait sourire.

