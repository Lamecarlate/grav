---
meta-création: "2024-09-01 12:45"  
meta-modification: "2024-09-15 17:53:14"  

stringified-date: "20240901.124542"

title: "Liens de septembre 2024 (première partie)"   
template: "blog-item"  

date: "2024-09-15 17:45"  
update_date: ""

taxonomy:
    type: article
    category:
        - liens
    tag:
        - humour
        - mignonneries
        - musique
        - technologie
        - design
        - cuisine
        - accessibilité

# plugin Highlight
highlight:
    enabled: false

---

Ce que j'aime dans ces liens bimensuels c'est que je finis toujours par tomber sur des choses improbables, et c'est la beauté du web et des gens dedans. Des collabs de musique incroyables, une heure sur les bouilloires, l'histoire des polices de caractères, des vases superbes, de l'accessibilité aux aveugles d'un jeu de tuiles chinois, bref, de tout.

===

[Spice Girls - Wannabe (in the style of System Of A Down)](https://www.youtube.com/watch?v=KWWBeaqdBas) (vidéo avec du son, de la musique et des paroles en anglais, sans sous-titres hélas, 3 minutes). Ohlala. C'est quelque chose. 

[« Dieu y pourvoira »](https://infosec.exchange/@i0null/113003868726736323) (vidéo sans son, gif animé plutôt, avec un alt, weeeeee).

---

[Why don't Americans use electric kettles?](https://www.youtube.com/watch?v=_yMMTVVJI4c) (vidéo avec du son, en anglais, sous-titres en anglais, 25 minutes). Vidéo très intéressante sur les différentes raisons pour lesquelles aux États-Unis on utilise peu les bouilloires électriques. Notamment : le câblage aux USA est en 120V, contrairement à la France où c'est 220V. Par conséquent, c'est bien plus long là-bas. J'avoue que je ne m'attendais pas à ça mais c'est logique. 

Je viens de faire le test avec ma bouilloire électrique pour 1 litre d'eau à température ambiante : 2 minutes 18 (et encore j'ai attendu le bip, car les premiers bouillons visibles étaient à 2 minutes).

Dans les commentaires ça parle des bouilloires électriques ultra-primitives qui étaient surtout une grosse résistance sur un fil, qu'on branchait d'un côté et plaçait dans un pot d'eau de l'autre (ne pas se tromper de sens). Je me souviens que ma mère avait ça dans son bureau il y a fort longtemps. Me revient la mémoire du thé pamplemousse-framboise de la marque Éléphant qu'on y buvait souvent quand j'allais lui rendre visite. Amusant comme ça marche, les souvenirs.

---

[I promise this story about fonts is interesting](https://www.youtube.com/watch?v=WVfRxFwVHQc) (vidéo avec du son, en anglais, sans sous-titres…, 30 minutes).

![Trois vases multicolores avec un émail brillant et craquelé (technique du raku)](raku-pottery.jpg){.figure-media--wide}

Source : [The Alchemist](https://mstdn.social/@alchemistsstudio/113063321052959625) (il y a trois autres photos derrière le lien, dont une dans le four en cours de cuisson, c'est trop beau, allez voir ; images avec texte alternatif en anglais).

---
[Des confitures de fruits rouges au bon goût de fruits !](https://jepensedoncjecuis.com/2024/05/des-confitures-de-fruits-rouges-au-bon-gout-de-fruits.html).

> L’eau libre s’évapore à 100°C (au niveau de la mer). Au fur et à mesure que l’eau libre s’évapore, la confiture se concentre en sucre. Il arrive un moment où il ne reste que des molécules d’eau liées au sucre. Et il faut de plus en plus d’énergie pour les détacher.
> 
> C’est pour cela que la température d’ébullition augmente en même temps que la concentration en sucre.

La cuisine, c'est de la science.

(j'adore tellement le nom de ce site : Je pense donc je cuis)

---

[Il existe un film sur un réalisateur appelé Alan Smithee](https://sfba.social/@dcreemer/113077114391044325), et c'est fort méta. J'ignorais d'ailleurs que la fameuse pratique des réalisateurices de désavouer un film en l'attribuant à Alan Smithee est révolue depuis 2000 – et que le film mentionné dans le lien plus haut a notamment contribué à cette disparition.

Bon, c'est une pub au final mais ça fait partie de cette catégorie de publicités vraiment, vraiment ingénieuses et belles (et en vrai on pourrait couper la fin avec la présentation du produit que ça ne changerait rien) : [Quand ton clavier s'éveille](https://www.youtube.com/watch?v=3bTI6WUJxD4) (vidéo avec du son, en coréen, avec sous-titres en anglais, 4 minutes). Via [n1k0](https://mamot.fr/@n1k0/113084349567264696).

Il y a de nombreuses années, un ancien collègue de boulot m'avait fait découvrir Ishii Takeo (germanisé en Takeo Ischi), un yodleur japonais, avec l'improbable [Chicken Attack](https://www.youtube.com/watch?v=miomuSGoPzI) (vidéo avec du son, en anglais et japonais, sous-titré en anglais, 4 minutes). Il est super connu pour cette chanson complètement barrée, en japonais et en anglais, avec des poulets et des ninjas. Mais il n'a pas fait que ça. Déjà, il a une sacrée discographie de yodel « sérieux », c'est un excellent chanteur. Et… et… oui, il adore les poules et n'hésite pas à faire des chansons sur le sujet, en gloussant entre deux vocalises. J'admire son mélange de sérieux et de fantaisie. Notamment sur ce qu'on pourrait considérer comme une suite de Chicken Attack, [Cow Attack](https://www.youtube.com/watch?v=ySxyPMZkrwU) (vidéo avec du son, en anglais et japonais, sous-titré en anglais, 4 minutes). J'ai, littéralement, pleuré de rire et d'émotion parce que c'est beau et idiot et incroyable et obonsang cette vache-ninja je l'aime. 

[Musique d'introduction du long-métrage animé _Ghost in the Shell_](https://www.youtube.com/watch?v=iTPNaUsjksM) (vidéo avec du son, musique, 5 minutes). Via [amatecha](https://merveilles.town/@amatecha/113112395454779368). Comme d'habitude, ça fait dresser les poils des avant-bras. Ces accords qui paraissent dissonants, ce qui les rend hypnotisants, viennent de la façon de chanter des chœurs bulgares. Notamment, cette harmonie pourrait venir de Pilentze pee, un classique, qu'on peut retrouver dans le disque _Le mystère des voix bulgares_. 

[Où vont les vilains arcs-en-ciel ?](https://mastodon.au/@Heliograph/113112412536453323) (image en anglais, avec texte alternatif et jeux de mots).

Quand je serai grande j'aurai une maison avec une pièce comme ça : [les délices d'un rat de bibliothèque](https://gooz.photography/a-bookworms-delight).

[Pourquoi le mahjong utilise les formes financières de certains chiffres sur les tuiles](https://toot.cat/@clarfonthey/112961200935292493) (en anglais).

[In Space, no one can bother you Chill](https://sunny.garden/@fringemagnet/113067586545767203) (image animée avec texte alternatif en anglais).

Tom Gauld, toujours bien trop pertinent (j'aimerais bien qu'il arrête de regarder dans ma maison ok ?) : [Autumn reading](https://bsky.app/profile/did:plc:oflombnj6v6xyohhatfkbzyd/post/3l444hktx6a2r) (image en anglais, avec texte alternatif).

Les esperluettes, c'est cool & stylé. En voilà tout un tas, dans ce [fil Mastodon rempli de belles photos](https://typo.social/@blag/113102333326616269) (images avec des textes alternatifs pour la plupart, en anglais). Tiens, quelqu'un fait remarquer qu'en anglais, "ampersand" pourrait s'écrire "ampers&".

Voilà le web que j'aime : [imprimer en 3D des disques pour de vieilles machines à coudre](https://craftopi.art/notice/AljgvRxH8fTax6wBfs). Pour archive, si jamais le message disparaît, le [dépôt avec les gabarits d'impression](https://github.com/ln-komandur/elna-cam-discs).

Connaissiez-vous [F. D. C. Willard](https://fr.m.wikipedia.org/wiki/F._D._C._Willard) ? Physicien renommé… pour être un chat.

[Une version japonaise du type de conte « Le lièvre et la tortue »](https://ohai.social/@TarkabarkaHolgy/112964876222622070) (en anglais).

---

J'ai bien avancé dans _Oncle Tungstène_ d'Oliver Sacks, c'est passionnant : il mélange son histoire personnelle avec celles des sciences, en suivant ses différentes passions enfantines. Et même si je n'ai aucune envie d'échanger mon enfance avec la sienne (au hasard, avoir 6 ans pendant la Seconde Guerre Mondiale et subir la méchanceté du directeur de la pension où on a été envoyé par sécurité, ça ne me tente pas), avoir une quinzaine d'oncles et tantes presque toustes scientifiques, et avoir une pièce dédiée pour en faire un laboratoire complet pour faire fondre du métal ou exploser du sodium, j'en rêve.

