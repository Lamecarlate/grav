---
meta-création: 2024-11-16 16:45
meta-modification: 2024-11-16 16:45
stringified-date: "20241116.164526"
title: La cheminée en plein air
template: blog-item
date: 2024-11-16 16:45
update_date: ""
taxonomy:
  type: article
  tag:
    - photographie
    - architecture
    - writing month
    - writing month 2024
highlight:
  enabled: false
subtitle: Au Château de Bon Repos, j'ai croisé un feu de plein air… dans une cheminée.
---

Au Château de Bon Repos, j'ai croisé un feu de plein air… dans une cheminée.

===

![Photographie d'une large cheminée avec un feu allumé ; le dessus de la cheminée est recouvert de mousse et de plantes diverses qui ont poussé dessus.](IMG_20241020_102755--p.webp)

Avec des ami⋅es nous sommes allé⋅es visiter le [Château de Bon Repos](http://www.chateaudebonrepos.com/Visites.htm) à Jarrie, en Isère. C'est un joli bâtiment, en cours de rénovation depuis des années, et il est ouvert au public un dimanche par mois. Ce jour-là, il y avait aussi un jeu type <i lang="en">murder party</i> en cours d'organisation par des étudiant⋅es, donc nous avons déambulé dans les pièces en croisant des personnes très affairées, parfois en costume, portant des accessoires de théâtre. Au sous-sol, la table était mise, avec des poulets rôtis et des montagnes de fromage. 'Font pas les choses à moitié, ces jeunes !

Et dans la grande salle, qui est à ciel ouvert par manque de toiture (elle s'est effondrée pendant la Première Guerre Mondiale), quelqu'un avait allumé le feu dans la grande cheminée. 

J'aime cette photo qui réunit tout un tas de contradictions : le feu en plein soleil, la chaise vide sur le sol nu, et surtout, surtout, la mousse et l'herbe qui ont envahi le manteau de la cheminée.
