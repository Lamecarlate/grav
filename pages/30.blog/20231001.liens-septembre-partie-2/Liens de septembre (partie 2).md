---
meta-création: "2023-09-18 18:55"  
meta-modification: "2023-09-18 18:55"  

stringified-date: "20230918.185504"

title: "Liens et pensées de septembre (partie 2)"  
template: "blog-item"  

date: "2023-09-18 18:55"  
publish_date: "2023-10-01 18:35:37" 
published: true # si besoin de date de publi + force publi  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - humour
        - création
        - design
        - linguistique
        - cuisine
        - musique
        - littérature

# image principale
header_image_file:
header_image_caption:
# miniature (remplace image principale, liste uniquement)
thumbnail: 
thumbnail_orientation: # landscape|portrait

# plugin Highlight
highlight:
    enabled: false

---

Deuxième partie des liens de septembre ! L'internet mondial vient à vous ! À travers mon cerveau ! (fuyez)

===

## 19 septembre

[Data presentation](https://mstdn.social/@12thRITS/111084568087848180) (en anglais) : la présentation dans les graphiques, c'est important.

[Trouver les couleurs dominantes d'une image](https://onlinejpgtools.com/find-dominant-jpg-colors) (en anglais) : c'est un outil web génial, on fournit une image et ça sort les cinq couleurs les plus fréquentes dedans. Pour des projets de broderie, de design web, ou bien pour peindre un cadre autour de l'image si elle est physique, ou un moodboard… il y a sûrement plein d'utilisations. Via [Krysalia](https://mastodon.social/@Krysalia/111091280336482078).

[Les abréviations numériques au Japon](https://piaille.fr/@Blanche/111091383477294132) : au Japon, au téléphone (et depuis les pagers) on utilise parfois des nombres, ou des suites de chiffres, pour signifier des mots. C'est lié à la prononciation des chiffres, avec des approximations. Je connaissais "38" pour "merci" : 3 se dit "sen" et 8 "kyu", donc "sen kyu", "thank you". J'adore !

Il y a quelques jours, j'ai revu l'épisode musical de _Buffy contre les vampires_ (dans la saison 6). Bon, c'était à la base pour me remonter le moral parce que j'étais une larve et… comment dire, c'est un épisode avec des moments drôles mais c'est surtout triste et très poignant (donc j'ai chouigné). C'est aussi un excellent épisode, qui fait vraiment avancer l'histoire, en faisant dire à ses personnages les vérités qu'ils et elles cèlent (et je n'en dirai pas plus, il faut regarder la série).

Voilà le morceau d'ouverture, qui ne divulgâche pas vraiment (à part que Buffy ne va pas très bien, mais ça ne dit pas pourquoi) : [Going Through the Motions](https://www.youtube.com/watch?v=FmLSjwam26E) (vidéo en anglais avec du son).

[De la difficulté de parler de science dans les médias](https://social.platypus-sandbox.com/notice/AZZFSVMdeRkkW197q4) (en anglais).

[Les Visiteurs, ou comment la comédie voyage dans le temps](https://www.youtube.com/watch?v=doAwemnMtK8) chez Calmos (vidéo avec du son, et sous-titres 😻). Calmos, c'est une chouette chaîne, qui parle de cinéma, surtout de comédie, et surtout de comédie française. Et c'est : très bien. Fines analyses, beaucoup de références et de travail. Ici, une étude tournant autour du film _Les visiteurs_ de 1993 (eh oui, ça a trente ans… et je l'ai vu au cinéma dis donc), attrapant au passage de nombreux autres films sur le voyage dans le temps, et en essayant de voir quelles cases de types de comédie le film coche (divulgâchage : beaucoup). Ça donne presque envie de le revoir, mais j'avoue que j'ai développé avec le temps une allergie à Christian Clavier, je ne sais pas si je supporterais.

## 20 septembre

[Floral Beaded Grid Embroidery with Tambour Beading](https://www.youtube.com/watch?v=sgw9TLP5_j8) (vidéo avec du son, mais très léger et non nécessaire), via [Krysalia](https://mastodon.social/@Krysalia/111086516814800634). C'est doux et tranquille, et les gestes très précis de placement des perles sont impressionnants.

![Image comprenant un texte et un montage photo. Le texte dit « Tu fais une embardée pour éviter un écureuil. Tu ne le sais pas, mais l'écureuil jure qu'il a une dette de vie envers toi. Dans ton heure la plus sombre, l'écureuil arrive. » L'image représente un écureuil en armure de plate complète, y compris la queue, avec un petit bouclier et une dague.](squirrel.png)
Source : [aliss](https://outerheaven.club/objects/f87f5fc5-56c3-4558-91f4-8594b378fb6a) (mais j'ai copié l'image ici parce qu'elle n'a pas de texte alternatif dans la source).

## 21 septembre

En ce moment je regarde les archives de _Tasting History_, une chaîne vidéo sur la cuisine des temps passés : Max Miller choisit un plat ou une situation (le halva des guerriers de l'armée ottomane, le gâteau renversé à l'ananas des années 1950, les repas à Alcatraz, le Christmas pudding des tranchées, etc), nous en parle, exécute la recette, et la goûte. C'est vraiment bien, Max est sympathique, il fait de bonnes recherches, fouilles dans des manuels de cuisine antique, explique clairement ses actions, et est franchement sincère dans les phases de dégustation : s'il n'aime pas, il le dit !

Et suite à une [recette de loseyns](https://www.youtube.com/watch?v=CilkAVJLBUY) (possiblement un ancêtre des lasagnes, vidéo avec du son, en anglais avec sous-titres), je découvre la [poudre-douce](https://en.wikipedia.org/wiki/Powder-douce) et la [poudre-forte](https://en.wikipedia.org/wiki/Powder-forte) (les deux en anglais), et j'ai terriblement envie d'essayer.

La poudre-douce (ou powder-douce) telle que l'a faite Max dans la recette plus haut :

- 2 teaspoons ginger 
- 1 tablespoons sugar 
- 2 teaspoons cinnamon 
- 1 teaspoon ground nutmeg 
- 1/2 teaspoon grains of paradise (Or other pepper)

Découverte du terme « avihonte », via [Émilie Nérot](https://piaille.fr/@emilienerot/111102253283640329).

[Porcupine Tree - The start of something beautiful](https://www.youtube.com/watch?v=Yw4Kq-hr63Y) (vidéo avec du son, chanson en anglais), via [gee](https://framapiaf.org/@gee/111102097805037445). C'est tout doux (je m'attendais à bien plus vif et brutal, au vu des premières images), une sorte de ballade. 

[Pas une pipe](https://hachyderm.io/@shafik/111043506851604072).

[Le trailer de la série animé _Dungeons & Kittens_](https://www.youtube.com/watch?v=nG2AKaPJC6Q) (vidéo avec du son, sans paroles) donne très très envie ! Il faudra aussi que je m'intéresse au jeu de rôle du même nom, par les mêmes personnes.

## 25 septembre

Via Epy, une vidéo sur les emprunts de langue notamment en anglais : [How languages steal words from each other](https://www.youtube.com/watch?v=TFpzps-DCb0) (vidéo avec du son, en anglais, avec sous-titres anglais). Moi qui me targuais de connaître plein de ces cas d'emprunts, j'en ai découvert plusieurs (vous verrez pour « amiral », moi ça m'a ébahie) et la petite démonstration finale est magistrale.

## 26 septembre

[Le renouveau de la chanson française](https://mastodon.social/@Lopinel/111127298994123883) (vidéo avec du son). C'est. Heu. C'est quelque chose. [Page Instagram de l'artiste, Léman](https://www.instagram.com/lemanofficiel/). [Une autre chanson du même artiste](https://www.youtube.com/watch?v=XUkw1VZ1LLw) (vidéo avec du son).

> Maintenant je vais te donner **une stratégie géniale pour ne pas spoiler: au lieu d’indiquer en page 43 que tel élément est un signe annonciateur de l’événement qui se passe en page 529, tu peux attendre la page 529,** et là tu te défoules, mon chou: là tu mets une note de bas de page en disant ‘moi je sais! Moi je sais! en fait on savait ça déjà en page 43 tu te souviens? C’était une petite anticipation très intelligente!’ et là moi je retourne page 43 et je me dis ‘ooooh! C’est vrai! C’est chouette! Merci, j’avais pas remarqué,’ et je te fais un bisou. Au lieu de t’envoyer mon poing dans la gueule.

Eh bin je connaissais pas cette dame mais je l'aime. C'est dans son article [Lettre ouverte aux éditeurs de romans classiques (tl;dr: ARRETEZ DE NOUS LES SPOILER, PUTAIN!)](http://clementinebleue.blogspot.com/2015/06/lettre-ouverte-aux-editeurs-de-romans.html), découvert via [Léo Henry](https://mastodon.gougere.fr/@Leo_Henry/111130743971361448). 

Je lis très peu de littérature dite blanche, encore moins des classiques annotés, et je ne crois donc pas avoir jamais expérimenté les notes de bas de page explicatives qui divulgâchent. Pour moi, les notes de bas de page dans un roman, c'est Pratchett (_Le Disque-monde_, où l'auteur ajoute des petites infos sur l'univers, ou bien fait des rappels utiles) ou Fforde (_Les aventures de Thursday Next_, où c'est un système de communication entre les personnages, si si). Ou bien _La maison des feuilles_ de Danielewski, où c'est partie intégrante de l'histoire (vu qu'il s'agit d'un roman sur un essai fictif sur un film fictif, avec trois niveaux de notes de bas de page). Ce sont donc des données ajoutées par l'auteur, et/ou le traducteur, pas des choses externes.

Par contre, dans tous types de littérature, j'ai vu des quatrièmes de couverture qui racontent la moitié de l'intrigue ou mentionnent le personnage qui apparaît aux 2/3 du bouquin. Insupportable. Le pire c'était pour les _Météores_ de Tournier, où la quatrième était un synopsis, avec situation initiale, péripéties 1, 2 et 3 et grand final.

## 27 septembre

> Le terme apparaît dès la fin du XVIe siècle : à cette époque, les imprimeurs lyonnais s'appelaient eux-mêmes « Suppôts du Seigneur de la Coquille »

Source : [la page Wikipédia sur « Coquille (typographie) »](https://fr.wikipedia.org/wiki/Coquille_(typographie)).

[Les coussins, la main coon](https://diaspodon.fr/@ptl/111131159911288878).

En anglais, on dit "to make something agree with" pour « accorder » au sens grammatical (comme dans « il faut accorder les adjectifs avec leur nom »), et c'est si chouette ! Ça sonne comme un faux-ami, mais non. Source : [accorder sur WordReference](https://www.wordreference.com/fren/accorder).

---

Ceci conclut cet article, parce que les jours suivants, j'étais à [Paris Web](https://www.paris-web.fr/), et n'ai pas trop traîné sur les internets (mais j'ai appris tant de choses et vu tant de gens cools… promis, j'ai un article à ce sujet sur le feu).
