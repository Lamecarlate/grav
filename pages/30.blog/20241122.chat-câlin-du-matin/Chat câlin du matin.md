---
meta-création: "2024-11-22 21:01"  
meta-modification: "2024-11-22 21:01"  

stringified-date: "20241122.210114"

title: "Chat câlin du matin"  
template: "blog-item"  

date: "2024-11-22 21:01"  
update_date: ""

taxonomy:
    type: article
    tag:
        - mignonneries
        - mes chats
        - writing month
        - writing month 2024

# plugin Highlight
highlight:
    enabled: false

---

Ce matin, Atrus était fort demandeur de câlins.

===

Il est comme ça, ce chat. Très indépendant, on ne le touche pas sans sa permission, il feule et siffle et peut griffer très vite s'il est mécontent, mais alors. Alors. Quand il veut du câlin. C'est une crème. Une serpillière (toujours vigilante cependant, un mouvement qui lui déplaît et le voilà parti).

Et là, ce matin, il **voulait**. Il a ce regard parfois, qui dit « arrête tout ce que tu fais, assieds-toi et laisse-moi tes genoux ». On s'exécute, quand il a ce regard 😽 

Il s'est installé sur les genoux susmentionnés[^1], s'est mis à ronronner[^2], et après ses multiples mouvements pour trouver la bonne position, il a fini par se coucher et lancer les pattes vers l'avant. Enfin le côté pour moi. Et donc, il a posé une patte sur l'accoudoir, et l'autre a choppé ce qu'elle pouvait pour rééquilibrer. Un T-shirt, en l'occurrence. 

L'est mignon, ce chat, hein ?

![Photo d'un chat tigré gris sur des genoux d'humaine, on voit surtout une partie de sa tête et ses pattes étirées vers l'accoudoir du fauteuil, une des pattes s'accroche à un T-shirt avec ses griffes.](IMG_20241122_081616--p.jpg){.figure-media--wide}

[^1]: Pourquoi parle-t-on des genoux, d'ailleurs ? Ce sont les cuisses sur lesquelles les enfants et les chats grimpent, et non les genoux, qui ne sont pas du tout adaptés pour ça, et sont la majorité du temps pliés lors de ces moments.

[^2]: À la maison, on dit même qu'il ronroucoule, parce qu'il émet un son en plus de la vibration, et toujours à l'expiration, comme ça : rrrrr RIIIII rrrrrr RIIIII (il va bien, je vous rassure, c'est un son pas inquiétant du tout).
