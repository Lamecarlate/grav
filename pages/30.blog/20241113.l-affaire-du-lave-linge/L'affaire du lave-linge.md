---
meta-création: 2024-11-13 18:58
meta-modification: 2024-11-13 18:58
stringified-date: "20241113.185829"
title: L'affaire du lave-linge
template: blog-item
date: 2024-11-13 18:58
update_date: ""
taxonomy:
  type: article
  tag:
    - maison
    - bricolage
    - writing month
    - writing month 2024
highlight:
  enabled: false
subtitle: <i lang="it">Chi va piano, va sano</i>, dit-on. On va très très <i lang="it">piano</i> par ici.
---

<i lang="it">Chi va piano, va sano</i>, dit-on. On va très très <i lang="it">piano</i> par ici.

===

Cela fait maintenant un mois que le lave-linge est en panne. Enfin, en panne. Disons qu'on ne le lance pas, vu qu'il fuit.

Et il fuit parce que le joint est joliment percé. L'hypothèse la plus en vogue est qu'un objet en plastique dur a été laissé à l'extérieur du tambour, heu, par moi (j'ai retrouvé une jupe avec le cordon coincé à l'extérieur, et il manquait le petit embout que je croyais d'ailleurs métallique, et la fois d'après, ça fuyait). On a ouvert le filtre, et une moitié de l'embout perdu y était. L'autre moitié est… quelque part.

On a mis des jours à se décider à acheter un joint pour le remplacer nous-même.

Ce soir, on a déplacé le lave-linge dans le salon pour qu'il n'encombre plus la salle de bain et surtout qu'on puisse lui tourner autour pour le démonter. Et on s'arrête là parce que maintenant on peut regarder derrière et voir qu'on n'a pas (encore) les outils pour l'ouvrir.

On avance. Pas à pas. On va le réparer, ce machin. Promis. Un jour. (En plus c'est super relou d'aller à la laverie automatique, c'est pas loin mais juste assez quand même pour ne pas donner envie.)