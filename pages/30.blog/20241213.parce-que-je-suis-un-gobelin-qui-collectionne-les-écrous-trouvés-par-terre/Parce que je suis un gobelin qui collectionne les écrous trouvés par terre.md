---
meta-création: 2024-12-11 08:24
meta-modification: 2024-12-11 08:24
stringified-date: "20241211.082428"
title: Parce que je suis un gobelin qui collectionne les écrous trouvés par terre
template: blog-item
date: 2024-12-13 08:24
update_date: ""
taxonomy:
  type: article
  tag:
    - bricolage
    - DIY
highlight:
  enabled: false
description: Tentation outillière
---

Tentation outillière.

===

En allant acheter vis et masque de protection à l'Entrepôt du Bricolage[^1], j'ai vu un calendrier de l'avent Bosch, avec une sacoche et des outils. J'ai vaillamment résisté (parce que même s'il me manque des outils, il y avait de fortes chances que j'aie déjà une bonne partie du contenu). Mais ce fut difficile (parce que je suis un gobelin qui collectionne les écrous trouvés par terre).

Et j'ai eu les vis gratuites parce quand j'ai dit au jeune monsieur du rayon que j'en avais besoin de six, il a répondu que comme iels les vendaient en vrac, même la plus petite boîte serait trop grosse, tenez, voilà, et vous dites à la caisse que c'est de ma part[^2].

[^1]: Pour le lave-linge, toujours. On approche de la fin : nous avons une dremel prêtée pour démonter la dernière vis qui retient le joint, et désormais de nouvelles vis pour remplacer les anciennes quand on aura mis le nouveau joint.
[^2]: <span role="img" aria-label="Visage souriant retenant ses larmes">🥹</span> ← ça c'était ma tête.
