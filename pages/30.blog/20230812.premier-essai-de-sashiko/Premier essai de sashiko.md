---
meta-création: "2023-08-12 14:08"  
meta-modification: "2023-08-12 14:08"  

title: "Premier essai de sashiko"  
template: "blog-item"  

date: "2023-08-12 14:08"  

taxonomy:
    type: article
    category:
        - création
    tag:
        - couture

# image principale
header_image_file: IMG_20230812_142023.jpg
header_image_caption: "Tote bag avec deux trous réparés par du tissu marron brodé de blanc"

# plugin Highlight
highlight:
    enabled: false

---

Suite à une vidéo croisée sur le fédiverse, j'ai voulu essayer le <i lang="ja">sashiko</i>.

===

Le [sashiko](https://fr.wikipedia.org/wiki/Sashiko), c'est une technique de réparation de tissu en brodant par dessus les trous et les parties à renforcer. Actuellement c'est surtout une technique de broderie de décoration, mais, bon, je suis souvent incapable de faire des choses qui n'ont pas une utilité en plus de la joliesse 😅

Je n'ai pas le matériel exact, mais j'ai retrouvé au fond de ma boîte à couture une longue et large aiguille, qui a parfaitement correspondu au besoin ; un fil de couture classique, que j'ai quadruplé pour le rendre plus large ; un tissu tout simple brun. Et je me suis lancée, sur un tote bag troué (à cause de frottements sur la roue avant du vélo. Oui. Hein. J'ai honte.).

Sur l'arrière du tote bag, un morceau de tissu, et le motif le plus classique : des lignes horizontales, avec des points alternés.

![Gros plan sur une réparation : un tissu brun brodé par petits points blancs sur plusieurs lignes horizontales ](IMG_20230812_141651--small.jpg)

Et sur le dessous, une variante : le tissu est à l'intérieur, et on ne le voit qu'à travers le trou. Le motif est un peu différent. Bon, à la base je voulais faire des escaliers. Et, quand le premier motif (les lignes horizontales) n'est pas assez régulier, ça devient très difficile, et… heu… bon, voilà. C'est des créneaux. 

![Gros plan sur le dessous du tote bag, on voit apparaître le tissu brun dans un petit trou triangulaire, entouré de points brodés blancs en créneaux](IMG_20230812_124345--small.jpg)

Ce n'est pas droit, pas régulier, et je pense que j'aurais dû tourner la première pièce de 90°, parce que les points sont dans le même sens que le fil. Mais 1) je pourrai toujours broder dans l'autre sens en plus : c'est d'ailleurs la force du sashiko, d'être remaniable, agrandissable, élargissable. Et 2) … en vrai, je suis franchement satisfaite ! C'est un premier essai, après tout. 

Je ne sais pas s'il y en aura d'autres, mais j'ai passé une matinée agréable à m'occuper de ça, et j'ai tellement d'autres vêtements à réparer, ça pourrait bien être l'occasion d'enfin m'y mettre avec plaisir !

Source : [DIY Sashiko mending with style - fun and stylish visible mending](https://www.youtube.com/watch?v=bj2eZcMWsNo) de The Green Wrapper, via [Krysalia](https://mastodon.social/@Krysalia/110853407496751747)