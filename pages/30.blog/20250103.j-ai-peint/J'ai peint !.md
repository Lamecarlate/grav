---
meta-création: 2025-01-03 19:03
meta-modification: 2025-01-03 19:03
stringified-date: "20250103.190336"
title: J'ai peint !
template: blog-item
date: 2025-01-03 19:03
update_date: ""
taxonomy:
  type: article
  tag:
    - création
    - peinture
    - 100DaysToOffload
highlight:
  enabled: false
  
---

Pfiou, ça faisait longtemps. Inktober 2021, je dirais.

===

Je passe beaucoup de temps à lire sur le net. Et régulièrement je retombe sur le site de [Maggie Appleton](https://maggieappleton.com). Elle est une designer qui raconte des tas de choses très intéressantes, et qui illustre ses essais, avec un style doux et fluide, que j'aime vraiment bien.

Elle m'a redonné envie de peindre. Je l'ai noté dans mon journal, donc c'est important (j'écris peu dedans ces temps-ci, donc… si ça y est, c'est que ça compte).

Je n'ai pas son style, et encore moins son habitude. Mais hé, on essaie de se remettre en selle, allez !

J'ai installé Krita[^1], parce que GIMP est très bien mais je n'ai jamais réussi à vraiment retrouver mes marques, le… toucher que j'avais avec Photoshop. Donc on teste d'autres trucs. (coucou [David Revoy](https://www.davidrevoy.com/), dont j'adore aussi énormément le style, et qui est un promoteur fort vocal de Krita 🥰)

![Peinture numérique représentant un paysage avec une rivière, des forêts et une montagne, dans un style un peu griffonné, presque abstrait.](kjudfhniksdnvikdnfv--p.webp)

À peu près 50 minutes de travail, en utilisant uniquement la brosse "Dry Bristles".

Ça a commencé comme une impro très simple, je laisse le pinceau aller çà et là. Progressivement c'est devenu une mer. Et quand j'ai posé le ciel, il est devenu clair que cette mer devait être en fait des plaines, collines et montagnes. Donc j'ai séparé ciel et terre[^2] en deux calques, bouibouité un peu les couleurs du calque de terre, ajouté les montagnes, creusé une rivière, arrangé ci, là et par là aussi. 

Ce n'est pas parfait[^3] mais je remets le pied à l'étrier. J'espère ne pas tomber et faire une belle balade[^4].


[^1]: Avec toutes ses dépendances vu que je suis sous Gnome et pas KDE 🥲
[^2]: Oui je peux faire ça, Dieu n'a pas le monopole, hé.
[^3]: Et si on disait que cette année j'arrêtais de m'auto-flageller ?
[^4]: Z'avez vu ma métaphore filée ? De cheval.
