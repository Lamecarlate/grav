---
meta-création: "2024-07-05 21:50"  
meta-modification: "2024-07-06 11:11:22"  

stringified-date: "20240705.215008"

title: "L'île de Myst en Lego"  
subtitle: "Il y a quelques mois, pour mon anniversaire, l'Amoureux m'a offert un ensemble Lego : l'île de Myst."
template: "blog-item"  

date: "2024-07-06 12:00"  
update_date: ""

taxonomy:
    type: article
    category:
        - jeu
    tag:
        - Lego
        - DIY
        - mignonneries

# plugin Highlight
highlight:
    enabled: false

---

Il y a quelques mois, pour mon anniversaire, l'Amoureux m'a offert un ensemble Lego : l'île de Myst.

===

> Je réalisai, à l'instant même où je tombais dans la crevasse, que le livre ne serait pas détruit comme je l'avais prévu. Il continua de tomber dans cette étendue étoilée dont je n'ai eu qu'une image fugace.

C'est un ensemble non-officiel, issu de la grande communauté de créateurices de Lego. L'Amoureux a trouvé le manuel et acheté toutes les pièces listées. Et à moi ~~la charge~~ le bonheur de monter tout ça !

![Assemblage de plusieurs plaques de Lego vertes, pour faire le socle.](IMG_20240509_161122--small.jpg "Il faut bien commencer quelque part.")

![Pièces bleu translucide pour faire la mer, et au centre l'île commence à se former.](IMG_20240509_213331--small.jpg "On commence à visualiser la forme de l'île.")

Pour les gens qui connaissent le jeu par cœur (je n'ai pas exactement levé la main, ça fait trop longtemps), on distingue bien la forme de l'île, avec le quai en bas et la petite crypte. Et sur la gauche, la plage devant la tour de l'horloge.

![Gros plan sur les roues dentées.](IMG_20240511_111148--small.jpg "Vue de la montée vers les engrenages")

![Gros plan sur la tour de l'horloge, qui tien dans un carré de 2 par 2.](IMG_20240510_193951--small.jpg "Regardez-moi cette horloge toute choupie")

Comment faire des arbres :

![Pièces détachées des arbres, cylindres bruns, frondaisons vertes.](IMG_20240510_202313--small.jpg)

![Arbres montés, un petit rond, un cylindre et trois frondaisons superposées.](IMG_20240510_202422--small.jpg)

![Gros plan sur les arbres au milieu de la forêt.](IMG_20240510_202529--small.jpg)

Une vue de dessus :

![Vue de dessus de l'île entière.](IMG_20240511_110836--small.jpg){.figure-media--wide}

J'aime ce rendu un peu… chibi, les bâtiments sont un peu plus gros qu'ils les sont dans le jeu, pour des raisons techniques (la taille des briques Lego), et je trouve que ça rend très bien. Et en vrai, quand on regarde une image du jeu, ce n'est pas si marquant, tiens.

![Image du jeu, vue aérienne de l'île.](Myst-Island.webp){.figure-media--wide}

![Vue aérienne de l'ensemble Lego, à peu près le même angle que l'image précédente.](IMG_20240511_110855--small.jpg){.figure-media--wide}

J'ai essayé de reproduire la toute première image du jeu, mais avec mon téléphone je n'ai pas réussi à me placer correctement, et quand j'y arrivais presque, c'était complètement flou parce que j'étais trop près et que le mode macro de mon logiciel de caméra n'était pas suffisant. Donc, une vue de côté, avec un petit personnage ajouté par mes soins à l'endroit où l'Étranger arrive au tout début. Imaginez juste un peu et vous verrez par ses yeux.

![Vue de côté, on a un bout de forêt, le quai avec le bateau coulé, un petit personnage, et dans le fond l'observatoire et la montagne.](IMG_20240511_111158--small.jpg)

Cet ensemble se trouve sur Rebrickable, c'est un modèle payant : [MYST Island - MOC 97547](https://rebrickable.com/mocs/MOC-97547/Desiera/myst-island). J'ai mis entre deux et trois journées et soirées tranquilles pour le réaliser. Le manuel est très clair, malgré quelques petits ratés : il arrive qu'une pièce doive être remplacée par une autre (j'ai eu le cas trois fois je crois, essentiellement des petits 1 par 1 lisse, un changement de couleur), et une fois, vers la bibliothèque, j'ai dû enlever puis remettre une petite partie pour arriver à placer la nouvelle pièce, qui dépassait physiquement d'un quart de millimètre.

<details markdown="1">
<summary>
Bonus rigolo, trouvé pendant que je cherchais le texte exact de l'introduction
</summary>
Les sous-titres automatisés de Youtube, parfois… ça marche pas trop.

![Extrait de l'introduction, on voit une crevasse dans le ciel, avec une silhouette qui tombe vers nous, les sous-titres disent « je réalisais à l'instant même où je tombais dans la crevette ».](les-sous-titres-automatisés-de-youtube.jpg)
</details>

<details markdown="1">
<summary>
Bonus rigolo bis, parce que je ne peux pas avoir un chat qui s'appelle Atrus et ne pas faire la blague
</summary>

![La même image que plus haut, avec la silhouette de mon chat Atrus remplaçant celle du personnage Atrus dans la crevasse.](atrus-dans-la-crevette.jpg)

Image de base :

![Le chat Atrus, un tabby extrêmement mignon, qui se roule sur la commode où il dort souvent, montrant son ventrou tout doux et pliant les pattes de façon adorable.](IMG_20240706_101349--small.jpg)
</details>
