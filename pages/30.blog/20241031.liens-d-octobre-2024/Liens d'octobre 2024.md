---
meta-création: 2024-10-07 08:58
meta-modification: 2024-10-31 09:35
stringified-date: "20241007.085848"
title: Liens d'octobre 2024
template: blog-item
date: 2024-10-31 10:58
update_date: ""
taxonomy:
  type: article
  category:
    - liens
  tag:
    - mignonneries
    - musique
    - sciences
    - géographie
    - web
highlight:
  enabled: false
---

Jolies images, fièvre, sagesse, « clac clac », « pouin pouin », découpage, CGI, des ânes hiiiii, une chanson dure, plantes comestibles contre leur gré, Ꝃ, illusions, « weeeeeee », je suis vieille et je fais des listes de mots-clés comme du SEO moche au secours.

===

Emily L'Orange est une artiste, et j'adore ce qu'elle fait : [quelques exemples de son art](https://mastodon.art/@emilylorange/113272753179266777) (images avec alt en anglais). J'adore particulièrement sa série des épées.

[Une mignonne histoire de SAV et de langue](https://notalwaysright.com/clients-from-helsinki/333861) (en anglais).

[La fièvre, c'est désagréable, mais c'est nécessaire pour combattre les infections](https://www.youtube.com/watch?v=cRZOUcpiOxY) (vidéo en anglais, avec du son, sous-titres anglais, 11 minutes). L'équipe Kurzgesagt, toujours sur le pont, fait une vidéo sur la fièvre et ses avantages. C'est bien raconté et c'est scientifiquement correct. Attention, ce n'est pas un pamphlet pour dire qu'il faut se laisser souffrir, hein, vraiment pas. En revanche, cela tacle un peu le réflexe « prends un doliprane dès que ça chauffe » que les humains semblent avoir pris depuis quelques décennies.

[La sagesse selon YouTube : plongée dans un océan de contenu généré par IA](https://www.youtube.com/watch?v=rJE2qkP0Gk4) (vidéo avec du son, avec sous-titres, 45 minutes). Monsieur Phi qui fait (volontairement) du Feldup, et ça fait tout aussi peur. (par contre, avec ses lunettes, Monsieur Phi ressemble à Bernard Werber, au secours)

---

Tiens tiens, à croire que quand on a des hardtacks en main on ne peut s'empêcher de faire ça :

- [Food That Time Forgot: Ships Biscuits](https://www.youtube.com/watch?v=W9tdBrpp1V0) (de la chaîne Townsend, vidéo avec du son, en anglais avec sous-titres en anglais, 12 minutes)
- [How to Eat Like a Pirate: Hardtack & Grog](https://youtu.be/oPTdSMOQRnY?t=1034) (de la chaîne Tasting History, vidéo avec du son, en anglais, avec sous-titres en anglais, 18 minutes mais je vous emmène à un point précis, et ce petit bout de vidéo est devenu un running gag sur la chaîne)

Dans les commentaires de la première vidéo, des gens disent que c'est aussi un geste fait par les marins et les soldats, si le hardtack fait un bruit un peu plus mou, il est peut-être gâté. Donc cela pourrait être un geste de sécurité, qui se transmet et perd une partie de son sens.

---

[Un chat ? Un seul ?](https://mstdn.social/@yurnidiot/112685032845089426) (vidéo sans son).

[Le mécanophone](https://www.youtube.com/watch?v=ML5pNfUfT0g) (vidéo avec du son, pas de paroles, 2 minutes), c'est un monstre ou un génie, je ne sais pas, mais j'adore. Un peu [plus d'info sur ce bestiau](https://coaa.us/index_archive/Issues_21_to_30/Le%20%E2%80%9CMecanophone%E2%80%9D_Ron%20Bopp_%20_27.pdf) (fichier pdf). Via [Geneviève, godasses-killeuse](https://mastodon.top/@vieveca@toot.aquilenet.fr/113362025087819766).

Voilà une citrouille qui fait bien peur pour Halloween, [elle veut nous aider](https://tech.lgbt/@Natasha_Jay/113302304739206918) (image en anglais avec texte alternatif en anglais).

[Qui l'eut cru ?](https://www.youtube.com/watch?v=ivttEOPKhqg) (vidéo avec du son, des sous-titres incrustés, 1 minute). Un homme transformé en carotte arrive dans un hôpital, et la réceptionniste n'est pas trèèès réceptive.

![Peinture d'un oiseau, le grimpereau des jardins, fait de quelques traces de gouache et de trait au feutre](grimpereau-des-jardins.webp)

Source : [ce fil rempli d'oiseaux](https://mamot.fr/@bbecquet/113364204629038218), par Benjamin.

[L'origine du gerry-mandering](https://www.youtube.com/watch?v=cwBslntC3xg) (vidéo avec du son, en anglais, sous-titres en anglais, 12 minutes). De la géopolitique étatsunienne avec humour anglais.

["No-CGI" is really just "invisible CGI"](https://www.youtube.com/playlist?list=PLgdTaHO8FLEve_XFiRBEcOSkRdd-Txjne) (liste de 4 vidéos, avec du son, en anglais, sous-titres en anglais, 30 minutes chacune environ). Très intéressant documentaire sur le revirement, depuis quelques années, d'Hollywood sur les effets spéciaux, parce que, vous comprenez, rien ne vaut le vrai, hein ! Et c'est… un peu faux. Il y a peut-être moins de choses entièrement en effets spéciaux numériques, mais c'est surtout qu'on les repère moins. Qu'ils sont, comme le dit le titre, invisibles. Via [HTeuMeuLeu](https://mastodon.social/@HTeuMeuLeu/111886648868496801).

[Ilona Maher's Disney Night Jazz](https://www.youtube.com/watch?v=nhGSqT74Gc8) (vidéo avec du son, paroles de chanson en anglais, sans sous-titres, 1 minute 30). C'est une épreuve de <i lang="en">Dance with the stars</i>, et Ilona interprète Luisa du film Encanto, dans sa chanson-phare. Ilona est forte, musclée et forte, et wow, c'est rafraîchissant à voir.

En regardant [Matrix Generation](https://www.youtube.com/watch?v=bNWY5Bd_KOA) (vidéo avec du son, sans sous-titres hélas, disponible jusque fin novembre 2024, la vidéo divulgâche les 4 films), je découvre que le chorégraphe Woo-ping avait assigné aux personnages principaux une prise spécifique, pour vraiment distinguer leur méthode de combat. Trinity, c'est le coup du scorpion, où elle frappe avec sa jambe par-dessus sa tête ; Neo, le triple coup de pied pendant un saut ; Morpheus, l'aigle, où il fend les airs ; et l'agent Smith est dans la force brutale, les rafales de coups de poing (qui se voient d'ailleurs démultipliés à l'écran). J'avoue n'avoir pas remarqué que c'était des <i lang="en">"signature moves"</i>… Pour moi, le style de combat de Trinity est effectivement dans le mouvement, les sauts (et la combi en latex luisant, qui souligne son corps et ses mouvements), mais je n'avais jamais vu, ou retenu, le coup du scorpion.

---

[Les Wriggles - Je vis toute seule avec un chien](https://www.youtube.com/watch?v=9wHhJzwAb9A) (vidéo avec du son, c'est une chanson, il n'y a hélas pas de sous-titres). Attention, le texte est dur à écouter. Via [TarValanion](https://eldritch.cafe/@TarValanion/113171040753044005).

<details markdown="1">
<summary>Mon avis sur la chanson, et divulgâchage</summary>
Oumf, que cette chanson est dure… J'avais compris le sujet dès la mention des voisins qui baissent la tête. Pourquoi les voisins détourneraient le regard pour un chien qui ab- oh. Ohhh. Ah. Oui. Ce genre de chien. 

Le sujet des violences conjugales est abordé d'une manière si douce, si fluide, que ça en fait froid dans le dos. Le dézoom progressif qui montre la situation est un excellent artifice.
</details>

---

[Mécanismes de défense des plantes](https://solarpunk.moe/@stellarskylark/113183215005274354) (en anglais, une traduction maison suit).

<blockquote class="conversation is-conversation" markdown="1">
C'est vraiment hilarant de voir le nombre de plantes que nous cultivons spécifiquement pour leurs mécanismes de défense.

<span class="conversation--person">Les alliacées</span> : on a de vilains composés soufrés qui se transforment en véritable gaz lacrymogène quand nos parois cellulaires sont brisées ! Aucun animal n'aura accès à *nos* réserves de sucre !

<span class="conversation--person">Les humains</span> : miam, succulents composés soufrés, je vais vous manger avec *tout*.

<span class="conversation--person">Le thé et le café</span> : haha, les insectes *meurent* quand ils tentent de nous manger parce que nous fabriquons notre propre pesticide !

<span class="conversation--person">Les humains</span> : argh, ne me parlez pas tant que je n'ai pas bu mon pesticide.

<span class="conversation--person">Tout un tas d'herbes et d'épices</span> : bien essayé, les bestioles ! Mais on est remplis d'huiles volatiles qui sont dangereuses et rebutantes ! 

<span class="conversation--person">Les humains</span> : ooooh celle-là est bonne avec mes pesticides, celle-là a un goût délicieux avec les composés soufrés, *celle-là* fait des desserts incroyables, oh mon dieu oui.

<span class="conversation--person">Les piments</span> : foutus mammifères qui broient nos graines avec leurs saletés de molaires ! Plus jamais ! Seuls les oiseaux peuvent supporter notre chaleur !

<span class="conversation--person">Les humains</span> : Je vais te faire sécher, te broyer en une poudre, et t'utiliser comme assaisonnement de base sur des continents entiers.
</blockquote>

---

Je découvre l'existence de [la lettre Ꝃ](http://www.tresor-breton.bzh/2024/04/07/k-barre/) (« K barré »), qui se prononce « Ker » et qui se retrouve dans plein de patronymes bretons, parce que cela signifie « village ». Sauf que lors de l'interdiction de parler breton en France (fin XIX<sup>e</sup> siècle et au cours du XX<sup>e</sup>) cette lettre est interdite de même, et on la voit parfois sous la forme K' ou K/ (l'imprimerie n'a pas aidé non plus). Il y a également [une élue française dont le nom est K/Bidi](https://fr.wikipedia.org/wiki/Emeline_K/Bidi). Là, c'est l'informatique qui a dû lui causer des soucis, à cette pauvre dame, un nom avec un slash dedans, vous n'y pensez pas, c'est invalide… (rappel : [aucun nom n'est invalide](https://mas.to/@yournameisinvalid))

[Une couverture lestée de qualité](https://mstdn.social/@ElleGray/112740151457775691) (image avec alt en anglais).

[L'invasion de la quatrième dimension](https://aus.social/@Unixbigot/111564128615007440) (micro-histoire en anglais, ya un chat).

[Un simulateur de pierre, papier, ciseau](https://mstdn.social/@yurnidiot/113099027323275145) (vidéo avec du son, sans paroles). Ce sont des emoji 🪨, 📜, et ✂️, qui se battent jusqu'à la suprématie d'un des types d'emoji. C'est génialement idiot, j'adore. Le message où je vous emmène est une vidéo qui présente une bataille, mais [le simulateur est sur cette page](http://peterganunis.com/game3.html) si vous voulez jouer vous-même. Enfin, jouer… lancer le truc et regarder avec une intensité bien trop grande pour ce que c'est, et relancer, encore et encore, et comment ça il est 3 heures du matin, écoutez, j'ai des choses importantes à faire, le papier a gagné trois fois de suite, je dois savoir s'il va gagner une quatrième fois.

[Helga Stentzel crée des illusions avec des vêtements](https://www.thisiscolossal.com/2024/10/helga-stentzel-clothes-illusions/) (article en anglais, avec des images et textes alternatifs en anglais). J'aime beaucoup, c'est très simple et joli. Très fan de la dernière image, le chat dont la truffe est faite d'une adorable culotte rose foncé.

---

[Un chat qui tente de chopper une feuille au bout de sa queue](https://mstdn.social/@yurnidiot/113381178877573870) (vidéo sans son, à peu près 15 secondes, il y a un alt en anglais parce que c'est un gif animé). Regardez bien jusqu'à la fin. 

<details>
<summary>Divulgâchage (à peu près)</summary>
Weeeeeeeeeeeeeee-oups.
</details>

---

[Un chat héliomancien](https://mstdn.social/@ElleGray/113386375802627922) (image avec texte alternatif en anglais).

---

Sur le Fédiverse, le compte [@MDN@mozilla.social](https://mozilla.social/@MDN) demandait quels avaient été nos premiers contacts avec le webdev. Et ça m'a rappelé un souvenir enfoui. À l'époque, je jouais à Hordes, un jeu multi-joueurs en ligne essentiellement textuel, beaucoup basé sur les messages des personnes, et aussi du Corbeau, un messager (de mauvais augure), géré par le jeu. Et sur les forums commençaient à fleurir des captures d'écran de messages du Corbeau qui n'étaient pas vraies. C'était pour de rire, bien sûr. Et c'était tellement bien fait, ça ne pouvait pas être un montage ! Ça ne l'était pas. Les gens utilisaient l'extension Firebug pour Firefox, afin de modifier les textes dans le navigateur et faire des illusions plus vraies que vraies. Ça m'a complètement chamboulé. On pouvait faire ça, on pouvait inspecter le code d'une page et le modifier pour voir ce que cela donnait ! (oui je suis vieille comme ça, mine de rien ça fait 15 ans que je travaille dans le dev, ohlala)
