---
meta-création: 2024-12-15 23:55
meta-modification: 2024-12-15 23:55
stringified-date: "20241215.235503"
title: Asphodèle met les pieds dans le plat
template: blog-item
date: 2024-12-16 18:00
update_date: ""
taxonomy:
  type: article
  tag:
    - jeux-de-rôle
    - personnel
highlight:
  enabled: false
description: Ou comment écouter dans les sessions de jeu de rôle, ça aide
---
Hier soir, lors de ma session de jeu de rôle Donjons & Dragons hebdomadaire, j'ai eu un moment d'égarement.

===

Le contexte : dans le groupe (dans le jeu), nous avons de nouvelles personnes, dont Edera, un faune barbare charmeur et bourru. Une nuit, nous enquêtons, car les habitant⋅es semblent avoir des problèmes de sommeil et de rêves. Une des raisons de notre enquête c'est que nous nous sentons coupables car le souci pourrait venir de nous, mais c'est une autre histoire.

Nous avons entendu des bruits étranges, comme si quelqu'un creusait la terre. Après quelques péripéties, déplacements silencieux, détection d'un nain bizarre armé d'une pelle, envoi de chat puis de chien vers le nain bizarre, puis discussion, nous apprenons qu'il est jardinier, et qu'il creuse la nuit pour mieux réfléchir.

Edera ne discute pas. Edera questionne.

– Nom, prénom, occupation ?

Et moi, incarnant Asphodèle, halfeline druide, un peu choquée, je ne comprends pas pourquoi il agresse verbalement ce type. Type certes un peu suspect, mais ça va, apparemment il n'y a rien de particulier, il creuse la nuit, quoi, si ça lui fait du bien. Donc :

– Mais t'es garde[^1] ou quoi ?  
– Biiiiin. Oui ?  
– Ah. Ah oui c'est vrai.

(Et oui, c'était vrai, il fait partie du groupe qui avait trouvé notre petite troupe évanouie après notre arrivée, mais j'avais oublié, et on en avait peu reparlé.)

Oups.

[^1]: Je voulais dire « Mais t'es policier ou quoi ? », et il fallait rester dans le bon univers, du médiéval fantastique.
