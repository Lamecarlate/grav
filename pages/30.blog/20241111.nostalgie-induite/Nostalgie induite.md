---
meta-création: 2024-11-11 17:53
meta-modification: 2024-11-11 17:53
stringified-date: "20241111.175318"
title: Nostalgie induite
template: blog-item
date: 2024-11-11 17:53
update_date: ""
taxonomy:
  type: article
  tag:
    - musique
    - personnel
    - cinéma
    - writing month
    - writing month 2024
highlight:
  enabled: false
subtitle: Dans mes oreilles à l'instant, dans le grand aléatoire de mon lecteur, une piste de musique instrumentale inconnue mais familière.
---
Dans mes oreilles à l'instant, dans le grand aléatoire de mon lecteur, une piste de musique instrumentale inconnue mais familière.

===

C'est un morceau de l'OST de l'animé <i lang="jp">Mushishi</i>. La seconde saison, que je n'ai jamais vue (il y a tant de choses à voir…), contrairement à la première, que je connais presque par cœur. Les musiques aussi, je les reconnais. Et celle-ci… sonnait exactement comme une mélodie que j'aurais entendue tant de fois, mais sans pouvoir mettre un nom dessus. C'est parce que j'identifiais le style. C'est une bande originale où le compositeur a réussi à insuffler quelque chose de marquant, de vraiment marquant. Et puis à reprendre ce style, huit ans plus tard, car, oui, il y a eu huit ans entre la saison 1 et la 2.

Ce style, c'est de la lenteur, de la douceur, des lumignons posés sur une fenêtre ; de la verdure, plein, plein de verdure, forêt humide ou orée lumineuse, et une sorte de nostalgie, aussi. La nostalgie d'un endroit où je ne suis jamais allée (ça porte un nom, d'autres la ressentent aussi, mais ça m'échappe pour l'instant). J'avais déjà écrit sur le sujet de [cette nostalgie](https://archive.lamecarlate.net/coquecigrue/mon-chez-moi-imaginaire/index.html).

Et entendre cette piste, <i lang="jp">Nukumori</i>, qui semble pouvoir se traduire par « chaleur », m'a replongée dans cette sensation. Cette douceur légèrement douloureuse, qui pique tout en enveloppant, qui relaxe tout en murmurant « il te manque quelque chose ».

![21. Nukumori](21.%20Nukumori.mp3)
