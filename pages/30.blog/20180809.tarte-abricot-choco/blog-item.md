---
title: Tarte abricot-choco

date: '2018-09-22 10:43'
published: true
taxonomy:
    type: article
    category:
        - cuisine
    tag:
        - sucré
        - tarte
        - abricot
        - chocolat
header_image_file: 20180805-tarte-abricot-choco.jpg
header_image_caption: 'Tarte abricot-choco'
author: 'Lara'

extra_content:
    items: "@self.modules"
---

Nous avons acheté environ une demi-tonne d'abricots mi-juillet. On avait des tas de projets avec.

===

"Ouéééé on va faire des gâteaux, et des tartes, et des cakes, et des glaces aussi". Mais en fait, les 9 kg tenaient très bien dans le bac à légumes du frigo, on en mangeait 3 à 4 chaque fois qu'on passait dans la cuisine, je me suis nourrie un soir de chips et d'abricots (anecdote authentique), le tas descendait tranquillement, on se disait qu'on aurait le temps en fait pour les gâteaux et les tartes et tout. Et puis… au bout de deux semaines, il restait environ 1,5 kg et quelques fruits donnaient des signes de faiblesse.

Branle-bas de combat ! Il faut faire des trucs avec ces abricots sinon les pourris vont contaminer tous les autres et ils vont se lancer en politiqu- hum pardon.

Le plus simple et le plus rapide : une tarte. (dans la même journée on a fait un sorbet mais ça a été un échec phénoménal, la sorbetière toute neuve a sorbeté mais sans geler donc bon bof, j'en reparlerai éventuellement)

On cherche des idées. Qu'est-ce qui va bien avec des abricots ? (des chips, oui, mais dans la tarte c'est moyen) L'Amoureux propose : du chocolat ! Au début j'étais un peu sceptique. J'essaie d'être créative, je peux mélanger des tas de trucs, mais j'expérimente plus souvent dans le salé, pour le sucré je suis peut-être un peu conservatrice (horreur), et pour moi le chocolat ça va avec la poire. Mais j'avais tort ! Le chocolat ça va avec tout ! (avec les chips aussi #runningGag, mais j'ai pas testé)

Cette tarte est extrêmement simple : une pâte feuilletée, un appareil compote de pommes et beurre de cacahuètes, des morceaux de chocolat, et des abricots.

![Fond de tarte garni de l'appareil compote et beurre de cacahuètes, de chocolat, et de quelques lunes d'abricot](20180805-tarte-abricot-choco-01.jpg){.media--wide}

Le beurre de cacahuètes est peut-être ici un peu incongru, je voulais augmenter un peu la compote, et j'avais le choix entre purée d'amandes et beurre de cacahuètes, j'ai soumis au vote et à l'unanimité du vote de l'Amoureux, la cacahuète l'a emporté.

*[insérer ici image de la cacahuète triomphant de l'amande, bon sang est-ce que je vais trouver ça dans une banque d'images libres]*

Le chocolat est coupé en gros morceaux à la tranquille, une demi-tablette parce que, parce que bon (et ça fait seulement 50 g !).

Les abricots sont tranchés et disposés - d'abord n'importe quand parce que, parce que bon (je me répète, je crois) et puis finalement joliment, histoire d'être bien sûre de couvrir toute la surface de la tarte. Je sais, d'habitude c'est l'inverse. On replie les bords de la pâte pour ne rien perdre. On peut aussi couper et faire d'autres choses avec les chutes, mais moi j'aime bien replier, ça a un ptit côté cosy.

![La tarte est entièrement recouverte par les abricots](20180805-tarte-abricot-choco-02.jpg){.media--wide}

Et après cuisson… (après prélèvement du premier quart, aussi, chuis pas assez rapide pour prendre les photos)

![La tarte est cuite, et un quart a été coupé dedans](20180805-tarte-abricot-choco-03.jpg){.media--wide}

Le mieux - si on arrive à résister - c'est d'attendre que la tarte ait refroidi, car le chocolat se sera resolidifié, et ça croque doucement sous la dent.

Verdict : c'est très bon. Plein de jus, avec le chocolat qui croque, la compote moelleuse avec le côté un peu lourd, un peu terrien (en anglais on dirait <i lang="en">earthy</i>) du beurre de cacahuètes, la pâte un peu détrempée…
