---
title: Tarte abricot-choco
image_file: 20180805-tarte-abricot-choco.jpg
ingredients:
    - 600 g d'abricots (masse comptée avec noyaux, environ 15 fruits)
    - 750 g de compote de pommes
    - 50 g de beurre de cacahuètes (environ 1 grosse cuillerée à soupe)
    - 50 g de chocolat noir (une demi-tablette)
    - 1 pâte feuilletée (vous pouvez la faire si vous voulez)
time:
    prep: 30 minutes
    cook: 20 minutes

steps:
    - Préchauffer le four à 160°C (le mien est à chaleur tournante, adapter selon les caractéristiques de son four)
    - Couper les abricots en deux, les dénoyauter puis recouper chaque moitié en quatre dans le sens de la longueur, pour obtenir des lunes.
    - Déposer la pâte feuilletée dans un moule à tarte.
    - Mélanger la compote et le beurre de cacahuètes dans un grand bol.
    - Verser l'appareil sur la pâte.
    - Couper le chocolat en petits morceaux. Répartir les morceaux sur l'appareil.
    - Disposer les abricots sur le tout, replier la pâte par-dessus.
    - Enfourner pour environ 20 minutes - la durée exacte dépend du four.
    - Laisser refroidir, la tarte est meilleure quand le chocolat est redevenu solide.

---

Le moule à tarte utilisé ici fait 26 cm de diamètre au fond, 28 au sommet.

Pour les morceaux de chocolat, le chocolat est coupé en carreaux encore dans son emballage (comme ça on ne s'en met pas plein les doigts), puis chaque carreau en deux, ça amène à 24 morceaux, tout à fait suffisant pour répartir.

Cette tarte peut être végétalienne si la pâte feuilletée l'est - c'était mon cas.
