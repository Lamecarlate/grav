---
meta-création: "2023-02-07 12:32"
meta-modification: "2023-02-07 12:32"

stringified-date: "20230207.20230207123200"

date: "2023-02-07 12:32"
template: "short-note"
taxonomy:
	type: short-note

title: "Jour de grève efficace"

---

Jour de grève. Aujourd'hui je ne vais pas manifester, mais je participe aux communs quand même ! Deux heures de balade tranquillou dans mon quartier, seule avec mon téléphone en mode GPS, et <span lang="en-GB">StreetComplete</span>. J'ai rempli 120 quêtes, ça va, bon rythme.

En quelques mots, <span lang="en-GB">[StreetComplete](https://streetcomplete.app/)</span> est un logiciel (pour <span lang="en-GB">Android</span>, mais il existe peut-être pour d'autres <abbr title="Operating Systems" lang="en-gb">OS</abbr>) qui permet d'améliorer OpenStreetMap, en enrichissant sa base de données sur le revêtement des chemins, l'éclairage, les poubelles publiques, les passages piétons ("Y a-t-il un îlot ?" "Y a-t-il un revêtement podotactile ?", etc). C'est plutôt simple, ludique, et – mais est-ce vraiment un avantage <span role="img" aria-label="Visage souriant avec une larme">🥲</span> ? – il y a beaucoup de quêtes (de tâches à effectuer).

À noter qu'il faut un compte [OpenStreetMap](https://www.openstreetmap.org/) pour jouer – c'est logique mais mieux vaut le préciser.