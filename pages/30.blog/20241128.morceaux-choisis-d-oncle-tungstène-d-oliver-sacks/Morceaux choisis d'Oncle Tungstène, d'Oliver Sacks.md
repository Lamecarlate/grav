---
meta-création: "2024-11-28 14:16"  
meta-modification: "2024-11-28 14:16"  

stringified-date: "20241128.141608"

title: "Morceaux choisis d'Oncle Tungstène, d'Oliver Sacks"  
template: "blog-item"  

date: "2024-11-28 14:16"  
update_date: ""

taxonomy:
    type: article
    tag:
       - lecture
       - physique
       - chimie
       - writing month
       - writing month 2024

# plugin Highlight
highlight:
    enabled: false
    
subtitle: J'ai terminé _Oncle Tungstène_, d'Oliver Sacks il y a quelques jours, et j'ai pris des notes.

---

J'ai terminé _Oncle Tungstène_, d'Oliver Sacks il y a quelques jours, et j'ai pris des notes.

===

C'était un livre passionnant (mais qui m'a quand même pris des mois à finir, je lis lentement quand c'est de la non-fiction…), une sorte d'autobiographie de Sacks qui mêle sa vie d'enfant de famille de scientifiques, avec ses propres découvertes, aux découvertes importantes de l'histoire de la chimie et de la physique. Les chapitres ne sont pas chronologiques, mais thématiques. Ce qui occasionne parfois des questionnements, comme « Mais du coup il a quel âge, là ? 🤨 », et la réponse est souvent « Douze ans, et il manipule du cyanure oui oui tout va bien. ».

Oliver Sacks, Oncle Tungstène, Paris, Seuil, 2003, trad. de l'anglais par Christian Cler, 412 p.

Quelques extraits de ce très chouette livre avec mes notes le cas échéant.

## Page 55

> Parce que les lourdes veines des minerais de tungstène sont associées à du minerai d’étain, l’isolation de l’étain était compliquée par la présence de ce tungstène : c’était pourquoi, précisa mon oncle, ce métal avait été initialement baptisé « wolfram » — il était censé « voler » l’étain tel un animal affamé.

C'est pourquoi le symbole chimique du tungstène est W, pour wolfram.

Wikipédia dit de la wolframite (dont a été extrait le tungstène) :

> Le minéral est identifié en 1556 par Agricola, qui lui donne le nom latin de lupi spuma (« mousse de loup »), en raison de la forte consommation d'étain lors de son extraction. En 1747, Wallerius le désigne par l'équivalent en allemand, wolf rahm (« crème de loup »), transcrit volfram en suédois. 

J'étais curieuse du lien entre le fait de consommer de l'étain et le loup, et je trouve :

<blockquote lang="en">It was so called due to the apparent tin "eating" during the extraction, like the wolf eats the sheep.</blockquote>

sur [Wolframium](https://elements.vanderkrogt.net/element.php?sym=W). Voilà l'explication ! (C'est un peu tiré par les poils quand même)

Traduction maison :

> Il a été appelé ainsi à cause de son apparente consommation d'étain pendant l'extrait, comme le loup mange le mouton.

## Page 65

> on s’aperçut que l’oxyde de calcium communément appelé « chaux » émet une éclatante lumière verdâtre quand il est chauffé. Cette « lumière oxhydrique » (<i lang="en">limelight</i>) découverte dans les années 1820 avait éclairé les scènes des théâtres pendant des décennies, m’apprit Oncle Dave — voilà pourquoi on continuait à parler des « feux de la rampe » (<i lang="en">limelight</i>) bien que la chaux incandescente ne soit plus utilisée.

Le terme "limelight" vient donc de l'usage de la chaux dans l'éclairage, notamment de théâtre.

## Page 81

> De nombreux éléments avaient reçu des noms d’origine folklorique ou mythologique qui permettaient parfois de reconstituer une part de leur histoire. Un kobold était un gobelin ou un lutin maléfique, tandis qu’un nickel était un démon : les mineurs saxons employaient ces deux mots chaque fois que des minerais de cobalt et de nickel trahissaient leurs espérances en s’avérant moins riches que prévu.

C'est là l'origine du nom de cobalt, qui vient directement de kobold. (Et après ça va nous abîmer les mines de fer de Nashkel, pas sympa / 20.)

Ma blague me fait réaliser que ce fait dans le jeu Baldur's Gate est peut-être bien volontaire de la part des scénaristes : les kobolds étaient déjà tenus responsables des mauvaises mines. Quelqu'un sur reddit a [la même théorie](https://www.reddit.com/r/baldursgate/comments/zk3qaw/kobolds_aka_cobalt/).

## Page 146

> Lavoisier fit en sorte que des termes tels que beurre d’antimoine, bézoard jovial, vitriol bleu, sucre de Saturne, liqueur fumante de Libavius ou fleurs de zinc cèdent la place à des vocables précis, analytiques et immédiatement compréhensibles.

J'ai pu retrouver les noms récents et les formules de la plupart de ces composés si poétiques.

- beurre d’antimoine : trichlorure d'antimoine (SbCl<sub>3</sub>)
- vitriol bleu : sulfate de cuivre(II) (CuSO<sub>4</sub>)
- sucre de Saturne : acétate de plomb(II) (C<sub>4</sub>H<sub>6</sub>O<sub>4</sub>Pb) (appelé ainsi parce qu'il a un goût sucré, et Saturne est associé au plomb)
- liqueur fumante de Libavius : tétrachlorure d'étain (SnCl<sub>4</sub>)
- fleurs de zinc : oxyde de zinc (ZnO)

Pour le bézoard jovial je n'ai pas trouvé plus précis, ce n'est peut-être pas un composé mais une sorte d'alliage ?
	<blockquote lang="en">Bezoardicum joviale, or bezoar of Jupiter, is a regulus made by melting three ounces of regulus of antimony and two of block tin. This is then powdered and mixed with six ounces of corrosive sublimate and distilled off in a kind of butter. It is then dissolved in spirit of nitre (nitric acid) and distilled three times. The bezoar remaining at the bottom is powdered, washed, and mixed with spirit of wine, until it grows insipid.</blockquote>

## Page 158

> Un morceau de sodium décrivit des cercles sur l’eau tout en bourdonnant, ce métal ne s’enflammant pas si le morceau est trop petit ; le potassium, en revanche, s’embrasa dès qu’il toucha l’eau, brûla en produisant une flamme mauve pâle et projeta des globules ardents tout autour de lui ; encore plus réactif, le rubidium crépita furieusement au milieu d’une flamme rouge groseille ; et le césium explosa au contact de l’eau — la réaction fut si violente que le récipient de verre où je l’avais transporté vola en éclats.

Le potassium aussi réagit au contact de l'eau, comme le sodium, il est même bien plus violent. Je me souviens de ce prof de physique-chimie au collège qui nous avait raconté qu'il avait jeté un petit morceau de sodium dans les toilettes et le geyser que ça avait provoqué. Il n'a pas mentionné le potassium ou les autres, je découvre donc dans ce texte que le sodium est petit joueur.

## Page 163

> Incarnant parfaitement l’optimisme ambiant, Davy s’était laissé porter par l’immense vague de cette puissance scientifico-technologique si prometteuse (en même temps que menaçante) : il avait découvert une demi-douzaine d’éléments, permis l’avènement de nouvelles formes d’éclairage, été à l’origine d’innovations majeures dans le domaine de l’agriculture et conçu une théorie électrique de la combinaison chimique, de la matière et de l’Univers — tout cela avant l’âge de trente ans.

Tout cela avant l'âge de trente ans…

## Page 166

> Essayées dès 1816, ces lampes perfectionnées non seulement se révélèrent parfaitement sûres, mais constituèrent même des grisoumètres très efficaces grâce aux changements de couleur de leurs flammes.

J'aime le terme de « grisoumètre ».

## Page 167

> Auparavant plus coûteux que l’or (les invités de Napoléon III mangeaient dans des couverts en or, l’empereur dînant dans de la vaisselle en aluminium), l’aluminium n’était devenu bon marché et abondant qu’à partir du moment où l’électrolyse mise au point par Davy avait permis de l’extraire de son minerai.

L'aluminium plus coûteux que l'or ??

Wikipédia dit :

> Cette méthode est utilisée de façon industrielle à travers toute l’Europe pour la fabrication de l’aluminium (notamment en 1859 par Henry Merle dans son usine de Salindres, berceau de la société Pechiney), mais elle reste extrêmement coûteuse, donnant un métal dont le prix était comparativement à l'or, très élevé pour un minerai, lui, très abondant : le kilo d'aluminium raffiné coûtait en moyenne 1 500 francs-or. Le métal est alors réservé pour fabriquer à la haute joaillerie ou à l’orfèvrerie, ciblant la haute-bourgeoisie. Il en fut ainsi pour les coupes d'honneur (réalisées notamment par Paul Morin et Cie) et les objets d'art fabriqués pour la cour impériale de Napoléon III. Ce dernier reçoit ses hôtes de marque avec des couverts en aluminium, les autres convives devant se contenter de couverts en vermeil. 

Sources :

- (en) Fred Aftalion, A history of the international Chemical industry, Philadelphie, University of Pennsylvania Press, 1991, p. 64.
- (en) A. J. Downs, Chemistry of Aluminium, Gallium, Indium and Thallium, Springer Science & Business Media, 31 mai 1993, 526 p. (ISBN [978-0-7514-0103-5](https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Ouvrages_de_r%C3%A9f%C3%A9rence/978-0-7514-0103-5 "Spécial:Ouvrages de référence/978-0-7514-0103-5"), [lire en ligne](https://books.google.fr/books?id=v-04Kn758yIC&printsec=frontcover&q=chemistry%2520of%2520aluminium) [archive](https://archive.wikiwix.com/cache/?url=https%3A%2F%2Fbooks.google.fr%2Fbooks%3Fid%3Dv-04Kn758yIC%26printsec%3Dfrontcover%26q%3Dchemistry%252520of%252520aluminium "archive sur Wikiwix")), p. 15.

En résumé : le résultat est comparable à l'or parce que le processus est complexe et coûteux.

## Page 201

> Davy avait été à l’avant-garde de la recherche dans le domaine des déplacements électrochimiques : pour protéger les carènes de cuivre des bateaux contre les effets corrosifs de l’eau de mer, il avait proposé de plaquer des métaux plus électropositifs tels que du fer ou du zinc sur leurs coques, afin que ces plaques soient corrodées à la place du cuivre. (Bien que cette « protection cathodique » ait paru efficace en laboratoire, elle donna de moins bons résultats en mer : ces placages attirant les bernacles, la suggestion de Davy fut tournée en ridicule. Mais le principe de la protection cathodique était brillant, et, après la mort de Davy, les carènes des navires de haute mer furent protégées de cette façon.)

Je connaissais le terme d'anode sacrificielle pour ces plaques de zinc.

D'après Wikipédia : 

> Une anode sacrificielle est le composant principal d'un système de protection cathodique galvanique utilisé pour protéger les structures métalliques enterrées ou immergées de la corrosion. 

Je suis contente de me souvenir de ce genre de termes, appris au collège, et c'est souvent arrivé lors de la lecture de ce livre.

## Page 245

> Notes infrapaginales

Pour « notes de base de page ». J'aime. C'est un poil pédant mais c'est beau, et plus concis. J'envie tout de même aux anglophones leurs <i lang="en">footnotes</i>.

## Page 250

> L’exactitude de la prédiction de Mendeleïev s’avéra stupéfiante : il avait non seulement prévu un poids atomique de 68 (Lecoq annonça 69,9) et une densité spécifique de 5,9 (5,94 selon Lecoq), mais décrit également en détail plusieurs autres propriétés physico-chimiques du gallium — sa fusibilité, ses oxydes, ses sels, sa valence.

Cela me fait penser à la prédiction de la présence de Neptune par Leverrier et Adams en 1846 et 1843. 

Wikipédia :

> N'étant pas visible à l'œil nu, Neptune est le premier objet céleste et la seule des huit planètes du Système solaire à avoir été découverte par déduction plutôt que par observation empirique.

## Page 271

Le césium, le rubidium, l'indium et le thallium tirent leur nom de la couleur de leurs raies spectroscopiques.

> « Sa raie spectrale est d’un bleu si pur que je propose de l’appeler césium. »

> L’indium et le thallium […] tirent aussi leurs noms de leurs raies spectrales brillamment colorées.

- césium : bleu très pur
- rubidium : rouge foncé
- indium : nommé d'après la couleur supposée analogue à l'indigo (en réalité, deux raies, une bleu foncé, une violette)
- thallium : une raie verte

Wikipédia : 

> cette couleur évoquant celle d'une jeune végétation, il nomme cet élément « thallium » (qui en latin scientifique, provient du grec θαλλός / _thallós_, « jeune pousse, jeune branche » ou « branche d'olivier »

## Page 335

> Un coupé de ville électrique nous attendait à la gare de Lucerne

En 1945, des voitures électriques existaient déjà ?

[Wikipédia](https://fr.wikipedia.org/wiki/Voiture_%C3%A9lectrique) :

> L'électricité est l'une des méthodes préférées pour la propulsion automobile à la fin du XIX<sup>e</sup> siècle et au début du XX<sup>e</sup> siècle, offrant un niveau de confort et une facilité d'utilisation que les voitures à essence de l'époque ne peuvent pas atteindre.

> L'invention du démarreur électrique en 1912 et la production de masse de voitures à essence par Ford renversent cependant le marché, et les voitures électriques disparaissent peu à peu.

Donc, oui, ça a existé, et ça a été supplanté. Que serait le monde si la voiture à essence n'avait pas pris un tel essor ? Peut-être pas beaucoup mieux, au final, ce qui pollue, c'est surtout la fabrication de la voiture…

En en discutant avec des ami⋅es, peut-être que ça n'aurait pas tenu de toute façon, car le problème pour l'électricité, ça a toujours été la conservation et le transport de l'énergie. La production d'électricité, et des batteries au plomb, pour ces voitures du début du XX<sup>e</sup>, ça n'était pas bien propre non plus. Cependant, peut-être que comme le besoin était présent, la recherche sur les batteries aurait pu avancer plus loin. Qui sait ?

## Page 357

> Aucun chimiste n’aurait pu le prévoir (seul un alchimiste en aurait été capable) parce que les transformations n’étaient pas du ressort de la chimie : aucun processus chimique, aucune atteinte chimique, ne pouvait altérer l’identité d’un élément, cette règle valant même pour les éléments radioactifs. Le radium avait un comportement chimique en tout point similaire à celui du baryum — mais sa radioactivité était une propriété radicalement différente, qui n’avait rien à voir avec ses particularités physiques ou chimiques. La radioactivité était un ajout merveilleux (ou terrible), une propriété totalement différente.

En fait, la radioactivité est la transmutation tant cherchée par les alchimistes. Elle transforme un élément en un autre. C'est de l'alchimie par la physique 😻

## Page 360

> [Rutherford] demanda donc à son jeune assistant Hans Geiger et à l’étudiant Ernest Marsden de […]

Tiens, tiens, Hans Geiger, le nom me dit quelque chose 😛

---

En bref, c'est un excellent ouvrage, très vivant, qui nous montre un gamin qui se façonne de manière à la fois académique, en lisant les grands maîtres (il y a un chapitre entier sur Humphry Davis, sur Dalton, sur Mendeleïev, sur les Curie), et très expérimentale (la scène où il jette du sodium, du potassium, du rubidium et du césium dans l'étang voisin avec ses camarades est très drôle, et j'envie un peu cette enfance). C'est très… sensoriel, aussi, Sacks décrit les couleurs, les odeurs, le toucher, comme des marqueurs très importants pour lui dans ses expériences.

> « Le tungstène fritté a une sonorité inimitable », m'avait dit Oncle Dave.
