---
title: Redshirts, de John Scalzi

date: "2021-02-23 18:54"
publish_date: "2021-02-23 18:54"
published: true
taxonomy:
  type: article
  category:
    - lecture
  tag:
    - anticipation
    - SF
    - parodie
author: "Lara"
thumbnail: cover-redshirts.jpg
thumbnail_orientation: portrait
highlight:
    enabled: false
---


Cela faisait longtemps que je voulais lire ce livre, sans y penser constamment, et quand une mienne amie bibliothécaire me l'a suggéré en prêt, j'ai sauté sur l'occasion. Bonne pioche. Très bonne pioche.

===

![Couverture du roman "Redshirts"](cover-redshirts.jpg){.media--align-left}

Comment faire pour résumer sans tout divulgâcher ? Si vous avez un peu de connaissances dans l'univers de la science-fiction télévisée, rien que le titre doit vous mettre la puce à l'oreille : les redshirts, les maillots rouges, ce sont les grouillots dans _Star Trek_, les dispensables, les enseignes qui se font croquer par le monstre de la semaine pendant que les personnages principaux devisent de la meilleure manière de ne pas se faire croquer.

Ici, on suit Dahl et quatre de ses comparses, affecté⋅es très nouvellement sur l'<i>Intrépide</i>, vaisseau spatial amiral. Et il y a des choses bizarres : rien que la mention des sorties d'exploration rend les gens nerveux, mieux vaut se cacher quand les supérieurs approchent parce qu'ils vont nous refiler des trucs dangereux à faire, et parfois on a soudainement des infos qu'on ne savait pas posséder. Et les sous-fifres meurent beaucoup. Lors des missions d'exploration, par exemple. Mais bon, c'est normal : l'<i>Intrépide</i> est le vaisseau amiral, quand même, les missions les plus dangereuses lui sont confiés. Non ?

L'histoire est bien sûr plus développée que ça, je veux éviter d'en dire plus.

---

J'ai beaucoup aimé les personnages de Dahl (ancien séminariste de la religion forshanique de gauche, expert en xénobiologie, très joli CV), Hester, Finn, Duvall et Hanson : leurs relations sont complexes, même si leur caractère est parfois stéréotypé (mais ça fait partie du jeu et rhaaah j'en révèle encore). Petit regret personnel qu'il n'y ait qu'une seule femme - Maia Duvall - dans le groupe. De même, j'ai dû faire plusieurs fois des efforts pour ne pas briser moi-même le quatrième mur de l'extérieur, en essayant de plus de ne pas deviner à l'avance ce qu'il se passait, pour vraiment garder le plaisir de la découverte (et oui, je mets ma main sur la page de droite quand c'est une fin de chapitre pour empêcher mes yeux d'y zigzaguer : je lis un peu erratiquement par moments, mon regard se portant sur l'ensemble des deux pages et mon cerveau reconstruisant l'histoire).

Après le roman, il y a une coda, plusieurs textes courts qui racontent ce qui se passe après la fin de l'histoire, du point de vue de plusieurs personnages. La première m'a un peu moins plu, je l'ai trouvée assez mal écrite, mais c'était peut-être voulu, par rapport à son auteur (son narrateur, plus précisément). Mais si ce sont des textes "surnuméraires", ils referment les histoires (et les plaies) ouvertes de bonne manière, et c'était chouette de pouvoir dire au revoir proprement.

Note globale : 4/4

(oh, et puis si vous voulez voir une autre très belle parodie de _Star Trek_, regardez _Galaxy Quest_ !)
