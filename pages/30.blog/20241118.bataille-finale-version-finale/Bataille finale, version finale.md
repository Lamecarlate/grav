---
meta-création: 2024-11-18 10:23
meta-modification: 2024-11-18 10:23
stringified-date: "20241118.102329"
title: Bataille finale, version finale
template: blog-item
date: 2024-11-18 10:23
update_date: ""
taxonomy:
  type: article
  tag:
    - jeu de rôle 
    - writing month
    - writing month 2024
header_image_file: 
header_image_alt: 
header_image_caption: 
thumbnail: 
thumbnail_orientation: 
extra_content:
  items: "@self.modular"
highlight:
  enabled: false
subtitle: On a fini la saison 1 de notre campagne de Donjons & Dragons !
---

On a fini la saison 1 de notre campagne de Donjons & Dragons !

===

Résumé de la soirée d'hier :

- Partir à la bourre de la maison ; 
- tenter d'installer le sac à dos planaire[^1] dans la sacoche et pas dans le cageot pour protéger la focaccia[^2] qui sinon ne rentrait pas ; 
- rouler 50 m et s'apercevoir que ça coince un peu, tiens ; 
- se rendre compte qu'une lanière du sac à dos est grave enroulée dans les rouages du vélo… ; 
- …passer un quart d'heure à l'en défaire, en pestant et désespérant par moments ;
- y arriver enfin ; 
- mettre le sac à dos dans le cageot, tant pis, quitte à casser ma si belle focaccia ;
- partir à l'heure où je devrais être arrivée ; 
- n'avoir que 20 minutes de retard, voir que la focaccia est entière, qu'elle a très bien supporté le trajet ;
- manger et battre le boss final avec une joie féroce (malgré une perte dans le groupe[^3]).

Et en bonus, mon Asphodèle Boutonbrillant, druide halfeline du cercle des Spores. Dans son enfance, son village, voire sa région, a été ravagée par une épidémie foudroyante et elle a fui, abandonnant les quelques petits frères et sœurs qui lui restait (y compris le tout petit dernier) et une bonne partie de sa santé mentale. Elle a été recueillie par des druides du cercle des Spores, qui l'ont soignée au mieux, et lui ont prodigué leurs enseignements. Ce qui fait qu'elle est paradoxalement attachée à tout ce qui vit et complètement détachée parce que pour elle la mort n'est qu'un changement d'état dans le grand cycle. Voyager avec cette bande de branqu- d'idio- de saltimb- avec ce groupe de personnes variées l'a un peu changée. Bon, elle se met quand même encore un peu en danger, notamment grâce à son bouclier magique qui lui permet de voler (littéralement) au secours de ses camarades et prendre l'attaque à leur place. Maiiiis ya du progrès. (Oui ce perso est improbable, elle est une sorte de tank de 90 cm de haut qui lance des sorts.)

Voilà son premier portrait :

![Une petite femme à la peau très très blanche, aux yeux bleus pâles, aux longs cheveux roux. Son expression est très neutre. Elle tiens un grand bâton orné de liens.](Asphodèle-1.webp){.figure-media--wide}

Et voilà l'actuel :

![Une petite femme à la peau très blanche, aux yeux bleus pâles, aux cheveux roux nattés. Elle sourit légèrement. Elle porte des lunettes sur le front, un bâton à tête de serpent et un immense bouclier doré.](Asphodèle-2.webp){.figure-media--wide}


[^1]: Ce sac à dos m'étonnera toujours, j'ai l'impression que je peux tout y mettre… La seule fois où j'ai déclaré forfait c'est pour notre bouilloire électrique : la boîte ne rentrait vraiment pas (par contre, la bouilloire, oui, sans aucun problème). 

[^2]: Faite d'une pâte à pizza prévue pour quatre personnes (c'est-à-dire permettant d'en nourrir huit, vu la quantité que j'ai obtenue la première fois), elle faisait bien 30 cm sur 40. Et je n'en ai pas fait de photos correctes 😿

[^3]: Mais ça va, c'était prévu, en fait. On savait que la joueuse concernée avait de moins en moins de plaisir à jouer cette classe de personnage, et on aurait dû se méfier en voyant que le boss s'acharnait un peu… et qu'elle ne râlait pas autant que d'habitude 😛 
