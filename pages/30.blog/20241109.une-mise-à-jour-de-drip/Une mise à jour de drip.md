---
meta-création: 2024-11-09 14:37
meta-modification: 2024-11-09 14:37
stringified-date: "20241109.143750"
title: Une mise à jour de drip.
template: blog-item
date: 2024-11-09 14:37
update_date: ""
taxonomy:
  type: article
  tag:
    - personnel
    - santé
    - informatique
    - logiciel 
highlight:
  enabled: false
subtitle: J'utilise le logiciel drip. (oui, le point fait partie du nom) depuis pas mal d'années maintenant pour suivre mes menstruations et prédire la date des prochaines.
---

J'utilise le logiciel drip. (oui, le point fait partie du nom) depuis pas mal d'années maintenant pour suivre mes menstruations et prédire la date des prochaines.

===

Mon besoin d'un logiciel de suivi de menstruations est vraiment très basique : savoir quand sont les prochaines, et importer et exporter les données, pour mon indépendance. J'ai testé plein d'applications différentes, aucune ne convient vraiment complètement… Parfois trop d'infos (je ne veux pas qu'on calcule la date d'ovulation ou la phase lutéale, je m'en fiche), parfois pas de notification (pas très pratique pour la planification, il faut penser à aller voir le calendrier, ce qui diminue l'intérêt), sans parler des traqueurs ou de l'impossibilité d'utiliser sans un compte (Clue, je te regarde, oui, toi qui as du jour au lendemain empêché d'accéder aux données locales tant qu'un compte n'était pas créé). 
 
Bref, [drip.](https://dripapp.org/) était pour l'instant l'application la moins désagréable.

Et elle est encore mieux, car on peut désormais ne pas afficher les infos sur la fertilité, qui étaient avant sur l'écran d'accueil.

Désormais l'écran affiche la date, indique à quel jour du cycle j'en suis et la date estimée des prochaines règles. Et. C'est. Tout.

Maintenant il lui manque juste un petit thème sombre et ce sera parfait.

(oh, ai-je dit que c'est un logiciel open source, pour iOS et Android, et trouvable sur F-Droid au besoin ?)
