---
meta-création: "2023-12-03 17:27"  
meta-modification: "2023-12-17 17:19:39"  

stringified-date: "20231203.172749"

title: "Liens de décembre (partie 1)"  
template: "blog-item"  

date: "2023-12-17 18:00"  
publish_date: "" 
published: true # si besoin de date de publi + force publi  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - cinéma
        - humour
        - musique
        - sciences
        - linguistique

# plugin Highlight
highlight:
    enabled: false

---

Une bien belle récolte, ma foi ! Du réalisme dans le cinéma, des blagues spécifiques, des corbeaux, de la chansonnette, des chats, un peu d'énervement, de la physique, des chats, de l'optique, de la philosophie, et des post-its.

===

[Coupez #2 - Dr Strange chez Primum Non Nocere](https://www.youtube.com/watch?v=DlCi7F2f3QE) (vidéo avec du son, pas de sous-titres) : Primum Non Nocere, médecin et chirurgien, étudie ici la première scène du film _Dr Strange_, une opération chirurgicale. L'idée est de voir dans quelle mesure elle est réaliste ou non. C'est intéressant ! (d'ailleurs, amusant, c'est moi ou bien Primum ressemble de plus à plus à Mark Ruffalo avec les années ?)

<details>
<summary>
Tu sais combien il faut de traducteurices pour changer une ampoule?
</summary>
Ça dépend du contexte
</details>

Auteur de cette mirifique blague : [dodosan](https://diaspodon.fr/@dodosan/111527776307936105).

[De jolis dahlias](https://mastodon.art/@naturesketchbook/111527937553755719).

[Un site qui permet de trouver la gare la plus proche d'une localité](https://www.garelaplusproche.fr) (regardez le nom du fichier image pour la petite loco, c'est tout mignon).

[Grosse fatigue](https://eldritch.cafe/@Jeanneadebats/111549539908130360).

[Panache, épisode de Crow Time](https://www.webtoons.com/en/canvas/crow-time/panache/viewer?title_no=693372&episode_no=119) (en anglais) : j'ai déjà dit que j'aimais _Crow Time_ ?

[On change pas à pas](https://www.webtoons.com/en/canvas/crow-time/panache/viewer?title_no=693372&episode_no=119) (vidéo avec du son, et des sous-titres incrustés) : ah, les Goguettes (en trio mais à quatre), vous connaissez sûrement, leur succès est phénoménal depuis le premier confinement. Ici, jolie chanson sur l'écologie, les petits pas individuels qu'on fait (qui ne suffisent pas en eux-mêmes mais hé, c'est déjà ça) et le fait qu'on n'est jamais dans la perfection.

[All I want for Christmas (Picard edition)](https://www.youtube.com/watch?v=3KvWwJ6sh5s) (vidéo avec du son, en anglais, avec sous-titres) : un monument, un tour de force, un superbe montage avec des vraies répliques de _Star Trek_ pour former la fameuse chanson de Mariah Carey. C'est si beau. Via [Lawrence Vagner](https://oisaur.com/@hellgy/111515408735846173). (et je sais, je **sais** que la femme tout à la fin de la vidéo n'est pas Samantha Carter de _Stargate: SG-1_ mais elle lui ressemble tellement, ça me choque à chaque fois)

[Des ptits chats et leur grand frère](https://www.youtube.com/watch?v=bS7wfBQOrHA) (vidéo avec du son, pas obligatoire, c'est juste de la musique accompagnante) : tout mignon, Marmelade le chat adulte rencontre ses petits adelphes, ça se cogne un peu mais c'est tout doux et plein de câlins. Et de poils.

[Langue des signes, langage des signes](https://nota-bene.org/Langue-des-signes-langage-des-signes), chez l'ami Stéphane : quelle est la différence entre « langue des signes » et « langage des signes » ?

[Ne dites pas « j'ai acheté un nouveau livre »](https://mastodon.social/@i_am_future/111551835723626253) (en anglais, une image avec un texte alternatif bien complet).

[Comment les iPods de première génération ont été rétro-ingénieurés](https://mastodon.social/@bagder/111538350617290554) (en anglais) : c'est fascinant. 

[Un troglodyte mignon qui fait la tronche](https://glitterkitten.co.uk/@babe/111544515961884464) (vidéo sans son, faux GIF en fait, qui boucle, rhaaah c'est relou cette intégration dans Mastodon)

[Mousse contre lichen](https://masto.nicolastissot.fr/@NiTi/111529350230382076) : je n'ai pas écouté l'émission, je mets surtout ce lien pour l'excellent comics de Tom Gauld (en anglais).

[You've heard of elf on a shelf…](https://ohai.social/@MeanwhileinCanada/111511340471600041) (image, avec alt en anglais, qui permet de mieux comprendre la blague).

OK, je pensais qu'il n'y avait pas pire que la pub animée sur les arrêts de bus. Et pourtant. Au moins une entreprise, ici Led Media Com, utilise voitures, camions, et [vélos modifiés](https://mastodon.social/@mariejulien/111511329113042933) pour afficher de la pub. J'ai envie de tout cramer. Mais ça va c'est des LED ça consomme pas trop-han (sbaf). Apparemment cette entreprise commercialise aussi des sacs à dos avec écran LED. Le retour des hommes-sandwichs ?

[My brushstrokes against AI-art](https://www.davidrevoy.com/article1007/my-brushstrokes-against-ai-art) chez David Revoy (en anglais). Je suis tellement, tellement d'accord. Je crois que je préfère amplement des œuvres graphiques avec une patte plutôt que parfaites, lisses.

[La glace est trop fine](https://h4.io/@brettezeleliquide/111509809431694585) (vidéo avec du son, sans paroles) : quelle bête magnifique. R'gardez voir ces papattes, là. 

[Fuzzy feelings](https://www.thisiscolossal.com/2023/11/fuzzy-feelings-anna-mantzaris/) (vidéo avec du son, sans paroles) : joli court-métrage d'animation mais pas que, sur la solitude et la compassion. Ne lisez pas la description, regardez directement.

[Woman Finds Tiny Kitten In Her Backyard | The Dodo](https://www.youtube.com/watch?v=tCT8_Q_CYQo) (vidéo en anglais, avec du son et des sous-titres incrustés) : hiiii mais c'est mignon.

[Boing boing boing](https://mas.to/@epiceneVivant/111523268129228133) (vidéo sans son) : un chat, de la neige, des heures d'amusement.

J'apprends via [ce tweet de Loovenelle](https://nitter.poast.org/Loovenelle/status/1733519839367495970#m) que 1) on ne peut pas réimplanter de pacemaker chez un humain (en France, c'est illégal), 2) on peut en revanche en faire une donation testamentaire pour un chien. Bien ! C'est déjà ça.

[McKinsey: Last Week Tonight with John Oliver (HBO)](https://www.youtube.com/watch?v=AiOUojVd6xQ) (vidéo en anglais avec du son, et des sous-titres anglais) : j'ai envie de mordre des trucs. Je savais à quel point la firme McKinsey était une usine à bullshit (on l'a beaucoup vu ces dernières années en France) mais là… Conflits d'intérêts, contrats avec l'Arabie Saoudite. Pfff. Je me sens impuissante. Au moins la vidéo est drôle, avec la parodie à la fin, mais c'est trop bien joué, en fait ça me fait pleurer.

Vous vous souvenez de _Animation vs. Animator_ ? Voilà _[Animation vs. Physics](https://www.youtube.com/watch?v=ErMSHiQRnc8)_ (vidéo avec du son, sans paroles, mais beaucoup d'écriture, regardez en grand !). Un bon quart d'heure de voyage de physique qui confine à la mystique (pour moi, certains concepts extrêmement avancés de la physique, c'est un peu de l'imaginaire pour l'instant : les ponts de Rosen-Einstein par exemple).

[Les attaques sur VLC, interview de jbkempf](https://www.youtube.com/watch?v=PYVqL4TsSOE&t=62s)(vidéo avec du son, sans sous-titres).

[The voice of Enbankment Tube station](https://mastodonapp.uk/@MarkHoltom/111566676222047835) (en anglais). Nan je pleure pas c'est toi tu pleures.

Beaucoup moins mignon : [Enquête de One Voice dans une fourrière 5 étoiles : l’abattage industriel des chiens et des chats](https://one-voice.fr/fr/blog/enquete-de-one-voice-dans-une-fourriere-5-etoiles-labattage-industriel-des-chiens-et-des-chats.html). C'est dur à lire. Moi qui habite avec deux chats, l'un qui est venu tout seul depuis la terrasse, et l'autre récupérée à la SPA, ça me fait mal… si ces deux-là s'étaient retrouvé⋅es dans ce genre de fourrière (vu que le premier n'était pas pucé, et la seconde ne l'a été que parce qu'elle a été attrapée par la SPA), iels ne seraient pas là. Huit jours, c'est incroyablement court.

[Cette histoire vous fera des nœuds au cerveau](https://www.youtube.com/watch?v=jpWrjgLxM3A) (vidéo avec du son, avec sous-titres) : ce que je peux détester les titres piégeaclic si fréquents sur Youtube… Mais le fait est que cette nouvelle de Monsieur Phi, narré par lui-même, provoque bien des nœuds. Il s'agit d'une nouvelle qui joue avec les concepts chers à son auteur : qu'est-ce que la conscience, les souvenirs d'un événement sont-ils la preuve que l'événement a eu lieu, etc. Bien joué !  

J'aime beaucoup le travail d'Andrea Love. Elle fait des vidéos en stop-motion avec de la feutrine, c'est tout tranquille, doux, et ça a ce délicieux « on dirait que » qui marche si bien avec moi. Ici, [Coffee](https://www.youtube.com/watch?v=dE_clON7sdE) (vidéo avec du son).

[La troisième couleur primaire](https://grisebouille.net/la-troisieme-couleur-primaire/) : un peu de science de la lumière avec Gee. En plus Gee mets des textes alternatifs très bien sur ses images, et ce qui peut rester du texte l'est, j'aime beaucoup (il parle rapidement [dans cet article](https://grisebouille.net/quoi-de-neuf-27/) de ses efforts pour rendre ses articles et ses BD le plus accessible possible, et il revient souvent sur d'anciennes BD pour les transformer)

L'article Wikipédia "Ship of Theseus" (en anglais) ne ressemble plus du tout à ce qu'il était lors de sa création de 2003. C'est… très méta ! Je vous invite à lire :

- [l'article en question](https://en.wikipedia.org/wiki/Ship_of_Theseus)
- [le diff de l'article entre sa première version et la version actuelle](https://en.wikipedia.org/w/index.php?title=Ship_of_Theseus&diff=cur&oldid=1154003), qui montre qu'en effet, aucune ligne n'est en commun entre ces deux versions

Source de l'information : [un pouet de Wikipedia](https://wikis.world/@wikipedia/111587024591220062) .

À noter que [la version française de l'article](https://fr.wikipedia.org/w/index.php?title=Bateau_de_Th%C3%A9s%C3%A9e&diff=209062687&oldid=2183169) semble avoir subi le même sort 😸

Et cela me fait découvrir [un manga du même nom](https://fr.wikipedia.org/wiki/Le_Bateau_de_Th%C3%A9s%C3%A9e_(manga)), ça me donne envie ! De ce que je comprends du résumé, je le décrirais comme « _Quartier lointain_, mais dark ». Ensuite, je n'en sais pas plus. Hop, c'est sur ma (looooongue) liste de trucs à lire.

[Estequoi ?](https://danstonchat.com/21724.html) : krrkrrrkrr. J'aime bien les histoires de Dans Ton Chat (ex BashFR, pour les vieux z'et vieilles qui me liraient). De temps en temps, des duos ou des groupes iconiques apparaissent : Ben et Magus, ou Cha et Iceteabe. Ces deux-là sont ensemble, et se font tout un tas de crasses bien marrantes. Aujourd'hui, des post-its. Plein. De. Post-its.

---

Voilà voilà 😊


