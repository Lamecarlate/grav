---
meta-création: "2022-08-02 07:57"
meta-modification: "2022-08-02 07:57"

stringified-date: "20220802075745"

date: "2022-08-02 07:57"
template: "short-note"
taxonomy:
    type: 'short-note'
title: ""

---


Décor : dans le lit, la nuit.

Personnages : un moustique, une humaine (moi).

Le moustique : "Oh, je vais m'approcher de cette montagne rose qui sent bon le CO<sub>2</sub> !"  
Moi : donne une grande baffe dans l'air pour virer le moustique.  
Le moustique : "Je viens de vivre une expérience mystique ! Un ouragan s'est élevé depuis la grande montagne, c'est un signe, elle me teste, je dois persévérer !"  
Moi : "Mais bordigaille."

