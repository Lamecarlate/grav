---
title: Festival Thalie 2018

date: 2018-07-25 10:00
publish_date: 2018-07-25 10:00
published: true
taxonomy:
    type: article
    category:
        - musique
    tag:
        - concerts
        - festival
        - Grenoble et alentours

header_image_file: 20180701-paysage-venon.jpg
header_image_caption: 'La montagne visible depuis les hauteurs de Venon'
author: 'Lara'
---

Le premier juillet, je suis allée à Venon voir le [Festival Thalie](http://www.ensemblethalie.fr/index.php/festival-thalie/edition-2018). Ce festival - à prix libre - est organisé par et pour de jeunes professionnel⋅les de la musique : musicien⋅nes ou compositeurices. Tout se passait dans la petite et charmante église (et sur son parvis).

Voici quelques notes en vrac sur ce que j'ai pensé de cette après-midi bien remplie.

===

"Schizophrenia" de Yann Robin. Morceau composé en 1974, pour une clarinette et un saxophone soprano : deux instruments bien différents (l'un est un bois, l'autre un cuivre) et pourtant semblables, notamment au niveau du son produit. Dissonnant, intéressant, perturbant. J'ignorais que la clarinette pouvait produire ces sons, assez "synthétiseur".

"Cafe 1930", d'Astor Piazzolla. J'aime beaucoup Piazzolla, depuis que j'ai découvert Libertango. Cafe 1930 ne fait pas partie de mes favoris, mais je l'aime bien quand même. Amusant, de temps en temps des notes faisant penser à du Ghibli période Chateau dans le ciel.

Dans le public, un homme aux cheveux foncés mais une barbe taillée toute blanche.

Il y a eu un moment "Carte blanche", où toutes les personnes qui voulaient pouvaient venir interpréter un ou des morceaux. Une jeune femme a ouvert le bal (aha) avec une danse contemporaine - joli, mais moi la danse contemporaine ça ne me touche pas beaucoup, j'y retrouve trop de répétitions de mouvements, et ici, dans une église sans estrade, ce n'était probablement pas la meilleure scène, je pense que seuls les trois premiers rangs l'ont vue. Mais c'était quand même sympa !

Un septuor vocal <i lang="it">a cappella</i> de jeunes femmes aux robes d'arc-en-ciel, des morceaux très touchants - hélas non identifiés car faisant partie de la carte blanche, donc pas au programme.

Un morceau très contemporain (finalisé en mars 2018, l'auteur est dans le public mais je n'ai pas retenu son nom) dans un trio violon, alto et violoncelle. Des accents de super-héros Marvel, ou de James Bond. Vivant.

Trois petites pièces de Chopin au piano, par coeur. Mignon. Un peu plat. Le 3<sup>e</sup> morceau est plus enlevé, il fait penser aux Djinns de Debussy, le moment où les paroles font

> La voix plus haute  
Semble un grelot.  
D'un nain qui saute  
C'est le galop.

![Vue de l'intérieur de l'église, on aperçoit un lustre en haut à droite, au centre il y a un pianiste (au visage flouté), et sur le devant, des spectateurices de dos.](20180701-eglise-venon.jpg "Pendant la carte blanche, le pianiste (au visage flouté) des petites pièces de Chopin"){.align-center}

L'église résonne fort, par moments le son *sature*, c'est intéressant.

Après cela, un opéra bouffe à l'extérieur : "La dinde farcie". Le thème est classique : un jeune commis de cuisine passionné qui a appris avec sa mamie, contre le grand chef médiatique mais pas sympa et qui préfère l'argent à l'amour, mais c'était une chouette prestation, très bien chantée et drôle ! Les accessoires étaient peu nombreux et assez parodiques : des légumes géants découpés dans du carton, des fait-tout et des couteaux. J'adore cette ambiance à la "on dirait que" de quand on est môme : bien sûr que les légumes en carton ne font pas illusion, mais je les "sentais" être découpés et mijoter avec le sel et le poivre. Ça m'a donné des frissons, comme à chaque "on dirait que" : ça me touche, ce genre de truc, c'est assez incroyable - bien plus que des effets spéciaux réalisés à la perfection, mais je m'égare :)

En soirée, un duo de percussions, la compagnie Tellurique. D'abord, un solo (pas au programme, ajouté à la dernière minute parce qu'un autre morceau n'était pas prêt), un *extraordinaire* moment d'utilisation du corps humain comme instrument de percussion : les mains qui frappent la cuisse, la joue, l'autre main, qui se posent en l'air pour jouer un silence pesant, les pieds qui cognent, la voix qui chante, parle, siffle. Des petits mots improvisés ou non, on aurait dit un message qui essayait de passer mais je n'ai hélas pas pu bien entendre. Et je n'ai pas osé allé voir le musicien à la fin pour lui demander d'où venait ce morceau… Ensuite, plusieurs morceaux non identifiés, au xylophone et/ou métallophone. Belles vibrations, et belle alchimie entre les deux musiciens. Sonorités d'orgue par moment, surprenant et beau. Un peu trop long quand même. Mais <i lang="en">standing ovation</i> méritée.
