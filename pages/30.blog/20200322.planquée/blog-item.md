---
title: Planquée

date: '2020-03-22 17:00'
publish_date: '2020-03-22 17:00'
published: true
taxonomy:
    type: article
    category:
        - humeur
    tag:
        - travail
        - Covid-19
author: 'Lara'

---

Je me sens… planquée.

J'ai un boulot que je peux faire en télétravail et une entreprise qui avait déjà tout prévu ; j'ai déjà du stock de nourriture parce que je gagne suffisament pour pouvoir prévoir et refaire ledit stock souvent ; je n'ai pas d'enfants - et je ne suis pas seule chez moi. Je joue clairement en mode facile.

===

Et je me sens vraiment comme si j'étais une planquée de la Grande Guerre. Une bourgeoise, bien installée dans son chez-soi, pendant que d'autres triment dehors au risque de leur santé, ou que d'autres perdent leur emploi.

Alors, je sais bien que je "fais ma part", en ne sortant pas, que ça permet de ne pas augmenter la propagation du virus. Mais voilà. Voilà ce que je ressens. Et même ça, ça m'agace. Parce que le ressenti d'autres est bien important que le mien.
