---
title: Pendants d'oreille "Elsweyr"

date: '2020-04-24 16:00'
publish_date: '2020-04-24 15:00'
published: true
taxonomy:
    type: article
    category:
        - création
    tag:
        - bijoux
        - perles
header_image_file: 20200424-pendants-d-oreille-1.jpg
header_image_alt: "Paire de boucles d'oreille, en miroir ; dans chacune, il y un triangle de bois, une grosse perle pourpre entourée de cristaux saumon, de petites perles rondes blanches, une goutte orange, de toutes petites perles de corail et deux perles rondes dorées"
author: 'Lara'

---

Cela faisait très, très longtemps que je n'avais pas ouvert ma "boîte à perles". Ce matin, prise d'une impulsion soudaine, je l'ai apportée sur mon bureau et ai fouillé dedans. Voilà le résultat.

===

Comme à mon habitude, l'inspiration est venue d'une à deux perles, et ensuite j'ai brodé (aha) autour.
 
Ici, j'ai extrait le petit triangle de bois, que je comptais utiliser en point central - je n'avais pas encore décidé de l'objet à ce stade, ce serait un pendentif ou une boucle d'oreille. Puis est venu l'ovale pourpre, que j'ai encadré de deux petits cristaux zwarovski saumon sur une tige de métal, recourbée à la fin pour passer sur le fil. Et ensuite j'ai ajouté les mini-cylindres de corail et les autres perles, en essayant de rester au maximum dans les tons chauds - les perles rondes, de ce blanc légèrement verdi, étaient là pour donner un contrepoint.

Je n'ai pour l'instant qu'une oreille percée, donc je n'ai pas pu tester la paire en grandeur nature, mais normalement les boucles sont chirales 😛

![Gros plan sur une boucle](20200424-pendants-d-oreille-2.jpg)

Au boulot - même chez moi - je porte un gros casque audio, les pendants d'oreille font rarement bon ménage avec ! Mais ces boucles tiennent bien, et ne gênent pas, comme elles sont très légères. J'ai hâte de pouvoir me faire repercer l'autre oreille pour les porter toutes les deux.

De manière générale, ça m'a fait du bien de remettre mes doigts à l'ouvrage. J'ai navigué un peu sur le net pour avoir des inspirations, et au final ce qui est ressorti est quelque chose qui ne ressemblait en rien à ce que j'avais vu, mais c'était justement ça l'idée : donner du grain à moudre à mon cerveau, pour qu'il crée. Tout simplement. Je vais essayer de me pousser à créer plus souvent. Des colliers - facile -, des bracelets - plus difficile -… Peut-être qu'il faudrait aussi que je fasse un tri dans mes milliers de perles : il y en a dont je sais pertinemment que je ne les utiliserai pas, mais que je garde par nostalgie, ou par "on sait jamais ça peut servir".

Le nom "Elsweyr" vient d'un ami très cher : il trouve que les boucles ressemblent à des griffes, et Elsweyr est l'endroit où vivent les Khajit, un peuple de félins anthropomorphes, dans l'univers du jeu <i lang="en">Skyrim</i>.

![Un autre gros plan sur une boucle](20200424-pendants-d-oreille-3.jpg "Un autre gros plan, le focus semble s'être fait sur la goutte orange à droite, j'aime bien le flou sur l'ovale pourpre, et la lumière qui s'y reflète."){.figure-media--wide}
