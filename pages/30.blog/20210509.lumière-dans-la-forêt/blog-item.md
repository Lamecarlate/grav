---
title: Lumière dans la forêt


date: "2021-05-09 10:00"
taxonomy:
  type: article
  category:
    - création
  tag:
    - écriture
    - Périlune
    - NaNoWriMo
author: Lara

---

En novembre dernier, à l'occasion du NaNoWrimo, je me suis lancée, et ai écrit une nouvelle dans l'univers de mon personnage Périlune.

===

<div class="writing" markdown="1">
Il y avait de la lumière dans la forêt. Les travailleurs étaient tous rentrés pour la nuit, et il n’y avait pas de route traversante, donc ce ne pouvait être des voyageurs. Si c’était le cas, ils s’étaient bien perdus ! Henriette regardait par la fenêtre, fascinée, cette lueur jaune, qui ne vacillait pas, ne semblait pas s’approcher, et pourtant être en mouvement.

Lorsque sa mère la héla, elle se tourna à regret, saisit le linge qui séchait près du feu pour le tremper à nouveau dans le baquet d’eau chaude, et le tendit à la femme agenouillée près du malade. Cette dernière lui sourit rapidement, prit le linge, le plia en deux, y déposa une cuillerée du mélange dans le bol à son côté, le replia et le déposa sur le torse de l’homme couché dans le lit. Henriette recula, tant l’odeur de moutarde du mélange était forte, exacerbée par l’eau chaude.

– J’aurais besoin d’eau chaude propre également, s’il te plaît, il lui faut du calme et une tisane de valériane lui fera du bien.

Sans un mot, la fillette s’affaira. Elle était habituée à recevoir des ordres, que ce soit à la maison, ou bien au moulin, où elle travaillait à chasser les rats - tâche bien ingrate, soit dit en passant. Le silence retomba. La doctoresse retira le cataplasme et redressa doucement le père d’Henriette, pour l’aider à boire la tisane. Puis elle le recoucha, et se leva.

Périlune s’étira ; le temps passé à genoux à piler les ingrédients pour le sinapisme l’avait complètement engourdie.

– Maîtresse miresse, comment va-t-il ? s’inquiéta la mère.  
– Pour l’instant, il se repose, et je pense qu’il ira bien demain. Dormir sera bon pour tout le monde.

La femme se détendit, désigna à Périlune une couche non loin du feu, et se glissa ensuite dans la sienne, pendant qu’Henriette s’adossait au mur près de la cheminée.

---

Sur sa couche, Périlune surveillait du coin de l’œil la lueur de la forêt. Puis, sans un bruit, la jeune femme attrapa sa besace et sa pèlerine, et sortit. Se retournant par réflexe, elle vit à la fenêtre, dans l’obscurité, le visage de la petite, plein d’étonnement et de curiosité. Un signe discret, et Henriette apparaissait à la porte, enveloppée dans sa cape. 

– Maîtresse, que faites-vous ?  
– Je vais voir ce qu’est cette lueur. L’avais-tu vue ?

Ravie qu’on lui pose une question, Henriette s’exclama à voix basse que oui, bien sûr, et elle décrivit ce qu’elle avait remarqué.

– La lumière me semble plus vive, à présent. Et elle… pulse, on dirait.  
– Tu as l’œil, petite.  
– N’avez-vous point peur ?

Périlune sourit.

– Non. J’ai vu beaucoup de choses dans ma vie, et celle-ci ne m’effraie pas.

Henriette hésitait, partagée entre sa crainte et son intérêt. Mais ce dernier fut le plus fort.

– Puis-je vous suivre ?  
– Si tu le souhaites.

La femme et l’enfant s’enfoncèrent alors dans la forêt.

Petit à petit la lumière se fit plus forte, plus nette. Des formes se dessinèrent en son sein. Henriette n’en détachait pas les yeux, et Périlune remarqua qu’elle tremblait. Elle posa doucement sa main sur l’épaule de la fillette, et murmura :

– Ce sont des esprits. Ils ne nous feront pas de mal. Tu peux laisser ta peur passer à travers toi.  
– Sont-ce les âmes des défunts mal enterrés ? Je peux prier pour elles.  
– Non, ce ne sont pas des humains. (« Curieux choix de mot », pensa Henriette) Les esprits sont… des forces de la nature, qui prennent des formes selon ce qu’elles croisent.  
– Vous dites qu’ils ne nous feront pas de mal, mais que font-ils ici ?  
– Tout comme le daim ou le sanglier, ils passent selon des chemins connus d’eux seuls.

Périlune plongea la main dans sa besace et en tira une branche d’aneth.  
– Si tu en ressens le besoin, brandis ceci devant toi, cela te protégera.

Puis, déposant son sac au sol, elle avança doucement vers les formes mouvantes. Ses mouvements se firent plus amples, plus agiles, en accord avec une musique inaudible. Henriette, subjuguée, restait sans bouger. Et puis ses pieds se manifestèrent. Elle ressentit un frisson de bas en haut, et entendit soudain la mélopée. Ça ne ressemblait en rien à ce qu’elle connaissait. Pas un chant, pas une vielle, bien autre chose… Sa peur évanouie, elle se mit à danser. La doctoresse et l’enfant, au milieu des lumières chaudes, dansaient lentement, souriant dans la nuit d’automne, entraînées par la troupe d’esprits.

« N’allons pas trop loin. » Périlune n’avait pas perdu sa lucidité, et elle savait qu’elle avait un malade à traiter dans la maison à l’orée du bois. Satisfaire la curiosité était une chose, s’y perdre en était une autre. Elle attrapa l’épaule de la fillette, qui se dégagea comme si cela faisait partie de la sarabande dans laquelle elle était plongée. Périlune fit un pas en avant, essaya de nouveau, et de nouveau l’enfant, leste comme une loutre, lui échappa. 

– Henriette !

Le son de sa voix claqua dans la musique silencieuse, mais n’eut aucun effet. La petite continuait de valser, les bras au-dessus de la tête et les yeux fermés. Périlune jura in petto. Elle commençait à regretter d’avoir laissé sa besace… où donc, déjà ? La danse des esprits n’était pas si rapide, mais apparemment elle les avait emmenées loin de leur point de départ.
Périlune remarqua que le fichu mauve de la fillette commençait à se dénouer. Elle s’en saisit et le décrocha. Puis d’un geste vif le noua autour de la main gauche d’Henriette, et autour du sien. La branche d’aneth dépassait largement du corsage de la petite et Périlune l’extirpa adroitement. Elle la tint devant elle - oui, c’était un peu de la superstition, mais ça ne faisait pas de mal - et se mit à rebrousser chemin, doucement, toujours dans le rythme, pour ne pas brusquer Henriette et la pousser à la suivre. L’enfant, toujours extatique, suivit docilement cette fois, trébuchant de temps en temps, contrainte qu’elle était, mais sans sortir de la danse.

Plusieurs fois Périlune dut ralentir, louvoyer, pour laisser à Henriette l’illusion de sa liberté de mouvement, mais au bout d’un temps qui lui parut bien long, les deux étaient sorties du champ de lumière des esprits. La musique étrange s’estompa dans la tête de Périlune, et elle attendit patiemment qu’il en soit de même pour la gamine. Cela ne se fit pas attendre, et c’est une Henriette déboussolée et confuse qui émergea.

– Ma dame ! Veuillez me pardonner pour mon mauvais comportement ! Me voilà bien honteuse !  
– Allons, Henriette, ce n’est rien. Perdre la tête pour danser n’est pas pécher. Rien de grave ne serait arrivé, au pire nous nous serions retrouvées de l’autre côté de la forêt, et le retour aurait pu être long.  
– De l’autre côté de la forêt ? Elle se termine donc quelque part ?

Périlune rit.

– Tu n’as jamais voyagé ?  
– Eh bien non ma dame, on m’a toujours interdit d’aller plus loin que les bois, et le village le plus proche est à un jour de marche, je ne l’ai jamais vu.  
– Un jour, tu iras, si l’envie t’en prend.

Henriette marmotta, ne sachant trop que penser. Partir, rien que l’idée était terrifiante. Ce voyage dans la forêt, avec ces choses (« Ces esprits », se reprit-elle), c’était déjà bien plus d’aventure qu’elle n’aurait pu penser. Mais… un jour, oui, peut-être.

Périlune regarda autour d’elle. La lumière des esprits était désormais loin, mais une autre avait pris sa place, plus froide et pourtant chaleureuse. Familière. La Lune éclairait largement la clairière où elles étaient arrivées. Et sur le bord, de grands bosquets de sauge officinale. La jeune femme, ravie, alla en cueillir une brassée.

– Il me manquait justement cet ingrédient pour les soins à prodiguer à ton père dans la matinée.

La petite mit les plantes dans son tablier, toutes deux repartirent vers l’orée, où Périlune récupéra sa besace. Elles arrivèrent devant la maison endormie, et enlevèrent leurs pèlerines pour faire le moins de bruit possible à l’intérieur. Henriette réintégra sa place près du foyer, tisonna distraitement, et se rendormit vite.

Périlune, après avoir jeté un œil sur son patient, qui dormait paisiblement, resta éveillée jusqu’à l’aube. Elle s’en voulait. Elle se reprochait son insouciance. Elle avait reconnu les esprits tôt, ne s’était pas inquiétée, et n’avait pas imaginé que la petite serait aussi sensible à leur contact. Alors, oui, comme elle l’avait dit tantôt, rien de grave n’aurait pu se passer. À court terme. Mais qui sait ce qu’Henriette avait vraiment entendu et vu durant sa transe ? La jeune miresse se promit de revenir dans quelques temps, juste pour voir si tout se passait bien.

---

L’année suivante, lorsqu’elle demanda des nouvelles d’Henriette, on lui répondit qu’elle avait un jour sauté sur un chariot en direction du village d’à côté. On ne l’avait jamais revue.

20201108-20201109

</div>

---

Périlune est un personnage créé il y a fort fort longtemps sur un forum de créatifs et créatives, dont je m'aperçois qu'il reste bien peu de traces sur Internet… quelques illustrations sur mon ancien blog [Au griffon griffonnant](https://griffon.lamecarlate.net/tags/perilune) tout au plus. Je n'ai pas republié les textes de l'ancien ancien blog désormais disparu, car son histoire est en refonte depuis 2011 au minimum.

En quelques mots : elle était à la base une lhymbienne (peuple inventé dans le forum en question, à l'aspect démoniaque, cornes et oreilles pointues à l'appui) incapable de magie contrairement à ses pairs, et elle se basait sur des poudres et des potions pour faire illusion. Avec le temps, le côté "heroic fantasy" un peu banal m'a lassé, et j'ai accumulé avec les années des données pour transposer son monde dans le XIIIe siècle de notre Terre, avec quelques variations mineures. Ce texte est la première véritable production de ce nouveau monde, beaucoup de choses ne sont pas encore dévoilées, mais j'espère que cela n'a pas entravé la lecture !

