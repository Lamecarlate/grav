---
meta-création: 2024-11-04 17:43
meta-modification: 2024-11-04 17:43
stringified-date: "20241104.174350"
title: Chauve-souris, c'est Halloween !
template: blog-item
date: 2024-11-04 17:43
update_date: ""
taxonomy:
  type: article
  tag:
    - loisir créatif
    - fête
    - DIY
    - writing month
    - writing month 2024
highlight:
  enabled: false
---

OK je n'assume absolument pas ce jeu de mot, faites comme si vous n'aviez rien lu.

===

Il y a quelques jours, [Lawrence Vagner a posté les plans](https://oisaur.com/@hellgy/113374748916104162) pour [imprimer en 3D un serre-tête](https://www.printables.com/model/1052073-giant-halloween-bat-headband-prusa-mini-xl) qu'iel a designé. C'est un serre-tête avec des oreilles et des ailes de chauve-souris. Donc : j'ai sauté dessus.

![Capture d'une conversation, moi qui dis à un ami avec une imprimante 3D : « OOOOOOOOOoooooh je voudrais ça, tu pourrais me le faire pour Halloween ? »](chauve-souris.webp)

![Suite de la conversation, une amie renchérit : « tu peux en faire deux stp ? »](chauve-souris-2.webp)

Ça s'est ensuite transformé en trois exemplaires lorsqu'une deuxième amie du groupe a dit qu'elle était aussi intéressée 😆

Et le lundi qui suit, le copain envoyait cette photo dans la conversation : 

![Photo de la plate-forme d'une imprimante 3D en cours d'impression d'une aile de chauve-souris.](chauve-souris-3.webp)

Donc jeudi, quand on s'est toustes retrouvé⋅es dans le chalet loué pour l'occasion, distribution de morceaux de plastique ! Un peu de colle, et voilà :

![Photo de trois serre-têtes chauve-souris sur une table en bois.](chauve-souris-4.webp)

Ils sont magnifiques. Ces p'tites oreilles làààààààà.

Alors, par contre, ça n'a pas très bien tenu (mais on a possiblement utilisé une super-glu pas si super). Et sur la tête, ça a tendance à glisser vers l'avant. Je vais étudier ça et voir ce que je peux modifier.

Et pis je comptais le peindre, aussi, j'avais même déjà passé la sous-couche, mais le temps a manqué (écoutez, lors de ce long week-end, on a fait une raclette, de la peinture de figurine, deux soirées de jeux de rôle, un escape game et un atelier bois, excusez du peu).
