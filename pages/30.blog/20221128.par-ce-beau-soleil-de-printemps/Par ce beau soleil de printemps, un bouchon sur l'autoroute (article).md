---
meta-création: "2022-11-28 08:47"
meta-modification: "2022-11-28 08:47"

stringified-date: "20221128.084743"

title: Par ce beau soleil de printemps, un bouchon sur l'autoroute
template: "blog-item"

date: "2022-11-28 08:47"
taxonomy:
	type: article
    category:
        - création
    tag:
        - micro-nouvelle
        - anticipation
        - horreur

# plugin Highlight
highlight:
	enabled: false

story: true
foreword: "Alors que je roulais moi-même sur l'autoroute, je me suis demandée ce qu'il se passerait s'il y avait une vitesse minimale, qui s'appliquerait même en cas d'embouteillage. En est sortie cette micro-nouvelle horrifique."

---

Mina regarda sur la carte de l'assistant de navigation avec appréhension. Le bouchon annoncé était toujours là. Il ne restait que quelques dizaines de kilomètres, et elle serait forcée de ralentir. Ce n'était vraiment, vraiment pas le moment de se chopper des amendes. Son compte bancaire était bien souvent vide, Mina était forcée de fonctionner en flux tendu. Elle avait pu payer son loyer hebdomadaire, les courses de la semaine avaient été faites la veille, et toutes les factures étaient réglées. Mais c'était bien tout ce qu'elle pouvait se permettre. S'approcher du bouchon, pire, se retrouver dedans, ce serait passer en dessous de la vitesse minimale autorisée sur la route.

Pas d'autre itinéraire possible, répétait l'assistant. Mina enrageait. Un demi-tour pour passer par une route différente aurait été envisageable, mais elle n'avait pas pu s'offrir l'option vol stationnaire pour sa voiture, et elle voyait avec dépit d'autres véhicules s'envoler pour faire un 180 sur l'autre voie. Ne restait plus qu'une option : configurer le régulateur pour ne pas atteindre la zone congestionnée trop tôt et espérer que cela suffise. Peut-être que l'embouteillage se serait résorbé d'ici-là. Et puis les voitures qui s'étaient extraites du trafic pour faire demi-tour aideraient à fluidifier.

Le véhicule couina et réinitialisa le régulateur : la vitesse actuelle n'était pas maintenable. Et lorsque Mina tenta de le remettre, la voiture refusa, par sécurité.

Et ralentit. Doucement, progressivement, en s'adaptant à la vitesse globale du flux de voitures, comme une rivière dont l'eau s'épaissirait.

La première notification vint de la voiture : "Vitesse minimale sur autoroute atteinte, attention, danger !". D'habitude, les systèmes autonomes remettaient en marche l'accélération, mais les mêmes systèmes détectaient l'environnement, et les véhicules amassés autour : l'ordre fut annulé avant même d'être lancé.

Mina suait. Mais c'était inéluctable. Elle était dans le bouchon.

La seconde notification vint de la tablette : "Vous avez été repérée en dessous du seuil de vitesse minimale autorisée sur l'autoroute E56, entre Dijon et Paris, aujourd'hui à 7h23. Une amende forfaitaire de 15€ vous a été adressée."

Et merde !

Désormais la voiture était à l'arrêt.

Quinze minutes plus tard, une troisième notification, de la tablette toujours : "Vous avez été repérée en dessous du seuil de vitesse minimale autorisée sur l'autoroute E56, entre Dijon et Paris, aujourd'hui à 7h38. Une amende forfaitaire de 15€ vous a été adressée."

Quinze minutes plus, une nouvelle notification : "Vous avez été repérée en dessous du seuil de vitesse minimale autorisée sur l'autoroute E56, entre Dijon et Paris, aujourd'hui à 7h53. Une amende forfaitaire de 30€ pour récidive vous a été adressée."

Instantanément, une nouvelle notification sur la tablette, de la banque cette fois : "Vous avez effectué un paiement de 60€ ce matin à 7h53. Merci d'avoir choisi notre banque et à bientôt ! Emoji sourire radieux, emoji fleur, emoji argent qui s'envole".

Le service communication de la banque était d'un cynisme… Ce n'était pas la première fois que Mina se prenait des amendes à débit immédiat, mais le ton était un peu plus sérieux, avant. Enfin, c'était le printemps, on pouvait supposer qu'on pouvait s'amuser un peu. 

Nouveau couple de notifications : l'amende, puis la banque (avec des emojis cœur palpitant et chat qui fait un bisou). Mina ne releva pas.

Puis un e-mail de son emploi : "En raison de votre retard, un quart de votre salaire journalier sera retenu." Les enflures ! Il n'était même pas 8h30, elle n'avait que dix minutes de retard – bon, le bouchon n'étant clairement pas sur le chemin de la délivrance, son retard ne ferait que s'allonger. Elle aurait dû envoyer un message à son N+1 dès qu'elle avait vu le bouchon, ça lui apprendrait. Elle le fit tout de même, n'osant pas demander la levée de la retenue, c'était trop tôt pour réclamer des faveurs : cela ne faisait que deux ans qu'elle travaillait dans cette boîte de rachat de crédit carbone. C'était sa mère qui lui avait trouvé cette place, quand elle y était consultante – maintenant elle dirigeait le service de gestion des collaborateurs en freelance, mais il n'était évidemment aucunement question de favoritisme, un emploi, ça se mérite !

9h. Nouvelles notifications : amende (de 90€ désormais), banque (emoji épi de maïs, emoji fantôme qui fait un clin d'œil), travail (moitié de salaire retenue).

Toujours aucun mouvement sur la route. Aucun bruit non plus.

Mina regarda autour d'elle, mais toutes les voitures avaient des vitres fumées, impossible de voir si les autres étaient dans la même agitation qu'elle. Elle se demanda si elle pourrait sortir et toquer aux portières. L'affichage tête haute de l'assistant de navigation affichait une température de 45°C dehors, un peu plus chaud que l'an dernier à la mi-avril, un peu moins que l'an prochain. Il n'était pas question de mettre un pied hors de l'habitacle climatisé du véhicule.

9h30. Le carillon des notifications continuait. Salaire de la journée entièrement retenu. Mina pesta. C'était tellement injuste… Elle n'avait jamais été en retard, jamais causé de problème, jamais parlé plus haut que sa position. La banque, toujours, cette fois ajoutant des aggios sur les amendes (emoji bombe, emoji feuille d'automne, cette fois Mina en était sûre, c'était choisi au hasard).

10h. Toujours aucun mouvement dans le bouchon. Elle se demanda si les autres aussi avaient des amendes – existait-il des assurances qui permettent d'y échapper ? Ce serait sûrement hors de sa portée si c'était le cas. Notification de la banque : "Voulez-vous contracter un prêt à la consommation pour régler vos dettes ? Contactez votre conseiller !".

10h14. Message du travail : "En raison de votre important retard, votre nom a été placé dans la loterie mensuelle des renvois pour l'exemple. Vous souhaitant une agréable journée, veuillez agréer l'expression distinguée de nos sentiments les meilleurs."

Mina éclata en sanglots.

Son compte bancaire était vide, voire négatif, son travail était sur la sellette – et malgré la retenue il n'était pas question de ne pas venir, car ce serait un renvoi immédiat. Qu'est-ce qui pouvait être pire ?

Nouvelle notification, de la voiture directement cette fois : "L'option pilote automatique a été désactivée pour manquement de paiement.". Bah, au point où on en était, cela n'avait plus d'importance, Mina ne bougea pas.

10h25. Sur la tablette, une notification sonore, un e-mail, vocalisé automatiquement. C'était sa mère. "J'ai appris pour la loterie. Je suis déçue." Rien de plus. Même pas un "Tu peux mieux faire", ce genre de phrase dont on ne sait jamais trop si c'est un compliment ou un reproche. Elle avait toujours été si efficace, si directe.

10h30. Un message apparut sur la tablette : "Désespérée ? Au bout du rouleau ? Nous pouvons vous aider." Sobre, simple, concis, avec une petite musique légère et calmante, qui fit relever la tête à Mina, intriguée. 

"Autrefois tabou, la mort est pourtant parfois la solution. À Facilidead, nous proposons une méthode rapide, facile, sous douleur, et gratuite*. Vous n'avez qu'à cliquer ICI et accepter les CGU.

\*En acceptant le contrat, vous reconnaissez renoncer aux poursuites, vous ou vos héritiers. Facilidead ne saurait être tenu responsable de la mauvaise utilisation de son service."

Est-ce qu'elle était prête ? Est-ce qu'il y avait une autre solution ? Dans la tête de Mina, tout se mélangeait. Elle cliqua sur "ICI" en respirant profondément. Une seconde notification de confirmation apparut, elle valida, autorisant le branchement de sa voiture sur le système de Facilidead – quel nom ridicule, pensa-t-elle, pendant que le système de climatisation commençait à diffuser le gaz anesthésiant, la première étape.

Au moment où elle sombrait, il y eut un frémissement dans l'embouteillage, et devant sa voiture immobile, le flux recommença à bouger. Derrière, un nouveau bouchon se forma.

20220717-20220719