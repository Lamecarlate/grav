---
meta-création: "2023-05-01 17:27"  
meta-modification: "2023-05-01 17:27"  

stringified-date: "20230501.172722"

title: "Session de cuisine du premier mai"  
template: "blog-item"  

date: "2023-05-01 17:27"  
publish_date: "" 
published: true # si besoin de date de publi + force publi  

taxonomy:
    type: article
    category:
        - cuisine
    tag:
        - salé
        - sucré
        - récup
        - gâteau


# plugin Highlight
highlight:
    enabled: false

---


Petite session de cuisine cet après-midi pour écouler les restes.

===

Trois bananes un peu moribondes, de l'aquafaba (jus de cuisson des pois chiches) résultant d'un houmous fait pour dimanche soir, des verts de poireaux déjà coupés qui patientaient depuis une semaine… J'ai essayé de cuisiner au mieux ces différents ingrédients.

D'abord, du pain. C'est presque une habitude hebdomadaire. Je mets des raisins secs dedans, parce que j'aime bien avoir des ptits machins dans le pain. Parfois je mets aussi des noix, des graines, etc, mais l'Amoureux n'est pas très fan, alors ce n'est pas systématique. J'ai déjà testé avec tomates séchées et olives vertes, ça marche très bien comme terrain de jeu pour tartinade salée, et ça fait son petit effet en soirée.

C'est la [recette de Mélanie, du Cul de poule, le pain <del>flemasse</del> sans pétrissage](https://leculdepoule.co/2019/02/06/video-pain-maison-en-5-minutes-sans-petrissage).

![Photo d'un pain fraîchement sorti du four.](IMG_20230501_172132--small.webp "Il a l'air un peu ramassé mais c'est l'angle de la photo, ce pain fait environ 25 cm sur 8."){.figure-media--wide}



Ensuite, j'avais prévu un gâteau, issu du livre [_Le gâteau dont tu es le héros_, de Owi Owi](https://owiowifouettemoi.com/2022/09/12/le-gateau-dont-tu-es-le-heros/), un peu mon bouquin de chevet ces temps-ci. Le choix se porte sur un « Tout-terrain » avec reste de bananes et framboises surgelées. Je véganise les recettes à chaque fois maintenant que j'ai le coup de main, soit avec des graines de lin moulues, soit avec de la compote de pommes. Cette fois, j'ai mis de l'aquafaba monté en neige, pour tester.

À la base je voulais faire en plus un autre gâteau avec l'aquafaba, mais, stupeur horreur malheur, le pain et le gâteau viennent de vider mes réserves de farine de froment. La peste soit de mon cerveau qui organise souvent très bien les choses… et applique rarement ladite organisation. Un jour je saurai faire un inventaire correct de ma maison. Un jour.

Donc, plan B, un seul gâteau. Oups.

![Photo d'un long gâteau beige doré avec des framboises.](IMG_20230501_172052--small.webp "Long gâteau is long (et les meilleures blagues sont les plus courtes)"){.figure-media--wide}

Ensuite, que faire de ces jolis poireaux caramélisés ? Les mettre dans un cake, pardi. J'ai choisi la recette de [cake salé végétalien sans gluten de Mélanie du Cul de poule](https://leculdepoule.co/2019/11/08/cake-sale-vegan-et-sans-gluten/) (sans gluten puisque, comme susmentionné, mon froment est fini) : j'ai un peu adapté, évidemment, et j'ai mélangé sarrasin, lupin et seigle, et mis un peu d'aquafaba. (et ça va, c'est bon, je suis pas morte, vouzinquiétez pas)

![Photo de dessus d'un cake recouvert de graines de tournesol, on distingue un peu la pâte et des poireaux sous les graines.](IMG_20230501_172115--small.webp " "){.figure-media--wide}

Des pois chiches rôtis (le principe de la recette vient de chez [Pick Up Lime](https://www.pickuplimes.com/recipe/roasted-vegetable-salad-with-crispy-chickpeas-596)) pour finir ceux qui n'avaient pas été houmoussés dimanche.

![Photo d'un pot de verre contenant des pois chiches rôtis, luisants d'huile d'olive et recouverts d'épices et graines.](IMG_20230501_172504--small.webp "L'huile d'olive; ça rend tout meilleur."){.figure-media--wide}

Et il me reste de l'aquafaba, sous forme de neige molle.

On dirait un sketch.

> Et deux croissants !