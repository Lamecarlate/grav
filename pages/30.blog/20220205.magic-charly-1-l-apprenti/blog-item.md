---
title: "Magic Charly, tome 1 : L'apprenti"

date: "2022-02-05 16:00"

taxonomy:
    type: article
    category:
        - lecture
    tag:
        - magie
        - littérature jeunesse

# plugin Highlight
highlight:
	enabled: false

thumbnail: cover-l-apprenti.jpg
thumbnail_orientation: portrait

---

Après cinq ans de silence, la grand-mère de Charly revient habiter chez lui et sa mère. On leur a dit qu'elle n'a plus toute sa tête, qu'elle a perdu la mémoire. Les souvenirs de Charly à son sujet sont assez flous aussi.

===

![Couverture du roman "L'apprenti"](cover-l-apprenti.jpg){.media--align-left}

Audrey Alwett, _Magic Charly, Tome 1, L'Apprenti_, Gallimard, 2019, 409 p. (ISBN 978-2-07-512145-3)

De prime abord, je n'ai pas trop aimé. J'y lisais une ambiance Harry Potter juvénile qui ne m'amuse plus. Mais rapidement, je me suis prise au jeu. Et j'ai finalement adoré !

> Quand tu étais petit, tu racontais à tout le monde que ta grand-mère était une sorcière ! Vous vous entendiez comme larrons en foire. Elle avait une sacrée imagination, elle aussi…

Très vite, on se rend compte que oui, Dame Mélisse est une sorcière – une magicière, dans les termes de ce monde –, et que Charly aussi, même s'il ne le sait pas. Et très vite aussi il se retrouve embrigadé dans ce monde, pour retrouver la mémoire de sa grand-mère. Pour cela, il a un chapeau magique, un chat (Mandrin, son familier, un espèce de gros matou à poils longs), et plus tard un mentor et une acolyte.

> Tu es un magicier, Charly.

(cette citation me paraît être un hommage à _Harry Potter_, mais comment pouvait-on dire cela autrement ?)

Et, sous les dehors rigolos des tartes chercheuses à la crème, des citrolles (carosses-citrouilles que l'on fait pousser) pourries et des termes à la Bertie Crochue, un monde très dur est décrit. Un monde où la magie est extrêmement réglementée parce qu'elle est rare, et même en cours de disparition si on en croit les rumeurs. Ce qui mène à un système injuste : les puissants le sont politiquement et magiquement, et ont la police et l'Académie magique dans leur poche, tandis que les autres survivent au mieux, en utilisant juste la quantité de magie qui leur est attribuée, espérant que la milice ne vienne pas trop fourrer son nez, parce que quand les miliciens veulent trouver des preuves, ils en trouvent. (toute ressemblance avec un système policier existant est sûrement une coïncidence, n'est-ce pas)

La première scène est très colorée : c'est un souvenir de Charly, où sa mère cuisine des beignets à prédictions (c'est comme des <i lang="en">fortune cookies</i>, mais avec des prédictions extrêmement rapprochées dans le temps : "ton chat va te voler ton beignet", "tu vas tomber de ta chaise"). On sent la chaleur et la lumière du matin, l'odeur de la confiture et des gâteaux, et la tendresse. C'est écrit du point de vue de Charly, on ne sait que ce qu'il sait, et c'est un tournant de l'histoire – comme souvent ce genre de prologue.

Charly, le protagoniste, est un jeune homme sympathique, et sa personnalité est bien cernée. J'apprécie notamment le détail de sa gestion des émotions, qu'il emprisonne dans son poing, c'est une idée ingénieuse, très réaliste – je m'imagine tout à fait en train de faire ce geste pour contenir une émotion trop forte. Et, sans en révéler trop, ce n'est pas un geste anodin. Charly est un grand Noir baraqué, qui a souvent peur de faire peur à cause de son apparence (et qui culpabilise beaucoup), terriblement gentil, qui fait passer les autres avant lui – dans le tome 2, on le traite même de paladin ! 

Le second personnage principal est Sapotille, camarade de classe de Charly, et magicière extrêmement talentueuse. Petite, blonde, un peu ronde, elle est orgueilleuse et solitaire, mais ce sont des armes et armures qu'elle a conçues pour se protéger. Pauvre noune… Elle n'a pas eu une vie facile du côté magique, et elle est assez difficile d'accès : elle ne fait confiance à personne au début, et peu à peu se laisse approcher.

Il y a une romance qui semble se construire entre les deux protagonistes mais elle n'est pas le centre de l'histoire. Pas de coup de foudre, ni de <i lang="en">"enemies to lovers"</i>, quelque chose de plus simple, deux jeunes qui vivent des aventures ensemble qui les rapprochent.

On a également June, meilleure amie de Charly et bêtisière en chef : comme elle est une gosse de riches et qu'elle déteste ça, elle met un point d'honneur à être la plus rebelle possible. C'est un peu l'élément comique, elle est agaçante tout en restant un personnage chouette et dynamique. 

Il y a de nombreuses trouvailles autour de la magie que j'aime beaucoup : par exemple, elle fait pousser les plantes, et donc il faut débroussailler les balais volants en route, pour éviter de s'écraser parce qu'un buisson ce n'est pas très aérodynamique. Et les sorts sont énormément liés à la mémoire : on écrit dans son grimoire pour les imprimer dans sa tête, on peut vendre ses souvenirs ou les encapsuler dans des objets pour augmenter la quantité de magie qu'on peut utiliser – en passant outre les réglementations.

J'y vois aussi un peu un lien avec la série _Dead like me_ : dans ce monde, on est vivant tant que la Mort ne vient pas nous chercher.

Morceaux choisis :

L'école de Charly n'est pas de la première fraîcheur.

> Le toit [de l'école] fuyait avec la nonchalance d’un vieux baron incontinent et il fallait placer des seaux sur les tables, les jours de pluie.

À propos des courses de citrolles organisées dans Thadam, la ville des magiciers.

> – Elles ont l’air super dangereuses, ces courses !  
> – Bah, n’exagérons rien. La plupart du temps, il n’y a pas de mort.

Charly vient d'animer une serpillière pour l'aider à faire le ménage. Au lieu de lui ordonner de travailler, il l'apprivoise.

> — Comment je vais t’appeler ? se demanda-t-il à haute voix. Pépouze, ça te dit ?
> 
> Pour toute réponse, la serpillière se mit à ramper loin de lui. Ce ne fut que lorsqu’elle approcha de la porte que Charly comprit qu’elle prenait la poudre d’escampette.
>
> — Attends ! Reviens !
>
> La serpillière hésita.
>
> — S’il te plaît ? tenta Charly.
>
> La serpillière reprit sa route vers la sortie. Charly était à peu près certain qu’il ne pouvait pas laisser une serpillière animée par ses soins se balader en toute liberté dans les rues. Il en était responsable magicièrement, ou quel que soit le mot adéquat. Il lui courut après et l’attrapa à pleines mains.
>
> — Il faut qu’on discute, toi et moi.
>
> En réponse à quoi la serpillière lui sauta à la figure et serra de toutes ses fibres.

(je l'aime tellement, ce gamin, il parle gentiment à tous les objets qu'il croise)

En bref : _L'apprenti_ est un charmant livre, qui ouvre une trilogie bien chouette. Et je suis déjà en train de dévorer le tome 2.

Note globale : 3/4
