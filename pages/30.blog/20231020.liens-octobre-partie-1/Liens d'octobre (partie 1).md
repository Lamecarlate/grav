---
meta-création: "2023-10-01 11:55"  
meta-modification: "2023-10-01 11:55"  

stringified-date: "20231001.115515"

title: "Liens d'octobre (partie 1)"  
template: "blog-item"  

date: "2023-10-20 7:55"  
publish_date: "" 
published: true # si besoin de date de publi + force publi  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - humour
        - musique
        - informatique
        - histoire
        - littérature

# plugin Highlight
highlight:
    enabled: false

---

Des chats, de la gymnastique, des pokémons, des blagues nulles, des épées, des chats, du git, des chats, des ombres, du rugby (méta), des hamsters, un webcomic qu'il est très très bien.

===

## 1<sup>er</sup> octobre

[Le photochatopieur](https://piaille.fr/@AgatheDeblouz/111159007223042439) (GIF animé sans son).

[Humour, politique et gymnastique](https://www.youtube.com/watch?v=ZuS_Qm80o2E) (vidéo avec du son) : étude bien intéressante sur l'humour de manière générale. Le premier extrait, avec Paul Hunt, je l'ai trouvé un peu gênant, notamment au niveau des rires, parce que je n'arrivais pas encore à savoir ce que j'en pensais : est-ce que Hunt se moque des femmes gymnastes, pourquoi un homme travesti ça serait drôle ? Mais c'est expliqué plus loin. Et il y a notamment un extrait d'une autre de ses performances en costume et discipline féminines. Cela montre qu'il ne se moque pas des femmes mais bien des carcans. Je le raconte mal, mieux vaut regarder la vidéo.

## 4 octobre

[Facebook Messenger has added ai-generated stickers](https://slime.global/@outie/111175777035842810) (en anglais). Au. Secours.

[Love rumble](https://troubledminnesotan.tumblr.com/post/659788437422063616/rivals-who-are-girlfriends-we-finally-got) : joli comic en anglais, avec du rose et de la bagarre.

[The new god](https://mastodon.art/@MicroSFF/111175950023992539) (en anglais), une micro-nouvelle de O. Westin. Brrr.

## 5 octobre

[Nouveau, à Ikea](https://peoplemaking.games/@eniko/111168242318041715) : blague de niche vidéoludique, en anglais (et en latin).

![Montage d'une antilope qui fait la gueule. « Les antilopes quand elles voient un lope »](antilope.jpg)

via [Spooky Cafou](https://raru.re/@cafou/111181990777921094).

## 6 octobre

[Lil astronaut, re-dessin d'une jolie photo](https://mastodon.social/@anubiarts/111183477886093330).

## 7 octobre

[Tout un tas d'épées très classes](https://mastodon.art/@emilylorange/111178386865572358) : chaque année en septembre il y a le défi "Swordtember", où il faut dessiner une épée par jour. J'adore tout particulièrement Void, Noodle, Spicy, et Mountain, mais toutes sont superbes. (jetez un œil au [tag swordtember sur mastodon.art](https://mastodon.art/tags/swordtember), c'est sublime)

[Plate prière, d'Anna Sylvestre](https://www.youtube.com/watch?v=lfcrb0MMq_o), via un message sur le fediverse de Kozlika. Une belle chanson d'Anne Sylvestre, une prière pour un monde moins grossophobe.

## 8 octobre

Non seulement les premiers requins sont apparus avant les anneaux de Saturne (voir [Measurement and implications of Saturn’s gravity field and ring mass](https://www.science.org/doi/10.1126/science.aat2965), en anglais), mais apparemment aussi avant l'étoile polaire. Oui oui, pas avant que Alpha Ursae Minoris soit « au nord de la Terre », avant qu'elle soit née, il y a environ 70 millions d'années ([page Wikipédia d'Alpha Ursea Minoris](https://fr.wikipedia.org/wiki/Alpha_Ursae_Minoris)). Les premiers requins dateraient du Dévonien, 420 millions d'années. Via [Wandering Star](https://dice.camp/@pawsplay/111190573225180263).

Et on en profite pour lire un peu le futur lointain astronomique sur la [page Wikipédia dédiée](https://fr.wikipedia.org/wiki/Chronologie_du_futur_lointain#%C3%89v%C3%A8nements_astronomiques).

## 9 octobre

[La lune à 360°](https://mastodon.social/@wonderofscience/111190935712502064) : vidéo sans son, assez hypnotique (est-ce que vous aussi vous avez l'impression de la voir en relief ?).

## 11 octobre

[Un extrait d'un sketch des deux minutes du peuple](https://piaille.fr/@2minutesdubot/111212141075213641) qui me fait hurler de rire à chaque fois.

> Vous créez un malaise aux six ! Passez-moi les saucisses.

L'inventivité de Pérusse pour les jeux de mots est impressionnante (et ma propension personnelle à cet art n'y est sûrement pas étrangère).

[Please do not resist](https://9gag.com/gag/aEqbAMG) (vidéo avec du son) : hiiiiii. (ce sera mon unique commentaire)

[Petite forêt de mousse](https://mastodon.sharma.io/@harshad/111215979877410732) : j'aime tellement le mot-croisillon "mosstodon" (oh, et "lichenSubscribe" aussi).

[What is in that .git directory?](https://blog.meain.io/2023/what-is-in-dot-git) (en anglais) : un article technique sur comment fonctionne git du côté fichiers. Je n'ai pas encore tout compris mais c'est passionnant. Via [Bearstech](https://mamot.fr/@bearstech/111215888776766546).

## 12 octobre

[Uruk-cat](https://mamot.fr/@matiu_bidule/111220882536873700) (en anglais dans l'image, le texte alternatif est en français pour la description) : alors. Euh. Mieux vaut que vous regardiez.

![Gros plan d'un œil avec un maquillage arc-en-ciel](rainbow-eye.jpg)

Source : [Sol R.](https://mastodon.online/@MarSolRivas/111222287706022223).

## 14 octobre

Le soir, en mangeant, nous avons une habitude, l'Amoureux et moi : nous lisons des histoires sur le site Not Always Right. Des histoires souvent horrifiques, avec des client⋅es épouvantables qui obtiennent des repas gratuits en faisant un tintouin parce que læ manager n'a pas de courage (ou que la boîte l'oblige à ne pas en avoir…). Et parfois, c'est super mignon : [One door opens… and never closes](https://notalwaysright.com/one-door-opens-and-never-closes/246584/).

![Une figurine d'un hippopotame rugissant, la gueule ouverte. Elle est faite en faïence égyptienne bleu-verte, et décorée de motifs floraux et de points.](hippo.jpg)

Source : [Nina Willburger](https://social.anoxinon.de/@ninawillburger/111232455201024898)

[Ombres créées par l'éclipse annulaire de soleil](https://wandering.shop/@sarenaulibarri/111234544980280997). ([bonus vidéo](https://piaille.fr/@Mnaudin/111251222020970300) avec du son)

## 15 octobre

[Un chat admirable](https://mamot.fr/@Linternee/111239621832320256). Admirez-le.

[Cette pub n'a pas vieilli du tout](https://www.youtube.com/watch?v=UmkbJlYx1v8) (vidéo en anglais ave du son), via [dodosan](https://framapiaf.org/@dodosan/111239631734717263).

Ça fait un temps que je n'ai pas parlé boulot ici, mais voilà encore un exemple de pourquoi les overlays dits d'accessibilité c'est à fuir à tout prix : [Web Accessibility. Solved. By making the web worse for everyone.](https://toot.cafe/@aardrian/111239897747001225) par Adrian Roselli.

[Adult merit badges](https://mstdn.social/@dalfen/111237109389551639). (moi j'ai le badge 3)

## 16 octobre

[Beatbox prank](https://9gag.com/gag/aoK6oQX) (vidéo avec du son) : ah, voilà un prank sympa, qui ne se moque pas des gens !

## 17 octobre

[Ça manque de hamsters qui cambriolent des banques](https://www.youtube.com/watch?v=_Goaw0gYEiA) (vidéo avec du son), via [Benjamin Bouvier](https://tutut.delire.party/@bnjbvr/111250685045867659). C'est très mignon !

[Vocalise Opus 34 n°14 de Rachmaninov, arrangement pour flûte, clarinette et piano](https://www.youtube.com/watch?v=CDoAiGSEX_o) (vidéo avec du son). C'est doux, lent, reposant. Musique de la page du jour du webcomic [_Court of Roses_](https://courtofroses.spiderforest.com), que je recommande chaudement : de la fantasy sans prétention, autour d'un groupe composé uniquement de bardes, lié⋅es par leur amitié à Merlow the Rose, demi-elfe amateur de bière et joueur de cornemuse.

Moins doux, tout aussi flamboyant : [Such rainbow powers](https://www.reddit.com/r/RainbowEverything/comments/16j7n0f/such_rainbow_powers/) (vidéo avec du son), un monsieur tout en arc-en-ciel et en perruques. 

---

Voilà pour cette récolte. 

Je me demande si les titres par date sont pertinents… Ça structure un peu, mais j'avoue que parfois ça me fait me dire « mais je n'ai qu'un seul lien pour ce jour-là ? quelle feignasse je fais ! », ce qui nourrit bien entendu ma culpabilité, etc etc, je suis sûre que beaucoup sont dans ce cas.

Je vais essayer sans titres pour la prochaine fois, on verra ce que ça donne.