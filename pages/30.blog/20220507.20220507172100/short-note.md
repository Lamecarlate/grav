---
meta-création: "2022-05-07 17:21"
meta-modification: "2022-05-07 17:21"

date: "2022-05-07 17:21"
template: 'short-note'
taxonomy:
    type: 'short-note'
title: "De l'ombre à la lumière"

---

Après un bon resto avec des ami⋅es, exposition Rembrandt au couvent Sainte-Cécile (Grenoble). Sont présentées des gravures et des eaux-fortes. C'est joli et touchant - et le musée fournit une grosse loupe à main pour mieux apprécier les détails, c'est tout à fait nécessaire sur certaines œuvres, minuscules études de visage.

Mention spéciale à l'animation en début d'expo, où on peut créer sa propre gravure, de façon numérique, mais en suivant toutes les étapes : pose du vernis, dessin, passage dans l'acide, encrage, pose sur papier… et impression pour récupérer son œuvre !

Il y a également des œuvres par les étudiant⋅es de l'école Émile Cohl, inspiré⋅es par l'exposition.

[Article à propos de l'exposition sur FranceTV.info](https://www.francetvinfo.fr/culture/arts-expos/peinture/a-grenoble-une-exposition-met-en-lumiere-les-gravures-en-clair-obscur-de-rembrandt_5051455.html)

(oh, et puis j'ai acheté des olives et de l'ajvar, et beaucoup trop d'ustensiles de cuisine trop cool, mais c'est une autre histoire)
