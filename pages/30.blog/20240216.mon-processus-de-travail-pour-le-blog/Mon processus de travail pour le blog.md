---
meta-création: "2024-01-01 12:31"
meta-modification: "2024-02-11 18:43:36 "

stringified-date: "20240101.123124"

title: "Mon processus de travail pour le blog"
template: "blog-item"

date: "2024-02-16 12:00"

taxonomy:
  type: article
  category:
    - informatique
  tag:
    - écriture
    - blog

# image principale
header_image_file:
header_image_caption:

# plugin Highlight
highlight:
  enabled: true
---

Suite à plusieurs discussions, j'ai voulu expliciter ma méthode actuelle pour écrire et publier des articles de blog avec le moins de friction possible (parce que sans ça je ne ferais rien, la flemme est ma colonne vertébrale).

===

## Mon moteur de blog

Mon site web est propulsé par [Grav](https://getgrav.org/) : c'est un CMS dit « à plat », qui n'a pas de base de données. Tout est géré par des fichiers (donc évidemment je suis tombée en amour). Et le contenu est dans des fichiers markdown (donc évidemment bis repetita). Chaque article est en fait un fichier principal dans un dossier : le nom du dossier sera le "slug" de la page web, et il peut contenir une date, qui ne sera pas affichée mais qui permet de ranger les dossiers de manière chronologique (donc évidemment jamais deux sans trois).

## Créer le plan de travail

La première étape pour écrire un article de blog, c'est de créer le dossier, et le fichier markdown. Ça peut bien sûr se faire à la main, mais comme je l'ai dit dans le chapô, je recherche à minimiser la friction. Comme je passe ma vie dans [Obsidian](https://obsidian.md), j'ai utilisé un plugin pour ce logiciel : [QuickAdd](https://quickadd.obsidian.guide/docs). C'est un plugin qui sert à créer des fichiers rapidement, avec une structure prédéfinie, et plus encore. Mon besoin était de créer en une seule fois le dossier (nommé et daté) et le fichier.

Pour cela j'ai écrit un script JS :

```js
module.exports = {
  getArticleName,
  getFolderName,
};

async function getFolderName(params) {
  const title = await params.quickAddApi.inputPrompt("Créer un article");
  const reformatted = `${formatDate(new Date())}.${title
    .toLowerCase()
    .replace(/[^A-zÀ-ž0-9]+/gi, "-")}`;

  // Used in getArticleName
  params.variables["title"] = title;

  return reformatted;
}

async function getArticleName(params) {
  return params.variables["title"];
}

const datePadding = (v) => `0${v}`.slice(-2);

const formatDate = (date) => {
  return `${date.getFullYear()}${datePadding(date.getMonth() + 1)}${datePadding(
    date.getDate()
  )}`;
};
```

Ce que ça fait, en gros : quand la méthode `getFolderName` est appelée (pas dans ce script, j'explique plus loin), cela ouvre une petite fenêtre dans Obsidian qui attend un nom d'article (par exemple « Mon processus de travail pour le blog »). Quand le nom est entré, il est stocké pour nommer le fichier, et il est transformé en « 20240101.mon-processus-de-travail-pour-le-blog » (j'ai commencé cet article le 1<sup>er</sup> janvier, je vais à mon rythme, ok 😆 ?). La méthode `datePadding` sert à, heu, pallier la manière ultra-louche qu'a JavaScript (natif) de gérer les dates : « février » correspond à « 1 » (oui, en plus les mois sont numérotés à partir de zéro)(mais pas les jours !) et je voudrais « 02 », donc j'ajoute un zéro devant si besoin.

Ah tiens il semblerait que la bibliothèque moment.js soit utilisable (soit dans Obsidian directement soit dans QuickAdd) ? Peut-être que je modifierai mon script, un de ces jours. Comme dit l'adage : partage ce que tu sais, tu apprendras. Ceci en est un exemple flagrant : en faisant des recherches rapides sur QuickAdd pour le présenter, je découvre des possibilités 😅

Puis j'ai configuré QuickAdd. Il faut créer une macro (ici elle s'appelle DossierArticle).

![Capture d'écran du gestionnaire de macros de QuickAdd, on voit une macro nommée « DossierArticle », un switch pour pouvoir lancer la macro au démarrage (non choisi), un bouton "Delete" et un bouton "Configure". En dessous, un champ et un bouton pour créer une nouvelle macro.](quickadd-macro-manager.webp)

![Capture d'écran de QuickAdd, centrée sur la macro DossierArticle, où l'on voit que j'ai ajouté le script create-folder-for-article.js.](quickadd-macro-DossierArticle.webp)

On ajoute dans cette macro le fichier JS susmentionné.

Enfin, on ajoute un template QuickAdd, ici je l'ai nommé « Article ». Je lui ai défini le chemin du template Obsidian (oui, c'est agaçant d'avoir utilisé le même terme…) qui correspond à ce que je veux mettre automatiquement dans le fichier qui sera créé.

Chez moi ça ressemble à ça :

```
---
meta-création: "2024-01-01 12:32"
meta-modification: "2024-02-14 19:10"

stringified-date: "20240101.123221"

title: "Mon processus de travail pour le blog"
draft: true
template: "blog-item"

date: "2024-01-01 12:32"
publish_date: ""
published: true # si besoin de date de publi + force publi

taxonomy:
    type: article
    category:
        -
    tag:
        -

# image principale
header_image_file:
header_image_alt:
header_image_caption:
# miniature (remplace image principale, liste uniquement)
thumbnail:
thumbnail_orientation: # landscape|portrait

# recette
extra_content:
    items: "@self.modular"

# plugin Highlight
highlight:
    enabled: false

---
```

C'est un template [Templater](https://silentvoid13.github.io/Templater/), plus précisément, d'où les variables préfixées par "tp". Templater est plus puissant que la version intégrée à Obsidian, mais on peut peut-être s'en passer ici.

Ce sont uniquement les métadonnées du fichier : le titre (prérempli), les dates (pré-remplies également), le template Grav (ouiiiii beaucoup trop de fois ce mot, désolée, mais c'est tellement passe-partout), la taxonomie, les petits trucs autour.

Ensuite, dans QuickAdd, on donne le nom du fichier. Comme on l'a vu, je génère ce nom via une macro, donc il faut l'indiquer : `{{MACRO:DossierArticle::getArticleName}}`. Puis il faut préciser le (ou les !) dossier de création ; là, il faut le chemin entier, puis la partie dynamique : `{{MACRO:DossierArticle::getFolderName}}`. Et ensuite je crois que j'ai laissé les valeurs par défaut.

![Capture d'écran de QuickAdd, très complète, décrite plus haut.](quickadd-template-Article.webp " "){.figure-media--wide}

Et on se retrouve avec ça :

![Capture d'écran des paramètres de QuickAdd, avec le nouvellement créé Article, et des icônes sur la droite, notamment un éclair.](quickadd-settings-Article-off.webp)

Pour avoir un raccourci dans la Palette de commandes, il faut cliquer sur le petit éclair, qui devient jaune.

![Capture d'écran des paramètres de QuickAdd, avec le nouvellement créé Article, et des icônes sur la droite, l'éclair est désormais jaune.](quickadd-settings-Article-on.webp)

Et voilà ! <kbd>Ctrl</kbd> + <kbd>P</kbd> pour ouvrir la Palette, chercher « Article », sélectionner « QuickAdd : Article », entrer un nom, valider, et commencer à écrire.

À noter que j'ai un type d'article différent, la notule : conçue pour s'affranchir de titre, de taxonomie (même si je peux en mettre), c'est pour du micro-blogage, et la création des dossier et fichier est semblable. Juste, le slug est une date (donc je n'appelle pas le script, j'ai mis `{{DATE:YYYYMMDDHHmmss}}` dans les paramètres au lieu de `{{MACRO:etc}}`), et le template Grav appelé est différent.

## Écrire

Bon bin ça ensuite, ça prend entre un quart d'heure et, heu, trois mois. (ne pas regarder l'article en brouillon depuis juillet, ne pas).

## Publier

Enfin, vérifier d'abord : je fais un lien symbolique du dossier (ici  20240101.mon-processus-de-travail-pour-le-blog) vers le dossier du blog dans Grav. Je peux ainsi regarder l'article dans mon navigateur, corriger, arranger, augmenter, et quand je suis satisfaite, je supprime le lien symbolique et je copie.

Cette étape n'est pas automatisée, mais je pourrais le faire ! Il faudrait un écouteur de fichiers dans tout le système. Peut-être rsync ? Je ne sais pas si on peut synchroniser dans un même système.

Souvent je renomme le dossier, aussi, puisqu'il est daté du jour où j'ai commencé l'article, et s'il s'est passé beaucoup de temps, d'autres dossiers se sont peut-être ajoutés, et je préfère garder l'ordre de publication visible.

Je fais relire par mon beta-testeur, c'est-à-dire l'Amoureux <span role="img" aria-label="Visage qui tire la langue">😛</span>.

Et enfin, j'envoie en ligne avec `git`. Pas d'automatique ici non plus, je fais les commits et pushes à la main. Puis je vais en SSH sur mon serveur, où j'ai un alias `publish_test`, qui va faire un `git pull` sur mon espace de test. C'est juste un sous-domaine, rien de folichon. Une fois que je me suis assurée que c'est OK, je lance `publish_prod`, qui fait pareil mais dans le vrai dossier.

Et après je me rends compte des fautes de frappe et des mots oubliés.
