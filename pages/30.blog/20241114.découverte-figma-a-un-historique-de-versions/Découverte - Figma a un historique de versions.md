---
meta-création: 2024-11-14 21:11
meta-modification: 2024-11-14 21:12
stringified-date: "20241114.211102"
title: "Découverte : Figma a un historique de versions"
template: blog-item
date: 2024-11-14 21:11
update_date: ""
taxonomy:
  type: article
  tag:
    - travail
    - développement
    - design
    - writing month
    - writing month 2024
highlight:
  enabled: false
---
Au boulot, l'équipe de designers utilise Figma, et nous avons souvent eu des tensions à ce sujet. On a peut-être trouvé une solution 😽

===

Le souci c'est que nous n'avons jamais eu d'organisation parfaite, et donc dans l'équipe de développement, on implémente parfois les maquettes pendant qu'elles sont en train d'être designées, ou en cours de finalisation. Ou bien parfois il y a des corrections.

Et donc quand les testeurs et testeuses nous remontent que le site ne ressemble pas à la maquette, difficile de savoir s'il y a eu des changements ou bien si on s'est trompé⋅es…

Bref, parfois c'est compliqué, tout le monde se plaint (les devs parce que les designers font rien qu'à changer des trucs sans prévenir, les designers parce que les devs démarrent des trucs sans prévenir et ne respectent pas les créas, etc).

Eh bien j'ai découvert ce matin que Figma avait un historique de versions ! C'est assez restreint et c'est leeeeeent mais ça va permettre de remettre un peu de sérénité dans les équipes sur le sujet. Comme ça, on pourra prouver que non, il y a deux jours, ce bouton n'avait pas la même couleur, c'est pas moi qui l'ai imaginé.

(Et peut-être qu'on va trouver comment faire pour que Figma envoie des notifications lors des modifications, et là, on se rapprocherait vraiment de la perfection.)
