---
meta-création: 2024-11-03 11:20
meta-modification: 2024-11-03 11:20
stringified-date: "20241103.112009"
title: Bataille finale !
draft: false
template: blog-item
date: 2024-11-02 23:20
update_date: ""
taxonomy:
  type: article
  tag:
    - jeu de rôle
    - writing month
    - writing month 2024
highlight:
  enabled: false
---

Nous avons démarré le dernier scénario de la première saison de notre campagne Donjons & Dragons !

===

![Photo d'un groupe de figurines sur un maillon de chaîne gigantesque en mousse peinte.](IMG_20241102_191534--small.jpg){.figure-media--wide}

La photo représente notre groupe dans la dernière ligne droite de la saison. Tout nous a mené à ce moment, sur ce maillon de chaîne immense dans le plan de l'ombre, vers le sanctuaire où a été vaincue la Première Maladie. Vaincue… mais pas forcément détruite. Et c'est ce que nous allons voir.

De haut en bas et de gauche à droite : Siegwald (avec la citrouille sur la tête), Ashalia, Asphodèle (mon personnage), Physillis, puis, devant, Jasper, Lucinda et Philippe le Magnifique.

(article anti-daté, on a fini la session bien tard)
