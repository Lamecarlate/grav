---
meta-création: "2023-11-11 12:23"  
meta-modification: "2023-11-11 12:23"  

stringified-date: "20231111.122311"

title: "Personnalisation de mon nouveau sac"  
template: "blog-item"  

date: "2023-11-12 12:23"  

taxonomy:
    type: article
    category:
        - création
    tag:
        - couture

# image principale
header_image_file: IMG_20231112_094101--small.webp
header_image_alt: "Photo d'un sac en bandoulière posé à plat sur une table de bois sombre. Le sac est gris-noir, avec une zone de tissu bleu-vert brodé de losanges en fil blanc, et une autre de tissu fleuri. Il y a un pin's représentant une petite souris trop mignonne sur le rabat."

# plugin Highlight
highlight:
    enabled: false

---

Mon fidèle sac bandoulière m'a lâchée après des années de vie commune. Et ce n'est pas réparable… Donc j'ai trouvé un nouveau sac, mais il est un peu tristou. Personnalisons-le !

===

Je n'ai pas de photos du sac avant, c'est un sac bandoulière assez grand, avec une poche pour ordi, et des logos devant. Je pense que c'est un goodie donné à une conférence, ou bien un objet d'entreprise. Je l'ai acheté dans une boutique du Relais, qui revend très peu cher les vêtements en bon état qui sont mis dans les bennes (mais si, vous connaissez sûrement). Fonctionnellement il est parfait pour moi, il est grand, il a l'air solide, il a deux (deux !) lanières, avec un renforcement pour poser sur l'épaule. (le mec idéal)(pardon)

Or donc, les logos. Ça m'importait peu, mais autant les cacher. Et puis, je voulais aussi y accrocher des badges et des pin's (oui j'ai quatorze ans, mais c'est surtout que j'ai reçu en septembre le pin's petite souris de Boulet, et je voulais vraiment l'utiliser).

Mon choix s'est porté sur un reste de satin bleu-vert (j'ai fait une cape dedans il y a quelques années, et aussi la doublure interne plus une décoration pour mon petit sac à clés, écoutez je le rentabilise), pour faire une zone « neutre ». Et j'ai voulu broder un peu avant (j'ai découvert la méthode de broderie sashiko il y a quelques mois, j'ai déjà réparé un sac et un pantalon) !

J'ai choisi un motif assez simple : des triangles. Source : [Free Sashiko Patterns sur The Spruce Crafts](https://www.thesprucecrafts.com/sashiko-embroidery-patterns-1177479).

![Motif régulier de triangles équilatéraux, en pavage.](CF_Sashiko7-56a28d493df78cf772776dab.webp){.media--center}

Les deux photos qui suivent ont une luminosité étrange parce qu'il faisait nuit, le tissu a pris une couleur indéfinissable, mais c'est bien bleu-vert.

Ici, la broderie est faite au sixième (mais en fait le quart, vous allez comprendre) : j'ai la moitié des lignes diagonales « descendantes » faites.

![Photo d'un morceau de tissu avec des lignes diagonales brodées à petits points, qui couvrent à peu près la moitié de la largeur du tissu. Ce n'est pas fini, l'aiguille est encore sur le fil.](IMG_20231028_184356--small.jpg)

Ici, c'est fini au tiers (enfin, la moitié) : toutes les diagonales « descendantes » sont faites, il reste les « montantes », et les lignes verticales. La photo montre également une ligne « montante » en commencement : on devine déjà les losanges.

![Photo d'un morceau de tissu avec des lignes diagonales brodées à petits points, cette fois les diagonales couvrent toute la largeur du tissu. Ce n'est pas fini, l'aiguille est encore sur le fil, et des losanges commencent à se créer avec une nouvelle ligne.](IMG_20231028_194905--small.jpg)

Et quand j'ai terminé toutes les diagonales, je me suis rendue compte qu'il me serait impossible de faire des lignes verticales… Parce que petit décalage après petit décalage (c'est visible sur l'image avec les diagonales pas encore finies, mes traits au crayon ne sont pas exacts), je me retrouve avec des losanges un peu trop déformés pour en faire des triangles. Tant pis ! C'est très bien comme ça aussi.

![Photo du rabas du sac, avec le tissu bleu-vert complètement brodé de losanges désormais cousu sur le rabas.](IMG_20231029_122003--small.jpg)

Puis j'ai réfléchi à la suite : j'ai depuis très, très longtemps un tissu à motif japonais, noir avec des fleurs. J'ai toujours hésité à l'utiliser parce que… eh bien quand on utilise quelque chose, après on ne l'a plus. (oui, je suis docteure es tautologie aussi) Et je suis maladive du « on sait jamais ça peut servir je garde », sans aller, heureusement, jusqu'au syndrome de Diogène. J'étais ce jour-là chez des ami⋅es, nous faisions un week-end DIY (Do It Yourself), et, me voyant désemparée avec mon bout de tissu, à le tourner dans tous les sens, l'un d'elleux me dit, péremptoire : « Allez ! Tu le fais ! Au pire, tu découdras. Sinon tu vas rien faire. » Électrochoc salutaire, je me suis lancée : découpe en diagonale (oui, j'aime ce qui est penché, en témoigne le design actuel de ce site), couture par le bas, et on avance, on avance ! J'ai dû faire un petit pli vers le milieu, pour rétablir l'équilibre (je n'ai toujours pas compris ce qu'il s'est passé), et on dirait vraiment que c'était prévu depuis le début. Pas peu fière.

![Photo du coin inférieur gauche du sac, couvert d'un triangle de tissu japonais, très fleuri.](IMG_20231112_094158--small.webp " On voit bien le pli, et l'angle après le pli est parallèle au précédent."){.figure-media--wide}

Et enfin, le pin's. L'est-elle pas mégachoupie, cette souris gris en train de se taper un camembert à peine plus grand qu'elle ? Remarquez la qualité de fabrication : Boulet est connu pour son design à « petits traits », et ils se retrouvent très bien sur le pin's.

![Photo en gros plan du rabas, on voit un pin's émaillé en creux, représentation une souris grise, assise sur un cambembert dont il manque une tranche, qu'elle tient dans ses pattes avant et dans sa gueule. Elle a une queue, des pattes et des oreilles roses, et de grands yeux noirs brillants. Sa queue s'enroule autour du fromage.](IMG_20231112_094125--small.webp)

J'ai aussi de très jolis patchs thermocollants (ou à coudre) du même dessinateur que le pin's, mais en les mettant près du sac pour tester, je les trouve trop grands au final. Ils iront autre part. Ou j'en ferai des sous-verres (conseil par Boulet lui-même).

C'est dommage que j'aie fini par jeter mes vieux badges Magic lors du dernier Grand Rangement (je n'y joue plus, mais je suis attachée à certaines factions, pour le folklore : Simic, Golgari, ou Temur…).

(D'ailleurs, c'est amusant, en ce moment dans notre campagne Donjons et Dragons 5, je joue une druide du Cercle des Spores, cercle apporté par l'extension « Voyage à Ravnica » de Donj' : les druides des Spores sont des Golgari ! Je l'ignorais au moment de choisir un cercle.)
