---
meta-création: "2024-11-17 11:34"  
meta-modification: "2024-11-17 11:34"  

stringified-date: "20241117.113413"

title: "« Fomalhaut existe vraiment ?! »"   
template: "blog-item"  

date: "2024-11-17 11:34"  
update_date: ""

taxonomy:
    type: article
    tag:
        - astronomie
        - fiction
        - writing month
        - writing month 2024

# plugin Highlight
highlight:
    enabled: false

---
Voilà la phrase qui est sortie de ma bouche hier soir.

===

Le contexte : j'utilise, depuis des années, le logiciel [Variety](https://peterlevi.com/variety/) (page en anglais) pour changer mon fond d'écran. Même si je le vois peu, parce que j'ai très souvent mes fenêtres maximisées, ça me plaît d'avoir accès de de jolies images à chaque fois que j'en ai l'occasion.

En ce moment, j'ai paramétré Variety pour aller chercher une des images sur le site APoD, <i lang="en">Astronomy Picture of the Day</i>, géré par la NASA ; [j'en parlais il y a peu dans mon article de liens](../liens-de-novembre-2024-partie-1). Et hier, vers 23h, il y avait ça sur mon écran :

![Photographie d'un paysage crépusculaire, on voit surtout le ciel empli d'étoiles avec leur nom écrit à côté et le ciel rougeoyant à l'horizon.](AllPlanets_Tezel_1680_annotated--p.webp)

L'image source se trouve sur la page [After Sunset Planet Parade](https://apod.nasa.gov/apod/ap230102.html) (page en anglais).

Et j'ai vu ça :

![Un gros plan sur une partie de l'image, centré sur l'étoile marquée « Fomalhaut ».](AllPlanets_Tezel_1680_annotated_detail--p.webp)

Mon cerveau a fait « sploing ».

Je me suis exclamée la phrase de l'introduction. C'était… une impression bizarre, parce que je ne retrouvais pas pourquoi ce mot, ce nom, me parlait. Peut-être un roman de SF ? Ça sonne comme du Ursula K. Le Guin.

Donc je regarde [la page Wikipédia de Fomalhaut](https://fr.wikipedia.org/wiki/Fomalhaut), en français d'abord, où un paragraphe très succinct « Dans la fiction » mentionne deux œuvres que je ne connais pas, donc, ça ne doit pas être ça… Dans la discussion, rien.

Et puis [la page en anglais](https://en.wikipedia.org/wiki/Fomalhaut  "la page Wikipédia en anglais"), qui a un très intéressant chapitre sur l'étymologie (« la bouche de la baleine » en arabe) et le nom de l'étoile dans différentes cultures, mais rien qui concerne la fiction. Dans la discussion, ça mentionne le fait qu'on l'appelle parfois l'Œil de Sauron (et que ça serait dû à un artefact du coronographe de Hubble, le <i lang="en">lens flare</i> pourrit tout, partout 😄), mais rien de plus.

Ne m'avouant pas vaincue, je recherche bêtement « fomalhaut fiction » sur mon moteur de recherche favori, et le premier résultat est [Catégorie : Fictions se passant dans les environs de Fomalhaut](https://en.wikipedia.org/wiki/Category:Fiction_set_around_Fomalhaut) (en anglais). Il y avait un lien vers cette catégorie dans l'article en anglais et je ne l'ai pas vu… (Je regarde peu souvent les catégories.)

Dans la catégorie, on trouve… _Le monde de Rocannon_, et plus précisément son prologue, _Le collier de Semlé_, qui se passe sur Fomalhaut II. C'est une nouvelle d'Ursula K. Le Guin. Que j'ai lue il y a quelques mois, dans le recueil _Aux douze vents du monde_.

Mon cerveau est un peu un connard mais des fois je l'aime.
