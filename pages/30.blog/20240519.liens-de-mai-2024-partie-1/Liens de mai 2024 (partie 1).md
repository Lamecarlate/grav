---
meta-création: "2024-05-01 22:02"  
meta-modification: "2024-05-01 22:02"  

stringified-date: "20240501.220230"

title: "Liens de mai 2024 (partie 1)"   
template: "blog-item"  

date: "2024-05-19 10:02"  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - cinéma
        - humour
        - science
        - création
        - histoire
        - sociologie

# plugin Highlight
highlight:
    enabled: false

---

Un documentaire sur un film qui parle d'une série fictive prise pour un documentaire, de l'impro, de la physique maison, de la feutrine et du chaton de pixels, de l'horreur, du doux, du drôle.

===

[Galaxy Quest: You've Never Heard it Told Like This Before!](https://www.youtube.com/watch?v=xQLQ4XO-WUM) (vidéo avec du son, en anglais, sous-titres anglais un peu approximatifs, sous-titres français pas mal à l'arrache, 50 minutes, plein de divulgâchage, attention). Ohlalala. Galaxy Quest. Ce film est une merveille. Parodie tout à fait sincère et adorable de Star Trek, il raconte l'histoire d'une bande d'acteurs et actrices un peu ringards, qui ont eu leur heure de gloire dans une série de SF… et qui se font enlever par des extra-terrestres persuadés que c'était un documentaire, leur demandant leur aide contre un ennemi implacable. C'est drôle, bien joué, touchant, sans jamais être méchant. Et la vidéo que je vous propose est un documentaire (un vrai) sur le film, racontant mille anecdotes. Je pensais que la production avait eu le nez creux en prenant Sigourney Weaver (Ripley dans Alien, tmtc comme disent les jeunes ok j'arrête c'est gênant) dans le rôle de la meuf écervelée de service, en contre-emploi parfait. Mais en fait, c'est Weaver elle-même qui a insisté comme pas possible pour avoir le rôle ! Ce que je trouve encore plus admirable. Bref : regardez le film (la version française est très bien traduite et doublée), puis cette vidéo.

["Never Has The Word No Been Used So Forcefully" | Whose Line Is It Anyway?](https://www.youtube.com/watch?v=8NyqPOAY2iI) (vidéo avec du son, en anglais, pas de sous-titres, 5 minutes) : whoa. L'improvisation, déjà, ça m'impressionne, mais avec du rap, rythmé et rimé, c'est encore plus fort. À noter que les participants se connaissent déjà très bien, bossent ensemble depuis des années, donc il y a sûrement des routines et du travail préalable, sans connaître le thème imposé, bien sûr – et ça ne diminue pas le résultat le moins du monde.

[How To Make Galinstan](https://www.youtube.com/watch?v=u1Ijupdjv_I) (vidéo d'une minute, avec du son, en anglais, sans sous-titres) : ahhhhh je veux je veux je veux. Je sais pas ce que j'en ferais mais ahhhh c'est trop bien. (et en plus Michael utilise des Lego Technic, cet homme a bon goût)

[Neko: History of a software pet](https://eliotakira.com/neko/) : connaissez-vous ce petit logiciel, neko, xneko, oneko (le nom varie selon les versions) ? Ce petit chat (ou chien, ou manchot) chassait le curseur ou roupillait sur vos fenêtres. Cette page raconte son histoire, et propose même de voir de nombreuses variantes… dont un Tentacule Pourpre (voir [mon épiphanie](../20220901.20220901201840/20220901201840.md)) ! (via [bhtooefr](https://snack.social/notice/Ahga94AkOtsuCmTfyy))

[Smoking is awesome](https://www.youtube.com/watch?v=_rBPwu2uS-w) (vidéo avec du son, en anglais, sous-titres en anglais, 12 minutes) : oumf, Kurzgesagt a encore frappé. J'avais découvert cette chaîne avec une vidéo sur les vaccins, qui avait un titre assez provocateur pour attirer les détracteurices des vaccins et leur montrer des faits, là, c'est la même tactique. Oui, la nicotine provoque des effets bénéfiques sur le corps et le cerveau… pendant quelques minutes, et après, c'est la spirale. À voir absolument.

[ChatGPT rêve-t-il de cavaliers électriques ?](https://www.youtube.com/watch?v=6D1XIbkm4JE) (vidéo avec du son, avec sous-titres, 50 minutes) : MonsieurPhi est vraiment passionné par les LLM depuis quelques temps, c'est super intéressant et ça ouvre des perspectives de réflexion. Quant à moi je ne sais toujours pas ce que j'en pense – mais je refuse toujours d'utiliser ces outils, pour des raisons écologiques.

J'ai découvert la créatrice Andrea Love il y a 3 ans à peu près, j'adore son travail sur la feutrine (j'avais déjà parlé d'elle en [décembre 2023](../20231215.liens-décembre-partie-1/Liens%20de%20décembre%20(partie%201).md)). Ici c'est une compilation de tous ses (très-)courts-métrages sur le sujet du [petit déjeuner](https://www.youtube.com/watch?v=EbSqjO-_nXs) (vidéo avec du son, sans paroles, sans sous-titres, 2 minutes). A'm'fait des frissons dans le dos. Et je pense que je vais me faire des gaufres sous peu.

[Cause, je te présente conséquence](https://mstdn.social/@yurnidiot/112367945916050327) (gif animé), avec un chat à qui… il arrive un truc (absolument pas grave, ne vous inquiétez pas).

[Analyse du court-métrage "This house has people in it"](https://www.youtube.com/watch?v=dxCQUgYEiM4) (vidéo avec du son, pas de sous-titres, trois heures 😮 mais ça vaut le coup). C'est… quelque chose. Ce court-métrage de 10 minutes a complètement retourné le cerveau de plein de gens. Il peut être vu seul, c'est un truc très bizarre, avec un peu d'horreur et de fantastique, et il se suffit à lui-même. Mais c'est, si vous me pardonnez cette expression galvaudée, la partie émergée de l'iceberg, en fait. Il y a beaucoup, beaucoup plus. Pendant des mois (des années ?) les gens ont fouillé autour du court-métrage et de son univers, et il fallait bien trois heures pour faire la synthèse. Comme le dit souvent Feldup, installez-vous confortablement, prenez à boire et à grignoter, et laissez-vous porter. (oui, j'ai découvert ce jeune vidéaste il y a quelques semaines, et je regarde ses archives donc forcément j'en partage pas mal ici 😛)

Je découvre l'existence du [docteur James Barry](https://fr.wikipedia.org/w/index.php?title=James_Barry_(m%C3%A9decin)&oldid=215051691), via [Sophie Labelle](https://assigneegarcon.tumblr.com/post/750295456911884288). Homme trans ou femme travestie de manière permanente, il a en tout cas été un sacré personnage et un très bon médecin. Le comics de Sophie dit qu'il est le premier européen à avoir opéré une césarienne, c'est un peu manipuler la réalité : il est le premier européen à l'avoir fait en Afrique.

[The Seeds we sow](https://www.youtube.com/watch?v=7LXqoYyQZYU) (vidéo avec du son, en japonais sous-titré en anglais, sous-titres incrustés, 30 minutes). C'est vert, c'est doux, doux-amer  aussi, et joyeux, et ça réveille encore une fois la petite personne en moi qui voudrait peut-être (peut-être) changer de métier et aller mettre les doigts dans la terre. Via [Joachim](https://boitam.eu/@joachim/111187675927665214), qui le décrit de façon plus factuelle que moi :

> Le Japon des paysages de Miyazaki (le réalisateur, la préfecture), le lien des paysans avec le paysage, le vieillissement de la population.

Je ne connaissais pas le jeu Buccaneer, mais voici [une belle histoire](https://hackers.town/@CyberpunkLibrarian/112046696080448828) à son sujet.

[Pierre Desproges refuse de draguer sur un banc (avec Evelyne Grandjean)](https://www.youtube.com/watch?v=Q7oSQMTYMSw) (vidéo avec du son, sans sous-titres, 5 minutes). Eh oui, sachez-le, quand on est sur un banc avec une autre personne, on n'est pas obligé⋅e de draguer. Et Desproges trouve très, très important de le préciser. Plusieurs fois. Non mais oh. Des fois qu'il voudrait finir son livre.

---

J'ai fini par céder et j'ai acheté un nouveau fauteuil de bureau. Le précédent, je l'avais récupéré du boulot, il a toujours couiné et grincé, et ça me va… enfin ça m'allait. Là, finalement, c'était le grincement de trop (oh, et il descendait tout seul parfois). J'essaie au maximum d'acheter en seconde main, mais ce n'est pas toujours possible, et pour cette fois, j'avais envie de neuf. Garantie 10 ans, écoutez, on va le rentabiliser ce fauteuil.