---
meta-création: 2024-11-10 19:01
meta-modification: 2024-11-10 19:01
stringified-date: "20241110.190140"
title: La belle Verdurière (détail)
template: blog-item
date: 2024-11-10 19:01
update_date: ""
taxonomy:
  type: article
  tag:
    - writing month
    - writing month 2024
    - photographie
    - art
highlight:
  enabled: false
subtitle: En décembre 2023, j'étais à Besançon, et j'ai visité le musée du Temps.
---

En décembre 2023, j'étais à Besançon, et j'ai visité le musée du Temps.

===

Dans (je crois bien) la salle de la cheminée, il y avait ce tableau, « La belle Verdurière », pièce hollandaise du XVI<sup>e</sup> siècle. J'ai été captivée par un détail, le bracelet de la femme au centre du tableau.

![Très gros plan d'un tableau, sur le poignet d'une femme blanche, qui porte un bracelet ouvragé, fait de perles grises, et de pierres (rouges ou noires) encastrées dans de l'or.](IMG_20231206_160530--small.webp)

La précision, la lumière sur l'or, les perles et les pierres, tout cela en quelques traits. Je suis très fan.

Apparemment ce tableau a été au Louvre aussi, il fait partie des [collections du Département des Peintures](https://collections.louvre.fr/en/ark:/53355/cl010061598).
