---
meta-création: 2024-11-01 18:32
meta-modification: 2024-11-01 18:32
stringified-date: "20241101.183255"
title: Tailler du bois
draft: false
template: blog-item
date: 2024-11-01 18:32
update_date: ""
taxonomy:
  type: article
  tag:
    - travail du bois
    - artisanat
    - writing month
    - writing month 2024
highlight:
  enabled: false
---

Avec des ami⋅es, on a fait un atelier de taille de bois au couteau.

===

De prime abord, j'ai eu un peu peur, avec le panneau « méditation et faciathérapie » devant l'entrée, et la dame nous a fait faire un peu de relaxation et d'étirements, connexion au corps, tout ça, machin. Pour elle, la spatule ou le couteau qu'on allait fabriquer n'était pas le but, mais un bonus. Pas très emballée, je me suis dit que ça ne vaudrait pas le coup de refuser tout en bloc, donc j'ai fait les étirements et attendu patiemment qu'on commence à tailler.

Elle avait coupé du noisetier juste avant qu'on arrive, et la première étape a été de fendre les rondins pour avoir six blocs à travailler. En tandem, l'un tenant la hache, l'autre tapant avec le burin. Des coups lents, espacés par le craquement du bois qui se fend tout seul, comme un chant, comme le son d'un feu qui crépite. Très agréable à écouter.

Le bois vert, c'est humide, c'est frais au toucher, et ça restera frais pendant les trois heures de travail. Et ça sent la noisette pas mûre.

![Sur une chaise, un rondin fendu en deux, et un couteau.](IMG_20241101_150339--small.jpg)

Puis, on écorce au couteau. C'est facile. C'est la *partie* facile. Ensuite, on égalise un peu le côté intérieur, pour aplanir, et surtout enlever le cœur du bois, qui, s'il est encore présent, risque de sécher d'une manière différente du reste, et provoquer des fissures.

![Sur mes genoux, un demi-rondin en cours de taillage, on distingue de moins en moins le cœur, une ligne rouge centrale.](IMG_20241101_152735--small.jpg)

Pour aplanir plus vite, chacun et chacune a fait un ou plusieurs passages sur un [banc d'âne](https://fr.wikipedia.org/wiki/Banc_d%27%C3%A2ne). C'est un outil d'une belle simplicité, où l'on referme une sorte de mâchoire autour de la tranche de bois en poussant avec les pieds sur un levier. Une fois la tranche bien maintenue, on rabote doucement avec une [plane](https://fr.wikipedia.org/wiki/Plane), un couteau à deux manches. C'est tellement agréable à tenir et à manipuler, et en effet, on a réussi à enlever beaucoup d'épaisseur en peu de temps.

![Une femme assise sur le banc d'âne refermé sur un morceau de bois, tirant la plane vers elle.](IMG_20241101_154333--small.jpg)

J'ai choisi de faire une spatule asymétrique, avec un manche un peu courbé, pour suivre la forme de la branche.

Comme le temps passait, et que je galérais pas mal, la dame m'a donné plusieurs fois des coups de main, passant même à la hache pour enlever une grande partie à l'extérieur de la courbe du manche.

Ma spatule n'est pas complètement finie, il lui reste des aspérités. Deux des ami⋅es ont acheté un couteau, pour les mettre en commun, peut-être que je pourrais reprendre ça avant que le bois ne sèche…

Dans l'ensemble, j'ai bien aimé ! J'aurais voulu avoir plus de temps, trois heures ça part vite quand on est concentrée sur quelque chose. Et je suis contente d'avoir une jolie spatule, un peu grossière mais hé, c'est moi qui l'ai faite, et elle est utilisable.

![Sur un plateau de bois, une spatule de bois vert, un peu courbée, asymétrique.](IMG_20241102_143049--small.jpg)

Bonus du lendemain : j'ai donc pris une grosse demi-heure pour retravailler la spatule. Au bout d'un moment, j'ai quand même décidé d'arrêter, le mieux étant l'ennemi du bien. Elle est un peu plus ronde, plus lisse, mais je manque encore de technique (bien entendu) donc parfois j'ai creusé au lieu d'aplanir… À noter que l'artisane nous a fortement déconseillé de finir au papier de verre, qui risquait de faire des micro-abrasions partout et de laisser entrer les bactéries et champignons, ce que le couteau ne provoque pas. Donc je vais résister à mon envie de poncer tout çaaaaa et admirer ma spatule qu'elle est quand même très belle.

![Sur un plaid vert sauge, la même spatule, un peuuuuu plus lisse.](IMG_20241103_114025--small.jpg)
