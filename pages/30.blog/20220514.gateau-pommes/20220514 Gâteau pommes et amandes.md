---
title: "Gâteau aux pommes et amandes"
date: "2022-05-14 19:00"
template: blog-item

taxonomy:
    type: article
    category:
        - cuisine
    tag:
        - sucré
        - pomme
        - amande
        - gâteau

# image principale
header_image_file: 20220513-gateau-pommes.jpg
header_image_alt: "Gâteau rond recouvert de tranches de pommes caramélisées"

# plugin Highlight
highlight:
	enabled: false

---

Toutes les deux semaines, je récupère un kilogramme de pommes. Et même si j'aime bien en manger simplement come ça, elles s'accumulent un peu. Donc j'étais bien contente de trouver une recette qui en utilise 500g ! Et en plus c'était très bon. (oui parce que sinon je n'aurais pas fait d'article)

===

La recette originelle est ici : [Gâteau pommes noisettes absolument irrésistible, chez la Fée Stéphanie](https://www.lafeestephanie.com/2022/03/gateau-pommes-noisettes-absolument-irresistible.html)

J'ai juste fait quelques ajustements : poudre d'amande de mon côté (parce que j'avais la flemme de mixer des noisettes), bicarbonate de sodium uniquement (j'ai mis environ 4g pour remplacer les 8g de levure chimique plus la cuillère à café de bicarbonate), et une cuisson un peu plus longue à 170°C (j'avoue que je ne sais pas très bien pourquoi je n'ai pas respecté les 180°C de la recette…). Ah et puis j'ai coupé les fruits au couteau à légumes parce que je n'ai pas de mandoline.

![Gâteau entamé, on voit l'intérieur, il y a des tranches de pomme](20220513-gateau-pommes-2.jpg?resize=640)

La moitié des pommes va sur le dessus du gâteau, c'est voulu, ne faites pas l'impasse sur ce détail ! Les fruits à l'intérieur de la pâte sont moelleux, presque fondus, ceux sur le dessus sont croustillants, mâchants, et caramélisés - et la peau ajoute aussi un petit quelque chose, une meilleure tenue, sans parler du visuel.

![Tranche de gâteau vue par dessus, on ne voit que la couverture de tranches de pomme](20220513-gateau-pommes-3.jpg)

Je conseille grandement la recette, elle est simple : couper et réserver, mélanger, dresser, enfourner. Même la disposition en cercle des tranches de pomme sur le dessus est rapide - et au pire, jeter les fruits en pluie devrait être tout aussi bon !

Bin c'est pas tout ça mais moi je vais me resservir.