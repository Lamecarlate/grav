---
meta-création: "2023-09-02 07:39"  
meta-modification: "2023-09-18 18:37:31"  

stringified-date: "20230902.073903"

title: "Liens de septembre (partie 1)"  
template: "blog-item"  

date: "2023-09-18 18:00"  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - art
        - sciences
        - humour
        - cuisine
        - informatique
        - histoire
        - linguistique

# plugin Highlight
highlight:
    enabled: false

---
On continue l'expérience 😸 Cette fois, en deux parties, comme on m'a effectivement remonté que ça faisait beaucoup d'un coup.

===

## 1<sup>er</sup> septembre

[Un mille-pattes qui marche avec détermination](https://mstdn.social/@Pandamoanimum/110938878079583627) (vidéo avec du son)

![Un petit cochon à la bouille sympathique, peint à la gouache](rotund-piggie_by_vmaderna.jpg) 
Source : [Victoria Maderna](https://mastodon.art/@vmaderna/110939676950403111)

[Les chiens ne voient pas la vie en rose. Ni en noir et blanc](https://theconversation.com/les-chiens-ne-voient-pas-la-vie-en-rose-ni-en-noir-et-blanc-212079) via [Khrys](https://mamot.fr/@Khrys/110971658852996770).

[L'alphabet symétrique de Scott Kim](https://mastodon.ie/@stancarey/110985365591678908), pas très utilisable mais une très jolie œuvre d'art typographique.

[Fresque murale, Paris, 12<sup>e</sup>](https://piaille.fr/@Isilfae/110984303186757714) : ça fait un peu mal au cerveau mais c'est beau.

<div lang="en" markdown="1">
HOW TO WRITE GOOD, by Frank L. Visco

1. Avoid Alliteration. Always.
2. Prepositions are not words to end sentences with.
3. Avoid cliches like the plague. (They’re old hat.)
4. Employ the vernacular.
5. Eschew ampersands & abbreviations, etc.
6. Parenthetical remarks (however relevant) are unnecessary.
7. It is wrong to ever split an infinitive.
8. Contractions aren’t necessary.
9. Foreign words and phrases are not apropos.
10. One should never generalize.
11. Eliminate quotations. As Ralph Waldo Emerson once said, “I hate quotations. Tell me what you know.”
</div>

## 2 septembre

[Big cat](https://mastodon.social/@stevelieber/110991377505473992) .

[Entraîner des Pokémon](https://twitter.com/Doodlelot/status/1695387160789082538#m) (comme l'avenir de Twitter/X ne me paraît pas bien stable, j'ai [archivé la page](https://web.archive.org/web/20230902090934/https://twitter.com/Doodlelot/status/1695387160789082538) ; pour voir les images en entier, clic droit > Ouvrir l'image dans un nouvel onglet)

[Comparaison entre un minibus et un SUV](https://mstdn.ca/@mdruker/110453103882472006) : j'enrage. Combien de personnes peut-on transporter dans chaque ?

![La voie lactée derrière les Sept géants de l'Oural](milky-way.jpg)

Photographe : Sergei Makurin

Source : [World Beauty](https://universeodon.com/@world_beauty/110990957473300735) (qui a inexplicablement un macaron Twitter vérifié… c'était la mode à un moment dans le fédiverse, plutôt pour se moquer)

[Mouton de mer](https://www.youtube.com/watch?v=vy5WTdjtCLM) ; j'allais beugler « nudibranche », mais non, en fait, les [_Costasiella kuroshimae_](https://fr.wikipedia.org/wiki/Costasiella_kuroshimae) sont de l'ordre des Sacoglossa et non des Nudibranchia, même si ça se ressemble. Tenez, je vous mets [la page Wikipédia des nudibranches](https://fr.wikipedia.org/wiki/Nudibranchia) en plus, les images sont tellement belles.

![Focaccia avec des fleurs faites de ciboulette, tomates et câpres](focaccia.jpg)

Source : [Une jolie focaccia fleurie par Rikiti](https://piaille.fr/@rikiti9/110996193181152999).

[Des dahlias](https://ecoevo.social/@heralyn/110969869438176088).

La fois où Karl Dubost a parsé des fichiers html avec une regex et n'aurait pas dû : [The lucky day of me falling hard professionally](https://www.otsukare.info/2023/08/31/career-mistakes). Une des réponses au pouet où Karl postait le lien vers son article menait vers [une sublime réponse sur StackOverflow](https://stackoverflow.com/questions/1732348/regex-match-open-tags-except-xhtml-self-contained-tags). Lisez bien jusqu'au bout, c'est un peu répétitif au début mais ça vaut la peine. Vraiment.

[James Webb Space Telescope gazes into the Whirlpool galaxy's hypnotic spiral arms](https://www.space.com/james-webb-space-telescope-captures-vortex-whirlpool-galaxy): 😻

![La galaxie du Tourbillon, photographiée avec le James Webb Space Telescope](whirlpool-galaxy-jwst.webp)

## 4 septembre

[Des pokémon faits par Boulet](https://octodon.social/@Bouletcorp/110990228167749784).

Je n'avais jamais entendu parler de ces boîtiers censés réduire la consommation électrique d'une maison. Bin c'est une arnaque. [L'escroquerie du fameux boitier économiseur d'électricité](https://www.youtube.com/watch?v=1FLwy4XPBg0) chez Deus Ex Silicium (vidéo avec du son).

[I secretly learned my mother's language](https://www.youtube.com/watch?v=wPJ0avBurdQ) (vidéo avec du son) via [Krysalia](https://mastodon.social/@Krysalia/111001553859322604) : super mignon, une jeune femme apprend l'urdu, la langue de sa mère, pour la surprendre. La vidéo est en anglais et elle parle trèèèès vite, pfiou.

[Abbaye en rénovation](https://social.sciences.re/@grototo/111001477092902296).

## 6 septembre

[Daniel Radcliffe and Erin Darke welcome first child](https://nitter.poast.org/BBCNews/status/1650886622009761792) (en anglais) : les réactions sont à vomir. Les gens traitent Erin Darke de femme transgenre parce qu'elle est plus grande que son compagnon Daniel Radcliffe, qui est en effet assez petit. Ça me tue : pourquoi des gens se ruent en masse pour insulter (oui parce que pour ces personnes-là, « transgenre » c'est une insulte, cherchez pas).

[Lunch](https://mastodon.social/@Chrishallbeck/111014773391276519) (comic de Chris Hallbeck).

Il y a un bot qui poste des extraits des _Deux minutes du peuple_ de François Pérusse et c'est tellement cool : [extrait des mauvais présentateurs de foot](https://piaille.fr/@2minutesdubot/111014903914323019).

[Les bains douches Delessert à Lyon sont gratuits](https://www.lyon.fr/lieu/bains-douches/bains-douches-delessert) via [Quineapple](https://framapiaf.org/@quineapple/111014497494000387) : 

> Accueil inconditionnel et gratuit de tout public pour l’accès à l’hygiène. Remise d’une serviette, savon et shampooing.

Je trouve ça tellement bien ! Et dans les réponses il y a des infos pour plusieurs autres villes.

Pour accéder à son CPF, il faut passer par France Connect + et c'est une purge : cela demande l'application [Identité Numérique de la Poste](https://piaille.fr/@louischance/111017272118028241), qui n'est pas installable autrement que via Google Play (pour Android) : j'ai vécu ça aussi. On peut aussi essayer de recevoir un recommandé numérique, mais ça demande de photographier sa carte d'identité, et ça n'a jamais marché avec moi. Et pourtant j'ai essayé dans plein de conditions différentes.

[Difference between a maze and a labyrinth](https://www.english-heritage.org.uk/visit/inspire-me/blog/blog-posts/whats-the-difference-between-a-maze-and-a-labyrinth) (en anglais) ; en français, la distinction est moins claire. À la rigueur, on a les labyrinthes parfaits et les labyrinthes à îlots, ou bien les labyrinthes (tout court) et les dédales, mais cela ne semble pas systématique. À noter que labyrinthe est aussi utilisé pour les trajets au sol, dans les églises par exemple. L'idée n'est pas d'être perdu⋅e mais de suivre une procession.

## 7 septembre

[Deter, vener, mais fluffy](https://mamot.fr/@Eris_Lepoil/111022408291754977).

[Diagramme de Venn entre curl et wget](https://daniel.haxx.se/blog/2023/09/04/the-curl-wget-venn-diagram/) chez l'auteur de curl, via [Sebsauvage](https://sebsauvage.net/links/?4Ptw4A).

Heureusement que ces [petits chiens qui se cassent la binette](https://mstdn.social/@yurnidiot/111020429521969157) sont ronds et pleins de poils, ils ne se font pas mal.

Harcèlement scolaire : [très beau texte de Félix Radu](https://www.instagram.com/reel/Cw2IG6FAUSo/) (vidéo avec du son), via [Boris Schapira](https://framapiaf.org/@borisschapira/111024654044690587).

## 8 septembre 

[Nobody's driving](https://www.okdoomer.io/nobodys-driving/) via [Khrys](https://mamot.fr/@Khrys/111025831108543895): un article (en anglais) assez déprimant sur l'absurdité croissante de notre monde et pourquoi les ados sont si tristes et remonté⋅es. 

[I believe I can fly](https://mstdn.social/@yurnidiot/111026044751755729) : un chat qui saute des obstacles de plus en plus haut (observez les patounes).

[Pilates des Caraïbes](https://framapiaf.org/@jeeynet/111028671152153755).

[La grande inondation de mélasse de Boston](https://www.youtube.com/watch?v=KMWrk_94L8Y) (vidéo avec du son, en anglais) : une histoire assez connue, mais c'est toujours intéressant d'y revenir. 

> un raz-de-marée de mélasse avançant à une vitesse estimée à 56 km/h

(source [Wikipédia](https://fr.wikipedia.org/wiki/Grande_inondation_de_m%C3%A9lasse_de_Boston))

Voilà voilà.

[Crusher of souls](https://mastodon.social/@warandpeas/111030335977682624) (en anglais).

[Copains depuis toujours](https://mastodon.world/@That_One_Guy/111024199347920755).

## 9 septembre

[Autocorrect](https://mstdn.social/@spaf/110998219294433954) (en anglais) : petit comic de type <i lang="en">meme</i>, et bon sang, c'est bien vrai.

[Le cycle de l'inaccessibilité](https://kolektiva.social/@shrugdealer/110985297693511061) (en anglais).

[Un panneau routier en Islande](https://mastodon.opportunis.me/@Grandasse_/110997108040995713) : pfiou, c'est chargé.

Pour dire « légende » dans le contexte d'une carte en arabe, on a un mot qui signifie littéralement « la clé de la carte » 😻 (source : [butterflyofZbeul](https://mstdn.fr/@ButterflyOfFire/110997641510285535)).

[Un joli chat](https://eldritch.cafe/@Divine_lu/110997403230288165) (et un coussin qui ressemble teeeeeellement à un speculoos).

[Détecter l'absence d'un bloqueur de pub](https://github.com/stefanbohacek/detect-missing-adblocker/tree/master) : ohhhh. Si je comprends bien, le principe c'est d'afficher un élément avec une belle quantité de classes détectées par les bloqueurs. Si cet élément est visible, c'est qu'il n'a pas été intercepté et caché, et donc on peut supposer qu'il n'y a pas de bloqueur. Ici c'est un plugin Wordpress, qui insère ce bloc, mais en fait, on peut probablement juste récupérer le code de la ligne 79 à 87 du [fichier php](https://github.com/stefanbohacek/detect-missing-adblocker/blob/master/ftf-detect-missing-adblocker.php), et l'adapter à son besoin (argl, un `<span>` avec un `onClick`, <i lang="la">vade retro</i>).

[Une maison de thé](https://mastodon.gamedev.place/@birdibirdson/111030325456978430). Littéralement.

[Une série de champidragons](https://imaginair.es/@SvanDendragon/110961409904602172).

[Poti serpent](https://mastodon.cloud/@c0nc0rdance/110956668196142835) (en anglais, mais c'est surtout des images de serpent).

[Become as cats](https://eldritch.cafe/@yorha/111016869318069392) : jolies photos avec des chats et une sculpture, réplique d'une machine de Nier Automata.

[Quand a-t-on cessé de parler latin en Afrique ?](https://www.youtube.com/watch?v=15lziZ2ch6M) chez Monté de Linguisticae, via [Augier](https://diaspodon.fr/@AugierLe42e/111034171467925101) : intéressant résumé de l'histoire (politique et linguistique, et ça va souvent de pair) du Nord de l'Afrique, qui a parlé latin pendant un bon bout de temps.

Attention, [blague de niche](https://social.mochi.academy/@Miari/111025538777597420), qui combine deux langues et deux disciplines. Je suis : ultra-fan. 

[Just found you](https://www.davidrevoy.com/article990/just-found-you) : damn, le mirifique David Revoy a encore frappé, avec un dessin tout tendre (et incohérent : comment ce petit chat peut-il être si blanc vu comme il pleut ?).

[Les diagrammes de Venn, c'est dépassé](https://masto.ai/@stavvers/110208147684752206) (en anglais).

[Un petit tardigrade](https://nitter.poast.org/tardigradopedia/status/1699088946871824611#m) (vidéo, sans son) : l'est-i pas choupi ?

[La lecture sensible, c’est presque l’inverse d’une censure](https://www.telerama.fr/livre/kevin-lambert-repond-a-nicolas-mathieu-la-lecture-sensible-c-est-presque-l-inverse-d-une-censure-7017071.php) : point de vue intéressant d'un auteur blanc dont un des protagoniste est afro-québécois, et qui a fait appel à une consultante éditoriale… ce pourquoi on le fustige parce que "sensitity reading" === « on peut plus rien dire », pour certains…

Au secours, <i lang="en">rabbit hole</i> en vue : je viens de découvrir le [le sub-reddit <i lang="en">TIL (Today I Learned)</i>](https://www.reddit.com/r/todayilearned/) !

[Laika, sur Crow Time](https://www.webtoons.com/en/canvas/crow-time/laika/viewer?title_no=693372&episode_no=109) : je pleure pas, c'est toi tu pleures. (bon, la puriste en moi veut dire que Laika est le premier animal qui a été envoyé _publiquement_ dans l'espace : on se doute bien que de nombreux autres essais ont été faits et tus, c'était… un moment compliqué, où la propagande comptait plus que tout 😿).

## 11 septembre

[Des moutons qui traînaillent sur une île d'Écosse](https://zirk.us/@EclecticHuman/111039465466974237).

## 12 septembre

[Un documentaire sur le clip en apesanteur de OK Go](https://www.youtube.com/watch?v=YwyXLBQUEC0) (vidéo avec du son, en anglais) : impressionnant de voir la technique derrière [le clip](https://www.youtube.com/watch?v=LWGJA9i18Co).  J'aime bien ce groupe, pas tant pour leur musique, que je trouve sympa sans plus, mais pour leurs clips, toujours très inventifs et majoritairement en plan-séquence. Même si je grince des dents parfois parce qu'il y a beaucoup de destruction physique d'objets fonctionnels, comme une télé ou une dizaine de guitares 🙀… mais bon, c'est de l'art, on va dire qu'il y a un passe-droit 😸

## 13 septembre

Suite au résultat du jeu Pédantix du jour (Odin), je découvre qu'il a deux frères… Très peu mentionnés, Vili et Vé participent à la création du monde et des humains.

> Odin leur donna le souffle et la vie, Vili l'intelligence et le mouvement, Vé l'apparence, la parole, l'ouïe et la vue.

Oh, et aussi, ils se partagent son héritage et sa femme Frigg quand Odin disparaît. La fraternité 🙃

Sources sur Wikipédia : [Vili](https://fr.wikipedia.org/wiki/Vili_(mythologie)), [Vé](https://fr.wikipedia.org/wiki/V%C3%A9_(dieu)).

## 14 septembre

J'avais oublié l'existence d'[INTERCAL](https://fr.wikipedia.org/wiki/INTERCAL) ! J'adore oublier et retrouver les choses comme ça. Ce langage est… merveill'horrible.

> $ remplaça ¢ en tant qu'opérateur MINGLE (« _mélanger_ »), lequel « représente le coût croissant du logiciel comparativement au matériel »

> Ce dernier mot-clé [PLEASE] fournit deux raisons au compilateur pour rejeter le programme : si "PLEASE" n'apparaît pas assez souvent, le programme est considéré comme insuffisamment poli, et le message d'erreur en fait part ; s'il apparaît trop souvent, le programme peut être rejeté pour cause d'obséquiosité. Bien que cette fonctionnalité existât dans le compilateur INTERCAL d'origine, elle n'était pas documentée. 

## 15 septembre

[La vraie évolution des starters Pokémon](https://app.schildi.chat/#/room/!uxWXIXVvBRtgUTQFWE:tedomum.net) ([explication de la blague](https://fr.wikipedia.org/wiki/Carcinisation)).

J'aime beaucoup la verve de Lizzie : [que faire contre la faille de sécurité WebP](https://corneill.es/@crowdagger/111062204532106059).

De bien belles chauve-souris : 

« 'Bats in the Fifth Act' - Tsukioka Yoshitoshi. »

![Estampe japonaise représentant deux chauves-souris, celle de gauche se débat au sol, celle de droite porte un parapluie, et il y a un accordéon par terre](bats.jpg)

[Source](https://mas.to/@curiousordinary/111066089731958042)

En cherchant une version plus grande je tombe sur l'article [I'm going bats (part two)](https://printsofjapan.wordpress.com/2012/12/21/im-going-bats-part-two-mostly-japan/) (en anglais). Et j'y découvre ceci :

> This image is a parody of Act 5, ‘The Yamazaki Highway’, of the Chūshingura which includes an assault, a robbery and an umbrella.

Je me disais bien qu'il y avait une action qui m'échappait, mais oui, c'est ça : la chauve-souris de droite est en train de voler le sac de celle de gauche (qui en a lâché son accordéon).

L'article est super intéressant, il montre plein de magnifiques bêtes. J'aime beaucoup le lien qu'il fait entre les embryons de chauve-souris et les sculptures de pleurants, c'est un joli saut de pensée.

## 16 septembre

[Edison à la conquête de Mars](https://www.youtube.com/watch?v=tiAGiFlrNoY) (vidéo avec du son).

[Un insecte qui change de couleur](https://nitter.poast.org/temptoetiam/status/969446915640238081#m)(vidéo).

## 17 septembre

[This happened this afternoon in The Hague at the demonstration of Extinction Rebellion](https://mstdn.social/@heideroosje/111076332351245227) (vidéo avec du son).

---

Voilà pour cette fois. On sent bien la différence entre les week-ends et les jours de semaine, hein ? (exception faite du samedi 16, où j'étais dans le coaltar après mon rappel de vaccin Covid-19)(faites-vous vacciner, les gens, j'ai oublié/traîné pendant plus d'un an, c'est vraiment pas top)

