---
meta-création: 2024-11-15 18:15
meta-modification: 2024-12-05 18:15
stringified-date: "20241115.181540"
title: Liens de novembre 2024 (partie 2)
template: blog-item
date: 2024-12-05 18:15
update_date: ""
taxonomy:
  type: article
  tag:
    - liens
    - musique
    - mignonneries
    - histoire
    - sciences
    - ingénierie
highlight:
  enabled: false
subtitle: "On n'arrête pas les mignonneries. Et sauver des gens en cartographiant les quartiers, c'est bien, non ? Des véhicules, du dév web rigolo, de la musique qui fait *\\*gloung\\**, la mer qui rapporte de jolies choses. Et : PATOUNES."
---
On n'arrête pas les mignonneries. Et sauver des gens en cartographiant les quartiers, c'est bien, non ? Des véhicules, du dév web rigolo, de la musique qui fait *\*gloung\**, la mer qui rapporte de jolies choses. Et : PATOUNES.

===

[La surprise est partagée](https://pixelfed.social/p/nhoizey/763087284820151183) (image avec texte alternatif et description en anglais) : mais quelle mignonne petite hermine !

---

[Fellowship – Hold Up Your Hearts (again)](https://www.youtube.com/watch?v=QYyxnpordVo) (vidéo avec du son, chanson, en anglais, pas de sous-titres mais les paroles sont dans la description, 5 minutes). Power metal, costumes bariolés, un barde avec un poulet, et un dragon ! (et des cœurs avec les mains). Ça met une de ces pêches, ohlala. J'ai écouté ce morceau trois fois aujourd'hui, dont deux fois avec le clip, le sourire s'élargissant à chaque fois devant leurs trouvailles visuelles, et l'ambiance générale. Via [L'ours inculte](https://piaille.fr/@oursinculte/113487202110229341).

<blockquote lang="en">Our aim is to make classic guitarey power metal in a way that doesn't make us cringe too hard. Also we dress like hobbits, so it's not going too well. Hopefully the music makes up for the silly costumes ;P</blockquote>

Mais au contraire, c'est ça qui est super cool, de voir des gens sautiller en costume de hobbit en chantant qu'on va y arriver, on va vaincre ce vilain magicien et trouver le trésor. Source : la [page Bandcamp](https://fellowshipmetal.bandcamp.com/) du groupe.

D'ailleurs, l'intro fait teeeeellement générique de début d'animé japonais, j'ai envie de dessiner quelques poses (genre vers 3:29 quand tout le monde danse en ligne) et de faire une animation. Problème : je ne sais plus vraiment dessiner et j'ai jamais su animer. Diantre.

---

[The map that saved the most lives](https://www.youtube.com/watch?v=Na9iO_HEe14) (vidéo avec du son, en anglais, avec sous-titres anglais, 8 minutes) : où comment John Snow (l'autre) a déterminé la cause d'une épidémie de choléra à Londres, avec une carte.

[Car Commercial 419](https://www.youtube.com/watch?v=G3ZA76dttys) (vidéo avec du son, en anglais, avec sous-titres anglais, 4 minutes). La civilisation de la voiture. Glaçant. Via [Clive Thompson](https://saturation.social/@clive/113499678253823104).

[Traverser des ponts avec un véhicule LEGO](https://www.youtube.com/watch?v=fPvHJJ9CzcA) (vidéo avec du son, sans paroles, 3 minutes). De belles images, du suspense, de l'évolution de personnage, vraiment un grand film. Via l'Amoureux.

[Light mode](https://calayucu.com/light-mode). À utiliser sur un ordinateur avec une souris (ou un trackpad).

[<i lang="en">Here comes the sun</i> à la kalimba](https://www.youtube.com/watch?v=F4we73GHH9k) (vidéo avec du son, sans paroles, 1 minute). C'est tout doux, tout tendre, et ya un chien mignon.

![Photo d'une centaine de morceaux de céramique roulés par la mer, de différentes formes et tailles, assemblés en un rectangle très propre.](beach-pottery-collection.webp){.figure-media--wide}

Source : [beach pottery collection!](https://www.reddit.com/r/knolling/comments/1gf2yti/beach_pottery_collection/) (j'avais déjà parlé du <i lang="en">knolling</i>, en voilà un autre exemple).

[Histoires de chats. Et de chiens. Et d'amour](https://sacripanne.net/post/2024/11/08/histoires-de-chats-et-de-chiens-et-d-amour). Je pleure pas, s'toi tu pleures.

Attention je vais crier de joie : [PATOUNES](https://fouroclockfarms.club/@david/113597618425338505). 

---

Je me remets du Writing Month 😄. 

(Non je n'ai pas relancé <i lang="en">Hold Up Your Hearts (Again)</i> pour la énième fois en 15 jours, je ne vous permets pas)(*\*gigote sur son siège\**)
