---
meta-création: 2024-11-20 20:36
meta-modification: 2024-11-20 20:36
stringified-date: "20241120.203635"
title: Le futur (j'espère) nouveau site
template: blog-item
date: 2024-11-20 20:36
update_date: ""
taxonomy:
  type: article
  tag:
    - développement
    - vie du site
    - writing month
    - writing month 2024
highlight:
  enabled: false
subtitle: Ça fait maintenant un an que j'ai lancé les bases d'une version 4 de ce site.
---

Ça fait maintenant un an que j'ai lancé les bases d'une version 4 de ce site.

===

 D'abord, en reprenant Grav et en faisant « simplement » un nouveau thème, et puis ça fait quelques mois que je bouibouite avec Eleventy. Évidemment, avec un nouveau-nouveau design, sinon c'est pas drôle. Et peut-être que j'arriverai à faire un jardin numérique, qui soit facile d'entretien. Le fait que Grav force une structure de dossiers est entre autres pourquoi je n'ai toujours pas fait ce jardin sur le site actuel, et la simplicité des permaliens d'Eleventy est… enivrante.

Comme je ne sais pas de quoi demain sera fait, je ne vais pas poster d'image du travail en cours (si ça se trouve je bazarde et recommence tout, hein), mais sachez que la couleur principale en mode clair est un joli vert turquoise (<i lang="en">mint</i>) et en mode sombre un beau rose framboise. Gourmande, moi ? [Nooooon](../rose-des-sables).

(j'y suis _presque_, il manque juste (« juste ») une mise à jour du plugin d'images qui ne saurait tarder)(j'espère)(je trépigne)

