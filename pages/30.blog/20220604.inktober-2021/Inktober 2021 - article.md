---
title: "Inktober 2021"
template: "blog-item"

date: "2022-06-04 07:27"
publish_date:
published: true # si besoin d'une date de publi + force publi

taxonomy:
    type: article
    category:
        - création
    tag:
        - défi
        - peinture numérique

# miniature (remplace l'image principale, liste uniquement)
thumbnail: "Inktober_2021_-_05_06_-_Raven_Spirit.jpg"

# plugin Highlight
highlight:
	enabled: false

---

Ça devient une habitude, de publier mon Inktober des mois après. Hum.

===

[Inktober](https://inktober.com/) est un défi annuel de dessin ou peinture – à la base de dessin à l'encre, d'où le nom, mais maintenant beaucoup plus large. Cette année, j'avais remarqué que les propositions de thème pour chaque jour allaient bien deux par deux, et ça tombait bien parce que je ne me sentais pas de faire 31 peintures au vu de comment j'avais géré [l'an dernier](/carnet/mi-inktober). J'ai donc décidé de faire une peinture tous les deux jours, qui mêle deux thèmes consécutifs. 

Divulgâchage : je n'ai fait que la moitié, encore une fois. Mais je suis assez contente du résultat !

![Un homme blanc dans un costume trois pièces (avec cravate) fait de facettes brillantes de ristal, dans les tons violet, bleu clair et vert.](Inktober_2021_-_01_02_-_Crystal_Suit.jpg "Jours 1 et 2, Crystal et Suit : le top de la mode. C'est pas dit que ça soit confortable."){.figure-media--wide}


![Une femme blanche et rousse en robe verte, de la lumière sort par ses yeux et part vers le zénith, elle semble en transe. Autour d'elle, des volutes de nœuds celtiques. Derrière elle, un ciel nocturne étoilé.](Inktober_2021_-_03_04_-_Vessel_Knot.jpg "Jours 3 et 4, Vessel et Knot. J'ai choisi pour vessel l'acception de réceptacle, qui peut être métaphysique, et pour knot j'ai repris un motif de nœud celtique."){.figure-media--wide}

![Un corbeau, presque une silhouette, au milieu d'une forme approximative d'oiseau translucide ayant la lune pour œil, sur un fond de nuit bleue.](Inktober_2021_-_05_06_-_Raven_Spirit.jpg "Jours 5 et 6, Raven et Spirit. Un gros oiseau dans un grand « oiseau »."){.figure-media--wide}

![Un éventail ouvert, décoré d'un paysage champêtre (de bas en haut : ciel, montagnes, collines, forêt et champs), à côté d'une montre en or au verre bombé.](Inktober_2021_-_07_08_-_Fan_Watch.jpg "Jours 7 et 8 : Fan et Watch, un éventail avec un paysage champêtre (franchement fière du résultat ! c'est la première idée qui m'est venue en lisant les amorces de cette année, et je suis contente d'avoir réussi à la réaliser), et une montre en or. C'est ma peinture préférée de cette année, sans hésitation."){.figure-media--wide}

![Un homme blanc, nu, qui porte une planète sur son épaule, se penche vers une petite fleur bleue.](Inktober_2021_-_09_10_-_Pressure_Pick.jpg "Jours 9 et 10 : Pressure et Pick, Atlas portant le monde (c'est quand même une sacrée pression) a repéré une jolie fleur et il veut la cueillir (pick). Pas spécialement contente du résultat, mais j'ai pas l'habitude de dessiner des messieurs tounus, alors l'anatomie n'est pas extra."){.figure-media--wide}


![Une table couverte d'une nappe blanche. Sur la table il y a une planche à découper en bois, avec une grappe de raisins verts dessus et un couteau planté dedans.](Inktober_2021_-_11_12_-_Sour_Stuck.jpg "Jours 11 et 12 : Sour et Stuck. Assez satisfaite du résultat même si c'est tout petiiiit. (j'ai commencé à dessiner avec un zoom trop important et je n'ai réalisé ça qu'en revenant à 100%…)"){.figure-media--wide}

![Un pin parasol avec des marques gravées sur son tronc.](Inktober_2021_-_13_14_-_Roof_Tick.jpg "Jours 13 et 14 : Roof et Tick. Ici on a un grand arbre qui peut servir de toit, sur lequel on a gravé les jours qui passent. Mon idée de base était un palmier pour un ou une naufragée sur une île déserte mais c'était pas drôle à dessiner, un palmier."){.figure-media--wide}

![Sur un fond de papier peint coloré, une table verte, avec une boussole rouge et un casque d'Astérix en plastique.](Inktober_2021_-_15_16_-_Helmet_Compass.jpg "Jours 15 et 16 : Helmet et Compass, la version « pour enfant ». (je passe toujours 2h par peinture, c'est beaucoup trop…)"){.figure-media--wide}

Et après, novembre est arrivé. Beaucoup trop vite. Et j'ai complètement laissé la peinture de côté, parce qu'en novembre, j'écris 😊 Ça aussi, faudra que j'en parle un jour, mais c'est "pas fini™".

Bilan : plutôt positif. Je ne suis toujours pas assez rapide, et je pense que l'an prochain je vais simplifier à mort mon processus, parce que le but n'est pas vraiment de produire au maximum, mais d'être constante dans le dessin, comme le dit le site :

> Note: you can do it daily, or go the half-marathon route and post every other day, or just do the 5K and post once a week. What ever you decide, just be consistent with it. Inktober is about growing and improving and forming positive habits, so the more you’re consistent the better.

Peut-être faire en noir et blanc, juste des crayonnés ? La couleur est une chose que j'adore manier mais qui est très chronophage. Ou même : sur papier 🙀 (bon, pour la simplification du processus, on repasserait, parce que scanner est un peu compliqué chez moi). Bref, ne nous mettons pas la rate au court-bouillon : en octobre 2022, je ferai mieux. Voilà. C'est décidé.