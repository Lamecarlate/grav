---
meta-création: "2024-11-23 11:12"  
meta-modification: "2024-11-23 11:12"  

stringified-date: "20241123.111256"

title: "Collectionnite"   
template: "blog-item"  

date: "2024-11-23 11:12"  
update_date: ""

taxonomy:
    type: article
    tag:
        - organisation
        - writing month
        - writing month 2024

# plugin Highlight
highlight:
    enabled: false

---

Bonjour, je m'appelle Lara et je suis accro aux thés et tisanes.

===

Le thé et la tisane[^1], ça fait partie de mes boissons chaudes favorites. Quand j'ai froid, mon réflexe (en plus de passer mon plaid à manches tout doux) est de faire chauffer de l'eau et fouiller dans mon tiroir à infusions. Je ne fais pas souvent attention au degré de caféine dedans, et si ma mère, par exemple, évite le thé noir en fin d'après-midi, j'avoue que ça ne m'a jamais dérangée. En vrai, je n'ai pas l'impression d'être réveillée par le thé, ou calmée, ou quoi qu'il y ait marqué sur l'étiquette. C'est peut-être vrai, mais je ne le remarque pas, et je choisis bien plus ma boisson pour son goût que ses bienfaits.

Et donc j'ai dans la cuisine un tiroir entier empli de boîtes et sachets avec du thé (noir, blanc, vert), du rooibos et d'autres plantes. Ça va du Grand Yunnan Impérial (thé noir nature) aux trucs improbables comme un mélange thé blanc, thé vert, violette et framboise, en passant par le thé vert, menthe et chocolat (de chez [1336](https://www.1336.fr/), allez leur acheter des trucs, c'est une coopérative de salarié⋅es ayant racheté leur usine, **et** leurs thés sont bons) et par la verveine de chez mes parents 🥰.

Et tout rentre très bien dans le tiroir, admirez l'organisation (vive les boîtes de jeu de société et l'Amoureux qui restructure beaucoup ses jeux en fabriquant des inserts et donc libère des boîtes).

![Photographie, vue du dessus d'un tiroir en plastique contenant de nombreuses boîtes et sachets de thé, très bien rangées.](IMG_20241123_151908--p.webp)

(Oui, le petit papier glissé sur la gauche est un inventaire, non, il n'est pas du tout à jour.)

Et le contenu, en pagaille, c'est ça :

![Photographie, vue de dessus de toutes les boîtes et sachets, étalées sur un plaid, sans ordre apparent.](IMG_20241123_152151--p.webp){.figure-media--wide}

Tiens, c'est une bonne idée d'avoir tout sorti, je vais peut-être changer l'ordre des boîtes, pour mettre en avant des choses moins bues ces derniers temps.

---

Ceci n'a évidemment aucun rapport avec le fait que je viens de faire l'acquisition d'une boîte de thé blanc, fève tonka et orange. Non non non. Du tout. Tss. Je ne vous permets pas.

(Il est très bon, on sent très bien la fève tonka.)

[^1]: La différence n'est pas bien grande à mes yeux : le thé est une tisane d'une plante précise, après tout 😛
