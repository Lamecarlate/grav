---
meta-création: "2024-07-31 15:28"  
meta-modification: "2024-07-31 15:28"  

stringified-date: "20240731.152812"

title: "Liens d'août 2024 (partie 1)"  
template: "blog-item"  

date: "2024-08-16 17:28"  
update_date: ""

taxonomy:
    type: article
    category:
        - liens
    tag:
        - mignonneries
        - diy
        - jeux vidéo
        - humeur
        - humour
        - linguistique
        - musique
        - archéologie

# plugin Highlight
highlight:
    enabled: false

---

Broderie, jeu vidéo, chat, publicité intrusive, ours, dragon, blaireau (eh bien eh bien que d'animaux), Johann Sebastian Bach, renard et corbeau (qu'est-ce que je disais).

===

Besoin d'un peu de verdure ? Testez [One minute park](https://oneminutepark.tv/) (vidéo, sans son, infini). C'est tout simple, une petite vidéo d'une minute d'un parc, quelque part dans le monde, et ensuite on est baladé⋅e à un autre parc, et encore, et encore. On peut participer et envoyer une vidéo. Via [pierrearlais](https://mastodon.social/@pierrearlais/112886415874623629).

[La broderie au XVI<sup>e</sup> siècle](https://www.youtube.com/watch?v=GGJbP-NeJWc) (vidéo avec du son, en anglais, sous-titres en anglais, 30 minutes). Que c'est beau. Comment reproduire aujourd'hui un vêtement ancien avec beaucoup, beaucoup, beaucoup de broderies. Attention, les sous-titres n'ont pas été très bien vérifiés, il y a des <i lang="en">"cruel work"</i> (qui devrait être <i lang="en">"crewelwork"</i>, un type de broderie de fil de laine) et des <i lang="en">"boolean"</i> (devrait être <i lang="en">"bullion"</i>, « lingot » en français) qui traînent.

[Hugo fait découvrir le jeu vidéo Balatro à sa grand-mère](https://www.youtube.com/watch?v=6iVPSPBn08A) (vidéo avec du son, pas de sous-titres, 40 minutes). C'est une belle histoire : la grand-mère d'Hugo n'a jamais vraiment joué aux jeux vidéo, elle est surtout cartes (rami), papier (sudoku) et un peu tablette (donc oui un peu de JV mais elle ne le voit pas comme ça), et son petit-fils veut lui faire essayer un jeu qui ressemble à ce qu'elle aime, un jeu avec des cartes et des chiffres. 

Dans les commentaires :

> Prochain episode : Nouny sur Dark Souls 😛

(la blague c'est parce que Hugo, même sans le vouloir, fait très souvent référence à Dark Souls et aux jeux From Software, que ce soit sur cette chaîne, Game Next Door, ou le podcast Fin du game qu'il co-anime)

---

![Une feuille de papier beige, texturée, avec au milieu deux empreintes profondes de patte de chat.](j284papmdge81.jpg)

Source : [Tried to make my own paper. Failed succesfully](https://www.reddit.com/r/CatsAreAssholes/comments/seu8aa/tried_to_make_my_own_paper_failed_succesfully/) 

Dans les commentaires quelqu'un demande comment on fait son propre papier. La réponse de l'auteurice :

<blockquote lang="en">[…] by recycling shredded paper (leave it overnight in water, then put the rough pulp in a blender, empty the blender in a bucket filled with water, make sure the pulpwater is as smooth as possible, run a paper sieve through it, press the sieve on a piece of cloth to remove the paper from the sieve, leave out to dry). I messed up at the last step: leave it out to dry IN A PLACE WHERE YOUR CAT CAN’T WALK ALL OVER IT. Live and learn. Although I am learning now that I should embrace it as a feature!</blockquote>

Traduction maison :

> En recyclant du papier déchiqueté (laisse-le une nuit dans de l'eau, puis mets la pulpe grossière dans un mixer, vide le mixer dans un seau rempli d'eau, vérifie que l'eau mêlée à la pulpe est la plus lisse possible, passe un tamis à papier dedans, presse le tamis sur un morceau de tissu pour retirer le papier du tamis, laisse sécher). J'ai foiré cette dernière étape : laisse sécher **à un endroit où ton chat ne peut pas marcher**. On n'a jamais fini d'apprendre. Mais j'apprends à présent que je devrais prendre ça comme une caractéristique !

(le « ça » de la dernière phrase se rapporte aux pattes de chat, puisque tout le monde dans les commentaires s'extasie et achèterait volontiers ce genre de papier)

---

Par curiosité, je regarde une page d'un blog de cuisine que je connais bien sur Chromium (où je n'ai pas de bloqueurs de pub). Mandieu. Une vidéo (la sienne, heureusement), mais qui se met automatiquement en miniature le long du scroll. Une bannière de pub flottante toujours visible en bas. Qui bouge. Aaaaah. Dans la barre latérale, 10 (dix) pubs, qui suivent le défilement. Entrecoupant la recette, 10 aussi (mais possiblement plus, il y avait des espaces vides, comme si les pubs n'avaient pas chargé. Et deux dans la fiche récapitulative de recette. L'enfer.

Extrait de l'enfer :

![Quatre blocs de pub sont affichés, dont deux identiques, prenant 80% de l'espace, et le texte intéressant, deux lignes de la recette et un bloc d'astuce, est noyé au milieu.](capture-2024-08-04-07-51-26.jpg){.figure-media--wide}

Je souhaiterais pas ça à mon pire ennemi.

Et je suis en train d'imaginer à quoi cette page ressemble via un lecteur d'écran. Brrr.

---

[Le ciel au milieu d'une courtine](https://aleph.land/@christianhajer/112914538775763612) (image avec un texte alternatif en anglais).

[Ounts ounts ounts](https://pouet.chapril.org/@miss__Tery/112908101661165997).

---

![Bande dessinée de SMBC, la transcription traduite par mes soins suit.](1714681809-20240502.png)

<blockquote class="conversation is-conversation" markdown="1">
<span class="conversation--person">Le dragon</span> : Je déteste la manière dont les humains écrivent les dragons ! Ils parlent toujours comme si ils étaient surpris d'être des dragons. Et ils ont toujours des noms faits de mots comme feu et griffe et aile. Mais ce sont des parties normales de notre anatomie ! Pourquoi est-ce qu'on y ferait constamment référence ?

<span class="conversation--person">L'humain</span> : Bof, j'ai vu comment les dragons écrivent les humains.

<span class="conversation--person">Le dragon</span>, vexé : Que veux-tu dire ?

Un extrait d'un roman suit.

« Jamais ! » dit Cou-court en langage humain, à travers ses dents de devant majoritairement carrées. Elle savait qu'elle était vulnérable à cause de sa faible peau humaine et son minuscule cerveau, mais elle devait sauver sa progéniture sans poils, Mains-à-pouces et Deux-jambes, du méchant sorcier humain, Boules-qui-pendent.
</blockquote>

Source : [Dragons, chez SMBC](https://www.smbc-comics.com/comic/dragons).

---

[Donner sa langue au chat](https://oisaur.com/@atsemtex/109613792636225636).

<blockquote lang="en" markdown="1">
What's the difference between gray and grey?

One is a color, the other is a colour.
</blockquote>

Je ne sais plus où j'ai vu cette blague mais je l'adore.

Vous avez probablement vu passer [cette vidéo avec un avocat coincé dans un filtre « petit chat mignon » dans une conversation vidéo](https://www.youtube.com/watch?v=KxlPGPupdd8) (très sérieuse au demeurant), il y a deux ans. Voilà [la fin de l'histoire du "lawyercat"](https://www.youtube.com/watch?v=AzWFEF267jk) (vidéo avec du son, en anglais, sans sous-titres, une minute).

[Une mitose en broderie](https://vis.social/@lia_pas/112956018315952413) (image avec texte alternatif en anglais).

En faisant un tri dans mes vieux fichiers (et notamment les fichiers swf, qui sont notoirement difficiles à lire ces temps-ci), je retrouve le fameux [Badgers](http://weebls-stuff.com/toons/badgers/) (vidéo avec du son, en anglais, sans sous-titres, mais les paroles sont essentiellement ce qui est visible à l'écran, 1 heure en boucle), et… sa version préhistorique [Stone Age Badgers](http://weebls-stuff.com/toons/stone-age-badgers/) (vidéo avec du son, en anglais, sans sous-titres, 3 minutes). C'est fait par le même gars (et c'est le même qui a réalisé [Narwhals](http://weebls-stuff.com/toons/narwhals/), je le découvre à l'instant, et l'Amoureux s'étonne, pour lui c'était évident 😛). 

Bonus : [Save the badger](https://www.youtube.com/watch?v=EllYgcWmcAY) (vidéo avec du son, en anglais, sans sous-titres, 1 minute), fait en 2013 en association avec Brian May (celui de Queen, oui oui oui) et Brian Blessed (le prince Vultan dans le film _Flash Gordon_, oui oui oui aussi) pour lutter contre une loi britannique qui prévoyait l'abattage de nombreux blaireaux.

On fait encore des fouilles à Pompéi, et [des archéologues viennent de mettre au jour une pièce contenant deux squelettes et de nombreux objets](https://social.sciences.re/@kipuka/112960481409529474) (photo de squelettes, si vous y êtes sensibles).

La [Toccata et fugue en ré mineur de Bach à la guitare](https://www.youtube.com/watch?v=ZQeVYnPAoPw) (vidéo avec du son, pas de paroles, 9 minutes), c'est rien classe.

[La même musique, mais sur un Gros Piano](https://www.youtube.com/watch?v=-iJyiOEDToY) (<i lang="en">Big Piano</i>, marque déposée, tout ça) (vidéo avec du son, sans paroles, 2 minutes). Il y a de très nombreuses versions de ce morceau sur un Big Piano, cela semble être un pré-requis dans les démonstrations publiques de cet instrument. La qualité vidéo ici n'est pas très bonne mais c'est celle où le son est le meilleur à mon sens et où l'angle de vue permet de saisir les mouvements.

Et pis on continue avec Bach, avec la [Petite fugue en sol mineur par quatre flûtes à bec](https://www.youtube.com/watch?v=-Iu1pA4sDbc) (vidéo avec du son, sans paroles, 3 minutes). Admirez ces flûtes ténor, basse, grande-basse et contrebasse. 

La tondeuse à gazon qui évite les pâquerettes de Gaston Lagaffe existe pour de vrai ! [Un fan de Gaston Lagaffe fabrique sa tondeuse miniature](https://www.francetvinfo.fr/culture/bd/un-gag-qui-m-a-beaucoup-touche-un-fan-de-gaston-lagaffe-fabrique-sa-tondeuse-miniature-pour-eviter-les-paquerettes_6435772.html). Via [sebsauvage](https://sebsauvage.net/links/?ItxMNg).

[Rentabilise ton temps libre !](https://mastodon.art/@kmcshane/109931573215494134) (BD en anglais, avec texte alternatif). L'idée nous vient souvent, quand on a un loisir chouette…

---

[Companions](https://www.webtoons.com/en/canvas/crow-time/companions/viewer?title_no=693372&episode_no=126) (BD en anglais, sans texte alternatif, la transcription traduite par mes soins suit).

<blockquote class="conversation is-conversation" markdown="1">
<span class="conversation--person">Le corbeau</span>, <i>dans le sac à dos du renard</i> : Je suis tellement désolé.

<span class="conversation--person">Le renard</span> : Pourquoi donc ?

<span class="conversation--person">Le corbeau</span> : Je ne me sens pas bien et tu me portes et je me sens coupable.

<span class="conversation--person">Le renard</span> : Chut.

<span class="conversation--person">Le corbeau</span> : Je ne peux pas te porter. Ça me semble injuste.

<span class="conversation--person">Le renard</span> : C'est vrai. Tu ne peux pas me porter. <i>(il y a une case où le renard imagine être sur le corbeau, qui est tout écrasé par le poids du renard)</i> Mais jamais je ne te demanderais de faire plus que ce que tu peux et te blesser uniquement pour rendre ça plus juste. Je ne peux pas voler, et pourtant quand je suis fatigué tu m'apportes des friandises depuis les branches hautes. Je ne peux pas construire de nids, c'est toi qui remplis notre couette avec du duvet et des feuilles tendres. Quand je me repose dessus, est-ce injuste pour toi ?

<span class="conversation--person">Le corbeau</span> : … Non. Je veux dire… C'est **notre** couverture.

<span class="conversation--person">Le renard</span> : Oui. Et ceci est **notre** voyage maintenant.

<i>Il reprend</i> : Nous marchons en tant que compagnons.
</blockquote>

---

Snif, mes vacances sont presque finies. Le blues de la rentrée m'atteint déjà. Je n'ai pas fait tout ce que j'avais prévu mais c'était bien !
