---
meta-création: "2024-12-23 16:26"  
meta-modification: "2024-12-23 16:26"  

stringified-date: "20241223.162615"

title: "J'ai joué à GRIS"  
template: "blog-item"  

date: "2024-12-25T19:26"  
update_date: ""

taxonomy:
    type: article
    tag:
        - jeux vidéo
        - art

# plugin Highlight
highlight:
    enabled: false

---

Motivée par des copains, j'ai testé le jeu GRIS. Beh c'est très très bien.

===

Samedi 21 décembre. 16h39. <del>Il pleut</del> La météo n'a aucune importance. Lapinours[^1] dit :

> J'ai commencé Gris. C'est d'une beauté, pfoulalalallaala.
> Et puis les p'tites trouvailles de gameplay. 

Aznörth renchérit, l'est très bien, ce jeu.

Ça faisait pas mal de temps que GRIS me faisait de l'œil, mais j'ai une liste de jeux à faire un jour longue comme le bras d'un géant acromégalique, et c'est que sur Windows, et c'est de la plateforme et… bon allez zut, je le trouve en piraté juste pour tester.

Après vingt minutes de jeu : oké c'est validé, sors le porte-feuille, Harpagon, là.

(note interstitielle : pour des raisons de concision, dans les textes alternatifs des images, je ne répéterai pas que ce sont des captures du jeu, ni que Gris est une jeune femme aux cheveux verts en robe flottante, donc je le dis ici 😉)

![Gris est dans un décor bleu-gris de colonnes et escaliers devant des pots plus grands qu'elle. ](2024-12-21%2019-49-04--p.webp){.figure-media--wide}

Il était en promo sur [GOG](https://www.gog.com/fr/game/gris) (lien non-affilié, juste que je préfère GOG à Steam, parce que je peux télécharger les fichiers d'installation, je possède les jeux que j'achète), hop, prenons.

Ça s'installe sans aucun problème avec [Lutris](https://lutris.net), vraiment c'est une merveille ce logiciel (et en plus leur nom et leur logo c'est une loutre, écoutez, comment puis-je lutter ?). 

Deux heures et demi plus tard, je m'arrête parce que j'ai faim, et je réalise que j'ai froid (vu que j'ai très peu bougé) et que j'ai les épaules tendues (vu que je suis restée concentrée sur la tâche) 😆

J'ai repris le lendemain, deux heures environ, et je l'ai fini le jour d'après (c'est un jeu assez court, j'ai mis au total 6h30 parce que les jeux à plateforme c'est pas mes potes).

## L'histoire

Difficile d'en parler sans trop révéler, d'autant qu'il n'y a jamais aucune parole ou texte[^2], donc c'est pas mal laissé à l'appréciation de la joueuse. Mais disons en quelques mots qu'il arrive quelque chose à Gris, la protagoniste, et qu'elle traverse des mondes étranges pour passer outre, tout en leur rendant progressivement des couleurs.

![Gris est dans un monde en noir et blanc, la nuit, et sous ses pieds il y a une image en miroir d'elle sur fond blanc avec des oiseaux noirs.](2024-12-21%2018-06-08--p.webp){.figure-media--wide}

<details markdown="1">
<summary><span role="img" aria-label="Attention">⚠️</span> Divulgâchage de mon interprétation et de celles d'autres</summary>

Assez rapidement j'y ai vu un processus de deuil. Notamment parce que la seconde zone, le tableau rouge avec les lances plantées dans le sol et les drapeaux, pour moi c'était la colère, qu'il fallait traverser, sans se laisser emporter, en s'ancrant dans le sol.

Puis j'ai trouvé une zone qui a fait apparaître une notification « Quatrième étape : dépression ». Pas très subtil, du coup, mais ça confirmait ma théorie. 

Enfin, la fin allait dans ce sens. Pour moi, Gris faisait le deuil de sa propre mortalité, comme Elisabeth Kübler-Ross l'avait théorisé (les [fameuses étapes](https://fr.wikipedia.org/wiki/Deuil#%C3%89tapes) ont été mal comprises et appliquées aux proches, même si ça marche aussi un peu). 

Après lecture de la page Wikipédia, apparemment mon interprétation est erronée, et je suis un tout petit peu déçue parce que je trouvais ma vision plus originale, mais c'était quand même très bien. Ensuite, pas mal de gens discutent sur le sujet, [sur Reddit](https://www.reddit.com/r/gris/comments/j6jen4/what_is_your_interpretation_of_the_story/) par exemple (en anglais), ou [sur Medium](https://medium.com/@Max_Goldstein/gris-is-a-game-about-death-of-the-author-a536e3780ea) (en anglais, je n'ai pas trop aimé le ton de l'article mais il fait un parallèle intéressant avec la théorie de [la mort de l'auteur](https://fr.wikipedia.org/wiki/La_Mort_de_l%27auteur)).  
</details>
## Les mécanismes

C'est entre plateforme et énigmes : on arrive sur un tableau, le but est de récupérer des sortes d'étoiles qui serviront à faire des ponts dans le vide, et aussi à terminer les niveaux. Pour cela, on saute, on casse des pots et des ponts, on plonge, on se propulse grâce à des oiseaux dans l'eau ou dans l'air (oui, c'est, heu, des oiseaux spéciaux).

Le but premier n'est pas de tester l'adresse de la joueuse, comme le ferait un Céleste, et les personnes habituées ne verront pas de difficulté. Moi, j'ai galéré (quand je dis que « c'est de la plateforme » est un argument contre, au début de cet article, c'est parce que je suis assez mauvaise dans ce type de jeux…) et l'Amoureux est venu trois ou quatre fois passer un point compliqué que j'avais tenté et retenté trente fois.

J'ai beaucoup aimé dans le dernier tableau le travail sur les mondes-miroirs, où passer une ligne dans le ciel nous inverse la gravité, et donc on peut alterner entre sauter et tomber en un mouvement. Ça met les doigts et les nerfs à l'épreuve, mais c'est très astucieux.

Une chose appréciable aussi, c'est que les différents pouvoirs obtenus sont utiles tout le long du jeu, pas seulement dans le tableau où ils apparaissent. Même la capacité de nager est utilisée jusqu'au bout, par exemple lorsqu'il faut alterner entre nage et saut et nage et saut pour atteindre le sommet d'une tour dont certains étages sont inondés (oui, la physique est étrange dans ces mondes).

## Le visuel et le son

Pouyayaya.

Il y a des ruines, des oiseaux, des poissons, des bâtiments gigantesques et des statues tout aussi gigantesques.

![Gris plonge dans de l'eau, dans un décor aux tons bleus, faits d'arches, avec de grands pots contenant des plantes.](2024-12-22%2014-56-50--p.webp){.figure-media--wide}

Les décors et les objets sont en aquarelle, les cieux sont pleins de taches de peinture. Ça me touche pas mal en ce moment, mon goût esthétique actuel est tourné vers l'aquarelle et les lignes douces (si elles sont plus claires que les fonds c'est encore plus fort).

Plus on avance dans le jeu, plus le monde récupère de couleurs et de nuances, et plus il se remplit de vie, plantes et animaux (et créatures artificielles).

![Gris est sur une plateforme très fine, en métal ornementé, le ciel est rouge.](2024-12-21%2018-30-06--p.webp){.figure-media--wide}

Les arbres (carrés) changent de forme de manière si fluide et si normale que j'en viens à me demander pourquoi c'est pas comme ça dans le monde réel. La vidéo qui suit n'a pas de son.

![Un arbre carré voit l'ensemble de ses feuilles apparaître et disparaître, comme la mer qui se retire brusquement de la plage.](gris-fluid-trees-2024-12-25_19.12.29.mp4)

<blockquote lang="en">Mastermind and main artist Conrad Roset is a well-known painter and his trademark style defines GRIS, but Nomada also had other sources of inspiration, including "from classic arts, from comics... basically from Moebius, from Disney movies (particularly some areas like the forest)", as Adrián admits, but also from other modern studios such as Studio Ghibli or other games such as Cuphead, Ori and the Blind Forest, and Journey, even though most of the team "never worked in video games", but came from other fields, mostly the arts.</blockquote>

Source : [What does GRIS mean? We talk to Nomada Studio](https://www.gamereactor.eu/what-does-gris-mean-we-talk-to-nomada-studio/) (en anglais).

> Le cerveau et artiste principal, Conrad Roset, est un peintre bien connu, et son style caractéristique définit GRIS, mais Nomada a aussi eu d'autres sources d'inspiration, incluant « les arts classiques, les comics… essentiellement Moebius, les films Disney (particulièrement dans certaines zones, comme la forêt) », comme l'admit Adrián, mais aussi d'autres studios modernes, comme le Studio Ghibli ou d'autres jeux comme Cuphead, Ori and the Blind Forest, et Journey, même si la majorité de l'équipe « n'avait jamais travaillé dans le jeu vidéo » mais venait d'autres domaines, surtout les arts.

Traduction maison.

J'y voyais en effet du Moebius, dans ces lignes infinies et ces tons pastels. Et les petites bêtes du chapitre de la forêt sont très ghibliesques.

![Gris est dans un monde fait de lumière, devant un ciel de nuit, il y a de nombreuses plantes luxuriantes.](2024-12-22%2016-45-19--p.webp){.figure-media--wide}

La musique est superbe. Elle n'est pas omniprésente, de nombreux moments sont simplement habillés du bruit de nos pas. Et quand elle est là… pouyayaya bis. 

Mention spéciale à la musique lors de la poursuite par l'anguille/murène dans le chapitre de l'océan. Ça prend aux tripes, en faisant passer par tant d'émotions. Celleux qui ont joué savent pourquoi.

## Verdict

Pour moi qui ne suis pas une adepte des jeux de plateforme (ça va, je ne l'ai pas trop répété ?) c'était bien, paaaas trop frustrant. 

Le jeu est très intuitif à plein de moments, et ça le rend vraiment malin et agréable, comme le moment où j'ai compris que je pouvais quand même marcher en étant « appesantie ». Et à d'autres, beaucoup moins… C'est peut-être juste moi, mais il y a des moments où je me suis retrouvée à errer parce que je m'attendais à une cinématique et non, on me rendait la main. Pour… faire quoi ? Il y avait bien des choses à faire, mais ça n'avait pas été assez évident à mes yeux (notamment après avoir obtenu le dernier pouvoir, je n'ai absolument pas capté ce qu'il faisait avant de très, très longues minutes de perplexité).

Et puis c'est très beau. Vraiment. J'ai passé un très bon moment, en en prenant plein les yeux et les oreilles. L'animation est fluide, les séquences cinématiques sont superbes, avec ce rendu dessiné qui change à chaque frame.

![Gris est dans une cave avec des flaques d'eau et des plantes énormes, dix fois sa taille.](2024-12-22%2014-58-53--p.webp){.figure-media--wide}

Les captures sont toutes faites maison, durant mes sessions de jeu (c'est un générateur à fond d'écran, ce jeu, je vous jure).

![Dans une ambiance de nuit lumineuse rose et brumeuse, Gris est entre deux lanternes.](2024-12-23%2010-59-50--p.webp){.figure-media--wide}

GRIS est disponible sur [GOG](https://www.gog.com/fr/game/gris), [Steam](https://store.steampowered.com/app/683320/GRIS/), Nintendo Switch, Playstation 4 et 5, Xbox Series et iOS.

[^1]: Il a d'ailleurs écrit [sa critique du jeu sur son blog](https://blog.erreur503.xyz/articles/jeux-video/gris.html).
[^2]: À part quelques notifications qui ressemblent à des trophées/succès mais qui sont internes au jeu – et apparemment la version Steam a des trophées en plus.
