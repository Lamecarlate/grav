---
meta-création: "2024-02-09 21:17"
meta-modification: "2024-02-09 21:17"

stringified-date: "20240209.20240209211750"

date: "2024-02-09 21:17"
template: "short-note"
taxonomy:
    type: short-note
    tags: 
        - informatique
        - développement
        - Firefox

title: ""

---

Je viens de découvrir que l'extension Stylus pour Firefox a une option de synchronisation avec un webdav. Donc je peux transférer via mon serveur Nextcloud mes 50 styles persos sur toutes mes machines, dont le téléphone <span role="img" aria-label="Visage souriant mais avec une larme à l'œil, le soulagement après la douleur">🥲</span> Joie, bonheur, félicité !

Notons que les espaces dans le chemin doivent être remplacés par `%20`, j'ai bieeeen galéré avant de comprendre ça.