---
title: Le bâton aux mille mains

date: 2021-03-21 18:58
taxonomy:
  type: article
  category:
    - création
  tag:
    - travail du bois
    - travail du métal
highlight:
    enabled: false
header_image_file: 20210321-baton-focus.jpg
header_image_alt: Gros plan sur un bâton parcouru de nervures, posé sur un plaid gris ; le bâton devient flou vers l'arrière-plan
---

Ce bâton est le fruit d'une collaboration inattendue. En vrai, tout le travail a été fait par les vers qui ont creusé les nervures. Moi, j'ai huilé et ferré 😁

===

Lors d'une session de débroussaillage du jardin d'une amie avec mes parents (en 2014), j'ai trouvé une branche, l'ai débarrassée de son écorce, et ai découvert un réseau de superbes lignes creusées par des vers. C'était si impressionnant que j'ai gardé le bâton.

Arrivée chez mes parents, les vacances se sont transformées en projet « travail du bois ». Élaguer proprement, nettoyer et poncer longuement, cirer soigneusement.

![Le bâton élagué, nettoyé et poncé, debout contre une table de jardin](20140423-gros-plan-baton.jpg "Le bâton élagué, nettoyé et poncé")

Ensuite mon père m'a aidée à ferrer les deux extrémités pour les protéger.

![Vue d'ensemble sur le bâton, posé sur une table de jardin](20140423-vue-d-ensemble.jpg "Long bâton is long"){.figure-media--wide}

![Gros plan sur la partie haute du bâton, on voit qu'elle est cerclée de cuivre, avec deux vis pour que ça tienne bien](20140423-gros-plan-fer-haut.jpg)

Il restait à huiler le bois pour le nourrir, j'avais prévu de le faire à la maison, tranquillement.

Je suis rentrée chez moi. En train. Avec le bâton. Ma voisine de trajet m'a très gentiment demandé si je revenais de Compostelle 😛 « on dirait un bâton de pélerin, vous comprenez ».

![Le bâton, fini, posé debout contre le mur](20210321-vertical.jpg){.media--align-center}

Et puis le temps a passé. Genre, longtemps. Le bâton a un peu traîné, je ne trouvais pas de moment pour le retravailler… À l'été 2019, cinq ans après, donc, j'ai enfin pris mon courage et mon huile d'olive à deux mains. Je me suis installée sur la terrasse et j'ai terminé le bâton aux mille mains.

(je sais que les vers qui mangent le bois n'ont pas de mains, c'est une métaphore)

![Gros plan sur les nervures, sur le bâton huilé, le bois est plus sombre](20210321-nervures.jpg "C'est quand même un beau travail que ces braves ptites bêtes ont réalisé là"){.figure-media--wide}

Et pour Halloween de la même année, j'étais une sorte de druide magicienne, avec des bottes, une cape de laine et un bâton ferré traversé de nervures comme d'antiques brûlures. Et les copains et copines m'ont demandé pourquoi j'étais en costume de bergère. Cuistres.

Mais c'est parce que j'avais pas pris mon épée (à la base j'imaginais une magicienne de combat, avec bâton et épée, Gandalf ma gueule) parce que c'est une vraie et que je voulais éviter les ennuis dans la rue et au travail - c'est moyennement autorisé.

Tiens, il faudra que je parle aussi de cette épée, dont j'ai fabriqué le fourreau d'ailleurs.
