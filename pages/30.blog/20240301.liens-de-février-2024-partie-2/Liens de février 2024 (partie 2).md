---
meta-création: "2024-02-18 16:45"  
meta-modification: "2024-02-18 16:45"  

stringified-date: "20240218.164520"

title: "Liens de février 2024 (partie 2)"  
template: "blog-item"  

date: "2024-03-01 22:00:16"  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - musique
        - mignonneries
        - cinéma
        - sciences
        - humour
        - développement
        - histoire

# plugin Highlight
highlight:
    enabled: false

---

Des chats (à croire que c'est mon animal préféré, dites donc, incroyable), du LEGO, de la politique et des brouzoufs (avec du feu dedans), de la culture pop, du bleu dans les LEDs, des images mignonnes, le CSS c'est trop bien (insérer ici le meme "You have my bow! And my axe!" et moi je rajoute "And my keyboard"), pas mal de musique (qui fait rire, qui touche et qui enseigne).

===

[Cat spaceship adventure](https://www.youtube.com/watch?v=IFT3sA_bYss) (vidéo avec du son, les quelques paroles sont écrites uniquement) : c'est… difficilement descriptible. En gros : un humain et des chats sont échoués sur une planète et doivent retrouver leur vaisseau, mais ya des obstacles.

[My daughters “colorful” Lego UCS Millennium Falcon is done!](https://www.reddit.com/r/lego/comments/92m7mx/after_8_months_of_collecting_and_building_my/) : ohhhhhh. C'est chouette, les LEGO. Via [Nicolas Holzschulch](https://piaille.fr/@nholzschuch/111884683981479322).

Ah tiens oui c'est vrai ça : [Men are from Mars, women are from Venus implies that…](https://social.coop/@shauna/111881406438562583) (image en anglais avec alt).

[Le funeste secret de l'industrie du diamant](https://www.youtube.com/watch?v=14GjsJpPpzo) (vidéo de 45 minutes, avec du son, sans sous-titres) : dernière partie du documentaire d'Alcide sur l'industrie du diamant. Passionnant. Un peu déprimant, mais il y a une belle touche d'espoir à la fin de la vidéo !

[La revanche des « japoniaiseries »](https://www.youtube.com/watch?v=1GJ5QTHA2Es) (vidéo avec du son, de 20 minutes, sans sous-titres, et c'est vraiment triste, toutes ces chaînes qui font un excellent travail de vulgarisation mais ne mettent pas de sous-titres…).

[Why It Was Almost Impossible to Make the Blue LED](https://www.youtube.com/watch?v=AF8d72mA41M) (vidéo de 30 minutes, avec du son, en anglais, avec sous-titres en anglais) : je crois que je ne m'étais jamais posé la question des couleurs des LED. Oui, la couleur du plastique autour n'est là que pour une reconnaissance rapide lorsque c'est éteint : c'est le matériau interne qui détermine la couleur de la lumière ! Et le bleu a été **compliqué** à obtenir. Une histoire de science, de politique et de brouzoufs, sur près de 30 ans. Via [Hteumeuleu](https://mastodon.social/@HTeuMeuLeu/111918579140045434).

[Side entrance](https://1041uuu.tumblr.com/post/741420064347193344/why-did-i-forget-about-this-coolest-social) (grand gif légèrement animé en pixel-art, sans texte alternatif). J'aime beaucoup le travail très doux de l'artiste japonais 1041uuu. Ici, on a une vue d'une porte vitrée depuis l'intérieur d'un bâtiment (on dirait une école, ou un bâtiment public). Il y a des toilettes pas loin de l'entrée, le néon clignote un peu. La lumière de dehors entre en mettant des couleurs mouvantes sur les tuiles du carrelage, il y a du vent assez violent, qui agite les branches de l'arbre en face. Et dehors, juste derrière la porte, un petit chat noir aux yeux verts attend qu'on lui ouvre, de moins en moins patient, sa queue fouette l'air et ses oreilles bougent.


[Des grottes sous la mer](https://mas.to/@alexskunz/111733040402720456).

![Dessin d'un oignon qui pleure parce qu'il est tombé et il dit « Aïe ! », tandis qu'une tête d'ail débarque en courant et dit « Oh Nion ! »](86da6f8ca7d15f50.webp)
Source (j'ai repris le alt également) : [Marité Battesti](https://toot.aquilenet.fr/@Maryteax/111979283419963880).

[« Votre nom est invalide, épisode 456 »](https://techhub.social/@nimphal/111957561640252936) (en anglais, c'est une image avec un texte alternatif). (ce n'est pas lié aux événements récents concernant Wikipédia France et les morinoms ou nécronymes de personnes trans, mais c'est tout aussi problématique)

Il y a quelques mois se tenait [un poticolloque](https://piaille.fr/@Chatsralent/111651676339419895), c'est-à-dire un colloque de chats, et le programme était particulièrement moelleux : du pâtépoulet, du dodo, des pouèmes. C'est sciontifique, on vous dit.

[CSS is logical](https://geoffgraham.me/css-is-logical/) (en anglais), via [Bastien Calou](https://piaille.fr/@bcalou/111997102212019479).

[Manny Spring Sonatas - New Sketch by Igudesman & Joo featuring Emanuel Ax](https://www.youtube.com/watch?v=MviD1JU2CXA) (vidéo avec du son, en anglais sans sous-titres mais les paroles sont assez accessoires). Comme le dit la description de la vidéo, pour ce spectacle, Igudesman (le violoniste) a engagé Ax au lieu de son comparse Joo (le pianiste), sans le lui dire. Une dispute s'ensuit, en musique bien sûr. J'aime beaucoup Igudesman et Joo, ce sont deux excellents musiciens qui arrivent à inclure énormément d'humour dans leurs sketchs (j'en avais déjà parlé en [août 2023](../liens-aout)).

[Level up, de Vienna Teng](https://www.youtube.com/watch?v=U4n_8R5lKnw) (vidéo avec du son, de la musique, du chant en anglais avec sous-titres en anglais) : c'est beau, c'est optimiste, et ça donne envie de danser. Oué, la vie c'est de la merde, des fois, et même si c'est dur, continuer à avancer, à aimer, à partager, c'est important. J'ai conscience qu'écrire ces mots, c'est un peu galvaudé. C'est difficile d'être optimiste ces temps-ci. Et je suis personnellement méfiante parce que l'optimisme à outrance cache souvent du new age (mais si, vous savez, vos pensées façonnent votre réalité, tout ça…). Mais là, cette musique m'a touchée, elle est douce tout en étant entraînante. La voix de Vienna Tang est claire. Les chorégraphies sont fluides (le grand monsieur en jean qui lance sa jambe à hauteur de son oreille, mandieu, que j'envie sa souplesse). Via [le compte Mastodon de Thunderbird](https://mastodon.online/@thunderbird/112004167188933987).

[Kangaroo Time](https://www.youtube.com/watch?v=RoSYO3fApEc) (vidéo avec du son, de la musique, pas de sous-titres, mais du texte affiché - qui ne sont pas les paroles) : woooh j'ai appris des trucs sur le comportement des kangourous, via le programme Dance your PhD ! Via [Sheril Kirshenbaum](https://mastodon.social/@Sheril/112009814568066442).

Et, via [Boris](https://framapiaf.org/@borisschapira/112013994891313927), le lauréat 2022 de Dance your PhD : [Electroporation of Yeast Cells](https://www.youtube.com/watch?v=dq5uYGNeOS0).

[Ours polaire curieux](https://mastodon.social/@VeroniqueB99/112012013882706091).

[Parole de chat – Les bêtises 2](https://www.youtube.com/watch?v=ynBinxdfra0) (vidéo avec du son, sans sous-titres… sauf en anglais traduit, alors que la vidéo est en français 😓) : toujours aussi idiotement hilarant, Parole de chat ! J'ai souffert pour toutes ces choses qui tombent et cet appartement saccazé (« Libérée ! Délivrée ! »).

Je me doute que le concept des cuillères pour symboliser l'énergie de la journée est bien connu maintenant mais c'est toujours intéressant de relire [l'article originel](https://lymphoma-action.org.uk/sites/default/files/media/documents/2020-05/Spoon%20theory%20by%20Christine%20Miserandino.pdf) (via [Chris Coyier](https://chriscoyier.net/2022/09/07/spoons/)).

---

Via [Johann](https://seine.fleuv.es/@joe/112010870052065154) je découvre le [chirographe](https://fr.wikipedia.org/wiki/Chirographe), qui me fait beaucoup penser au [bâton de taille partagé](https://fr.wikipedia.org/wiki/B%C3%A2ton_de_comptage#B%C3%A2ton_de_taille,_partag%C3%A9) : dans les deux cas c'est un système physique que l'on découpe ou entaille de manière à ce que les parties correspondent parfaitement une fois réunies, pour sceller un contrat. D'ailleurs, en relisant l'article sur le bâton de taille, je suis surprise, je pensais la méthode bien plus ancienne. J'ai souvenir de l'exposition de l'association Aconit à Grenoble sur les maths et les ordinateurs (un de mes profs préférés y officiait, c'est lui qui avait fait la visite quand j'étais venue, c'était trop bien), et il me semble bien qu'on y lisait que ces bâtons étaient utilisés depuis avant l'écriture. Mais je confonds peut-être… 

En cherchant un peu, je tombe sur [Le bâton de comptage, un moyen de paiement oublié et pourtant officiel en France jusqu’en 2016](https://martouf.ch/2020/01/le-baton-de-comptage-un-moyen-de-paiement-oublie-et-pourtant-officiel-en-france-jusquen-2016/), article fort intéressant et bien écrit, qui ouvre des tas et des tas d'idées dans ma tête et d'onglets dans mon navigateur. Notamment ce point : 

> La manière la plus simple, c'est de faire 1 encoche pour une unité. Mais ça fait vite beaucoup de coches pour les grands nombres. J'ai testé une convention qui me semble plausible:
> 
> - une encoche droite simple = 1 unité
> - une encoche en V = 10 unités
> - une encoche large = 100 unités.
> 
> Puis en regardant mes encoches je me suis dis que ça ressemble beaucoup aux chiffres romains !! Peut être que c'est là une origine des chiffres romains ?

Cette hypothèse est reprise dans la page Wikipédia dédiée à la numération romaine, mais je n'ai pas encore creusé plus loin.

Se méfier des hypothèses qui ont l'air trop jolies, cependant !

---

La petite chatte, en boule bien serrée dans le fauteuil à côté de moi, rêve. Sa patte tressaute. Le vent agite le laurier que je peux voir par la fenêtre. Le temps est calme. Je vais chercher notre nouvelle bouilloire électrique tout à l'heure[^1]. Mars commence bien.

[^1]: Et elle est parfaite : avec des vrais boutons et un bouton tournant pour le réglage de la température !