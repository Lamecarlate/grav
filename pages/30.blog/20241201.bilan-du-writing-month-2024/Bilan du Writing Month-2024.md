---
meta-création: 2024-12-01 16:13
meta-modification: 2024-12-01 16:13
stringified-date: "20241201.161339"
title: Bilan du Writing Month 2024
template: blog-item
date: 2024-12-01 16:13
update_date: ""
taxonomy:
  type: article
  tag:
    - vie du site
    - personnel
    - écriture
highlight:
  enabled: false
subtitle: Trente billets en trente jours. Plus jamais ça. Mais c'était bien !
---

Trente billets en trente jours. Plus jamais ça. Mais c'était bien !

===

Plusieurs années de suite, j'ai fait le NaNoWriMo (National Novel Writing Month). C'est un événement initié aux États-Unis pour aider, émuler les aspirants et aspirantes écrivaines à écrire. L'objectif d'origine, c'est 50000 mots en un mois, ce qui correspond à la taille minimale d'un roman aux USA[^1] . Il se trouve que cette année, il y a eu polémiques sur polémiques au sujet de l'organisation du NaNo. Notamment des accusations d'abus sur jeunes personnes (ce qui a conduit de très nombreux bénévoles à couper les ponts, dont les personnes qui géraient la communauté NaNo dont je faisais partie). Et, quelques mois avant le démarrage, un communiqué comme quoi être contre l'IA générative pour écrire était validiste. Un clou supplémentaire dans le cercueil, pour beaucoup. 

Moi, j'étais déjà pas mal détachée, parce que cela fait des mois que je n'arrive plus à écrire de la fiction (un comble, parce que je n'arrive à lire que ça 😆 la non-fiction a du mal à maintenir mon attention…). Mais le mot a circulé qu'un nouveau site se montait, pour permettre de suivre sa progression d'écriture, et peut-être créer une nouvelle communauté, pour ce qui serait connu comme le [Writing Month](https://writingmonth.org/). Le mois de l'écriture. Pas de na, pas de no, juste de l'écriture.

[Benjamin “Amin” Hollon](https://benjaminhollon.com/) a travaillé dur pour coder le site. J'ai suivi sa progression, avec une impatience que je m'étonne un peu d'avoir eue. Et quelques jours avant novembre, on pouvait s'inscrire, trouver des camarades d'écriture, se choisir un projet (roman, nouvelle, poème, billets de blog, etc) et un but  (en mots, en pages, en nombre, etc). J'ai choisi d'écrire et de publier 30 articles en 30 jours[^2]. Ambitieux. L'idée était de me pousser à écrire chaque jour. Pour relancer la machine, pour me prouver que j'en étais capable.

Et je l'ai fait. Je ne suis pas fière de tous les billets, il y a certains qui sont, disons-le, inutiles[^3], mais dans l'ensemble, rien que je regrette d'avoir publié, rien de secret ou d'inavouable dévoilé.

Je sais donc que je suis capable de poster plus de deux billets par mois. Et ça a aussi, comme je le disais le 15, validé ma méthode de travail. J'ai encore des brouillons, des trucs qui traînent depuis longtemps, et pour lesquels il manque surtout des photos. Parce qu'écrire, ça va, je sais faire <span role="img" aria-label="Visage souriant, apaisé">😊</span>.

Mais alors par contre, plus jamais j'écris un billet par jour. C'était vraiment drainant, et pourtant j'y passais moins de temps qu'à avancer sur mon/mes romans les années précédentes. Probablement parce qu'au lieu d'un premier jet non-montrable et pour lequel je devais juste renseigner un nombre de mots, ma production était publique le jour-même. Donc en plus du contenu, il fallait corriger, relire, réfléchir. L'an prochain, si je refais le Writing Month avec un objectif semblable, ça sera plutôt 15, ou 10 billets, et sans obligation de les poster.

Voilà voilà. 

Maintenant je vais dormir. Enfin pas ce soir, on commence la saison 2 de notre campagne de Donj' (et ça me fait penser que je n'ai toujours pas réussi à faire une photo correcte de ma figurine, mais faut avouer qu'elle cherche, aussi, c'est une halfeline donc la figurine fait moins de 2 cm…).

[^1]: Bon, on peut arguer que quand on a écrit les 50000 mots à la fin du NaNo, c'est un premier jet, on va devoir enlever beaucoup, et donc rajouter beaucoup. La portée est surtout symbolique. 
[^2]: À la base j'avais mis 31 articles, parce que, heu, bon, heum, je me suis persuadée que novembre avait 31 jours. Et j'ai réalisé que non… le 30. En voyant fleurir les "I did it!" sur les rézosocios. Youpi, objectif atteint plus tôt que prévu ! 
[^3]: Par exemple, les billets tirés d'une amorce du bloguidien : je manquais vraiment d'inspiration, c'était le dernier recours. Et je ne suis pas très satisfaite, ce n'est pas vraiment ce que j'ai envie d'écrire ici (même si, avouons, parler de mon violon m'a donné envie de m'y remettre, mais bon, je crains le vœu pieux). J'ai écrit ça parce que je *devais écrire*, pas parce que j'en *avais envie*.
