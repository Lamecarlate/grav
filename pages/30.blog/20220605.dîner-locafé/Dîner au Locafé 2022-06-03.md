---
meta-création: "2022-06-04 15:11"
meta-modification: "2022-06-04 15:11"

stringified-date: "20220604151123"

title: "Dîner au Locafé, Grenoble"
template: "blog-item"

date: "2022-06-04 15:11"
publish_date: "2022-06-06 15:11"

taxonomy:
    type: article
    category:
        - cuisine
    tag:
        - restaurant

thumbnail: "dessert-2.jpg"

# plugin Highlight
highlight:
	enabled: false

---

Dîner au Locafé, restaurant végétalien, bio et local. Et excellent. Tout ça à la fois. 

===

Invitée par ma tante à découvrir ce restaurant, j'y suis allée vendredi soir avec elle et son fils.

Salle un peu bruyante au début, et après, je ne sais pas si je me suis habituée ou si le niveau sonore a effectivement baissé.

Nous avons opté pour le menu dégustation : les deux entrées, le plat et les deux desserts.

> Salade de lingots, moutarde et fanes de betterave. Betterave rôtie, pickles et en purée. Suprêmes de pamplemousse. Granité pamplemousse moutarde. Réduction betterave. Mayonnaise aux feuilles de moutarde. Gel vinaigre à la moutarde.

Une merveille pour commencer la soirée. Salade froide — très délicatement tiède pour les tranches de betterave rôtie. Les lingots (haricots blancs) sont cachés sous le reste dans la photo, et sont mêlés à des fanes de betterave cuites, et il y a un goût citronné très… cocoonant. Très évident. En fait, ça sera pour moi un peu le fil conducteur de ce repas : des associations surprenantes, et qui pourtant sonnent comme si je les connaissais depuis toujours, comme s'il n'y avait pas d'autres possibilités.

Les petites feuilles de moutarde sur le dessus, un délice ; la deuxième fois de ma vie que je mange cet aliment : la première fois, c'était lors d'une balade, une amie de mes parents en avait cueilli dans la garrigue.

![](entree-1.jpg " "){.figure-media--wide}

> Salade de pois chiches au sumac. Carottes rôties au sumac, pickles et en purée. Haricots verts blanchis et pickles. Purée haricots verts. Condiments aux fanes de carottes, amandes, citron confit et sumac. Crumble aux amandes et sumac. Suprêmes de citron, gel citron.

Cette seconde entrée est à notre goût un peu moindre : le moelleux des haricots blancs laisse place à des pois chiches un peu farineux, un peu bourratifs.  Mais les petites carottes et les haricots croquants relèvent le niveau. Le crumble d'amandes et les petits tas de purée de carottes détonnent.

![](entree-2.jpg " "){.figure-media--wide}

> Épeautre tiède. Choux fleur rôti au xawaash et en purée. Mange-touts rôtis au xawaash, pickles et en purée. Oignons rôtis et pickles. Mascarpone au xawaash. Noix torréfiées et tuiles aux noix.

Un délice. Les mange-tout, j'adore, et ceux-ci sont tendres, pliables, goûteux. Les petits condiments sont d'une douceur ! Le mascarpone m'intrigue – c'est un restaurant végétalien – j'aurais dû leur demander la recette 😛 

Le xawaash, découvré-je, est un mélange d'épices somalien, qui pourrait être un équivalent du ras-el-hanout maghrébien : chaque famille a sa recette, mais il semble que cumin et coriandre soient la base, et ensuite gingembre, poivre, cannelle. Peut-être un peu trop subtil pour mes papilles – va falloir retester !

Le petit oignon rôti : une tuerie. Les petits radis (ou bien était-ce des navets minuscules ?) sont succulents.

![](plat.jpg " "){.figure-media--wide}

> Sorbet à la cerise. Gelée à l'estragon. Condiment estragon. Cerises fraîches et cerises au sirop. Biscuit au son de blé.

L'estragon ne me semble pas une herbe facile à traiter, et ici il l'est merveilleusement. La gelée à l'estragon est une belle trouvaille – il m'a semblé qu'il était aussi au jus de cerise, mais à un moment ça devient difficile de distinguer exactement les goûts, les papilles et le cerveau étant fort sollicités depuis le début de la soirée.

![](dessert-1.jpg)

La photo vu de dessus n'étant pas extra, j'en ai fait une autre :

![](dessert-1-gros-plan.jpg " "){.figure-media--wide}

(oui, il y a beaucoup de bruit, j'ai évité le flash)

Je discute un peu avec la serveuse sur l'association cerise-estragon, qui ne me serait pas venue à l'idée, et elle répond que fruits et herbes, ça va toujours bien ensemble. "Je fais des mojitos à la sauge, par exemple". Joyeuse, j'assène que la prochaine fois que je viens au Locafé, je prendrai ça, et elle, un peu confuse, dit "Non non mais c'est chez moi que je fais ça". Et moi, ne perdant donc pas une seconde pour me rendre ridicule : "Ah ! Vous m'invitez ?". Gentille serveuse, si vous lisez ceci, sachez que la honte est ma nouvelle colocataire. Encore pardon.

(mais ça m'a donné des idées pour piocher dans le bloc de sauge du jardin partagé)

> Soupe glacée de fraises et Rhubarbe. Siphon menthe. Meringue menthe. Gel Fraise et Rhubarbes. Fraises fraîches. Menthe. Rhubarbe pochée.

Et pour finir ce repas, encore un accord fruit rouge et herbe aromatique, plus conventionnel celui-là, mais toujours efficace. La crème de menthe au siphon est extra, grasse juste comme il faut pour napper le palais et légère en même temps. La rhubarbe n'est pas sucrée du tout et fait un contrepoint avec la fraise, naturellement plus forte en goût.

![](dessert-2.jpg " "){.figure-media--wide}

Plus un kombucha maison rhubarbe et estragon, parfaitement raccord avec les plats. Je n'ai pas aimé son odeur, assez fermentée, presque ammoniaquée, mais son goût était excellent, frais, très pétillant, avec la note verte de l'estragon très marquée. Et il m'a duré tout le repas.

---

Je conseille grandement, nous nous sommes absolument régalé⋅es. Le menu change toutes les semaines : je reviendrai.

Prix : 28€ pour les 5 plats, 3€ pour le grand verre de kombucha.

Les quantités étaient très bien ajustées : nous étions rassasié⋅es après notre repas, mais pas alourdi⋅es (mais le retour en vélo a été bienvenu).

Locafé  
31 Rue d'Alembert, Grenoble  
[Site web du Locafé](https://www.locafegrenoble.com)
