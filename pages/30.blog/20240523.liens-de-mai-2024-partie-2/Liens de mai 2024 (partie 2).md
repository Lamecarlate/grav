---
meta-création: "2024-05-23 07:37"
meta-modification: "2024-06-03 20:23:44"

stringified-date: "20240523.073702"

title: "Liens de mai 2024 (partie 2)"
template: "blog-item"

date: "2024-06-03 19:23:50"

taxonomy:
  type: article
  category:
    - liens
  tag:
    - art
    - mignonneries
    - humour
    - cinéma
    - linguistique

highlight:
  enabled: false
---

Allez, on fait dans le doux, en cette fin de mois de mai / début du mois de juin. Ya besoin.

===

![Aquarelle d'un ciel très nuageux, sombre, au-dessus d'une colline vert foncé](394683c0332075fd.jpeg){.figure-media--wide}

J'aime beaucoup cette aquarelle de [Julia Bausenhardt](https://mastodon.art/@naturesketchbook/112483663103394201), d'une simplicité redoutable.

[Élan femelle et ses faons, par Choy Mears](https://sfba.social/@MicheleV_AK/112477990241246890).

[Face Your Fears (feat. Donna Lynne Champlin) - "Crazy Ex-Girlfriend"](https://www.youtube.com/watch?v=brzZQBSVMX0) (vidéo avec du son et des sous-titres en anglais, 3 minutes) : c'est un extrait de la série _Crazy Ex-Girlfriend_, qu'il faudra bien que je voie un jour (c'est quand qu'on a des journées d'une semaine pour avoir le temps de tout faire ?), et c'est bien drôle. Via [Rouge](https://piaille.fr/@Blanche/112501816180068232).

> Chez eux, pas de poumons, trachées ou branchies : ils respirent par la surface de leur peau. Celle-ci est très fine (une seule couche de cellules) et est recouverte de mucus (produit par des cellules a la surface de la peau) […]. Le dioxygène passe par le mucus puis les cellules de la peau puis le système sanguin du ver de terre.

> Comme la respiration dépend de la surface et que leur besoin en oxygène évolue avec le volume, on peut calculer que leur diamètre maximal est de 3 cm. Et en effet certains vers ont ce diamètre, pas plus. Et ils font 3 m de longueur.

Ohhhh. Un peu d'infodump sur les vers de terre, par [Marie-Chantal/Nausicaa](https://pipou.academy/@Nausicaa/106381755168071368).

[Arrêtez de Tout Politiser ! - 2 Girls 1 Cup](https://www.youtube.com/watch?v=nldE2UqC71g) (vidéo avec du son et sous-titres, 15 minutes). Analyse intéressante sur la notoriété d'une fameuse vidéo (si vous ne connaissez pas, ne la cherchez pas ; et, que vous connaissiez ou non, il n'y a aucun extrait dans cette vidéo, c'est <i lang="en">safe</i>). J'adore la séquence nostalgie du début des années 2000 sur Internet. (je n'ai pas fait la choré de Yatta en me dandinant sur mon siège, vous n'avez aucune preuve)

[Why French sounds so unlike other Romance languages](https://www.youtube.com/watch?v=a2TWBBxwhbU) (vidéo avec du son, des sous-titres anglais, mais la traduction automatique vers le français semble plutôt excellente, 12 minutes) : comment la langue française a évolué depuis le latin, incluant les langues alentour. C'est présenté de manière sympathique, comme une recette de cuisine (et pourtant, j'ai d'habitude horreur de cette analogie, que je trouve sur-utilisée), avec de nombreuses sources en description. Le narrateur prononce très bien, passant de l'anglais au français médiéval sans sourciller, il se fait même féliciter dans les commentaires par des Français qui ont étudié l'ancien français. Et j'ai enfin compris pourquoi au Québec on dit « frette » quand en métropole c'est « froid » !

Dans les commentaires :

<blockquote lang="en">Let's be honest here, the French just change their language every time they feel like too many foreigners can understand them. "Quick Jean-Pierre, the peasants are figuring out we are mocking them again. Release a bunch of new letters with funny little hats and let's stop pronouncing five old ones."</blockquote>

Traduction à la volée :

> Soyons honnêtes, les Français changent juste leur langue chaque fois qu'ils ont l'impression que trop d'étrangers peuvent les comprendre. « Vite, Jean-Pierre, les paysans commencent à réaliser qu'on est encore en train de se moquer d'eux. Ajoute un paquet de nouvelles lettres avec des petits chapeaux rigolos et arrêtons d'en prononcer cinq autres. »

![Dessin numérique d'une hyène et ses petits : chacun représente un drapeau de la communauté LGBTQIA+](e8a9d5c4f2b74215.jpg){.figure-media--wide}

Texte de l'image : <i lang="en">Safety for LGBTQIA+ people is the only gay agenda</i>.

Voilà. Aimez-vous, bon sang. Et laissez les autres aimer et être. Bon sang.

Via [Mlice](https://meow.social/@Mlice/112502508887524419) qui est l'auteurice du dessin.

[The Story of Chuggy The Channel Tunnel Digger](https://www.youtube.com/watch?v=C8gW3oi3urM) (vidéo avec du son, en anglais, sans sous-titres, 5 minutes) : les humains sont très forts pour anthropomorphiser les objets… Ce sketch est hilarant et terriblement triste à la fois. Via [Carbonara based life form](https://tech.lgbt/@risottobias/112502217794697486).
