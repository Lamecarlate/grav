---
meta-création: "2024-02-04 08:40"  
meta-modification: "2024-02-18 16:04:58 "  

stringified-date: "20240204.084012"

title: "Liens de février (partie 1)"  
template: "blog-item"  

date: "2024-02-18 08:40"  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - histoire
        - musique
        - philosophie
        - mignonneries

# plugin Highlight
highlight:
    enabled: false

---

Au programme : de l'histoire, des chats, une enluminure qui claque, du jeu vidéo, des souvenirs altérés, du jeu de mot, des chats, de la musique à la scie, et de la philosophie.

===

[The gay man who brought Batman to life](https://www.youtube.com/watch?v=nryCo713s20) (vidéo avec du son, en anglais, sans sous-titre, environ une demi-heure) : une courte biographie de Kevin Conroy, acteur notamment connu pour avoir doublé Batman dans la série animée des années 90. Je ne savais pas qu'il était gay, et de ce que je comprends de la vidéo, le fait qu'il ait dû cacher cette part de sa personnalité pendant sa carrière, le fait qu'il ait eu comme une double vie, l'a rapproché du personnage de Batman et lui a permis de porter sa cape, avec brio. Via [Augier](https://diaspodon.fr/@AugierLe42e/111845991870369291) (qui poste beaucoup de liens vers des vidéos de vulgarisation, d'histoire, d'éducation populaire, allez le lire, et suivez-le si vous êtes sur le fédiverse).

Bon, hein, comment tu veux que je nomme ce lien autrement que ['Murica](https://pizzacakecomic.com/post/741342943740411904), hein ?

[L'épidémie du sommeil](https://www.youtube.com/watch?v=-zBa1wEAE5E) (vidéo avec du son, en français, sans sous-titres, environ 20 minutes) : j'ignorais tout à fait cette histoire, des milliers de gens qui étaient dans une sorte de demi-sommeil, mais comme ça a démarré en 1916, l'Europe et les alentours avaient d'autres inquiétudes… Via [Trucs de philo](https://www.youtube.com/@Trucsdephilo) que je découvre par [Monsieur Phi](https://www.youtube.com/channel/UCqA8H22FwgBVcF3GJpp0MQw).

![2fb8624141dbd32a](2fb8624141dbd32a.jpg)

Source : [FluffyKittenSpamBot](https://mastodon.bida.im/@fluffykittenspambot/111739090700487414).

[Pub Société Générale 1972](https://youtu.be/C2FQSKrWwCE?si=jWE9Z8_SUxrtkgry) (vidéo avec du son, en français chanté, sans sous-titres). Whooosh. Ça oscille entre l'extrêmement malaisant et l'incroyablement génial. Tellement typique des années 1970 (ou plutôt de l'imagerie qui en a été transmise) : bottes de cuir et mini-jupes, orange et marron partout, image psychédélique sur le mur, et les lunettes ! Source : [Oracle](https://piaille.fr/@oracle/111886556700415399). Notons tout de même que la Société Générale fait partie [des banques qui financent le plus les énergies fossiles](https://piaille.fr/@charles@akk.de-lacom.be/111886697428548474), que la jolie musique pop ne le fasse pas oublier.

[La plus cool des enluminures](https://mamot.fr/@kawoua/111884168314673490) (divulgâchage : c'est une chauve-souris). Il faut ouvrir l'image ou bien passer sa souris dessus, c'est animé.

![Peinture numérique d'une branche de mimosas, feuilles vert sombre et petites boules jaunes toutes fluffy, lumineuses, sur fond d'un ciel gris un peu tristou.](652d74dd531ebeb5.jpg "De jolis mimosas"){.figure-media--wide}

L'image plus haut vient de [Reports From Unknown Places](https://mastodon.art/@clever_reports/111898276935494865). J'aime bien le travail de Ninn Salaün, et je suis impressionnée parce qu'elle semble faire une peinture par jour. 

Imagine, tu fais un jeu vidéo, et tu veux mettre une porte dedans. C'est simple, non ? Non ? Non : [The door problem, par Liz England](https://lizengland.com/blog/2014/04/the-door-problem/) (en anglais). Via [l'épisode de Game Next Door « Créer un jeu vidéo c'est compliqué »](https://www.youtube.com/watch?v=ZRem0Na5kCE) (vidéo avec du son, avec sous-titres), regardez GND, c'est vraiment sympa.

[Can generative AI help write accessible code?](https://tetralogical.com/blog/2024/02/12/can-generative-ai-help-write-accessible-code/) (en anglais) : Leonie Watson se pose la question de si les IA génératives sont capables de produire du code accessible. Le test a été effectué avec Bard, ChatGPT, et Fix my code.

[Altérée, le podcast](https://www.radiofrance.fr/franceinter/podcasts/les-podcasts-du-week-end/les-podcasts-du-week-end-du-samedi-21-octobre-2023-6901492) : une fiction sonore bien chouette en trois petites heures. En quelques mots : Alicia ne se sent pas très bien ces temps-ci, elle a des souvenirs qui ne lui appartiennent pas, et ne comprend pas ce qui lui arrive. Elle demande autour d'elle, discute avec sa mère, fouille les forums, voit un hypnothérapeuthe, bref, la base. C'est une bonne histoire, bien ficelée, bien jouée. Via [Air](https://framapiaf.org/@Air/111883824429543240).

[L'accro d'Nîmes](https://grisebouille.net/laccro-dnimes/) : Gee en forme, sur les acronymes. Article rempli de jeux de mollets, mais moi je trouve ça beau. (voilà, ça c'est fait)

[Extraordinaire livepouet d'une partie de jeu de rôle, vue de l'extérieur](https://mamot.fr/@juliegiovacchini/111917634239220581)

> "Vous dépecez le loup" "on peut prendre les intestins ?" "mais tu veux faire quoi avec les intestins ?"  
> Je vais fermer la porte, il faut savoir ne pas récolter certaines informations.

Hihihihihihi.

> Ils viennent de s'enfiler 7 pizzas

Petits joueurs.

> Ils jettent des dés en chantant baby shark ?!

Les gens qui font du jeu de rôle c'est tous des, heu, ah, je vais me taire je crois sinon je vais me faire virer de ma table de JdR.

[Si un voyageur monte à Nation et descend à Étoile, il est impossible de dire s'il est passé par la ligne 2 ou la 6](https://piaille.fr/@slaettaratindur/111934340274108344) : démonstration magistrale de physique quanto-métroïde.

[Pourriel la chatte est très photogénique](https://boitam.eu/@joachim/111925015961563537).

[Murmuration d'oiseaux](https://piaille.fr/@baktelraalis/111892547794154602) (vidéo avec du son, pas forcément nécessaire mais qui accompagne bien, c'est une valse) : woooooh. Hypnotique.

[Synchronized Sleeping](https://toot.cat/@yudderick/111899485801259987)

[Nan mais c'est une remarque pertinente](https://piaille.fr/@scudery/111900599410542483). 

[Ghost fight, de l'OST d'Undertale, interprété par InstrumentManiac](https://www.youtube.com/watch?v=ExBMKyJ4SnA) (vidéo avec du son, un peu de paroles à la fin, en anglais, sans sous-titres). Je découvre le musicien et vidéaste InstrumentManiac, qui s'amuse à interpréter des musiques, notamment de jeux vidéo, avec des instruments classiques. C'est très chouette ! Ici, <i lang="en">Ghost fight</i>, de l'OST du jeu _Undertale_, où la mélodie est jouée à la scie.

---

J'ai dormi 11 heures cette nuit, ça devrait al- \**bâââââââââille\** -ler.
