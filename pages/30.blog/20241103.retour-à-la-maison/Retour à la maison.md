---
meta-création: "2024-11-03 12:06"  
meta-modification: "2024-11-03 12:06"  

stringified-date: "20241103.120614"

title: "Retour à la maison"   
template: "blog-item"  

date: "2024-11-03 12:06"  
update_date: ""

taxonomy:
    type: article
    tag:
        - paysage
        - vacances
        - personnel
        - writing month
        - writing month 2024

# plugin Highlight
highlight:
    enabled: false

---

C'était bien les vacances. C'est très bien de rentrer aussi.

===

On a été exemplaires. La maison de location a été rangée et sommairement nettoyée avant 9h.

Sur la route du retour, des vaches blondes et rousses, des moutons avec une chèvre au milieu du troupeau, un très joli plissement de terrain, un lac (à peine aperçu, j'ai surtout vu le barrage qui en est à l'origine).

![Photo à l'avant-plan flou à cause du mouvement, avec une montagne à l'arrière-plan et un ciel très bleu.](IMG_20241103_093617--small.jpg)

Nan mais regardez-moi ce plissement :

![Gros plan de la photo précédente sur un plissement de terrain très marqué, avec deux vagues.](IMG_20241103_093617--closeup.jpg)
