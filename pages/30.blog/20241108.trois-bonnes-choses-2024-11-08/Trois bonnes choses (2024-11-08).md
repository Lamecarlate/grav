---
meta-création: 2024-11-08 21:51
meta-modification: 2024-11-08 21:51
stringified-date: "20241108.215118"
title: Trois bonnes choses (2024-11-08)
template: blog-item
date: 2024-11-08 21:51
update_date: ""
taxonomy:
  type: article
  tag:
    - personnel
    - chats
    - mignonneries
    - cuisine
    - writing month
    - writing month 2024
highlight:
  enabled: false
---

Trois bonnes choses aujourd'hui. (et peut-être un peu plus)

===

Atrus le chat est venu s'installer sur mes genoux quand il est rentré de dehors, il ronronnait et me donnait des coups de boule d'amour en se blottissant dans le creux de mon bras. (pendant qu'Edora la chatte, auparavant couchée en rond sur le siège à côté, était assise, un air boudeur sur le visage, comme si c'était une honte que ça ne soit pas elle sur moi à cet instant précis, et c'est mignon quand même)

Hier j'ai acheté plein de bonnes choses au magasin asiatique du coin, et aujourd'hui j'ai recouvert mon bol de riz sauté avec du <i lang="jp">furikake</i> sésame et wasabi, c'est trop bien. (et en plus c'est dans un petit pot en verre, que je pourrai donc récupérer et utiliser plus tard)

Petite marche rapide pour rejoindre l'Amoureux à l'arrêt de bus afin d'aller s'empiffr- se rempl- baffr- dîner au resto japonais avec des mini-<i lang="jp">shinkansen</i> rigolos sur des tapis roulants pour apporter les plats. (après ça nous avons acheté quelques chocolats et la dame nous en a offert un de plus chacun, juste pour goûter, et décidément chocolat et menthe forte, ça marche bien)
