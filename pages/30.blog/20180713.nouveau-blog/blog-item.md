---
title: Nouveau site, nouveau blog, nouveau départ ?

published: true
date: 2018-07-12 14:00
taxonomy:
    type: article
    category:
    tag:
header_image_file: If_it_fits,_we_sits_-_Imgur.jpg
header_image_caption: 'Source : <a href="https://imgur.com/OFSNsIY">If it fits, we sits</a>'
author: 'Lara'

---

Longtemps j'ai tergiversé : je suis Lara dans le monde physique, développeuse, gribouilleuse et gourmande, et je suis Lamecarlate sur le net, développeuse, gribouilleuse et gourmande. Mais c'est quoi la différence, en fait ? Il semble bien qu'il n'y en ait guère, après tout…

===

Je profite de la refonte de mon site - tant repoussée elle aussi - pour définitivement fusionner mes identités tout comme mes sites. Je suis une pierre à multiples facettes : autant qu'elles soient toutes visibles d'un coup ;)

Côté technique : j'ai découvert le CMS [Grav](https://getgrav.org) il y a près d'un an, et suis tombée en amour direct. Pensez donc ! Un CMS dont le contenu est fait en [markdown](https://daringfireball.net/projects/markdown/), ma manière préférée d'écrire ! Sans base de données ! En Symfony, et dont les thèmes s'écrivent en Twig ! Tout pour me plaire. J'ai donc entrepris cette refonte, par petites touches, avec de grands hiatus quand l'inspiration ne venait pas. Donc, de nombreux mois et deux thèmes plus tard (parce que j'ai complètement réécrit le précédent), voilà la première mouture.

Ce n'est pas parfait, ni complet : il manque notamment toute la partie Portfolio, pour laquelle je n'ai pas encore statué côté design.

Je suis développeuse et non <i lang="en">designer</i> : le processus de création est assez long pour moi, car je code en même temps, et le résultat est quelque fois trop… personnel, alors qu'il devrait être au service de l'utilisateurice. Ici j'ai fait au plus sobre, au plus simple (ça ne veut pas forcément dire facile :P), j'espère que c'est suffisamment lisible et agréable.

Pour la ligne éditoriale ici : en gros, de tout. Je parlerai de développement, d'informatique en général, de création si le feu sacré me revient, de cuisine également, il y aura des billets d'humeur et probablement des photos de chats et des macros de buissons. Chuis chez moi chfais c'que je veux.

Et je vous souhaite un agréable voyage.

P.-S.

Vous pouvez aller voir mes anciens blogs : ils ne seront plus à mis à jour, mais je les maintiens en ligne pour les archives.

Chacun décrivait une facette de moi. Je m'étais amusée à prendre comme mascotte pour chacun un animal mythologique (j'ai gardé la Coquecigrue pour ici, symbole de la fusion)

* cuisine et gourmandise : [Basiliculinaire](https://basilic.lamecarlate.net/)
* informatique, surtout développement : [Codex sphinxial](https://informatique.lamecarlate.net/)
* création graphique : [Au Griffon griffonnant](https://griffon.lamecarlate.net/)
* et l'ancien [Coquecigrue](https://coquecigrue.lamecarlate.net/)
