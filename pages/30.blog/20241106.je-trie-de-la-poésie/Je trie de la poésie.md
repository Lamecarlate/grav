---
meta-création: 2024-11-06 18:59
meta-modification: 2024-11-06 18:59
stringified-date: "20241106.185953"
title: « Je trie de la poésie »
template: blog-item
date: 2024-11-06 18:59
update_date: ""
taxonomy:
  type: article
  tag:
    - écriture
    - création
    - nouvelle
    - writing month
    - writing month 2024
highlight:
  enabled: false
---

Micro-texte pensé il y a quelques années, il était temps qu'il arrive ici.

===

Cette soirée traînait un peu en longueur, et, voyant que je m'ennuyais, mon hôtesse me présenta un des invités.

– Et qu'est-ce que tu fais, comme métier ?  
– Je trie de la poésie.

Je marquai un temps, pensant avoir mal compris.

– Tu écris de la poésie ?  
– Non non, je la trie. Je prends des textes, et je les arrange, lettre par lettre, pour que ça prenne moins de place.

Il me montra un de ses derniers travaux.

Et c'était vrai que ça prenait moins de place.
