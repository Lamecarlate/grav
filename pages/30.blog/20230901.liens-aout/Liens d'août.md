---
meta-création: "2023-08-05 22:15"  
meta-modification: "2023-09-01 18:30:00"  

title: "Liens et pensées d'août"  
template: "blog-item"  

date: "2023-09-01 18:00"  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - sciences
        - art
        - politique
        - histoire
        - informatique
        - humour

# plugin Highlight
highlight:
    enabled: false

---

Je tente un nouveau type d'article : comme je lis beaucoup sur les réseaux et le net en général, je récolte de nombreuses choses, et j'ai envie de les partager chez moi.

===

J'ai décidé d'en faire un article mensuel plutôt que quotidien ou hebdomadaire parce que je ne suis pas sûre d'avoir la volonté nécessaire pour poster si souvent. Nous verrons comment cela évolue.

Les liens sont chronologiques par rapport à moi, à ma découverte : ce sont des actus, des jolies photos, des cartoons, des vieux articles de 2017 ( 😛 ).

## Lectures du mois

- Je m'habillerai de nuit, Terry Pratchett (fini)
- La maison des feuilles, Mark Z. Danielewski
- La nouvelle vie d'Arsène Lupin, Adrien Goetz
- L'espoir malgré tout, première partie, Émile Bravo (fini)
- L'espoir malgré tout, deuxième partie, Émile Bravo (fini)

## 5 août

[Une magnifique photo d'un magnifique insecte](https://mastodon.online/@alexwild/110812058695845726).

Via [Krysalia](https://mastodon.social/@Krysalia/11085340749675174), (re)découverte de la [broderie sashiko](https://www.youtube.com/watch?v=bj2eZcMWsNo). Envie de m'y mettre, j'ai pas mal de vêtements à réparer, et cela me semble un bon moyen, assez résilient. Cela met un peu à mal ma croyance que plus on passe l'aiguille plus ça fragilise le tissu : ça semble être littéralement l'inverse. 

Note : j'ai commencé, et j'ai posté [un article sur le sujet](https://lamecarlate.net/carnet/premier-essai-de-sashiko). Et puis [un autre](https://lamecarlate.net/carnet/sashiko-la-suite) 😸

[Defund the police](https://mastodon.online/@knwmoon/110852362975711992) : en quelques mots, pourquoi définancer la police (un mot-clé de plus en plus fréquent aux États-Unis d'Amérique) pourrait marcher. Parce que la police ne devrait pas avoir à tout gérer, ce n'est pas son boulot. Éduquer les jeunes au lieu de les laisser sans soutien et après les coffrer. Soigner les personnes dépendantes de drogue au lieu de les criminaliser. Travailler à loger tout le monde. Décriminaliser et réguler le travail du sexe (qui est un vrai travail, et ne devrait pas être entre les mains de gangs ou de maquereaux). Etc, etc.

## 7 août

[Kern Type](https://type.method.ac) : un jeu où il faut réaligner des lettres. J'ai eu 97/100, je ne suis plus que frime et orgueil (hey, pour une fois que ma malédiction personnelle, celle de voir les mauvais crénages (oui, c'est le mot français pour <i lang="en">kerning</i>), peut me servir !)

## 9 août

Un onglet dans mon navigateur depuis des mois : [Pain](http://accessiblejoe.com/pain/), chez Accessible Joe. C'est un gros morceau, difficile à lire, à comprendre (cognitivement), à assimiler (émotionnellement). Il est empli de liens vers d'autres sites, vers des vidéos, c'est une spirale infernale.

Tiens, intéressant comme le neutre et littéraire <i lang="en">rabbit hole</i> devient en français très connoté : spirale infernale, puits sans fond. Voir [The Thing about Rabbit Holes](https://medium.com/learning-paths-io/the-thing-about-rabbit-holes-3dd0960975ee)

> While the meaning conveyed by this definition has a negative connotation, the term **rabbit hole** is generally used in positive connotation in various contexts. Examples of rabbit hole being used in positive connotation include “Alice in Wonderland”, internet, learning and research.

Note : je n'ai toujours pas fini…

Note supplémentaire : son auteur étant décédé il y a quelques mois, j'ai archivé [la page dans la Wayback Machine](https://web.archive.org/web/20230827113020/http://accessiblejoe.com/pain/).

## 10 août

En parlant de douleur : je lis _Je m'habillerai de nuit_, de Terry Pratchett. Sans grand divulgâchage, le vieux baron meurt assez tôt. La protagoniste, Tiphaine Patraque, sorcière, s'est occupé de lui jusqu'à la fin, en lui ôtant sa douleur constante (c'est un truc que savent faire les sorcières). Lors des funérailles du baron, les gens disent « C'était un brave homme », « Il a bien profité de la vie » et « Au moins, il n'a pas souffert ». Ce sont des platitudes tout à fait acceptables dans ces moments-là, et la dernière résonne bien entendu plus fort, parce que personne ne sait vraiment ce que faisait Tiphaine. 

Je retombe sur [Wolf](https://www.smbc-comics.com/comic/wolf) sur SBMC, à propos du quatrième petit cochon de l'histoire.

[Sand Spheres & Neolithic Balls](https://mastodonapp.uk/@tess_machling/110853093306651096) : 1) je veux une de ces balles 2) les néolithiques ou les modernes ? OUI. 3) il faut que je me renseigne sur les [balles néolithiques]( https://en.wikipedia.org/wiki/Carved_stone_balls ), pour comprendre leur fonction (divulgâchage : on ne sait toujours pas, en fait, comme souvent pour les objets préhistoriques).

Via un échange d'e-mail avec l'Amoureux, me revient en mémoire [Lucien de Samosate](https://fr.wikipedia.org/wiki/Lucien_de_Samosate), auteur satirique grec du II<sup>e</sup> siècle apr. J.C. Je l'ai découvert au collège, en cours de latin ou de grec (impossible de situer pour sûr entre la 5<sup>e</sup> et la 4<sup>e</sup>). Son _Histoire véritable_, où il décrit un voyage en navire vers la Lune, est considérée comme une des premières œuvres de science-fiction, même si, comme le dit Wikipédia, « il s'agit plutôt d'un conte facétieux sans aucune base scientifique ». Je me souviens que le mari de ma prof de latin-grec, lui aussi prof de latin, avait fait des illustrations suivant les descriptions des luniens, je crois qu'il était question de gland à la place du nez (j'ose espérer, désormais adulte, qu'il s'agissait bien de glands de chêne, et j'avoue n'être pas sûre de vouloir vérifier).

[Une histoire tragique](https://mamot.fr/@nhoizey/110860240348923349) : j'ai envie d'écrire un article sur l'origine de la bière, et du pain, et des deux ensemble, en fait. Un jour.

Dans mes onglets, à lire : [History of water supply and sanitation](https://en.wikipedia.org/wiki/History_of_water_supply_and_sanitation) .

[Une vidéo de chat qui se frotte à un sac à dos](https://piaille.fr/@Lesdondons/110865663670557811) (mes chats font ça aussi, la fascination pour le sac ou le panier venant du dehors est intense).

Tiens, ça faisait longtemps qu'un strip de xkcd m'avait bien fait rire, de manière intelligente : [What to do](https://xkcd.com/2813/) La [page sur xkcd explained](https://www.explainxkcd.com/wiki/index.php/2813:_What_To_Do) décrit bien l'image.

![Une grille d'actions (parler fort et reculer, courir vers un bâtiment, quitter calmement le bâtiment, appliquer une forte pression) et de problèmes (un puma, la foudre, une alarme incendie, et un saignement), croisé⋅es, qui montre, par exemple qu'appliquer une forte pression sur un puma n'est pas la bonne solution.](https://imgs.xkcd.com/comics/what_to_do.png)

[Bad Apple! en 8 bit](https://www.youtube.com/watch?v=Qsxp6U0M_NA), tant le son que l'image. Bon, ça ne détrône pas [la version originale](https://www.nicovideo.jp/watch/sm8628149) dans mon cœur. Pour enlever les messages qui s'affichent sur la vidéo, il faut cliquer sur le petit phylactère en bas à droite de l'interface (yargl, une div dans un button, il faut vraiment que j'arrête de regarder le code des pages que je visite).

![Capture d'un message sur le fediverse, le contenu est dans le lien qui suit](myawn.png)

[Myawn](https://hackers.town/@netkitty/110863154050855758). KITTEN.EXE est le compte fediverse d'un petit chat virtuel très sympathique, hérité du vénérable logiciel, où ce chat se baladait sur l'écran, dormait sur le dessus des fenêtres et coursait la souris (je l'ai eu ! il y avait pas mal de variantes, en plus, c'était drôle). 

J'ai enfin (re ?) trouvé [le script du film _Le Roi et l'Oiseau_](http://marnie.alfred.free.fr/script.htm) !!! \**danse de la joie\**

## 11 août

[ChatGPT's odds of getting code questions correct are worse than a coin flip](https://www.theregister.com/2023/08/07/chatgpt_stack_overflow_ai/) via [AnnaBaguenaude](ttps://cupoftea.social/@AnnaBaguenaude/110870072211974823). Trop petit échantillon (520 articles, 12 participants) pour être significatif, mais c'est intéressant. Il faudra refaire, à plus grande échelle.

[Tigers are virtually extinct](https://mastodon.world/@loadingartist/110553792432806201) ([page sur le site de l'artiste](https://loadingartist.com/comic/virtually-extinct/), mais sur le pouet Mastodon il y a un texte alternatif très descriptif).

[Pouet de niche de Denis Colombi](https://piaille.fr/@uneheuredepeine/110870567254413658) et [réponse encore plus de niche](https://piaille.fr/@shaft/110870588123341850).

["I'm a Barbie girl" again, but in the style of 6 classical composers 🎹 🎤 - Josep Castanyer Alonso](https://www.youtube.com/watch?v=WRfsibwR5x4) via [fx dechaume-moncharmont](https://mamot.fr/@fxdm/110870765741506502) : un très beau tour de force, et le pianiste a l'air de bien se marrer.

## 12 août

[Comment avoir une conversation philosophique avec son chat](https://eldritch.cafe/@TheOtterDragon/110875478077313475).

## 13 août

[3D-printing nice pots](https://mastodon.gamedev.place/@mar_lard/110683901431086855) : pots à plantes succulentes imprimés en 3D ; j'adore le second pot, avec le petit escalier. Le pot a l'air bien complet, en plus, avec gestion de l'excès d'eau : voir [sa page sur Thingiverse](https://www.thingiverse.com/thing:2821572).

Chromodoris willani par Alan Lee, via [Romain_EdlB](https://piaille.fr/@romain_edlb/110824735832960521). Les nudibranches c'est trop mignon.

![Photo d'un nudibranche bleu, rayé de noir, avec de petites antennes grises perlées de bleu, ultra-choupi](Chromodoris%20willani.jpg)

## 14 août

[Jolies images de temple japonais](https://toots.benpro.fr/@benoit/110887985572194027).

[Tout ça est-ce de la science fiction ?](https://peertube.ecologie.bzh/w/6Zx32skw4Ma1SbgaNter14) d'Olivier alias Greenman dans le cadre de l'évènement "Entrée Libre #3" : une conférence (1 heure) sur les liens entre SF et réalité, électronique, robots et IA. Plutôt sympa, et il **faut** que je lise le cycle de la Culture de Iain Banks, ça n'a que trop traîné.

[Des oiseaux en pixel-art](https://mamot.fr/@bbecquet/110911291614256571).

## 15 août

[🌫️ Social Brouillard (comment les réseaux nous embrouillent)](https://www.youtube.com/watch?v=DmO_yisKBRo) , de Defakator : la vidéo n'est pas récente, je remonte dans mes flux RSS, mais c'est un excellent exemple de vidéo « interactive », ou d'escape game, pour permettre à des informations intéressantes de rentrer plus facilement.

[Neil Gaiman fait de la magie](https://mastodon.social/@neilhimself/110890933089005040).

## 16 août

[La contribution d'une enfant de 4 ans à la documentation du kernel Linux](https://mementomori.social/@ikkeT/110897755861054765) (le [commit en question](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=690b0543a813b0ecfc51b0374c0ce6c8275435f0), pour preuve)

[We use too many damn modals. Let's just not.](https://modalzmodalzmodalz.com/) via [annso](https://framapiaf.org/@annso/110899378796254236)

[Des potichats qui font la bagarre](https://www.reddit.com/r/Kitten/comments/15rs8a0/kitten_wrestling/)

J'aime les collisions : hier j'ai regardé [un épisode de _Kurzgesagt_ sur la variole](https://www.youtube.com/watch?v=Kr57ax0OWMk), et aujourd'hui dans mes oreilles [le podcast de _La méthode scientifique_](https://www.radiofrance.fr/franceculture/podcasts/la-methode-scientifique/variole-le-retour-7609834) en parle aussi. Leurs dates de diffusion sont très différentes (8 août 2023 pour Kurzgesagt, 27 septembre 2018 pour La méthode scientifique, j'écoute les archives et remonte dans mon flux RSS), mais c'est amusant de les rencontrer aussi proches dans le temps de mon point de vue. Et je suis ravie d'exister dans un monde où la variole a été éradiquée.

Je me souviens d'une petite histoire qui circule sur le net : une maman et son petit garçon sont dans un parc. Le garçonnet demande à sa mère quel est l'étrange marque ovale qu'elle a au bras. Elle lui répond que c'est son vaccin contre la variole. Le petit reprend : « Pourquoi je n'en ai pas, moi ? » « Parce que ça a marché. ».

(j'ai envie de faire un article complet sur la variole)(un jour)

## 17 août

Via [Krysalia](https://mastodon.social/@Krysalia/110903952368505780), [une réflexion sur la fragilité du contenu d'Internet](https://indieweb.social/@emilygorcenski/110901482577172444), notamment après la mort des personnes.

[Un chat arc-en-ciel](https://piaille.fr/@Emmaf_77/110904676190990688)/

[Poème pour un petit chat virtuel](https://beige.party/@Tim_McTuffty/110898961998738560) (le même KITTEN.EXE vu plus tôt).

Via [Dryusdan](https://social.dryusdan.fr/notice/AYorhJLrwBhnDvqtGq), une [pétition de Mozilla pour essayer d'empêcher le gouvernement français de voter une loi qui forcerait les navigateurs à censurer les sites](https://foundation.mozilla.org/fr/campaigns/sign-our-petition-to-stop-france-from-forcing-browsers-like-mozillas-firefox-to-censor-websites/). La page manque cruellement de sources. En voilà un peu :

- [Un fil sur le fediverse, chez Phitassel](https://mamot.fr/@Phitassel/110900927799852606)
- [L'article sur le blog de Mozilla](https://blog.mozilla.org/netpolicy/2023/06/27/francaise-bloquer-sites/)

Via [Sebsauvage](https://sebsauvage.net/links/?Y19OBw) : [Home, not so Sweet Home](https://gist.github.com/sharadhr/39b804236c1941e9c30d90af828ad41e) sur les dossiers personnels (Linux et Windows)

[It's your day](https://kolektiva.social/@MnemosyneSinger/110905749078894725) : hihihihihihihi.

## 18 août

[Une excellente vidéo de KaleidosPop.c sur Frankenstein](https://www.youtube.com/watch?v=CkWxENCxYoQ) : j'y ai appris que Victor Frankenstein dans le roman est un étudiant de 19 ans qui cherche seulement à explorer les possibles (plutôt qu'un scientifique rendu fou par 1) la perte d'un être cher, 2) son athéisme, 3) que sais-je), que la créature parle (ça, en vrai je le savais, mais ça a été perdu dans les films), que Mary Shelley avait juste 19 ans quand elle a écrit son œuvre… Pfiou. Sans parler de la dimension politique (lutte des classes, végétarisme). Pfiou bis. Il faudrait vraiment que je lise le livre… oui, c'est honteux, je ne connais de cette histoire que des bribes et des métadonnées. 

Note de KaleidosPop.c dans un commentaire à propos du livre : « Et si tu veux le lire un jour, assures-toi d'avoir la version de 1818 (ou 1823, mais elle est rare), pas celle de 1831 (moins intéressante). » C'est intrigant.

Via [mhoye](https://mastodon.social/@mhoye/110900325877777520), [iPhone alarm as a piano ballad](https://www.youtube.com/watch?v=wzR9HwhhlkA), de Tony Ann (et en plus la vidéo affiche la partition, joie d'arriver plus ou moins à suivre 😸). Ça me rappelle Igudesman et Joo qui faisaient semblant d'improviser sur une sonnerie de Nokia dans la salle (mais je ne retrouve pas de vidéo…).

Et ça fait réagir plein de gens dans les pouets en réponse :
- [Windows XP Theme but it's an rpg soundtrack](https://www.youtube.com/watch?v=41V50KDvlFE) ([archive](https://web.archive.org/web/20230818093804/https://www.youtube.com/watch?v=41V50KDvlFE))
- du même auteur, Tony Ann : [My neighbour car alarm](https://www.youtube.com/watch?v=6Z-8vy6sPQQ), qui me touche fort fort, parce que ma came en terme de musique, c'est les bourdons et les basses obstinées (on retrouve ça dans [Riverdancing Violin](https://www.youtube.com/watch?v=QKZITB_r8t0&pp=ygUYSWd1ZGVzbWFuIGpvbyBub2tpYSB0dW5l) d'Igudesman et Joo, où la musique irlandaise démarre suite au bourdonnement de l'aspirateur)(ne le répétez à personne mais je chante quand je passe l'aspi, exactement pour cette raison)
- [Dryer Jingle Counterpoint](https://www.youtube.com/watch?v=y3vWVynL9Q4) (que j'aime quand les gens inventent des musiques par-dessus les bruits du quotidien)

> Se tenir au courant de tout ce qui est un peu alternatif, tout en restant en prise avec la réalité.

Dans la description de la vidéo [🔌 De l'électricité gratuite avec l'énergie libre - DEFAKATOR](https://www.youtube.com/watch?v=k5O73SY47LI) (c'est du débunkage de grosses arnaques à l'énergie libre)

[Des lapins qui jouent dans l'herbe](https://mstdn.social/@yurnidiot/110906312803583224).

[I 🟥 Quadrilaterals](https://www.youtube.com/watch?v=asTywgpiSkQ), Michael, de VSauce, égal à lui-même (encore que là… il est calme).

## 23 août

[Workout.lol](https://workout.lol/) via [Maiwann](https://app.flus.fr/links/1775041924056970150) : à tester !

[Les couleurs du vent en Irlande](https://mastodon.ie/@stancarey/110810032102564006).

## 26 août

[Fusion par lévitation](https://www.youtube.com/watch?v=DkpEz7znpnc) : impressionnant. Difficile d'imaginer une utilisation pratique, mais c'est beau. Je n'arrive cependant pas à savoir si la bobine est coupée, ce qui provoque la chute de l'objet, ou bien si la chute vient d'autre chose. 

Le rutile c'est du dioxyde de titane, et c'est la classe : [Le minéral préféré des stripteaseuses 🧚‍♀️💃🤸‍♀️](https://www.youtube.com/watch?v=082Ivd1zpJs)

[Micro-histoire de SF](https://mastodon.social/@radhikalism/110947479574234254) (un peu flippante quand même).

## 27 août

[Building A Warrior Becorn](https://www.youtube.com/watch?v=U_qp3a5ka1s) : j'adore les créations que David M Bird fabrique à partir de glands, de branches et d'écorces. Ici, un grand guerrier, avec un sublime bouclier en pomme de pin.

[Un cartoon représentant Katherine Johnson](https://mastodon.social/@mcnees/110956306232015030), une mathématicienne qui a travaillé à la NASA pour calculer des mécaniques orbitales.

Dans les [réponses](https://c.im/@justinbuist/110956744508387515) :

> <span lang="en">101. Always sad to see a mathematician cut down in their prime years.</span>

Traduction maison, le jeu de mot se perd un peu, hélas :

> 101 ans. C'est toujours triste de voir une mathématicienne fauchée dans sa prime jeunesse.

(<i lang="en">prime number</i> = nombre premier)

[Déclaration d'intention](http://laelith.fr/Zet/Declaration) de Christophe Michel (connu sous le nom de son projet « Hygiène mentale »). J'aime bien ce type. Surtout à la lumière des récentes polémiques dans le milieu sceptique (pas envie de m'étaler dessus, je n'ai aucune compétence ni connaissance à apporter, je m'abstiens donc).

[Preuve irréfutable de l'existence de blés arborescents (ou de chouettes millimétriques) en Grèce au IVe siècle avant notre ère](https://octodon.social/@temptoetiam/110957954080769516): hihihihi (c'est une photo d'une pièce grecque en argent).

Il y a quelques jours, le Chandrayaan-3 s'est posé sur la Lune. Le budget de ce projet indien était de 75 millions de dollars. Moins que certaines maisons ou certains films. C'est à la fois très réjouissant et rageant. [India landed on the Moon for less than it cost to make Interstellar](https://www.independent.co.uk/space/chandrayaan-3-budget-interstellar-cost-b2399815.html)

[Gif animé d'un cercle qui se transforme en spirales](https://mastodon.social/@bleuje/110907193617651370) : hypnotique.

['Newly Published Collection of Cats' - Utagawa Kunisada III, Japanese, Meiji era.](https://mas.to/@curiousordinary/110717164613348917) : des chats japonais, en kimono, en train de faire des trucs normaux comme boire du thé, jouer du <i lang="jp">shamisen</i> ou aller aux bains publics.

[Full Disclosure](https://mastodon.art/@kmcshane/110961901958590384), comic par Kevin McShane : la communication entre partenaires, c'est important. Même (et surtout) si on est Batman et Catwoman.

## 28 août

[#Eggcrackchallenge, la trend TikTok à tuer dans l’œuf](https://www.liberation.fr/lifestyle/eggcrackchallenge-la-trend-tiktok-a-tuer-dans-loeuf-20230822_FRR4LUWLT5BMDEN24HMTFDYZHA/) via [sebsauvage](https://sebsauvage.net/links/?XzhEtQ). Parce que l'humiliation fait toujours aussi rire, hein.

## 29 août

En écoutant _Qui veut gagner la flûte à bec ?_, émission de jeu musical bien barré de Thomas VDB, je découvre le floppotron à travers un extrait de _The Final Countdown_ d'Europe, et il **fallait** que je partage, parce que j'aime cette chanson d'amour, et que j'aime bien le chiptune, et que j'adore les instruments expérimentaux. Voilà la version intégrale de [_The Final Countdown_ d'Europe au floppotron 3.0 par Paweł Zadrożniak](https://www.youtube.com/watch?v=e-WakfBNHD0).

## 30 août

J'ai la chance de ne jamais m'être faite emmerder pour ce que je portais, mais ça n'empêche pas ce schéma d'être terriblement vrai en général :

![Diagramme qui part de la question « Quelle est la longueur de ta robe ? ». Embranchements : courte/longue. Puis au dessus du genou ? / Es-tu racisée ? Toutes les réponses conduisent à des conclusions négatives : Tu es une pute, Tu es une aguicheuse, Tu es une islamiste, D'aussi belles jambes et on peut pas en profiter ?](diagramme-robe.jpg)
Source : [pouet de MadameMollette](https://piaille.fr/@MadameMollette/110972965535477025).

## 31 août

[La théorie de la fiction-panier (par Ursula K. Le Guin)](https://www.partage-le.com/2018/01/29/8645/) (traduction en français de Jérémie Bonheure) : un beau texte sur comment écrire des fictions où l'on récolte plutôt que de prendre (grossièrement résumé). C'est ça que j'aime lire, et c'est ça que j'essaie d'écrire.

Une jolie chanson triste de Steven Wilson : [Song for the unborn](https://www.youtube.com/watch?v=SuxGW7xuTes), via [gee](https://framapiaf.org/@gee/110983513436999883).

[Si les élèves étaient des chats](https://www.youtube.com/watch?v=h3WJDzyDbMs) : j'adore le compte Parole de chat, c'est complètement n'importe quoi avec une voix nunulle et c'est bien ça qui fait son charme. Je crois l'avoir découvert avec les chats ninja il y a… oulalalala longtemps, ça a 11 ans apparemment.

Et ceci conclut ma tournée de liens d'août. 

J'essaierai de refaire l'expérience, c'était plutôt chouette. Si j'arrivais à écrire ça chaque semaine, ça serait probablement plus succinct, peut-être plus intéressant ? Un mois, c'est long. Mais une semaine, c'est court 😛 N'hésitez pas à me le dire sur le fediverse (@lamecarlate@pouet.it), je n'ai toujours pas de commentaires ici, c'est trop compliqué à mettre en place pour l'instant.

