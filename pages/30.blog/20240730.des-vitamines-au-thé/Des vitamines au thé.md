---
meta-création: "2024-07-08 18:39"  
meta-modification: "2024-07-29 15:07:07"  

stringified-date: "20240708.183932"

title: "Des vitamines au thé"  
subtitle: De boîtes de compléments en métal à jolies boîtes à thé. (@TODO)
template: "blog-item"  

date: "2024-07-30 15:07:02"  
update_date: ""

taxonomy:
    type: article
    category:
        - création
    tag:
        - DIY
        - art

# plugin Highlight
highlight:
    enabled: false

---

« C'est des boîtes en métal, je vais pas les jeter ! ». OK, mais faut en faire quelque chose. J'ai transformé des boîtes de compléments en métal en jolies boîtes à thé.

===

Comme je me complémente en vitamine B12, je fais une assez grande consommation de Veg-1, deux boîtes par an minimum (je force l'Amoureux à en prendre aussi 😛 et tout le monde devrait se complémenter en B12, en fait). Et comme ce sont des boîtes en métal avec un couvercle qui se visse, parfaitement fonctionnel, pas question de les jeter.

J'ai décidé d'en faire des boîtes à thé (ou à épices, mais vu le volume ça sera plutôt pour du thé).

![Boîte cylindrique en métal, on voit le logo de la marque de comprimés et des écritures.](IMG_20240708_183600--small.webp)

Première étape, couvrir de peinture blanche, sans soin particulier, surtout pour harmoniser un peu le fond et diminuer les risques qu'on voie les textes et logo sous le papier s'il se révèle un peu transparent.

Et ensuite, une belle couche de colle type Decopatch, du papier fin, et de nouveau une couche de colle, pour bien fixer et protéger le papier dans un même mouvement.

La circonférence des boîtes étant légèrement supérieure au côté des petites feuilles d'origami (de seulement 5 mm 😿), j'ai dû utiliser une feuille et une languette d'une autre la plupart du temps. De la contrainte naît la créativité, ça m'a poussée à assembler des motifs.

Bon, le séchage rapide en 10 minutes, mon œil, par contre. Comme j'ai fait cette partie chez des ami⋅es dimanche soir, j'ai emballé mes boîtes dans du papier journal pour le retour, et j'ai bien fait. J'ai eu quelques dégâts mineurs, avec du papier journal qui est resté accroché, et un majeur, où deux boîtes se sont effectivement accouplées…

Heureusement, la dernière partie prévue était d'ajouter des autocollants ou d'autres jolies choses, et j'ai pu camoufler les catastrophes sous un lapin des neiges et un écureuil. Et une couche supplémentaire de vernis-colle pour mieux fixer les autocollants, car après quelques jours je voyais les oreilles du lapin essayer de se décoller, les coquines.

Toute dernière étape : deux couches de vernis mat en bombe parce que ça restait poisseux au toucher et que, vraiment, ça ne m'inspirait pas confiance. L'effet est immédiat. C'est incomparable. Au toucher c'est un peu poudreux et plus du tout poisseux. C'est presque comme si je touchais le papier directement, étrange comme sensation.

Et voilà !

![Cinq boîtes en métal recouvertes de papier dans les tons bleus et verts pour la plupart, avec des autocollants lapin, écureuil et étoiles.](IMG_20240708_183638--small.webp){.figure-media--wide}

Les papiers sont de la gamme [Origami de Clairefontaine](https://www.clairefontaine.com/fr/360/papiers-origami-kirigami). Les autocollants lapin et écureuil sont d'[Anaïs Fae](https://fr.anaisfae.art/), et les petites étoiles et planètes de [LittlesOtters](https://www.littlesotters.com/).

---

Maintenant je vais pouvoir passer à l'étape suivante : passer des mois à réfléchir à comment réorganiser le tiroir à thés pour mettre mes jolies boîtes en valeur 😆