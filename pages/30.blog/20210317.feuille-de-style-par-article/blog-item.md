---
title: Une feuille de style par article dans Grav

date: "2021-03-18 19:24"
taxonomy:
  type: article
  category:
	- développement
	- vie du site
  tag:
    - css
    - twig
author: "Lara"
header_image_file:
header_image_caption:
highlight:
    enabled: true
---

J'avais vu sur divers blogs - de designers, surtout - des pages complètement différentes suivant les articles, et même si ça semblait demander plus de boulot, je trouvais ça super cool. Donc je l'avais implémenté sur la v1 de ce site (et il a fallu attendre la v3 pour que je m'en serve). Voilà comment j'ai fait.

===

On utilise ici plusieurs points intéressants de Grav :
- on peut ajouter des fichiers dans le dossier qui correspond à une page (Grav est un <abbr title="Content Management System">CMS</abbr> dit "à plat", où tout est piloté par des fichiers), et y accéder dans le thème, via `page.media`
- on peut ajouter du CSS, soit inline soit par un fichier, et ce fichier peut venir de n'importe où, avec `assets.addCss`.

Je crois que ce qui m'a pris le plus de temps c'est de trouver la bonne combinaison de filtres Twig pour atteindre exactement mon fichier CSS.

Dans mon template blog-item.html.twig, j'ai ajouté ceci :

```twig
{% block stylesheets %}
	{{ parent() }}
	{% if page.media["style.css"] is not null %}
		{% do assets.addCss(page.media["style.css"].filepath|replace({(base_dir): ""}), {
    		'priority': 50,
    		'pipeline': false,
			'id': 'custom-style'
    	}) %}
	{% endif %}
{% endblock %}
```

Si j'ai un fichier `style.css` dans mon dossier (par exemple, pour cet article, le dossier est "20210317.feuille-de-style-par-article"), je l'ajoute dans les assets, avec une priorité moindre que celle du fichier CSS global (chez moi c'est 101), afin que la ligne soit ajoutée après et que le CSS puisse surcharger le global. 

`pipeline: false` sort ce fichier de la compilation/minification - puisqu'il est spécifique, inutile de créer un nouveau fichier combiné. 

L'id est là pour identifier le `<link>` ; pour l'instant je ne m'en sers pas, j'imagine que je pourrais faire un système avec un bouton pour désactiver le style perso s'il est dérangeant. Peut-être que des gens ont déjà ça dans leur navigateur, je ne sais pas.

Et… et voilà, en fait.

Le fichier `style.css` peut contenir tout ce que je veux. Je n'ai bien sûr pas accès aux variables SCSS du thème, ce n'est pas grave, j'ai déjà les propriétés personnalisées avec toutes les couleurs, et avec ça je peux déjà jouer. Et puis rien ne m'empêche d'écrire plus 😉 Par contre, je suis toujours pas designeuse, c'est toujours aussi compliqué 😛.

Exemple d'article bénéficiant d'un style personnalisé : [Redshirts](../redshirts) (attention, c'est rouge). Et la page actuelle, bien sûr : [voir le fichier style.css](style.css).

Pour en savoir plus, lire [la documentation sur les assets de Grav](https://learn.getgrav.org/16/themes/asset-manager).

---

Crédits images : "504 tileable awake website backgrounds", du site Webtreats - apparemment ce site n'existe plus… On trouve leurs textures sur Flickr et [Deviantart](https://www.deviantart.com/webtreatsetc/gallery), mais le site de base a complètement changé.






