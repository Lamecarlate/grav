---
meta-création: "2024-11-05 21:13"  
meta-modification: "2024-11-05 21:13"  

stringified-date: "20241105.211352"

title: "Rentrer à pied"   
template: "blog-item"  

date: "2024-11-05 21:13"  
update_date: ""

taxonomy:
    type: article
    tag:
        - personnel
        - writing month
        - writing month 2024

# plugin Highlight
highlight:
    enabled: false

---

Aujourd'hui, beaucoup des bus et trams de ma ville ne roulaient pas.

===

 C'est un mouvement de solidarité envers un des employé⋅es qui s'est fait menacer d'une arme, si j'ai bien compris.
 
L'Amoureux devait donc finir son trajet à pied. Comme il se trouvait être pas très loin de mon boulot, je l'ai rejoint et nous avons marché. Tranquillement. Sous le ciel sombre. C'était chouette. Il a commencé à pleuvoir et c'était un peu moins chouette alors nous avons trouvé abri dans un fast food, mangé des frites et relu la description du Monstre Spaghetti Volant. 

Et pis on est rentré⋅es, l'Amoureux faisant quelques tours de pédale sur mon vélo, pour tester.

J'aime bien ce genre de soirée.

(et Atrus était dehors à nous attendre, tout câlin, c'était très mignon)
