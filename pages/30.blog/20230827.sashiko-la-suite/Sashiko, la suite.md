---
meta-création: "2023-08-27 12:45"  
meta-modification: "2023-08-27 12:45"  

stringified-date: "20230827.124527"

title: "Sashiko, la suite"  
template: "blog-item"  

date: "2023-08-27 12:45"  

taxonomy:
    type: article
    category:
        - création
    tag:
        - couture

# image principale
header_image_file: IMG_20230827_124357--small.jpg
header_image_alt: "Gros plan sur la cuisse d'un pantalon vert, où est cousue une grosse pièce de tissu à motifs floraux noirs, oranges et rouges, avec des points larges et croisés."

# plugin Highlight
highlight:
    enabled: false

---

J'ai continué 😊

===

Cette fois, un peu plus ambitieux : j'ai un pantalon que j'aime fort, un pantalon vert en velours, acheté à Damart, et porté porté porté, jusqu'à élimer le velours, ce qui a fini par fragiliser le tissu, qui a rompu sur la cuisse. J'ai mis une grosse pièce colorée devant, et vogue la galère.

Oui, c'est toujours pas droit. Mon crayon blanc ne tient pas quand je manipule le tissu, et le feutre prêté par une amie était illisible, à cause du motif et des couleurs vives de la pièce. Bah !

![Auto-portrait dans un miroir, on ne voit que mes jambes, dans le pantalon réparé, un lit derrière et un gros panier à mes pieds.](IMG_20230821_165517--small.jpg)

Petites réparations supplémentaires pour la forme : un trou sous une des boucles de la ceinture, où mes doigts finissaient toujours lorsque j'enfilais le pantalon, et un renfort au niveau des fesses, où le tissu est bien fatigué.

Je suis contente du rendu final : c'est réparé, utilisable, avec une couleur un peu flashy, c'est marrant. Et le renfort aux fesses ne se voit pas du tout 😸

Là où je dois vraiment m'améliorer : la régularité des points (il faut que je trouve un crayon ou un feutre qui tienne), et je devrais probablement faire un petit ourlet sur la pièce avant de la coudre, parce qu'il est bien possible que ça s'effiloche…