---
meta-création: "2024-11-25 21:03"  
meta-modification: "2024-11-25 21:03"  

stringified-date: "20241125.210311"

title: "Bloguidien du 25 novembre 2024"  
template: "blog-item"  

date: "2024-11-25 21:03"  
update_date: ""

taxonomy:
    type: article
    tag:
        - musique
        - personnel
        - writing month
        - writing month 2024

# plugin Highlight
highlight:
    enabled: false

subtitle: Rare possession.

---

Rare possession.

===

> Quel est l'objet le plus rare/étrange/inhabituel que vous possédez ?

Ici je choisis le mot « rare ».

Mon violon.

Il a été fabriqué par mon père (dont c'est le métier) sur un modèle d'Amati et la tête a été sculptée par ma mère (qui a longtemps fait de l'ébénisterie, et sculpte toujours les têtes des violes de gambe de mon père). Son vernis est jaune doré, et il n'a pas de volute mais une tête de chat. C'est, je crois, le cadeau le plus précieux que mes parents m'aient jamais fait.

(Pas de photo parce qu'il est tard, et que je suis fatiguée ; j'essaierai de reprendre cet article dans les jours qui viennent.)

J'aimerais bien me remettre à jouer, mais je ne trouve jamais (je ne prends jamais…) le temps ou la motivation pour ça. J'ai peur de faire du bruit trop fort parce que je suis en appartement avec des voisins, aussi… Un jour, cela changera.

Amorce tirée du site [Bloguidien](https://bloguidien.fr/bloguidien-du-lundi-25-novembre-2024) pour le lundi 25 novembre 2024.
