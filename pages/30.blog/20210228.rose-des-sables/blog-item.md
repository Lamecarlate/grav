---
title: "Éclosion d'une rose des sables : histoire d'une refonte"

date: "2021-02-28 18:24"
publish_date: "2021-02-28 18:24"
published: true
taxonomy:
  type: article
  category:
	- développement
	- vie du site
  tag:

author: "Lara"
header_image_file: illus.jpg
header_image_caption: Un montage entre du CSS à gauche et une rose des sables en chocolat à droite
highlight:
    enabled: true
---

Enfin une v2 ! Enfin, une v3, si j'en crois mon kanban (j'avais oublié jusqu'à l'existence de la première version, qui n'a probablement jamais vu la prod). Au programme : du beige, du brun, du rose, des trucs penchés et de la typographie.

===

Cela faisait des mois que je voulais changer mon site - j'avais même noté dans mon kanban que la v3 serait en html/css à la main, avec un blog externe, parce que Grav me gonflait, j'arrivais pas à faire ce que je voulais… Et puis, je ne sais pas très bien quel a été le déclencheur, j'ai créé un nouveau dossier dans les thèmes, et commencé à trifouiller.

Bon, en vrai, je ne pars pas de rien, et ce thème partage certains points avec l'ancien : de gros et grands titres, un premier paragraphe plus gros et large, et ensuite le reste de l'article plus étroit, avec certains éléments élargis (codes, images). C'est un rendu qui me vient en partie de [A List Apart](https://alistapart.com), et que j'aime beaucoup, donc je l'ai déjà utilisé sur [mon ancien blog de trucs](https://coquecigrue.lamecarlate.net/code-francais-et-chouette-mot/).

## De la typographie

Ce que je préfère dans le web, c'est le texte. J'apprécie bien entendu les images, et les expérimentations cheloues à base de boing boing WebGL, mais la base chère à mon cœur est le texte. Il fallait donc en prendre soin dès le début du projet.

Je voulais tout d'abord avoir un rythme vertical, parce que je trouve ça chouette (même si je suis possiblement la seule à le voir), et parce que des designers ont dit que c'était important (et comme je ne suis pas designeuse, je leur fais confiance). Mon article de référence est [A guide to vertical rhythm](https://iamsteve.me/blog/entry/a-guide-to-vertical-rhythm) (d'ailleurs, les images me cassent toujours mon beau rythme, et je rechigne à mettre du JavaScript pour ça…).

Ensuite, une belle échelle typographique : [Modular Scale](https://www.modularscale.com/) m'a été d'une grande aide. Au début, j'avais choisi le ratio "quarte augmentée" (1/√2), car cela me donnait *de beaux titres* (pas designeuse, je rappelle), et puis c'était un peu trop gros quand même, alors je suis descendue à une "quarte parfaite" (2/3).

Je ne résiste à vous faire part de mon petit code tout simple pour calculer les tailles de police, et surtout la hauteur de ligne (c'est vraiment ça qui fait tout pour conserver le rythme vertical). C'est inspiré de [Create you design system, de Sebastiano Guerriero](https://medium.com/codyhouse/create-your-design-system-part-1-typography-7c630d9092bd), j'ai réécrit les formules pour utiliser la puissance de Sass (`math.pow` power!).

```scss
@use 'sass:math';

@mixin type-setting($setting, $line-height: false) {
	$unitless-font-size: $base-font-size;
	@if $setting < 0 {
		$unitless-font-size: 1 / math.pow($scale-ratio, math.abs($setting));
	} @else {
		$unitless-font-size: math.pow($scale-ratio, $setting);
	}

	font-size: rem($unitless-font-size);

	@if $line-height == true {
		line-height: ceil($unitless-font-size / $rhythm-unit) * ($rhythm-unit / $unitless-font-size);
	}
}
```

`$setting` varie de -1 (petit) à 5 (énorme), `$base-font-size` est actuellement à 1.25 sans unité (`html` a une taille de police de `$base-font-size * 1rem`, et ensuite toutes les tailles de texte sont en `rem` et donc en dépendent), et `$scale-ratio` c'est le ratio mentionné plus haut.

À utiliser ainsi :

```scss
h2 {
	@include type-setting(4, $line-height: true);
}
```

Ce qui sera compilé en :

```css
h2 {
	font-size: 3.1573345183rem;
	line-height: 1.4252528435;
}
```

(au fait, il y a mon guide de style sur le site, fouillez un peu, vous le trouverez 😛 )

J'ai bien entendu activé les ligatures : les communes et les spécifiques ("discretionary", [ce genre-là de ligatures](https://flylib.com/books/en/2.479.1/discretionary_ligatures.html "ligatures spécifiques"), celles entre "c" et "t", "s" et "t", etc… parce que j'aime leur léger aspect désuet).

(ff st ct sp fi, si vous voulez tester 😁)

J'ai aussi décidé d'avoir une "font-stack" qui lâche du lest, qui laisse tout loisir au système du ou de la visiteuse de charger ce qui est dispo : 

```scss
font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen-Sans, 
Cantarell, 'Helvetica Neue', Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 
'Segoe UI Symbol';
```

Donc, il se peut que vous ne voyiez pas les ligatures - ou certaines d'entre elles - mais au moins, pas de fichier supplémentaire à télécharger, et mon design n'en pâtit probablement pas.

## De la couleur

Que la lumière soit. Et je fis aussi un thème sombre, parce que c'est un besoin personnel - je passe trop de temps le soir sur mon téléphone dans l'obscurité - et parce que comme mon thème visuel sur mon OS est sombre, Firefox ajuste automatiquement la media-query `prefers-color-scheme` à `dark`, et que c'était une occasion de tester ça.

Le thème précédent, Qoharis, était monochrome, autour du rouge foncé qui est ma couleur préférée depuis des années. C'était un exercice intéressant, mais que je n'avais pas envie de refaire pour l'instant. Donc j'ai réfléchi à deux couleurs, une principale et un "accent", en m'inspirant de cet [article de Max Böck](https://mxb.dev/blog/color-theme-switcher/).

Difficile de retrouver exactement le processus de création des couleurs, c'est essentiellement parti de 

> Je ne veux pas d'un fond blanc, on va prendre du beige. Pour aller avec, un brun chaud, et du rose un peu pétant comme accent - un rose pas trop "bonbon", vraiment comme un "rouge foncé clair"

En fait, j'ai peut-être bien éclaté mon rouge fétiche en brun et rose 😅

(et il est tout à fait possible que la gourmande en moi ait pensé : chocolat + tahin + framboise…)

Et pour la version sombre, l'inverse : beige (qui s'est mué en jaune clair) sur brun, et rose (légèrement différent, à peine plus clair et moins rouge). J'ai également beaucoup joué sur les opacités et les superpositions, ce qui m'a donné accès à d'autres couleurs ✨! (voir le bloc de citation plus haut)

Les couleurs ont toutes été testées à l'aide du [Contrast Finder de Tanaguru](https://contrast-finder.tanaguru.com/), un de mes sites de chevet.

Ce sont d'ailleurs ces couleurs qui m'ont apporté le nom du thème : j'avais demandé à des ami⋅es leur aide en leur montrant simplement des captures de la page d'accueil dans les deux versions :

> Lara 10:11  
> rhaah je suis trop mauvaise pour nommer les choses  
> je refais donc mon site (perso/pro), j'ai créé un nouveau thème, et j'arrive pas à le nommer  
> les couleurs dominantes sont beige/marron/rose (ou marron/jaune/rose suivant si on est en variante claire ou sombre), et y a des trucs penchés partout  
> je sais que ça n'a aucune réelle importance, maiiiiis mon cerveau est pas d'accord

> Flo 10:57  
> un sondage, un sondage ! La place publique veut voter !

> Lara 11:02  
> bin j'ai aucune proposition… en fait je demande de l'aide  
> voilà la page d'accueil dans son état actuel (très possiblement la version finale)

> Flo 11:14  
> Thème désert : Sahara, Atacama, Kalahari  
> Thème chocolat : Valhrona, weiss, cosse (de chocolat), Xocoatl (la boisson chocolatée des incas)

Et là, illumination, comment lier désert et chocolat : **"rose des sables"** (le dessert, pas le minéral, vous pensez bien). Le mot "rose" fait en plus écho à ma couleur d'accent, à croire que ce nom était fait pour ce thème.

## La déco

Ensuite, à part ces deux points (typographie et couleurs), j'ai procédé par itération, et directement dans le navigateur, comme je le fais toujours pour mes sites perso. Par hasard, j'ai tenté un machin à base de `transform: skewX` sur un élément, et ça rendait tellement bien que j'ai mis des machins penchés et superposés partout. J'ai eu un retour comme quoi ça pouvait être fatigant pour la lecture, il faudra que je me penche dessus ( oui, ok ->🚪).

## Architecture & technique

Un brin de technique : j'utilise SCSS pour sa facilité à concaténer de multiples fichiers, comme ça je peux faire des dossiers pour bien ranger - je m'inspire fortement des [Sass Guidelines](https://sass-guidelin.es/) de Kitty Giraudel, notamment le "7-1 Pattern". J'utilise aussi beaucoup les variables, même si à la réflexion, plusieurs d'entre elles pourraient devenir des propriétés personnalisées (custom properties) CSS, car n'entrant dans aucun calcul pré-compilation.

Pour la compilation, Webpack - que je maîtrise mal, je fais vraiment par essai et erreur, et c'est assez frustrant… Je crois que ce qui me hérisse le plus c'est de devoir importer mon SCSS dans le fichier JS pour qu'il soit compilé. Rendez-moi Grunt.

Côté JS, justement : j'ai essayé d'être minimale. Le composant natif de Grav pour la recherche nécessite du JS et je ne sais pas encore comment contourner cela. Et j'ai ajouté un bouton pour changer de thème entre sombre et clair, encore une fois inspirée par l'article de Max Böck, et je suis assez fière du résultat, qui prend 66 lignes de JS (et 6 de Twig, en comptant les sauts de ligne).

## Et le contenu, alors ?

### Articles de blog

Je ne garantis pas que j'écrirai plus 🤔 mais j'aimerais y arriver : j'ai envie d'écrire des critiques des livres que je dévore, partager les quelques photos que je fais (en évitant de vous noyer sous celles d'Atrus, le chat blotched tabby qui a pris possession de ma maison), poster un peu plus de code…

![Atrus, un chat gris tigré, qui dort, la tête entre les pattes](atrus-dodo.jpg "Il dort, le bougre")
![Le même chat, la tête relevée, les yeux encore englués de sommeil](atrus-pas-dodo.jpg "Mgné ?")

### Recettes de cuisine

J'avais un blog spécialisé dans la cuisine, et comme je voulais réunir toutes mes facettes ici, j'ai également prévu d'afficher des recettes. Pour l'instant, il y en a une… J'espère arriver à bouter hors de moi cette personne perfectionniste qui prend plein de photos de ses plats mais n'en poste aucune parce que tu comprends il faut les rogner retravailler la lumière et puis il faut écrire la recette et holà.

À noter que l'affichage d'une recette était cassé en production, et je n'avais jamais pris le temps de corriger - désormais c'est visible. Et avec un lien direct vers la recette très haut dans l'article pour les gens pressés 😉

### Travaux professionnels et personnels

L'étape suivante est de vraiment terminer la partie "Travaux", qui est en suspens depuis la version précédente, n'ayant jamais réussi à faire tout bien comme je voulais. Ce qui explique d'ailleurs l'existence d'un autre site à mon nom, qui est tout aussi officiel, à l'abandon mais que je garde parce que je ne veux pas casser les urls (une vraie url est éternelle etc). Quand j'aurai fini de remettre mes anciens projets ici, je ferai des redirections propres pour chaque, et ensuite une redirection globale de l'autre site.

## Et la suite

Bien sûr qu'il reste des choses à faire !

![Aperçu du kanban du site v3, trois colonnes remplies](kanban.png "Le kanban dans lequel je mets toutes mes idées, design, dev, bugs ou données (articles etc)"){.figure-media--wide}


---

Crédits : Photo des roses des sables au chocolat par Asmoth — Travail personnel, CC BY-SA 4.0, [lien vers l'original](https://commons.wikimedia.org/w/index.php?curid=69566098).
