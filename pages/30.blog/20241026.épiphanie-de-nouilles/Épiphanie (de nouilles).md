---
meta-création: 2024-10-26 09:18
meta-modification: 2024-10-26 09:18
stringified-date: "20241026.091845"
title: Épiphanie (de nouilles)
template: blog-item
date: 2024-10-26 09:18
update_date: ""
taxonomy:
  type: article
  category:
    - 
  tag:
    - horreur
    - internet
    - humour
    - culture
highlight:
  enabled: false
---

En regardant la vidéo sur le Slenderman de Bolchegeek and Pâcome Thiellement, et surtout en lisant les commentaires…

===

… je réalise que « Les nouilles rampantes » est une traduction littérale de "creepy pasta". Et pourtant, j'en ai écouté, des épisodes ! Mais ça ne m'a jamais frappée.

J'ai bien aimé les essais de traductions dans les commentaires : flippé-collé, copiépouvante, ou copier-glauquer.

[La vidéo en question, dans l'émission Deep Web Phantasia](https://www.youtube.com/watch?v=qP4tnqmN3u4), et [le podcast en question, Les nouilles rampantes](https://shows.acast.com/les-nouilles-rampantes).
