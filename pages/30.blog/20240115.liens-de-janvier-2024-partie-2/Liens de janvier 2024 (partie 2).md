---
meta-création: "2024-01-15 08:43"  
meta-modification: "2024-02-03 19:01:46"  

stringified-date: "20240115.084340"

title: "Liens de janvier (partie 2)"  
template: "blog-item"  

date: "2024-02-03 19:20"  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - images
        - musique
        - DIY
        - humour
        - littérature

# plugin Highlight
highlight:
    enabled: false

---

Tiens, surtout des images et de la musique cette fois-ci. Et des chats. Sploing.

===

![Une boîte à vis, clous, chevilles, et autres petites choses de bricolage, extrêmement bien rangée, avec 9 colonnes et 12 lignes.](0thlqs6jbacc1.jpeg "Je défaille un peu, au vu de cette image 😻. Chacun⋅e ses faiblesses, hein."){.figure-media--wide}

Source : [Garage drawer miscellaneous fasteners and stuff using Toolgrid](https://www.reddit.com/r/OrganizationPorn/comments/19609r8/garage_drawer_miscellaneous_fasteners_and_stuff/).

[Des chouettes et des z'hiboux](https://mastodon.art/@ThompsonArt/111778710191063728) (j'adore tellement la deuxième, la petite chouette effraie toute ronde et choupie).

[The Rose - Ola Gjeilo](https://www.youtube.com/watch?v=Ph44oJ7WpGU) (vidéo avec du son, une musique, avec la partition qui suit) : c'est juste beau. Ça accompagnait une page de [Court of Roses](https://courtofroses.spiderforest.com/index.php?comic_id=0), je vous ai déjà conseillé ce webcomic, j'en remets une couche (mais je ne mets pas le lien vers la page en question, c'est au milieu d'un arc narratif qui dévoile des choses). Allez, à la relecture de ce billet, je me le réécoute, il y a tant de petites choses qui me plaisent : les jolis chromatismes, les répons entre sopranes et altis (déso les voix d'homme, je ne vous entends presque pas), et le texte si doux (c'est un poème de Christina Rossetti). 

> When with moss and honey  
> She tips her bending brier  
> And half unfolds her glowing heart  
> She sets the world on fire.

[La housse et la couette](https://www.youtube.com/watch?v=yq-OzjYJLf0) (vidéo avec du son, une chanson, les paroles sont affichées dans la vidéo à mesure) : une chanson de Juliette que je ne connaissais pas 😻 Que j'aime l'œuvre, la voix, la poésie et l'humour de cette femme ! Ici, les rimes sont presque toutes en « ousse » et c'est un plaisir que d'essayer de les deviner avant leur arrivée. Via AnnaBalade (pouet privé, en réponse au message de désespoir très digne de [Sacrip'Anne](https://pastafaristes.fr/@sacripanne/111761672547913369)).

---

Oh, c'est intrigant, ce truc : [The Eternal Jukebox](https://jukebox.davi.gq/jukebox_index.html). 

Ça ne marche pas toujours très bien, ça doit dépendre des musiques, mais j'adore le concept : le logiciel calcule les sauts possible dans une musique, et à chaque instant, il y a une chance de prendre un raccourci vers l'avant ou l'arrière dans la musique, la rendant ainsi éternelle, sans fin. Bon… parfois l'éternité bloquée sur un seul accord c'est moins chouette (ça arrive, c'est le risque de l'aléatoire).

Exemple qui rend très bien : [Gangnam Style, par Psy](https://jukebox.davi.gq/jukebox_go.html?id=03UrZgTINDqvnUMbbIMhql&noprocess), ou [Mediterranean Sundance, par Al Di Meola](https://jukebox.davi.gq/jukebox_go.html?id=0og2U8tsBAR7NJysRR6uBU&noprocess), que je découvre et que j'adore (non je n'ai pas remué mon popotin sur de l'excellente guitare flamenca pendant des heures, vous n'avez aucune preuve).

Exemple où j'ai beaucoup entendu les sauts : [Let it go au piano, par Glen Stefani](https://jukebox.davi.gq/jukebox_go.html?id=2mKeoPrR1BFkUzdbnm4EK0&noprocess).

Via [sebsauvage](https://sebsauvage.net/links/?m0oT3g).

---

[Unfulfilled: Cats, Autism, and Family](https://veocorva.xyz/2024/01/19/unfulfilled-cats-autism-and-family/) (en anglais) : c'est un texte touchant sur Jynx, petite chatte écaille-de-tortue qui a été maltraitée dans son enfance (j'aime le mot "kittenhood", l'anglais permet de créer tellement de mots).

<blockquote lang="en">Some part of Jynx believes that the only way to enforce her boundaries, the only way to say ‘no’, is to attack. Hard.</blockquote>
[De l'herbe gelée. Très gelée.](https://mastodon.nl/@suzi_1960/111792862296081402), c'est magnifique.

Je ne vous ai pas assez rasé⋅es avec Bad Apple, je crois. [Bad Apple, mais c'est une musique traditionnelle japonaise, et une danse onnogata](https://www.youtube.com/watch?v=oTWJnfVJpQw) (vidéo avec du son, c'est une musique, sous-titres en japonais). Le style, ou rôle, ou acteur onnagata, est issu du kabuki, et plus précisément de l'interdiction en 1629 des femmes sur scène, donc des hommes se sont spécialisés dans les rôles féminins. Source : [Wikipédia](https://fr.wikipedia.org/wiki/Onnagata).

> L'acteur _onnagata_ stylise la féminité de telle manière que son physique réel n'a plus d'importance dans l'interprétation de ses rôles. Seule la théâtralité compte.

[Un impressionnant diorama](https://www.youtube.com/watch?v=KTt9aBki5UM), avec un énorme scarabée (avec des mains, aaaaaah- *\*cri étouffé*\*). Vidéo avec du son mais aucune parole. J'ai été un poil frustrée par les différentes ellipses, mais je suis habituée aux vidéos de DIY plus détaillées, qui montrent les sessions de ponçage, de nettoyage, qui expliquent un peu plus. Là : bin on profite de la vue.

C'est cruel mais c'est une chouette manière de voler du temps à des scammeurs : [I Trapped 200 Scammers in an Impossible Maze](https://www.youtube.com/watch?v=dWzz3NeDz3E) (vidéo avec du son, en anglais, sous-titres automatiques). Parce que pendant que ces scammeurs sont coincés dans ce labyrinthe de site web mal foutu et de centre d'appel, ils ne sont pas en train de tromper des personnes innocentes pour leur piquer des sous.

[Des magnolias](https://mastodon.art/@JanMatson/111730781845628579) peints, admirez cette texture !

Intriguée par le titre, _Four-bytes burger_, j'ai dévoré (hahahahum) cette vidéo, sur [la reproduction d'une ancienne œuvre de pixel-art](https://www.youtube.com/watch?v=i4EFkspO5p4). C'est assez impressionnant, la recherche du support originel, les calculs pour trouver la taille et surtout la palette, puis la fabrication, et les efforts pour l'afficher sur une machine semblable à celle de l'époque.

Mais c'est mignon ! [Une surprise miaulante sous une poule](https://www.tiktok.com/@goran.surchi/video/6939491016785349889) (vidéo avec du son, pas de paroles). Source : [A Guy Finds His Chicken Taking Care Of Three Orphaned Kittens And Captures It In A Viral Video](https://www.boredpanda.com/chicken-protects-orhpaned-kitties-goran-surchi/), via [Mandu](https://mstdn.social/@yurnidiot/111812987203508775).

[C'est un tripode, je vous jure](https://mindly.social/@grayladywriter/111253491129217568).

[Camouflage](https://chaos.social/@Wifi_cable/111793498820029720) (en anglais, image avec texte alternatif).

[Je ne parle pas bébé](https://toot.aquilenet.fr/@DoubleArobase/111843790340717315).

![Un plafond orné, principalement dans les tons bruns et les jaunes. Le plafond a des voûtes arquées et nervurées, et il y a une petite coupole en verre teinté en haut à droite du cadre.](9acbbefb707d36a2.jpeg "Plafond dans un des bâtiments du Hospital de Sant Pau, à Barcelona."){.figure-media--wide}

Source : [Rick Gaehl](https://mstdn.social/@RickGaehl/111821710543014229) (j'ai plus ou moins traduit le texte alternatif que Rick a mis sur l'image dans son pouet).

[Va pour sploing](https://mastodon.social/@pizzaroquette/111821104592474518) : des extraits du livre « Carnets d'un traducteur » de Nicolas Richard. C'est très drôle !

---

Il fait pas mal froid ces temps-ci chez moi, heureusement que je vis sur Internet, c'est confortable.