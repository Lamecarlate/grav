---
meta-création: "2022-11-24 20:34"
meta-modification: "2022-11-24 20:35"

stringified-date: "20221124.203454"

title: Drôle de temps, de Joe Hill
template: "blog-item"

date: "2022-11-25 09:20"

taxonomy:
	type: article
    category:
        - lecture
    tag:
        - fantastique
        - SF
        - thriller

thumbnail: cover-drole-de-temps.webp
thumbnail_orientation: portrait

# plugin Highlight
highlight:
	enabled: false

---

Dernièrement, j'ai lu _Drôle de temps_, un recueil de quatre romans courts de Joe Hill.

===

![Couverture du roman "Drôle de temps"](cover-drole-de-temps.webp){.media--align-left}

Joe Hill, Drôle de temps, Jean-Claude Lattès, 2021 ((en) <i lang="en">Strange Weather</i>, 2017), trad. Nicolas Thiberville.

Hill est un auteur états-unien de romans, novellas et scénarios de comics. Accessoirement fils de Stephen King, et je **jure** que j'ai découvert cette info en me renseignant pour cet article : il a bien réussi à avoir son style personnel alors qu'il tape dans le même registre que le paternel, chapeau. À noter que c'est pour cette raison qu'il écrit sous pseudonyme[^1], "afin de pouvoir bâtir sa carrière sur ses seuls mérites sans qu'on puisse l'assimiler à son père" (merci Wikipédia qui le dit vachement mieux que moi).

## "Instantané"

Michael Figlione raconte l'été de ses 13 ans dans sa petite ville dans les années 60 (mais si, vous savez, celle qu'on voit dans les films états-uniens, grandes rues vides, bâtiments blancs et gamins à vélo). Par un concours de circonstances, il se retrouve à s'occuper de sa vieille voisine un peu sénile – juste retour des choses, car c'est quasiment elle qui l'a élevé, lui qui a une mère absente et un père qui ne sait pas y faire. Et, comme elle, il est poursuivi par un étrange personnage, le Phénicien, qui possède un appareil photo, type Polaroid, avec des pouvoirs inquiétants : il vole des souvenirs en prenant des photos.

Ça se lit d'une traite, le mystère sur l'appareil photo n'est que peu levé : ce sont les mémoires d'un jeune garçon qui n'a jamais vraiment compris ce qui lui est arrivé, mais dont la vie est restée hantée par ces événements. Ça parle – bien évidemment – de souvenirs, de la vie qu'on a eue au travers des visions des autres.

Pour les lecteurices de _Locke & Key_, comics également écrit par Hill, vous verrez un lien, que je vous laisse découvrir 😉 (mais ne cherchez pas trop dans les wikis sur le net à ce sujet, ils en dévoilent trop, et de manière un peu fausse, d'ailleurs).

Note : 4/4. La relecture peut s'avérer nécessaire.

## "Chargé"

Le prologue pourrait être une nouvelle indépendante. Concise, cinglante, elle fait mal, frappant tellement juste. On y suit Colton, jeune homme noir qui fait office de grand frère pour la jeune Aisha Lanternglass. Il est cool, sympa, adorable, il fait du théâtre, et l'an prochain il partira en Angleterre pour étudier, et il sera le premier Noir à interpréter Hamlet.

L'histoire principale se passe une dizaine d'années plus tard. On suit Kellaway, un type désagréable au possible, misogyne, raciste, pro-arme, à la gâchette facile, bref le gendre parfait dans la moitié des États-Unis. Kellaway est vigile dans un centre commercial. Le boulot parfait pour quelqu'un qui est empli de rage, de frustration, qui vient de se voir signifier une interdiction d'approcher son épouse et son fils, pour violences, et qui n'a pas le droit de porter une arme en service. Mais une arme, il en a une, donnée par son meilleur ami, vétéran de la guerre en Afghanistan comme lui. Et il "arrête" une fusillade dans la bijouterie du <i lang="en">mall</i>. Je ne veux pas trop en révéler, mais ça ne se passe pas comme il le raconte après, ou comme on le raconte pour lui, car il devient instantanément un héros.

Lanternglass est devenue journaliste, et les deux se croisent : elle investigue sur cette histoire, et lui voudrait bien l'en empêcher.

Il n'y a pas vraiment de mystère dans cette histioire : lorsqu'on suit Kellawy, on est dans sa tête et on sait très bien la vérité sur l'événement du centre commercial, sans ambiguité. Ce qui importe c'est comment les relations entre les gens se tissent, et la manière dont l'histoire officielle héroïque se détricote.

Le dernier quart de la novella est soufflant, accéléré, on retrouve un peu le rythme du prologue, et tout dégringole. On n'est pas là en présence d'un tueur "qui n'a plus rien à perdre", c'est plus subtil que ça. Et toujours aussi désagréable d'être dans la tête de Kellaway. Il a sa vision des choses, il a ses évidences, et le roman ne cherche pas à les camoufler. Pour lui, oui, la flic latina qui est venue lui lire son interdiction d'approcher sa famille l'a fait de manière éhontée et a pris beaucoup de plaisir à lui rabattre son caquet d'homme blanc hétéro, cette sale lesbienne moche. On n'est pas obligé⋅e d'être d'accord (que ça soit clair, moi ce n'est absolument pas le cas), mais on n'a pas le choix. L'histoire nous montre l'intérieur de gens, avec leurs contradictions et leurs opinions.

La fin, en quelques mots (attention, divulgâchage) : <span class="spoiler">what. Je n'ai absolument pas vu venir ça. Je ne sais pas trop ce que j'attendais, un simple (à peu près) happy end, que ce soit Kellaway arrêté ou tué, une… justice ? Mais non. Pas de justice. Pas de jugement. Pas de morale. C'est dur. C'est glaçant.</span>

J'ai aimé cette novella, je ne savais pas à quoi m'attendre (la quatrième de couverture racontait un bout de l'histoire mais à part ça, rien de très précis), et je n'ai pas été déçue du voyage. J'ai juste… besoin de souffler un peu, là.

Note : 4/4. Sans hésiter. Parce que même j'ai détesté être dans les pensées de Kellaway, qui concentre tout ce que je n'aime pas dans l'être humain, même si son personnage ne m'a pas émue,  j'ai été emportée dans le récit et n'en suis pas sortie indemne (ouh que c'est bateau comme expression, mais c'est **vrai**).

## "Là-haut"

Aubrey va sauter en parachute pour la première fois. Il est mort de trouille. Mais c'est un souhait de son amie June, récemment décédée, et Harriet, la fille dont Aubrey est désespérement amoureux, insiste pour le faire. L'avion a soudain des ratés et tout le monde doit sauter prématurément. Aubrey débarque sur un étrange nuage.

La narration alterne entre le présent, où Aubrey explore le nuage où il tombé, et le passé, ses souvenirs, qui expliquent par morceaux comment il en est arrivé là. Structure classique mais très efficace.

Il est difficile d'en parler sans trop révéler, mais oui : ce n'est pas un nuage normal. Aubrey le découvre peu à peu, cet endroit est conscient, et l'écoute, et lui offre ce dont il a envie ou besoin… sauf la possibilité de partir. Progressivement le jeune homme réfléchit sur sa vie, sur ses sentiments pour Harriet.

Cette novella m'a un peu fait penser à _Face au dragon_ d'Isabelle Bauthian (attention je vais divulgâcher la révélation de _Face au dragon_), <span class="spoiler">car dans les deux cas on est face à un environnement où les personnages ne sont pas censés être, qui n'est pas conçu pour elleux, mais qui ne sait pas vraiment comment les laisser partir, alors les garde</span>.

Note : 3/4. Personnellement, cette histoire m'a peu touchée, je ne saurais pas vraiment dire pourquoi. Peut-être parce qu'elle suivait _Chargé_ et que j'étais encore un peu sous le choc ? Quoi qu'il en soit, c'est un petit roman initiatique intéressant.

## "Pluie"

Une pluie dévastatrice d'aiguilles de cristal – on apprendra vite qu'il s'agit de fulgurite – tombe soudainement et cause la mort de nombreuses personnes. L'histoire suit Honeysuckle (Chèvre-feuille, quel beau prénom) qui a vu sa petite copine Yolanda mourir sous ses yeux, et part, à pied, annoncer la nouvelle à son père. Le monde est passé en mode apocalypse en quelques heures : très vite on découvre que les cristaux sont artificiels, et Daesh revendique l'attentat, donc le ton monte et le bouton rouge de l'attaque nucléaire est menacé d'être pressé. Et le nuage se déplace…

Il y a de nombreux personnages : le petit garçon allergique à la lumière à cause d'un traitement antibiotique et sa mère, veuve depuis un an ; la strip-teaseuse et son irascible compagnon, toustes deux russes ; les illuminé⋅es de l'Église du Christ de la Septième Dimension ; le voisin homophobe des parents de Yolanda… et chacun, chacune aura son importance.

J'ai trouvé subtil de parler du président en ne mentionnant que ses tweets "engagés" et puis moins subtil de le nommer un peu plus tard (oui c'est Donald Trump). J'aurais préféré que ça reste dans le non-dit, une petite énigme pour le futur : si on ne sait pas qui c'est, eh bien c'est juste un président qui ne fait que parler et non agir, et si on sait, ça rajoute un peu de saveur.

Le tweet en question :

> Nos ennemis vont regretter ce qu'ils ont fait ! Rira bien qui rira le dernier !!! #Denver #Colorada #USA !!

Le vice-président, quant à lui, prie aussi fort qu'il peut.

Ce qu'en pense Honeysuckle :

> C'était rassurant de savoir que nos dirigeants utilisaient toutes les ressources mises à leur disposition pour venir en aide aux désespérés : Jésus et les réseaux sociaux.

Dans _Pluie_, sans en dévoiler trop, c'est bien l'œuvre humaine qui est à l'origine de ces orages meurtriers : on sait déjà "ensemencer" des nuages pour "pour disperser le brouillard, diminuer la grosseur des grêlons ou augmenter la quantité de précipitations, faire pleuvoir avant une cérémonie ou un évènement. Il est utilisé dans différents domaines dont l'agriculture, la lutte contre la désertification ainsi que dans le domaine militaire." (dixit [Wikipédia](https://fr.wikipedia.org/wiki/Ensemencement_des_nuages)). Alors pourquoi pas pour créer de la fulgurite ?

Ici les particules de fulgurite se répandent dans l'atmosphère et ensemencent d'autres nuages, les transformant en nuages de cristaux, empêchant la pluie "normale" d'eau.

Je fais un parallèle avec _Le berceau du chat_, de Kurt Vonnegut[^2]. Dans les deux cas, on a un événement provoqué par l'humain, et qui semble inexorable. Dans le _Berceau du chat_, un scientifique découvre un moyen de faire de la glace d'eau solide jusqu'à 40°C. Et après plusieurs péripéties révélées dans le roman (mais dont la conclusion est connue dès le début), le monde devient glacé. Car les océans se sont solidifiés, et… rappelons que la majorité des animaux sont constitués d'eau en très grande partie. Toucher la glace-9 (c'est le nom donné à ce type de cristallisation de l'eau), c'est geler sur place en quelques secondes.

Ce que ce roman m'a terrifiée à l'époque, quand je l'ai lu à 19 ans ! Je voyais de la glace-9 partout, je recherchais son existence (sur Ganymède on trouve de la glace XII, et même si ça n'a rien à voir, je tremblais). Avec le recul, j'ai aimé ce livre, pour son côté science-fiction terriblement réel, terriblement "vrai". Ça pourrait arriver. Ce n'est pas un dragon ou un vaisseau spatial, mais des bouibouitages de chimiste de la Terre. Oui, bien entendu que ça part d'un postulat qui, dans l'état actuel des sciences, est farfelu, parce qu'il défie les lois de la physique telles que nous les connaissons. Mais que dire de la radio-activité, de la mécanique quantique ou l'expansion de l'univers : c'était inenvisageable, même pas concevable il y a 200 ans.

Honeysuckle fait un voyage d'une cinquantaine de kilomètres, à pied puisque les voitures se sont faites rares en très peu de temps, elle croise un catcheur, des membres de la secte voisine, des militaires, des gens "bien sous tous rapports" qui deviennent soudain des vautours (les deux profils principaux en temps de crise : les solidaires et les égoïstes…).

C'est un portrait triste d'un pays déjà rongé par la religion (les religions) et la peur. Presque étonnée qu'il y ait si peu de gens avec un fusil, mais l'auteur ne voulait peut-être pas insister sur ce point. Ensuite, peut-être que toustes les survivalistes étaient enfermé⋅es dans leur bunker pendant toute l'histoire 😄

Et la révélation finale, que je ne dévoilerai pas, est très bien amenée, et en relisant… tout était là, tout pouvait être deviné, les indices étaient semés très proprement. Du grand art, à mon goût.

Note : 4/4. Un roman à mystère très bien ficelé, qui emprunte juste un tout petit bout à la SF pour créer une histoire de relations. Encore et toujours, notons ! Ce sont les personnages qui font les récits.

[^1]: Son nom de plume est une référence à ses prénoms : Joseph Hillstrom. Et ses prénoms sont une référence à un syndicaliste états-unien du début du XX<sup>e</sup> siècle, si c'est pas la classe.

[^2]: En relisant pour prendre des notes et vérifier que je n'ai rien oublié, je tombe sur cette citation :

    > […] Il existait une possibilité pour que tous les nuages de pluie se changent en usines à cristaux. Il [le chimiste interviewé] appelait ça le "scénario Vonnegut".

    Je n'ai pas fait le lien au moment de la lecture, mais maintenant cela me paraît évident que c'est une allusion au _Berceau du chat_.
