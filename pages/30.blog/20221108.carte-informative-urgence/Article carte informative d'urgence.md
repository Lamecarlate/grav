---

meta-création: "2022-11-08 19:11"
meta-modification: "2022-11-08 21:49"

stringified-date: "20220930191133"

title: Carte informative d'urgence
template: "blog-item"

date: "2022-11-08 21:49"

taxonomy:
    type: article
    category:
        - vie pratique
    tag:
        - urgence

# plugin Highlight
highlight:
	enabled: false

---

Pour mon besoin personnel de <del>gestion de flippe</del> préparation à l'imprévu, j'ai fabriqué une petite carte informative d'urgence avec des informations de base (nom, groupe sanguin, allergies, etc), et je vous partage le gabarit.

===

Le format est celui d'une carte de crédit[^1], je l'ai imprimé et plastifié[^2].

![Recto de la carte informative, avec des informations, décrites plus bas](Template-Carte-informative-urgence-recto.jpg){.media--align-center}

On a, de gauche à droite et de haut en bas :

- nom et prénom
- date de naissance
- groupe sanguin et rhésus
- adresse
- numéro de sécurité sociale[^3]
- allergies (ça peut être aussi médications, etc)
- personnes à contacter

Pour le verso, j'ai choisi les numéros d'urgence les plus importants à mon sens, c'est surtout pour avoir un verso, hein, et j'avais envie de couleurs.

![Verso de la carte informative, avec des numéros d'urgence ](Template-Carte-informative-urgence-verso.jpg){.media--align-center}

Évidemment, comme c'est du SVG, c'est entièrement modifiable à votre guise, avec Inkscape ou InDesign ou autre.

Les marques sur les coins sont des traits de coupe, c'est en les suivant qu'il faudra découper les cartes pour avoir le bon format. Oui, les marques snt différentes, parce que sur le recto c'est fait à la main, et sur le verso, via `Extensions > Rendu > Mise en page > Marques d'impression` dans Inkscape (et j'ai eu la flemme de reporter ma découverte sur le premier fichier).

Les fichiers : [Recto](Template-Carte-informative-urgence-recto.svg) et [Verso](Template-Carte-informative-urgence-verso.svg).

Si ça intéresse des gens (dites-le moi sur le Fédiverse ou par e-mail), je pourrai parler plus longuement de ce que je fais dans le domaine de la survie. Citadine mais dans une zone inondable, je ne connais pas grand chose donc ça peut prêter à rire mais c'est toujours passionnant de partager des points de vue (j'avoue que pour l'instant je suis surtout à l'étape "faire une liste de trucs à réunir" et je lis beaucoup[^4]).

[^1]: D'ailleurs c'est passionnant les formats standards pour les objets d'identification, voir [l'article Wikipédia sur ISO/CEI 7810](https://fr.wikipedia.org/wiki/ISO/CEI_7810)

[^2]: J'avais un copain avec une plastifieuse sous la main mais vous faites comme vous voulez.

[^3]: Et je suis terriblement contente de trouver des occasions d'utiliser le "№", qui est le véritable caractère pour "numéro", oui j'étale ma science typographique et alors kestuvafèr.

[^4]: Conseil numéro 1 : beaucoup d'articles sur la matière sont états-uniens, et ça parle très vite de flingues, filtrez.

