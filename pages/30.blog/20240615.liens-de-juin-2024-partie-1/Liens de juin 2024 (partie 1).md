---
meta-création: "2024-06-04 21:38"  
meta-modification: "2024-06-15 19:59:06"  

stringified-date: "20240604.213849"

title: "Liens de juin 2024 (partie 1)"  
subtitle: Glanage d'Internet, des sciences, de belles images, de l'histoire, du poème pour potichat, du webcomic western, de l'humour, de la musique, et du chat mignon.
template: "blog-item"  

date: "2024-06-15 21:38"  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - sciences
        - histoire
        - développement web
        - humour
        - musique
        - mignonneries

# plugin Highlight
highlight:
    enabled: false

---

À chaque fois, en début de mois, je me demande si je vais avoir assez de choses à présenter ici, les premiers jours, j'ai deux, trois liens, et puis ça se remplit, ça se remplit, et paf, 7 minutes de lecture ici (et 4 heures à l'extérieur, sans compter le cas où vous iriez lire toutes les archives des sites où je vous emmène). Cette fois encore, des sciences, de belles images, de l'histoire, du poème pour potichat, du webcomic western, de l'humour, de la musique, et du chat mignon. 

===

[Une horloge, en javascript, qui s'affiche dans son propre code](https://aem1k.com/qlock/). Via [Andy Baio](https://xoxo.zone/@andybaio/112558775736219190).

[Le secret des pyramides enfin révélé](https://www.youtube.com/watch?v=LjD_bOPJWpQ&t=133s) (vidéo avec du son, sans sous-titres, 40 minutes au total mais je vous emmène à 2 minutes 13 pour une séquence de 3 minutes). Partez pas, partez pas, c'est une vraie actu scientifique ! On a probablement découvert pourquoi les pyramides de Gizeh ont été construites à cet endroit-là.

[Le ciel est planté](https://pixelfed.social/p/Kantoman/704113758727375195).

[Les chiffres de Kaktovik](https://www.youtube.com/watch?v=oudyQnI0B8g) (vidéo avec du son, sous-titres incrustés, une minute). Dans les années 1990, des étudiants Iñupiat du nord de l'Alaska ont inventé une manière d'écrire les chiffres et les nombres en base 20 pour mieux coller à leur langue, et ça a plutôt bien marché, l'échec scolaire a diminué dans cette région.

[Ancient Greeks Couldn't See Blue DEBUNKED Once and For All](https://www.youtube.com/watch?v=omPGq_cu58Y) (vidéo avec du son, en anglais, sans sous-titres, 25 minutes). Connaissiez-vous cette anecdote comme quoi les Grecs anciens (et autres peuples de l'Antiquité) ne voyaient pas la couleur bleue ? Notamment liée à une description de la mer « comme du vin », par Homère. C'est un mythe, vraiment. Il y a des tas de raisons pour que la mer soit décrite comme telle (indice : l'Iliade et l'Odyssée sont des poèmes), en grec les mots kuanos et glaucos désignent des nuances de bleu, on retrouve des descriptions de plantes et d'animaux clairement bleus avec des termes bleus, bref, c'est complètement une croyance. Cette vidéo, en anglais et sans sous-titres hélas, est assez compréhensible si vous entendez car l'auteur est italien et articule très bien l'anglais. 

Et dans les commentaires je trouve ceci :

<blockquote lang="en">Thank you so much for this. They were doing this about Celtic peoples and I'm like uh. *Woad*. Hello. They had words for blue. They had distinct dyes for textiles. They used woad paste to make markings for literal thousands of years.</blockquote>

Je ne connais pas ce mot, <i lang="en">woad</i>. Donc je cherche un peu et c'est le [Pastel des teinturiers](https://fr.wikipedia.org/wiki/Pastel_des_teinturiers) (ou guède, ou vouède, ou waide). Déjà, 1) c'est cool, 2), ça m'emmène évidemment sur la page du pastel en tant que bâtonnet de couleur, et à ça : [Portrait de Louis XV au pastel par Quentin de la Tour](https://upload.wikimedia.org/wikipedia/commons/0/0e/Maurice_Quentin_de_La_Tour_-_Louis_XV_-_WGA12356.jpg?uselang=fr). Oui oui oui c'est du pastel sec. Proprement impressionnant.

[Chute libre, ou pourquoi on pète les plombs](https://www.youtube.com/watch?v=qHnYOPYt1d4) (vidéo avec du son, sans sous-titres, 2 heures) : une bonne analyse de l'équipe de Bolchegeek sur le film de Joel Schumacher _Chute libre_. Cette vidéo replace le film dans son contexte et ajoute des infos sur des tueries de masse qui ont été faites par des gens qui ont « pété les plombs ». Glaçant et passionnant. J'ai vu ce film en 3<sup>e</sup> en cours de français, et je ne me souvenais plus du tout que le protagoniste est un antipathique WASP raciste, violent avec son ex-femme, et bloqué dans le passé. Cela a son importance dans l'analyse. Note : la vidéo a été dépubliée un temps sur demande de Warner Bros, elle est de nouveau en ligne, espérons que ça dure… espérons que l'éditeur comprenne que ça n'est pas une diffusion du film mais bien un travail original sur le sujet du film.

[The existence of trains debate](https://leftycartoons.com/2019/08/19/the-existence-of-trains-debate/) (en anglais, image avec une transcription en dessous). Surtout ne pas mordre mon poing surtout ne pas aïeuh.

[Queueing for an Ice Cream, poème](https://mastodon.online/@brianbilston/112569769745034936) (en anglais). Pfiou, ça remue.

> 🎶  
Quand on a que la mue  
A s'offrir au poilage  
Au jour du nettoyage  
Qu'on nous brosse le fu  
Quand on a que la mue  
Des poils ici et là  
La sieste sur le sofa  
Et la nuit où l'on joue  
Quand on a que la mue  
Pour garnir nos fefesses  
Sans nulle autre richesse  
Que l'eau que l'on a bue  
Quand on a que la mue  
Pour habiller de poils  
Et d'odeurs pas banales  
La froideur du séjour 🎶

Par [Wildduck](https://mamot.fr/@Wildduck/112579263700781661). En réponse à un haiku (un chaiku ?) de Teneko, qui est un chat, dans une discussion entre chats (oui, vous connaissiez peut-être le Twitwi des potichats, mais cette communauté existe aussi dans le fédiverse).

[Introduction de la finale de l'Eurovision 2015](https://www.youtube.com/watch?v=gA3m28bHnqM) (vidéo avec du son, de la musique, des paroles en anglais, sans sous-titres mais les paroles sont dans la description). Je pleure pas, enfin, si, mais de joie. C'est bondissant, un mélange de techno/pop (j'y connais rien en musique) et de classique (avec l'orchestre symphonique de la radio de Vienne), et franchement ça fait du bien. Via [Johann](https://seine.fleuv.es/@joe/112592644710682010).

[Goth Western](https://www.gothwestern.com/) (webcomic en anglais, sans textes alternatifs). Jack est une chasseuse de primes dans un ouest des États-Unis un peu différent de ce que l'on connaît, avec des pactes avec les dieux des morts, des flingues, des fleurs et du sang. Ça se lit vite, c'est une belle histoire d'amour et de mort en noir et blanc (et rouge).

[Un husky sur un piano](https://beige.party/@RickiTarr/112527573004256834) (vidéo avec du son, sans paroles, 1 minute). Bon, au risque de casser tout de suite l'ambiance, c'est re-enregistré derrière, ce ne sont pas les vraies notes, mais hey, c'est joli et c'est très bien fait. La [vidéo originelle est ici](https://beige.party/@Carnivius@masto.ai/112529858444156806) (en vrai de vrai elle est sur Instagram mais quelqu'un l'a re-téléversé sur le fediverse). Je plains un peu le piano, c'est lourd un husky.

[Rattrapage de toute beauté par le CM de Lego](https://wetdry.world/@nonfedimemes/112558635977585796) (ou alors c'est du marketing pour présenter le nouvel ensemble, mais hé, c'est bien fichu).

![Décanteur et quatre verres Art Nouveau avec des motifs floraux](c7c0a580b54218d3.jpg "Décanteur et verres Art nouveau, 1900-1905. Verrerie Meyr's Neffe.")

C'est un magnifique ensemble, composé de quatre verres ressemblant à des fleurs, la tige étant la jambe du verre, verte, très fine et très longue, du verre, et il y a des pétales bleus sur la [paraison](https://fr.wikipedia.org/wiki/Paraison) (le corps) ; et un grand décanteur, très haut, avec des pétales verts et bleus au bas, et une poignée verte.

Source : [ElleGray](https://mstdn.social/@ElleGray/112594027623735068) 

[« Avec nous, c’est la tolérance des euros »- Le Billet de Charline](https://www.youtube.com/watch?v=abNMmwTAAzM) (vidéo avec du son, des sous-titres, 3 minutes). Huhuhuhu, quand Charline Vanhoenacker et ses comparses venaient se fiche de la tête des invités du 7/9 de France Inter… Ici, c'est Ciotti, qui a un sourire comme s'il venait de croquer dans un citron. Niark.

[111 instruments… 111 seconds](https://www.youtube.com/watch?v=qOvPWvJaMuc) (vidéo avec du son, sans paroles, 111 secondes 😸).

[Un vieux chat de ferme](https://www.youtube.com/watch?v=lQjQ33qOI_E) (vidéo avec du son, en anglais, avec des sous-titres incrustés, 3 minutes). Mouuuuh c'est mignon. Un gros vieux chat tellement câlin, et bavard. Via l'Amoureux.

[Aziraphale et Crowley vont à la marche des fiertés](https://mastodon.social/@RubyGold/112543027478118196). (et c'est cute)

![Planche botanique avec plusieurs plantes dessinées, montrant la partie aérienne et les racines.](80d49e997ffd0dec.webp){.figure-media--wide}

Quand on dit que les racines sont l'organe le plus important d'une plante, on ne plaisante pas. Image tirée du livre [ The ecological relations of roots, par John E. Weaver](https://archive.org/details/ecologicalrelati00weav/page/32/mode/2up). Via [juli g pausas](https://fediscience.org/@jgpausas/111890597594720934).

[These Faces Are The Same Color!](https://www.youtube.com/watch?v=ccLUxJvViUA) (vidéo avec du son, sans sous-titres, une minute). Gnnnnnn. Même dans la dernière image, je vois un gradient. Mon cerveau est un connard (mais vous avez le même, ça me rassure).

<blockquote lang="en">“It’s better to be alive than right”</blockquote>

J'aime cette phrase, tirée de la vidéo précédente.

[Arrêtez de mesurer le temps !](https://eldritch.cafe/@UnePorte/111328839039692212)

[🌹 Traditional Rose Jam: Homemade Recipe from Fresh Roses](https://www.youtube.com/watch?v=dENZZg0xbTI) (vidéo avec du son, quelques paroles, pas de sous-titres, 30 minutes). Que c'est doux. La cueillette des roses, leur cuisson pour en faire de la confiture ou une boisson, le fauchage du pré, la construction d'une barrière, ces images sont parsemées de chatons, [corneille mantelée](https://fr.wikipedia.org/wiki/Corneille_mantel%C3%A9e) (et un pôtit corneilleau), lapin, grenouille et diverses petites bêtes mignonnes. J'aime beaucoup le rythme, très tranquille (des plans sur l'étang où il « ne se passe rien »), le montage est bon, et ça donne bien envie, ce calme, cette vie pastorale. Via [Krysalia](https://mastodon.social/@Krysalia/112615139875711038).

---

J'ai terminé cet article après avoir rattrapé mes vidéos de retard en cassant des noix, je n'ai plus de doigts, je souffre, send help (par exemple sous forme de sorbet à la framboise).
