---
meta-création: "2024-03-01 19:29"
meta-modification: "2024-03-01 19:29"

stringified-date: "20240301.192917"

title: "Liens de mars 2024 (partie 1)"
template: "blog-item"

date: "2024-03-16 19:29"
publish_date: ""
published: true # si besoin de date de publi + force publi

taxonomy:
  type: article
  category:
    - liens
  tag:
    - musique
    - linguistique
    - mignonneries
    - société
    - artisanat
    - philosophie
    - sciences

# plugin Highlight
highlight:
  enabled: false
---

Et non pas lièvre de mars.

De la linguistique, de la musique, de l'accessibilité numérique, des chats, de la santé mentale et du transhumanisme, des insultes et des rimes, des maths, de l'artisanat médiéviste, et des chats.

===

Je cherche l'orthographe de « à-coups » (comme dans « se déplacer par à-coups »), et donc je me demande… la lettre « à », dans combien de mots la trouve-t-on ? Eh bien la réponse est : pas beaucoup mais quand même. [Quelqu'un s'est déjà posé la question il y a 9 ans](https://french.stackexchange.com/questions/13014/liste-des-mots-contenant-%C3%A0).

Les simples :

- à
- çà (bonus cédille !)
- déjà (et son ancêtre [jà](https://www.cnrtl.fr/definition/j%C3%A0), que je découvre, car « déjà », c'est « dès jà », « dès à présent »)
- là

Et puis tous les mots conçus à partir de ces derniers :

- delà
- deçà
- voilà ((1538) Grammaticalisation de l’impératif « vois là » me dit le [wiktionnaire](https://fr.wiktionary.org/wiki/voil%C3%A0))
- holà

Et les mots composés : à-coups, vis-à-vis, etc.

Bonus : pietà, mais c'est de la triche c'est un mot estranger 😛

---

[Kangaroo words](https://kangaroowords.com/) : des mots qui contiennent leur synonymes (en anglais). Je me demande si ça existe en français ! Via l'Amoureux.

J'ai la chance de ne jamais avoir entendu de vive voix des gens dire que finalement ça vaaaa le bug de l'an 2000 n'a pas eu lieu, tout s'est bien passé. Oui, tout s'est bien passé **parce que** des milliers de gens ont bossé pendant des années pour préparer les systèmes. Et ce message de [Damien Guard](https://mastodon.social/@damieng/112016981000597860) résonne fort, vu qu'apparemment il y a eu des soucis fin février. Le 29. Parce que tout un tas d'ordinateurs, de serveurs, de montres n'ont pas prévu un événement qui arrive tous les quatre ans. C'est déprimant.

On a beau être en mars il fait-tu quand même bin frette encore, non ? [Coton ouaté](https://invidious.fdn.fr/watch?v=_whvVXX0hCk) (vidéo avec du son, de la musique, sans sous-titres), via [Clochix](https://mastodon.social/@clochix/112027487204762966).

[iPhone live captions wow my 91 year dad](https://www.lflegal.com/2024/02/accessibility-for-elders/) (article en anglais) : excellent article qui montre que les outils d'accessibilité dans les objets technologiques (ici un téléphone) peuvent aider les personnes âgées. Via [Adrian Roselli](https://toot.cafe/@aardrian/112037870025617999).

Rholalalala c'est mignon : [Synchronised snooze session](https://mstdn.social/@yurnidiot/112039931146247618) (c'est un gif animé, cliquez ou bien laissez votre souris au-dessus).

[Suicide et santé mentale - Philosophy Tube](https://www.youtube.com/watch?v=eQNw2FBdpyE) (vidéo avec du son, en anglais, avec sous-titres anglais et français, 30 minutes à peu près). Oumf. C'est une vidéo difficile à regarder mais elle est importante. Attention : comme le titre l'indique, ça parle de tentatives de suicide, de pensées suicidaires et de la médicalisation de la santé mentale.

[Transhumanism - Philosophy Tube](https://www.youtube.com/watch?v=DqPd6MShV1o) (vidéo avec du son, en anglais, avec sous-titres anglais, 45 minutes à peu près). Réflexions sur le transhumanisme, qui passe par la définition de ce qu'est être humain, par la perception du monde et par comment déterminer les limites de ce qu'est qu'être humain. Toujours un peu perchées, les vidéos de Philosophy tube, mais j'aime bien, et elles ont des bases solides et une sacrée bibliographie. Je ne retiens pas énormément des choses, hélas, mais ça met des petites graines dans ma tête.

[Cathedrals of Mourning](https://www.youtube.com/watch?v=SkdkZN1rduo) (vidéo avec du son, des paroles en [chant guttural](https://fr.wikipedia.org/wiki/Chant_guttural) et des sous-titres sous forme de cartons interstitiels). Je lis le blog de faux-documentaire de [Belzebubs](https://fr.wikipedia.org/wiki/Belzebubs) depuis pas mal de temps, et j'ai mis… des mois à réaliser qu'il s'agissait d'un vrai groupe de musique, pas seulement de personnages de comics. Pas forcément très fan du son mais j'aime bien un peu de death metal de temps en temps, et le clip est très chouette : de belles animations, et un truc sérieux… qui ne se prend pas au sérieux. Comme le dit une personne dans les commentaire de la vidéo : "How on earth can something be absolutely adorable and brutal at the same time?"

[Les insultes, un art chez les Grecs anciens](https://www.youtube.com/watch?v=P6gO1f78OxA) (vidéo avec du son, avec sous-titres, 15 minutes environ, via [Augier](https://diaspodon.fr/@AugierLe42e/112039074459215077)) : l'idée que les insultes servaient d'exutoire aux Grecs pour empêcher les meurtres impulsifs est intéressante. Les ritualiser, les codifier, c'est encore plus drôle. J'aime bien un des commentaires de la vidéo, de thesophoclean : « J'adore comment cet épisode fait un pendant passionnant à celui sur les insultes du moyen-âge. On y retrouve l'exact inverse : dans l'Antiquité, l'insulte ritualisée permettait d'éviter le combat, mais au Moyen-Age, l'insulte avait de très fortes chances de provoquer le combat! ». Ça serait un autre sujet chouette à étudier. Et à la Renaissance ensuite !

Cela me ramène évidemment à la merveilleuse BD « De cape et de crocs » et son duel dans une auberge, non au premier sang mais au dernier mot. La BD en entier est extraordinaire, mais ce passage… ce passage, oh.

![Début du duel (transcription après les trois pages).](P00007.webp "Fin de la page 4, début du duel."){.figure-media--wide}

![Le duel proprement parlé, dans tous les sens du terme (transcription après les trois pages).](P00008.webp "Page 5, le duel"){.figure-media--wide}

![Fin du duel (transcription après les trois pages).](P00009.webp "Début de la page 6, le duel est fini."){.figure-media--wide}

<div markdown="1" class="conversation">
<span class="conversation--person">Armand de Maupertuis</span>, <i>un renard anthropomorphique en costume de gentilhomme XVI<sup>e</sup> siècle, chapeau à plume inclus, en pose agressive d'escrime, sa rapière dégainée</i> : En garde, maraud !

<span class="conversation--person">Son interlocuteur</span>, <i>un humain en costume en gentilhomme mais un peu plus pirate, gilet sur le dos nu, foulard sur le crâne, debout, bien droit</i> : Réglons cette affaire à la rixme.

<span class="conversation--person">Armand</span> : Qu'est cela ?

<span class="conversation--person">L'autre</span> : Un duel.

<span class="conversation--person">Armand</span> : Au premier sang ?

<span class="conversation--person">L'autre</span> : Au dernier mot.

<span class="conversation--person">Armand</span> : Qu'importe le bâton, du moment qu'il te rosse.

<span class="conversation--person">L'autre</span> : Terrien présomptueux ! Sais-tu bien qui je suis ? Je suis Adynaton, favori de Calliope, des rhéteurs redouté, fatal manieur de trope ! Sur mon défi rimé, sauras-tu renchérir ?

<span class="conversation--person">Armand</span> : À ces jeux de Villon, je vaincs sans coup férir !

<span class="conversation--person">Adynaton</span>, <i>en contre-plongée, faisant un geste de sa main très baguée, pointant vers le bas (il est très grand et Armand est un renard, donc plutôt petit)</i> : Fais rire le parterre, histrion qui ânonne, avalant la voyelle et toussant la consonne !

<span class="conversation--person">Armand</span> : Qu'on sonne le tocsin pour pendre, c'est urgent, au gibet du bon goût ton bagout d'indigent !

<span class="conversation--person">Adynaton</span> : Dix gens de ta farine en deux vers je terrasse ! Sens-tu sous mes soufflets ton rictus qui s'efface ?

<span class="conversation--person">Armand</span> : Fasseyant va le foc de ton discours fumeux quand sur la mer des mots voile au vent je me meus !

<span class="conversation--person">Adynaton</span> : Meuh ! Meuh ! Quelle assonance et quelle rime obtuses ! Tu mugis tel un veau ! Bouffon, va ! Tu m'amuses !

<span class="conversation--person">Armand</span> : Ma muse aimant les veaux, je meugle avec brio.

<span class="conversation--person">Adynaton</span> : Ta déesse est génisse ?

<span class="conversation--person">Armand</span>, <i>un peu acculé, prenant appui contre le mur</i> : Elle se nomme… <i>(Il saute sur des tonneaux pour être en hauteur.)</i> Io ! Iotarcique est mon style ici je supplicie piteuse au pilori l'orale impéritie. Ma faconde est sans faille elle a noyé ton flot, sur ton chant sans éclat claque un bec qui se clôt ! <i>(Il continue sans laisser le temps à son adversaire de réagir.)</i> L'ultime alexandrin sera le coup de grâce, assénant sans merci… douze pieds dans ta face !

<span class="conversation--person">Adynaton</span> : … … Mon débit ! … Je suis coi !

<i>La foule rugit de joie, voyant le duel terminé.</i>

<span class="conversation--person">Adynaton</span> : Adynaton d'Hyperbolie s'incline devant ton éloquence. Il ne lui reste plus qu'à faire vœu de silence et partir au désert parmi les mimes et les bêtes sans voix.

<span class="conversation--person">Armand</span> : Souffrez que je m'incline à mon tour ! <i>(Il le fait, patte sur le cœur.)</i> Vos saillies furent piquantes et l'entretien pointu. Au diable la querelle !

<span class="conversation--person">Adynaton</span>, <i>topant là </i>: Et au feu vers moqueurs !
</div>

Que j'aime ce duel, qui rappelle bien sûr ceux du _Cyrano_ d'Edmond Rostand (je ne connais pas assez la vraie vie du sieur de Bergerac pour affirmer qu'il le faisait aussi). Et ce n'est pas du tout une coïncidence… <i>(chuchoté)</i> Lisez *De capes et de crocs* !

Petit bonus technique : afin de rendre ce dialogue correctement j'ai recherché, et le W3C ne précise pas de sémantique particulière. Ce sont donc de simples paragraphes, j'ai mis visuellement en valeur les noms des personnages (les petites capitales c'est la vie) et les didascalies sont dans des éléments `i`, qui signifient en HTML5 « lire avec une voix différente » ([voir la doc à ce sujet](https://developer.mozilla.org/fr/docs/Web/HTML/Element/i)). C'est utilisé pour les mots en langue étrangère, les noms de bateau, et donc, les didascalies. J'aurais bien voulu reproduire la mise en page de mon exemplaire de _Cyrano_, avec nom et didascalie centrées et sur leur propre ligne, mais cela rendait l'écriture plus difficile. En effet, j'écris en markdown mâtiné de html, et pour faire cela il aurait fallu beaucoup plus de html et là, tout de suite maintenant j'avais la flemme ; plus tard, peut-être.

---

[La tribu d'Hanouna](https://www.youtube.com/watch?v=DTcPAysvftU) (vidéo avec du son, sous-titres incrustés) : des grandes Goguettes en trio (mais à quatre). Paroles aux petits oignons, diction impeccable, de la cornemuse. Le bonheur.

Une preuve visuelle que les parenthèses dans les multiplications n'ont pas d'importance : [Brackets don't matter](https://blog.ncase.me/brackets-dont-matter/) par Nicky Case (l'autrice de [The evolution of trust](https://ncase.me/trust/) ou le très connu [Parable of the polygons](https://ncase.me/polygons/)). En anglais, avec des chiffres, des gros points et des boîtes !

Wo-ah. Ce projet ambitieux est magnifique : pour l'anniversaire de son cher K, Eliness décide de :

- calligraphier un poème qui leur tient à cœur à toustes les deux
- et l'enluminer
- et peindre des illustrations
- et relier l'ensemble
- et tout ceci le plus possible avec les techniques médiévales : l'encre, la peinture, la reliure…

Donc : wo-ah. Le projet c'est [Chiroto T. Datoca](https://www.hypothermia.fr/2024/03/chiroto-t-datoca). Via [la souris](https://mamot.fr/@la_souris/112077829989678891).

[La visibilité en vélo, avec style](https://mastodonapp.uk/@dutchbarracuda/112093028290534844).

Toujours vélo : [pourquoi il faut proscrire les supports de type pince-roues ou râteliers](https://www.cc37.org/pourquoi-il-faut-proscrire-les-supports-de-type-pince-roues-ou-ratelier/) (via [Emeline](https://piaille.fr/@emeline/112104568973091866)).

---

![Trois personnes discutent, en anglais : "What's up? Was Doug gonna come? Doug loves brunch." On lui répond : "Nuh uh, Doug's stuck 'cause of a tunnel obstruction. A truck dumped a ton of onions." "Ugh." Le sous-titre de l'image dit "The schwa is the most common vowel sound in English. In fact, if you stick to the right conversation topics, you can avoid learning any other ones."](https://imgs.xkcd.com/comics/schwa.png)

Source : [xkcd](https://xkcd.com/2907/).

Le [schwa](https://fr.wikipedia.org/wiki/Schwa) est en effet la [voyelle moyenne centrale](https://fr.wikipedia.org/wiki/Voyelle_moyenne_centrale) de beaucoup de langues, celle qui demande le moins d'effort à prononcer, et ça pourrait expliquer pourquoi l'indécision est représentée par ce son (« euuuuh ») en français.

Une bonne vidéo sur le sujet : [Ə: The Most Common Vowel in English](https://www.youtube.com/watch?v=qu4zyRqILYM) (vidéo avec du son, en anglais, et des sous-titres en anglais).

---

J'aime bien l'humour tranquille et littéraire de Tom Gauld : [Wordle addiction](https://twitter.com/tomgauld/status/1768652990796218447) (en anglais ; c'est sur X/Twitter, désolée, mais la version originelle postée sur le Guardian n'a pas de texte alternatif).

<details markdown="1">
<summary>Explication de la blague de Wordle addiction</summary>
Si vous ne connaissez pas Wordle, c'est un jeu web, dans lequel il faut trouver un mot  avec des indications de lettre présente et de placement correct. C'est inspiré du jeu de plateau Mastermind et aux mécaniques similaires au jeu télévisé Lingo, appelé Motus en France. Dans Wordle on doit trouver des mots d'exactement 5 lettres, d'où le vocabulaire restreint du patient, qui n'a plus que des mots de cette longueur en tête. 
</details>

---

Roger est un trébochat (mais ne lui répétez pas, il va prendre la grosse tête, le chef) : [Quoi encore](https://piaille.fr/@RogerBlblblblbl/112095491889597523).

---

Journée rangement, nettoyage et bricolage (on profite de ce que la petite chatte en convalescence n'est plus confinée dans son enclos pour passer l'aspirateur sans risquer de la terroriser sans cachette possible). Pfiou. Épuisée du week-end. À la prochaine fois !
