---
title: "Conférence de Tûtie : Tempête de boulettes géantes"

date: 2018-10-07 12:00
publish_date: 2018-10-07 12:00
published: true
taxonomy:
    type: article
    category:
        - informatique
    tag:
        - erreur humaine
header_image_file: boulettes.058.jpeg
header_image_caption: "Un cygne sur l'eau (vous comprendrez pourquoi en lisant la conférence)"
author: 'Lara'
---

Faire des boulettes, des erreurs humaines, ça arrive. Dans tous les métiers. Là, ça parle d'informatique, avec humour et bienveillance.

===

![La diapo dit 'drop table sur le dev. C'était bien le dev, hein ? #héMerde'](boulettes.064.jpeg)

[Tempête de boulettes géantes](https://tut-tuuut.github.io/conferences/tempete-de-boulettes-geantes) est une conférence de Tûtie (Agnès Haasser).

![La diapo dit 'En résumé. Faites de la revue de code systématique par poule (pull) requests. Discuter avec un canard, c’est chouette. Et enfin, faites cygne (signe) si vous êtes en galère.'](boulettes.062.jpeg)
