---
meta-création: "2023-04-10 16:29"  
meta-modification: "2023-04-10 16:29"  

stringified-date: "20230410.162902"

title: "Inktober 2022"  
template: "blog-item"  

date: "2023-04-10 16:29"  

taxonomy:
    type: article
    category:
        - création
    tag:
        - défi
        - crayon à papier

# plugin Highlight
highlight:
    enabled: false

---

Bon, mon Inktober 2022 est un échec. Soyons réaliste. J'ai conscience que je dévalue souvent mon travail, mais bon, hein. 

===

D'accord, je m'étais peut-être compliqué la tâche (souvenez-vous de [ce que je disais l'an dernier](../inktober-2021)), en faisant au crayon à papier. D'accord, le 2 octobre, j'ai glandé. D'accord, le 3 je partais à Paris pour une semaine, et j'oubliais mon crayon **et il n'y en avait pas dans les locaux de mon entreprise** <span role="img" alt="Visage d'un chat terrifié">🙀</span> (oui, on est tellement dans le digital que trouver un crayon s'est avéré impossible). Donc, voilà, à peine commencé, le défi retombait comme un soufflé de Gaston Lagaffe sur ma motivation. J'ai un peu rattrapé après coup, mais mufffffff.

Voilà quand même le résultat. J'ai du mal à l'aimer (même si je trouve la gargouille mignonne et la chauve-souris choupie, allez, je le concède).

Jour 1 : gargouille. Elle est peut-être installée là parce que le ciel est joli à l'aube.

![Une tour gigantesque, dont on ne voit ni pied ni tête, une petite gargouille est nonchalamment assise sur un rebord saillant.](Inktober%202022%20-%2001%20-%20Gargouille.webp)

Jour 2 : débandade. Ici j'ai hésité à mettre le lapin, et à seulement le suggérer par les traces, mais j'ai préféré être plus directe.

![Une forêt avec des arbres très fins et très verticaux, de la neige, et des traces de pattes la traversant ; à gauche, on voit le lapin à l'origine des traces en train de fuir à toute berzingue.](Inktober%202022%20-%2002%20-%20D%C3%A9bandade.webp)

Jour 3 : chauve-souris. J'aime bien le style « tout petit personnage dans un décor immense », si loin de mes habitudes.

![Un étang près d'un bois, une chauve-souris vient de passer y boire, comme le montrent les ronds dans l'eau.](Inktober%202022%20-%2003%20-%20Chauve-souris.webp)

Jour 6 : bouquet. Il fallait bien que je cale un jeu de mot, hein, on ne se refait pas.

![Une bouteille de vin réutilisée comme un vase avec quelques fleurs dedans.](Inktober%202022%20-%2006%20-%20Bouquet.webp)

Jour 9 : nid. Très simple. L'arbre est probablement un platane parce qu'il est moche. Non je ne m'excuserai pas auprès des platanes.

![Un nid très cliché, fait de paille et de trucs duveteux, avec trois œufs dedans ; il est dangereusement posé sur la branche d'un gros arbre.](Inktober%202022%20-%2009%20-%20Nid.webp)

Voilà l'ensemble de mes dessins pour Inktober 2022. 

On se dit qu'on fait mieux en 2023 ? Ça sera pas difficile.