---
meta-création: "2023-11-04 08:42"  
meta-modification: "2023-12-02 22:35:08"  

stringified-date: "20231104.084219"

title: "Liens de novembre"
template: "blog-item"  

date: "2023-12-03 08:42"  
publish_date: "" 
published: true # si besoin de date de publi + force publi  

taxonomy:
    type: article
    category:
        - liens
    tag:
        - linguistique
        - mathématique
        - accessibilité numérique
        - humour
        - musique
        - biologie

# plugin Highlight
highlight:
    enabled: false

---

On ne va pas se mentir, le NaNoWriMo a été peu fructueux (c'est pas mon année, hein). Mais j'ai un bon paquet de chouettes liens et images. Au programme : des (gros) chats, des oiseaux à cul jaune, de l'accessibilité numérique, du mystère, des poneys de l'adopocalispe, de la musique, d'excellentes recommandations venant de potichats très sages, des maths, des chiens en 3D moche, une coccinelle. 

===

L'Amoureux m'envoie une vidéo (avec du son, sans paroles) : [The Powerful Puma Screams!](https://www.youtube.com/watch?v=4shrHLbkliY). C'est très chouette ! Et cela me fait retomber dans le trou du ~~lapin~~ puma : pourquoi les pumas ne rugissent pas ? Parce que leur appareil hyoïde est totalement ossifié, contrairement aux animaux du genre <i>Panthera</i> (le jaguar, le lion, le tigre, par exemple). On pensait d'ailleurs jusqu'à il y a peu que toutes les espèces de la sous-famille <i>Pantherinae</i> rugissaient, mais non : [Structure crânienne des panthérinés](https://fr.wikipedia.org/wiki/Pantherinae#Structure_cr%C3%A2nienne) sur Wikipédia.

Et donc je vais regarder la page Wikipédia de la Panthère nébuleuse (au lieu d'écrire mes nouvelles je fais des articles, tssss)(je vais les compter dans le nombre de mots que je mesure 😆)(note de fin novembre : je l'ai fait ! j'avais un tableur de comptage de mots, qui comprenait mes nouvelles, mon journal et cet article), et : déjà, c'est mignon.

![Panthère nébuleuse perchée sur une plateforme. C'est une sorte de gros chat avec de grandes ocelles sur le dos, les pattes et la queue, elle a de grandes oreilles rondes, des yeux jaunes et un gros pif de tigre tout mignon. La queue est longue, à la verticale elle descend plus bas que les pattes. La panthère nébuleuse regarde sur la droite de l'objectif. Elle a une laisse et un collier.](Clouded_Leopard_SanDiegoZoo.jpg)

Ensuite, je découvre le terme de « prusten » (parce que <q>La Panthère nébuleuse fait partie des cinq félins émettant le prusten</q>, avec le tigre, le jaguar, <i>Neofelis diardi</i> et le Léopard des neiges), une vocalisation qui serait à but social, et qui fait une sorte de « frlfrlfrlfrlfrl ». Pas de jugement, c'est ce que j'entends, hein 😄 d'ailleurs, voilà un exemple (tiré de la page Wikipédia sur le prusten) : 

![Prusten de tigre](Tiger-chuffing.ogg)

[An ode to butterbutts](https://wandering.shop/@rosemarymosco/111348760097537949) : tout petit comics (en anglais) à propos de la déprime d'automne et des <i>Setophaga coronata</i> (anciennement Paruline à croupion jaune).

[Le complot vieux — Groland](https://www.youtube.com/watch?v=DTMHLTvyn3w) : hihihhihin (vidéo avec du son, en français, sous-titres autogénérés).

[The beach where Lego keeps washing up](https://www.youtube.com/watch?v=3FxfXVuHRjM) (vidéo avec du son, en anglais, avec sous-titres anglais) : il y a des morceaux de plastique partout dans les plages, et la plupart du temps nous ne le remarquons pas, par habitude. C'est déprimant. Via un [article Reddit à propos du Great Lego Spill](https://www.reddit.com/r/todayilearned/comments/17nbf1j/til_on_february_13_1997_about_five_million_legos/).

[Le fils de Mélaka fait des choses très cool avec Minecraft](http://www.melakarnets.com/index.php?post/2023/11/06/Mes-enfants-ont-un-incroyable-talent).

[Télécharger les choses que vous aimez](https://birchtree.me/blog/a-note-to-young-folks-download-the-videos-you-love/) car ça ne sera pas toujours disponible sur Internet…

[Personnages de Tolkien ou antidépresseurs ?](https://antidepressantsortolkien.vercel.app/) : un petit jeu (en anglais mais c'est peu impactant) pour savoir si vous savez faire la différence entre des noms de personnages de l'univers de Tolkien (Le hobbit, le seigneur des Anneaux, le Silmarilion, etc) et des noms d'antidépresseurs. C'est marrant ! J'ai eu 16 bonnes réponses sur 24 😸

[Comment bien sous-titrer vos vidéos ?](https://emmanuelle-aboaf.netlify.app/blog/article/comment-bien-sous-titrer-vos-videos) : Emmanuelle Aboaf est sourde et explique dans cet article très clair comment on fait de bons sous-titres. Et montre des exemples de mauvais sous-titres, comme ceux qui pullulent sur TikTok ou Instagram, un mot après l'autre, pour donner du dynamisme je suppose, mais c'est insupportable… 

[Just found out my wife has been storing a sword under our bed](https://dice.camp/@contrarian/111154743093742313) (en anglais) : hihihihihi.

[Une discussion ubuesque entre un utilisateur et une personne du service client](https://dragonscave.space/@simon/111154531096840083) (en anglais) : l'idée n'est évidemment pas de dire du mal de l'entreprise, ou de la personne du service client, mais de montrer à quel point les gens n'ont absolument aucune idée de ce qu'est l'accessibilité numérique, ici un site web dont des informations importantes sont uniquement dans des images, donc inatteignables si on ne voit pas bien ou pas du tout.

[Je jalouse les gens qui ont des ours siestant dans leur jardin](https://mastodon.social/@mlmartens/111304095271476634).

[Les clignotants](https://www.youtube.com/watch?v=GfEnGTvitS8) (vidéo avec du son, sans sous-titres) : en chanson, pourquoi il faut mettre ses clignotants, tout le temps.

[Labour, de Paris Paloma](https://www.youtube.com/watch?v=jvU4xWsN7-A) (vidéo avec du son, en anglais, avec sous-titres en anglais) : oumfffff, cette chanson avec une mélodie toute douce frappe fort, à propos des injonctions faites aux femmes de tenir la maison, de tout faire, enfants, repas, lessive, pour que l'homme n'ait rien à faire. Et même plus encore : "therapist, mother,maid, nymph, nurse, servant"… Le passage qui dit « si nous avons une fille, je ne pourrai pas la sauver » tord le ventre… (note : il ne semble pas s'agir de violence conjugale, mais le moment où le mari casse une noix dans son poing m'a envoyé une petite aiguille dans la base du crâne… j'ose espérer que je sur-réagis et que ce n'est pas un sous-texte)

> !important is a shield to protect your own, not a sword to conquer others.

Dans les commentaires de la vidéo de Kevin Powell "[These CSS best practices might be holding you back](https://www.youtube.com/watch?v=7Q7qlquojQk)". J'aime bien cette phrase.

(je découvre Kevin Powell grâce à un collègue, j'aime bien ce qu'il fait, c'est didactique et il montre de chouettes choses en CSS)(et il me redonne envie de refaire tout le style de mon site au secours)

![Peinture numérique dans les tons roses et oranges. Il y a des sortes d'arbres à l'arrière-plan. De grosses billes translucides traversent le paysage, il y a des spirales lumineuses à l'intérieur. Au premier plan, à gauche, sur une bille, un rat, assis, avec une toute petite boule de lumineuse dans les pattes.](Speedpainting%2011112021%20-%20Sylvia%20Ritter.jpg "De jolies billes contenant l'avenir, que seul l'empereur-rat peut déchiffrer."){.figure-media--wide}

Source : [Speedpainting 11112021 via Sylvia Ritter](https://www.deviantart.com/sylviaritter/art/Speedpainting-11112021-897612876).

[Le talent n'existe pas](https://www.youtube.com/watch?v=JAdhaZqU_Fw), par Sleud (vidéo avec du son, pas de sous-titres) : en dessin animé (mais pas que), Sleud explique qu'il y a pas de talent ou de don, dans l'immense majorité des cas, contrairement à ce que l'on dit trop souvent dans le cas des professions artistiques.

[L'Atomium en construction](https://semaphore.social/statuses/111388104796119607).

[Le mystère de la mort d'Edgar Allan Poe](https://www.slate.fr/culture/histoires-sans-fin/mort-edgar-allan-poe-dernier-mystere-baltimore-cause-inconnue-theories-alcool-rage-suicide-meurtre), toujours non-résolu… J'ignorais totalement cette histoire.

[bocaux.club](https://bocaux.club/index.html) via [Maïtané sur flux](https://app.flus.fr/links/1780039450635368925) : des recettes simples et sympas à mettre dans des bocaux. Visuellement c'est très joli et très clair. Il y a très peu de recettes pour l'instant, mais un flux RSS !

[L'histoire d'Harpalyké, princesse-chasseresse de la mythologie grecque](https://mastodon.top/@hist_myth/111257710120123041).

[Adrift, par Andy P](https://mastodon.art/@otterlove/110472301142207809) (en anglais) : une BD courte, à propos d'une femme perdue en plein océan, qui rencontre une créature des profondeurs. C'est drôle et touchant.

![Une femme assise en tailleur dans une bibliothèque tient un livre dans ses mains, devant un parterre d'enfants. Elle dit "And then there'll be four pals on ponies, named Sicky, Stiffy, Stabby, and Starvy". Il y a un texte en dessous qui dit "The best part of the Unabridged Children's Bible is Revelations".](1699747377-20231111.webp)

J'aime bien les dessins de Zach Weinersmith sur son site SMBC, mais il ne met jamais de descriptions à ses BD. Oui, c'est du boulot, mais comme l'essentiel de ses blagues sont textuelles, ça serait vraiment bien…

Ici j'ai essayé de décrire au mieux dans le alt, même si j'ai laissé le texte en anglais. Essayons de traduire !

– Et il y aura quatre monsieurs sur des poneys, appelés Pepeste, Momort, Guéguerre et Famimine !

Source : [Sicky](https://www.smbc-comics.com/comic/sicky) (en anglais).

[Le Procès d'Éric Dupond-Moretti - un masto-feuilleton](https://piaille.fr/@RascarCapacOfficiel/111370964841202593). Mais qu'est-ce que.

[On the nature of daylight, par Max Richter, chez Spline LND](https://www.youtube.com/watch?v=eY_uRwL9dEs) : vidéo en français avec son (important !) et sous-titres. Il s'agit de revenir sur une musique très utilisée dans les films, et de son origine. (et en plus c'est très beau) via [Augier](https://diaspodon.fr/@AugierLe42e/111403097527983018).

[How Deadly Is Quicksand?](https://www.britannica.com/story/how-deadly-is-quicksand) (en anglais) : les sables mouvants qui engloutissent leur proie, c'est commun dans la fiction. Et apparemment ça ne serait pas possible, en fait !

Je découvre la série [Connections](https://archive.org/details/bbc-connections-1978) via [Xavier B.](https://boitam.eu/@xibe/111408251585435361) : c'est une série éducative sur la science des objets (technologiques) qui nous entourent et construisent notre monde. J'ignorais jusqu'à l'existence de cette série, et les deux premières minutes du premier épisode m'ont agrippée. En anglais, sans sous-titres (mais ça doit pouvoir se trouver sur Internet).

[La procédure de sieste préconisée par la circulaire du 14 novembre 2023, c'est important](https://pouet.chapril.org/@Moumousse_et_Gros_Doudou/111408568175850990).

[Isabela's alternate universe song](https://www.youtube.com/watch?v=CtzDvj1CqTc) (en anglais, avec du son et des sous-titres anglais intégrés et flottants si besoin) : si vous avez vu le film _Encanto_ de Disney, vous connaissez sûrement _What else can I do?_, la chanson d'Isabela, la sœur aînée. Si vous ne l'avez pas vu… ya des chances que cette chanson vous divulgâche, tant l'originale (qui se trouve partout) que cette version alternative.

<details>
<summary>Divulgâchage : description des deux chansons</summary>
Dans la chanson originelle, "What else can I do?", « Que puis-je faire d'autre ? », est une phrase de découverte : je pensais que je ne savais faire que des jolies fleurs, mais apparemment pas, alors je veux essayer d'autres choses, encore et encore !

Dans la chanson alternative, la demande en mariage n'a pas été interrompue, et Isabela doit épouser Mariano, alors qu'elle ne le veut pas. Elle le fait pour le bien de la famille. « Que puis-je faire d'autre ? » devient une phrase de résignation : je ne peux pas faire autrement. 
</details>

[Natural lawyer](https://yiff.life/@erkhyan/111384357953832532) (en anglais, blague bilingue avec le français)

[Le plus grands des petits hexagones](https://www.youtube.com/watch?v=N-ZGCsvxBB0) (vidéo avec du son et sous-titres) : ça faisait longtemps que Michaël « MicMaths » Launay n'avait pas fait de vidéo, c'est chouette de le revoir, avec une jolie histoire géométrique (et un nouveau décor).

[Was reality different than when you were a kid?](https://www.youtube.com/watch?v=KElHrydCYkc) (vidéo en anglais avec du son) : Michael de VSauce parle, en si peu de temps (il ne fait plus que des shorts, ça m'attriste) de pourquoi on ressent notre passé différemment de qu'il a été.

[Une fractale dans le sang](https://www.youtube.com/watch?v=6N5iXI_bSn4) (vidéo avec du son et des sous-titres) : bon bin Mickaël Launay est en forme, ne boudons surtout pas ! Ici, pourquoi la matrice des groupes sanguins est un (simili) triangle de Sierpinski ?

> Pour info, la dénomination du groupe O vient de l'allemand "Ohne" qui veut dire "sans" (sans antigène A ni B)

Source : dans les commentaires, par @oliviervancantfort5327

[Putting software in containers is cruel and unnatural.](https://social.chinwag.org/@mike/111417626303690346) (en anglais) : hihihihihi.

Dans les commentaires de [Comment envoyer des textos sans électricité](https://www.youtube.com/watch?v=uA73EwS2Soc) (vidéo avec du son et sous-titres) : 

> C'était très rapide : 9 minutes pour Paris - Lille. Et la ligne Paris - Bordeaux a fait l'objet du premier piratage de l'histoire, avec 2 frères qui ont graissé la patte d'opérateurs pour faire passer des infos sur la bourse, et ainsi faire des petits délits d'initiés. Crime resté impuni puisque les sanctions n'étaient pas prévues dans la loi !

J'adore cette histoire !

C'est le mois des Mickaël/Michael (oui je remonte mes flux RSS, ça se voit tant que ça ?) : [Easy Photography Life Hack](https://www.youtube.com/watch?v=tBFErdYD1wU) (vidéo avec du son, en anglais, sans sous-titres).

Nicky Case écrit de chouettes choses (entre autres) : ici [une micro-nouvelle à propos des reptiliens](https://blog.ncase.me/lizard-person/) (en anglais).

Mandieu, cette phrase ! (oui oui, c'est **une** phrase)

> « À peine eut-elle débouché des gorges de Saint-Yrieix sur le plateau marneux qui les surplombe et d'où l'on découvre, à travers l'immense plaine s'étendant du dernier chaînon des Cévennes aux assises des Pyrénées, ces montagnes dont la beauté grandiose arracha jadis des cris d'enthousiasme au peu sensible Béarnais, déjà roi de Navarre, et faillit le rendre aussi troubadour que bien longtemps avant lui l'avait été Richard Cœur de Lion, alors simple duc du Pays des Eaux, où l'on trouve encore quelques vestiges des monuments érigés en l'honneur de ce descendant de Geoffroy, comte d'Anjou, lequel seigneur, aucun historien n'a su pourquoi ni comment, ornait en temps de paix sa toque, en temps de guerre son haubert d'une branche de genêt, habitude qui lui valut le surnom de Plantagenet, porté plus tard par toute la famille française à laquelle le trône anglo-saxon, après la mort d'Étienne de Blois, le dernier héritier du trône de Normandie, avait été dévolu, ma monture prit peur et manqua de me désarçonner. »

Léon Cladel. Source : [Récréactions littéraires sur Wikipédia](https://fr.wikipedia.org/wiki/R%C3%A9cr%C3%A9ations_litt%C3%A9raires).

[Short "ɪ" long "iː" – SOLVED for French speakers!](https://www.youtube.com/watch?v=GNpbv7hJf6c) (vidéo avec du son, en anglais, avec des sous-titres anglais, et plein de gros-mots-pour-de-faux) : comment différencier **vraiment** les deux sons "i" de l'anglais (live/leaf par exemple), expliqué en direction des francophones. Et ça marche ![^1] Via imni (lien fediverse privé).

Un peu de légèreté, ça fait du bien : [Dog of Wisdom](https://www.youtube.com/watch?v=D-UmfqFjpl0) et [Dog of Wisdom II](https://www.youtube.com/watch?v=TnlakHr-O4w) (vidéos avec du son, en anglais, avec sous-titres incrustés mais on peut aussi ajouter des flottants si on préfère). Via l'ami Aznörth.

[Liste des oeuvres littéraires ou cinématographiques considérablement améliorées en remplaçant les gros trucs par des contrebassons.](https://mamot.fr/@legendarybassoon/111443583405355831) : pfffrrrrrrhuhuhuhouuu (j'ai perdu toute dignité à ce stade, je suis affalée sur mon bureau à rire comme une balei- comme un contrebasson).

[There are two wolves inside you.](https://aus.social/@Unixbigot/111439453153964793) (en anglais). Non, ce ne sont pas les loups auxquels vous pensez, c'est bien mieux.

[Un très beau chat. Admirez.](https://social.vivaldi.net/@jcutting/111446741178195059)

[Intéressant article sur l'anonymat de Banksy](https://www.telerama.fr/arts-expositions/banksy-enfin-demasque-pourquoi-la-levee-de-son-anonymat-n-a-vraiment-aucun-interet-7018180.php).

[Techno-optimisme : et si nous arrêtions d’écouter les milliardaires qui veulent détruire la démocratie et la planète ?](https://zinzolin.fr/souvenirs/techno-optimisme/) sur le blog Zinzolin. Oui, oui, trois fois oui. Anthony dit bien mieux que moi que le techno-solutionnisme n'est pas la direction à suivre, et pourtant beaucoup pensent l'inverse… et souvent ce sont des gens très riches, qui ont donc le pouvoir.

Extrait du manifeste qu'Anthony décortique :

>  les décès qui auraient pu être évités par une intelligence artificielle que l’on a empêché d’exister constituent une forme de meurtre

Oh bon sang. (ça sonne bien trop anti-IVG à mon goût avec tous les petits Mozart et Einstein qu'on aurait empêché de naître… yurk)

Nous avions tout faux depuis le début [à propos de PNG](https://floss.social/@FineFindus/111477918303650050) (en anglais).

[Gemstone – Crow Time](https://www.webtoons.com/en/canvas/crow-time/gemstone/viewer?title_no=693372&episode_no=118) (en anglais) : _Crow Time_, décidément. Que j'aime ces corbeaux.

Je découvre que le nom scientifique de l'axolotl est _Ambystoma mexicanum_. Maintenant je comprends la mélodie du refrain de [The Axolotl Song](https://www.youtube.com/watch?v=MxA0QVGVEJw) ! (oui bon avec le « tl » final je me doutais que c'était d'Amérique du Sud ou centrale, mais je n'avais jamais fait le lien). Cette chanson a 13 ans. Bon sang.

[Un ver à soie qui fait sa toilette](https://piaille.fr/@LaFox/111473071881425951).

[Dans une épicerie, une femme trouve une coccinelle](https://www.youtube.com/watch?v=6H7PW8tMIe0) (vidéo avec du son, en anglais, sous-titres anglais incrustés) : c'est tout mignon ! Sans en révéler plus, une jeune femme trouve une coccinelle sur un brin de coriandre, et la coccinelle n'a clairement pas l'air en forme, alors la femme tente de la sauver. Via l'Amoureux.

[There's Lava Inside You](https://www.youtube.com/watch?v=4Nv1qRjJMII) (vidéo avec du son, en anglais, sans sous-titres) : qu'est-ce que. 





---

[^1]:  Alors, pas pour tout le monde : l'Amoureux déclare qu'il ne comprend absolument pas, peut-être parce qu'il parle déjà bien anglais et n'a pas besoin de cette méthode.

