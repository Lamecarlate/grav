---
meta-création: 2024-12-06 23:02
meta-modification: 2024-12-06 23:02
stringified-date: "20241206.230209"
title: Liens de décembre 2024 (partie 1)
template: blog-item
date: 2024-12-15 15:02
update_date: ""
taxonomy:
  type: article
  tag:
    - musique
    - mignonneries
    - linguistique
    - informatique
    - art
highlight:
  enabled: false
description: Pas mal animaliers, ces liens, ce mois-ci. Également, un peu de linguistique, de musique et de vélo.
---
Pas mal animaliers, ces liens, ce mois-ci. Également, un peu de linguistique, de musique et de vélo.

===

[Enregistré en 1912, ce tapissier découvre son accent parisien](https://www.youtube.com/watch?v=amPtXEaXQO0) sur France Culture (vidéo avec du son, des sous-titres incrustés qui apparaissent en animation mais c'est plus supportable que sur TikTok, 4 minutes). Via l'ami Lapinours.

[Peter Gunn, par The Art of Noise et Duane Eddy](https://www.youtube.com/watch?v=tK-vUY6erQU) (vidéo avec du son, sans paroles sauf une petite intro en anglais, 5 minutes). Vous connaissez ce thème. Vous l'avez entendu quelque part. Forcément. Moi, c'était dans le film _Les Blues Brothers_. Et c'est à la base le thème d'une série télévisée de la fin des années 1950. Ici, c'est une interprétation par The Art of Noise avec le guitariste Duane Eddy (qui est le premier à avoir repris le thème), et le clip est une parodie de film noir de qualité. Inspecteur séduisant, femme fatale, flics en… heu… en imper de vinyle noir, rien n'est laissé au hasard et c'est très drôle. Via l'Amoureux.

[Look at you](https://www.youtube.com/watch?v=cxZ_fIAJZpw)  (vidéo avec du son, en anglais, avec sous-titres, 3 minutes). Ah oui dakor. C'est la même Rebecca Black du fameux Friday (vous vous souvenez, en 2011 ?). On s'est bien fichu⋅es de sa gueule sur Internet, bin elle a grandi, et en tant qu'artiste aussi. Sacrée voix.

[Raspberry Pi 500 Review: The keyboard is the computer, again](https://www.tomshardware.com/raspberry-pi/raspberry-pi-500-review) (en anglais). Ah dakor. Un Raspberry Pi, donc un ordinateur complet, dans un clavier. Je… je crois que je veux ça. Je ne sais pas encore ce que j'en ferais, mais ahhhh je veux. (L'idéal serait une version Azerty, quand même 😄)

---

![Photographie d'un junco ardoisé (petit oiseau gris et blanc avec des yeux noirs) qui a un flocon de neige accroché au coin de l'œil](dark-eyed-junco-wearing-a-snowflake.webp)

Source : [Jocelyn Anderson](https://bsky.app/profile/jocelynanderson.bsky.social/post/3lclnmulmvc2s).

(j'avoue avoir passé un temps à étudier les autres images de la personne, parce que je me méfie des images générées par IA, qui sont de mieux en mieux faites)

---

[Why drivers should want cycle lanes](https://www.youtube.com/watch?v=_DNNIB_PdaA) (vidéo avec du son et des sous-titres, en anglais, 10 minutes). Oui, il faut le répéter, mettre en place des politiques pour les vélos bénéficie aussi aux conducteurices de voiture, sur le long terme. Et en plus c'est raconté de manière marrante par Jay Foreman.

[Black girl with pearl](https://mastodon.art/@martinus/113623601130859938) (l'image originelle est de [Jenny Boot](https://www.jennyboot.nl/ode-black-girl-with-pearl/) mais sur son site il n'y a pas de description d'images…). C'est très beau. Une belle photo en hommage à la Jeune fille à la perle de Vermeer. Très personnellement, j'aurais habillé la modèle avec des vêtements colorés et non noirs, car tout se fond dans le décor à mes yeux, mais je suppose que c'était totalement volontaire.

---

![Une série de dessins d'hybrides entre des oiseaux et des mammifères (je fais la liste plus bas)](trash-gryphons.webp){.figure-media--wide}

Ce dessin, de [Mel](https://bsky.app/profile/cyaneus.bsky.social/post/3ld3ba2zs6c2u), contient une dizaine de griffons, mais pas le classique lion et aigle. On a :

- une pie mouffette (<i lang="en">magpie skunk</i>)
- un pigeon rat
- un vautour opossum (<i lang="en">vulture opossum</i>)
- un geai bleu écureuil (<i lang="en">blue jay squirrel</i>)
- un étourneau chat (<i lang="en">starling cat</i>)
- un ibis possum (<i lang="en">ibis possum</i>) (tiens, un deuxième opossum)
- un moineau souris (<i lang="en">sparrow mouse</i>)
- un corbeau raton laveur (<i lang="en">crow raccoon</i>)

J'adore particulièrement le moineau souris (juste parfait, petit, ramassé, mignon), l'étourneau chat (avec son poitrail somptueux, même si je lui aurais vu des oreilles plus pointues) et l'extraordinaire pie mouffette, prête à toutes les exactions.

Via [CoyoteTraveler](https://plush.city/@CoyoteTraveller/113639742293619788).

---

[Sesame Street: Jingle Bells](https://www.youtube.com/watch?v=JljlTFLX9g0) (vidéo avec du son, en anglais, avec sous-titres). Les sous-titres sont parfaits : je ne pensais pas lire un jour <q lang="en">quacking in beat</q>.

---

[Foone a une nouvelle manette de jeu vidéo, sa tourne mal (pour la manette)](https://digipres.club/@foone/113653353285819066)[^1] (en anglais). Foone a acheté une manette de jeu vidéo, et… elle clignote. Tout le temps. Donc Foone la démonte.

<blockquote lang="en">This thing doesn't have lights, this thing is full of light and it escapes through any seams or holes in the containment vessel.  
I'm doing surgery on an angel here and I don't expect them to survive it.</blockquote>

C'est beau.

---

[Le pire message de changement de mot de passe](https://phpc.social/@davidbisset/113641884532524972) (en anglais). Rassurez-vous, c'est une blague. N'est-ce pas ? N'est-ce pas ?! 🙀

---

![Photographie d'un chat gris, couché sur le flanc, avec le poil ratissé comme du gravier, un petit râteau et un caillou sont posés sur lui, pour imiter un jardin zen.](zen-garden-kitty.webp)

J'ai trouvé cette image chez [CatSalad](https://infosec.exchange/@catsalad/113620192288175520) mais elle tourne depuis des années, je n'ai pas réussi à trouver sa source première… C'est une jolie illusion de jardin zen, mais je ne peux m'empêcher de penser que le caillou a été posé après le ratissage et c'est de la triche, on aurait dû ratisser autour (en plus ça fait une jolie forme). Hihihi.

---

En début de semaine, l'envie soudaine de faire plein de biscuits pour ma famille que je vois à Noël m'a prise. J'ai fait un rétroplanning (🙀), hier j'ai fait les courses manquantes et dans la foulée, une fournée de test pour les biscuits. Bilan, ils sont tous validés ! J'en referai donc le double samedi prochain. En attendant, *\*bruits indistincts de croques et de crounches, assortis de légers slurps parce que le sablé, là, il passe bien avec le thé\**.

[^1]: Oui, le « sa » est parfaitement volontaire, culture Youtube, tout ça, j'ai même pas honte.
