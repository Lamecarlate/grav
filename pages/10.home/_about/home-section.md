---
classes: "home-section"
---

# Je suis Lara « Lamecarlate » Dufour.

Je fais du développement web, plutôt **front-end**[^1], à Grenoble, en créant de chouettes choses, que je veux **utiles**[^2] tout autant que **belles**[^3], avec mon cerveau et mon clavier.

[^1]: HTML et CSS n'ont pas beaucoup de secrets pour moi (mais il reste encore à apprendre ♥). JavaScript est juste derrière, et je sais aussi jouer avec PHP (et je fais un peu de bash pour le plaisir).

[^2]: J'accorde une importance capitale à l'accessibilité et l'inclusivité de ce que je produis : les gens qui vont sur le net ne sont pas toujours valides ou au top de leurs capacités. Un web accessible, c'est un web pour tout le monde.

[^3]: J’aime les chats, les chauves-souris et les ligatures typographiques.
