---
title: Accueil
body_classes: "page--home"
highlight:
  enabled: false

simplesearch:
  process: false

content:
  items: "@self.modules"
  order:
    by: default
    dir: asc
    custom:
      - _about
      - _writing

notebook_article:
  items:
    '@page.children': '/carnet'
  filter:
    type: blog-item
  limit: 1
  order:
    by: date
    dir: desc

notebook_short_note:
  items:
    '@page.children': '/carnet'
  filter:
    type: 'short-note'
  limit: 1
  order:
    by: date
    dir: desc

---

