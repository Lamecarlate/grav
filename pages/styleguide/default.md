---
title: Styleguide
body_classes: "styleguide"
imgcaptions:
  enabled: false
simplesearch:
  process: false
---

# Colors

<div class="colors">
  <div class="color color-primary"></div>
  <div class="color color-primary-offset"></div>
  <div class="color color-accent"></div>
  <div class="color color-accent-offset"></div>
  <div class="color color-text"></div>
  <div class="color color-text-offset"></div>
  <div class="color color-background"></div>
  <div class="color color-background-offset"></div>
  <div class="color color-border"></div>
</div>

# Atoms

# Title 1

Paragraph

## Title 2

Paragraph

### Title 3

Paragraph

#### Title 4

Paragraph

##### Title 5

Paragraph

###### Title 6

Paragraph

Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

> Blockquote Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
>
> Veggies es bonus vobis, proinde vos postulo essum magis kohlrabi welsh onion daikon amaranth tatsoi tomatillo melon azuki bean garlic.

Bluh!

> Veggies es bonus vobis, proinde vos postulo essum magis kohlrabi welsh onion daikon amaranth tatsoi tomatillo melon azuki bean garlic.

Bluh.

> Veggies !

[Link](#)

---

---

Veggies es bonus vobis, proinde vos postulo essum magis kohlrabi welsh onion daikon amaranth tatsoi tomatillo melon azuki bean garlic.

- List item 1
  - Nested list item
    - Nested list item lv3
- List item 2
- List item 3 with a long log text onec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- List item 4
- List item 5

Veggies es bonus vobis, proinde vos postulo essum magis kohlrabi welsh onion daikon amaranth tatsoi tomatillo melon azuki bean garlic.

1.  Ordered list item 1
    1. Nested ordered list item
       1. Nested ordered list item lv3
       2. Nested ordered list item lv3
2.  Ordered list item 2
3.  Ordered list item 3
4.  Ordered list item 4

Veggies!

---

```bash
#!/usr/bin/env bash

dir="[ici le chemin vers le dossier qui contiendra les notes]"
date=$(date +"%F_%H-%M-%S")
file=$dir$date".txt"

read text
touch "$file"
echo $text > "$file"
```

`sudo apt update && sudo apt upgrade`

Pour sauvegarder, il faut taper <kbd>Ctrl</kbd> + <kbd>S</kbd> en même temps.

<small>Je suis écrit en petit :( </small>

<sub>En indice</sub> et <sup>en exposant</sup> : le cation cuivre(II) (Cu<sup>2+</sup>) et l'anion sulfate (SO<sub>4</sub><sup>2-</sup> )

# Molecules

## Form

<p><button type="button">Button</button></p>

<form>
  <label for="input-demo">Label for input</label>
  <input placeholder="Text input" type="text" id="input-demo"> 
  <input value="Submit" type="submit">
</form>

<form>
  <input placeholder="Text input" type="text"> 
  <input placeholder="Text input" type="text"> 
  <input placeholder="Text input" type="text"> 
  <input placeholder="Text input" type="text"> 
  <input placeholder="Text input" type="text"> 
  <input placeholder="Text input" type="text"> 
  <input placeholder="Text input" type="text"> 
  <input placeholder="Text input" type="text"> 
  <input placeholder="Text input" type="text"> 
  <input placeholder="Text input" type="text"> 
  <input placeholder="Text input" type="text"> 
  <input placeholder="Text input" type="text"> 
</form>

<form>
  <label for="textarea-demo">Label for textarea</label>
  <textarea id="textarea-demo"></textarea> 
  <input value="Submit" type="submit">
</form>

# Components

## Article teaser

<article class="article-teaser article-teaser--with-image">
	<h1 class="article-teaser__title">
		<a href="" rel="bookmark">Article test</a>
	</h1>
	<div class="article-teaser__meta">
		<time class="article--teaser__date" datetime="2030-05-07T19:43:00+02:00" title="7 mai 2030">
			<span class="date__day">07</span>
			<span class="date__month">mai</span>
			<span class="date__year">2030</span>
		</time>
		<span class="article__taxonomy article__taxonomy--tags">
			<span class="article__taxonomy__title">Tag :</span>
			<a href="">du miam</a>
		</span>
		<span class="article__taxonomy article__taxonomy--categories">
			<span class="article__taxonomy__title">Catégorie :</span>
			<a href="">cuisine</a>
		</span>
	</div>
	<img class="article-teaser__image" loading="lazy" alt="" src="https://picsum.photos/id/10/640/240" width="640" height="240">
  <div class="article-teaser__content">
    <p>Intro mise en avant. Avec du texte. Plein. Un moulon. Un lorem ipsum de texteuh dolor sit amet, consectetur adipiscing elit. Donec ultricies tristique nulla et mattis. Phasellus id massa eget nisl congue blandit sit amet id ligula. Praesent et nulla eu augue tempus sagittis.</p>
  </div>
  <a href="" title="Lire « Article test »" class="article-teaser__read-more">Lire la suite</a>
</article>

<article class="article-teaser">
	<h1 class="article-teaser__title">
		<a href="" rel="bookmark">Article test</a>
	</h1>
	<div class="article-teaser__meta">
		<time class="article--teaser__date" datetime="2030-05-07T19:43:00+02:00" title="7 mai 2030">
			<span class="date__day">07</span>
			<span class="date__month">mai</span>
			<span class="date__year">2030</span>
		</time>
		<span class="article__taxonomy article__taxonomy--tags">
			<span class="article__taxonomy__title">Tag :</span>
			<a href="">du miam</a>
		</span>
		<span class="article__taxonomy article__taxonomy--categories">
			<span class="article__taxonomy__title">Catégorie :</span>
			<a href="">cuisine</a>
		</span>
	</div>
	<div class="article-teaser__content">
		<p>Intro mise en avant. Avec du texte. Plein. Un moulon. Un lorem ipsum de texteuh dolor sit amet, consectetur adipiscing elit. Donec ultricies tristique nulla et mattis. Phasellus id massa eget nisl congue blandit sit amet id ligula. Praesent et nulla eu augue tempus sagittis.</p>
	</div>
	<a href="" title="Lire « Article test »" class="article-teaser__read-more">Lire la suite</a>
</article>

## Article mini-teaser

<article class="article-teaser--mini article-teaser--mini--with-image">
  <div class="article-teaser--mini__content">
    <h1 class="article-teaser--mini__title">
      <a href="" rel="bookmark">Article test</a>
    </h1>
    <div class="article-teaser--mini__meta">
        <time class="article--teaser--mini__date" datetime="2030-05-07T19:43:00+02:00" title="7 mai 2030">
          <span class="date__day">07</span>
    	    <span class="date__month">mai</span>
    	    <span class="date__year">2030</span>
      </time>
    </div>
  </div>
  <img class="article-teaser--mini__image" loading="lazy" alt="" src="https://picsum.photos/id/1056/150/150" width="150" height="150">
</article>

## Spoiler

Passez la souris sur la zone colorée, ou bien donnez-lui le focus : les renards sont des animaux <span class="spoiler" tabindex="0">très mignons</span>, mais pas autant que les <span class="spoiler" tabindex="0">chats, parce que faut être sérieux un moment quand même</span>.

## Pagination

<nav class="pagination" aria-label="Liste des pages">
	<span class="pagination__item pagination_item--current" aria-label="Page courante, page 1">1</span>
	<a class="pagination__item" aria-label="Aller à la page 2" href="">2</a>
	<a class="pagination__item" aria-label="Aller à la page 3" href="">3</a>
	<span class="pagination__gap" aria-disabled="true">…</span>
	<a class="pagination__item" aria-label="Aller à la page 11" href="">11</a>
</nav>

## Recipe

<article id="recipe-fr15338425923c5fbea131755082c8d750ce1544033f" class="recipe" itemscope="" itemtype="https://schema.org/Recipe">
	<h1 class="recipe__title" itemprop="name">Tarte abricot-choco</h1>
	<img itemprop="image" class="recipe__image" src="https://picsum.photos/id/23/1200/702" alt="" width="1200" height="702">
		<dl class="recipe__meta">
			<dt>Préparation</dt>
			<dd>
				<time datetime="PT30M" content="PT30M" itemprop="prepTime">30min</time>
      </dd>
			<dt>Cuisson</dt>
			<dd>
				<time datetime="PT20M" content="PT20M" itemprop="cookTime">20min</time>
			</dd>
		</dl>
		<div class="recipe__ingredients">
			<h2 class="recipe__subtitle">Ingrédients</h2>
			<ul class="recipe__ingredients__list">
				<li itemprop="recipeIngredient">600 g d'abricots (masse comptée avec noyaux, environ 15 fruits)</li>
				<li itemprop="recipeIngredient">750 g de compote de pommes</li>
				<li itemprop="recipeIngredient">50 g de beurre de cacahuètes (environ 1 grosse cuillerée à soupe)</li>
				<li itemprop="recipeIngredient">50 g de chocolat noir (une demi-tablette)</li>
				<li itemprop="recipeIngredient">1 pâte feuilletée (vous pouvez la faire si vous voulez)</li>
			</ul>
		</div>
		<div class="recipe__steps">
			<h2 class="recipe__subtitle">Étapes</h2>
			<ol class="recipe__steps__list" itemprop="recipeInstructions">
				<li itemprop="step">Préchauffer le four à 160°C (le mien est à chaleur tournante, adapter selon les caractéristiques de son four)</li>
				<li itemprop="step">Couper les abricots en deux, les dénoyauter puis recouper chaque moitié en quatre dans le sens de la longueur, pour obtenir des lunes.</li>
				<li itemprop="step">Déposer la pâte feuilletée dans un moule à tarte.</li>
				<li itemprop="step">Mélanger la compote et le beurre de cacahuètes dans un grand bol.</li>
				<li itemprop="step">Verser l'appareil sur la pâte.</li>
				<li itemprop="step">Couper le chocolat en petits morceaux. Répartir les morceaux sur l'appareil.</li>
				<li itemprop="step">Disposer les abricots sur le tout, replier la pâte par-dessus.</li>
				<li itemprop="step">Enfourner pour environ 20 minutes - la durée exacte dépend du four.</li>
				<li itemprop="step">Laisser refroidir, la tarte est meilleure quand le chocolat est redevenu solide.</li>
			</ol>
		</div>
		<div class="recipe__content">
			<h2 class="recipe__subtitle">Notes</h2>
			<p>Le moule à tarte utilisé ici fait 26 cm de diamètre au fond, 28 au sommet.</p>
			<p>Pour les morceaux de chocolat, le chocolat est coupé en carreaux encore dans son emballage (comme ça on ne s'en met pas plein les doigts), puis chaque carreau en deux, ça amène à 24 morceaux, tout à fait suffisant pour répartir.</p>
			<p>Cette tarte peut être végétalienne si la pâte feuilletée l'est - c'était mon cas.</p>
		</div>
	</article>

<script> 
(function (window, undef) {
  'use strict';

  const getCSSProp = (element, propName) => getComputedStyle(element).getPropertyValue(propName);

  const colorBlocks = document.querySelectorAll('.color');
  colorBlocks.forEach(block => {
    const colorClass = Array.from(block.classList).filter(className => className.indexOf('color-') !== -1)[0];

    const name = document.createElement('input');
    name.setAttribute('readonly', '');
    name.value = '--' + colorClass;

    const value = document.createElement('input');
    value.setAttribute('readonly', '');
    value.value = getCSSProp(block, '--' + colorClass);

    block.append(name);
    block.append(value);

    document.body.addEventListener('theme-changed', function() {
        value.value = getCSSProp(block, '--' + colorClass);
    });
  });
})(window);
</script>
