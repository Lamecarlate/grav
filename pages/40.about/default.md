---
title: À propos
menu: À propos
slug: a-propos
id: about
highlight:
  enabled: false
---

===

# À propos

## Qui je suis

Dans mes signatures de mail, on trouve, pêle-mêle :

- Dompteuse de calissons
- Peintre de <abbr title="balises">&lt;/&gt;</abbr> et codeuse de choses
- Multi-tâche et multi-envies
- Façonneuse de mana

J'aime le web parce que l'idée d'écrire pour créer des mondes me plaît (oui bonjour c'est l'univers de Myst qui appelle, il aimerait récupérer sa métaphore).

J'aime le chocolat, mais pas à l'excès.

J'aime le tahin (spécialiste des "houmous chelous" apportés aux soirées jeux de rôle).

J'aime le son des pas dans la neige mais je préfère celui du ronron du chat sur le canapé pendant qu'il neige dehors.

On me dit très gentille mais je me sais assez rancunière.

Je suis plutôt du matin.

J'ai du mal à décoller de l'écran (j'y travaille… et j'y travaille)(j'y joue et écris aussi, mais du coup le jeu de mot ne marche plus).

---

(cette page est inspirée de celle de l'ami [Stéphane](https://nota-bene.org/_Stephane_))

## Contact

Vous pouvez me joindre à cette adresse e-mail: bonjour [@] laradufour [.] fr. Ou bien sur <a rel="me" href="https://pouet.it/@lamecarlate">Mastodon</a>.

## Ce site

Actuellement c'est surtout un blog, avec un portfolio minimal et une page "à propos" qui commence à partir dans tous les sens.

### Version actuelle

À la technique : GravCMS. Le thème est "Rose des sables", fait maison dans le sang la sueur les larmes et le chocolat chaud. Adapté (je l'espère) pour thèmes sombre et clair, il se veut simple et chaleureux, en utilisant des couleurs accessibles et parmi mes préférées. Je suis d'ailleurs ouverte à toute suggestion d'amélioration.

Est également prévu un système de feuilles de style par article, via notamment les propriétés personnalisées CSS (souvent appelées "variables CSS") ; pour l'instant (<time datetime="2021-02-12">en février 2021</time>), je ne l'ai pas utilisé - j'écris trop peu. Vivement un article sur un film de SF bien kitch pour y mettre un fond d'étoiles et du texte en néon.

Mise à jour du <time datetime="2021-03-18">18 mars 2021</time> : j'ai écrit [un article pour expliquer comment j'ai fait](../carnet/feuille-de-style-par-article), cet article a donc une feuille de style personnalisée. Avant ça, j'ai effectivement produit une critique de [Redshirts, de John Scalzi](../carnet/redshirts), mais pas de fond d'étoiles, déso.

### Versions précédentes

[Version 1 du site](https://v1.lamecarlate.net) : GravCMS également, avec le thème "Qoharis", fait maison, monochrome autour du rouge foncé.
