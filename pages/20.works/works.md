---
title: Portfolio
blog_url: portfolio
slug: portfolio
id: portfolio
menu: Portfolio
visible: true # temp: le temps que je finalise le portfolio
redirect: "https://laradufour.fr"
external_url: "https://laradufour.fr"

sitemap:
  changefreq: monthly
  priority: 1.03

content:
  items: "@self.children"
  order:
    by: date
    dir: desc
  limit: 10
  pagination: true

feed:
  description: Réalisations de Lara Dufour

highlight:
  enabled: false

pagination: true

simplesearch:
  process: false
---

Alors. Nous savons, vous et moi, que les pages "en travaux" c'est has been. Mais c'est un peu la vie d'un site, aussi, d'être en travaux. (et le mien l'est depuis sa mise en ligne, en pointillés)(des gros pointillés).

Sacrifions à la tradition : 🚧

J'ai travaillé avec la start-up Litii (aujourd'hui disparue), avec l'agence web [Asdoria](https://www.asdoria.com/), à l'agence de Grenoble de l'<abbr title="Entreprise de services du numérique">ESN</abbr> [Smile](https://www.smile.eu/fr) ; et suis aujourd'hui chez l'éditeur de logiciels [ProwebCE](https://prowebce.com).

J'ai participé à la conception et la réalisation de sites vitrines, e-boutiques ou intranet pour ces différents clients :

- Lafarge
  - pour leur site principal avant la fusion (site désormais inaccessible)
  - ainsi que le [site de la fusion LafargeHolcim](https://web.archive.org/web/20160305004717/http://www.lafargeholcim.com/)
  - et la partie connectée du [site Lafarge France](https://web.archive.org/web/20180630105534/https://www.lafarge.fr/)
- Mérieux Nutrisciences, pour leur [site d'entreprise](https://www.merieuxnutrisciences.com/corporate/fr "Site web de Mérieux Nutrisciences") et les sites pays
- Le département de la Vendée, pour le [site "Vendée Globe Junior"](https://web.archive.org/web/20180419155111/http://www.vendeeglobejunior.fr), qui explique aux enfants la course du Vendée Globe
- La communauté internationale des cardiologues ♥, pour leur [site de ressources PCRonline](https://web.archive.org/web/20170603205811/https://www.pcronline.com/)
- Allibert Trekking, pour la refonte de leur [site e-commerce](https://web.archive.org/web/20180523092741/https://www.allibert-trekking.com/ "Site web d'Allibert Trekking") de vente de randonnées autour du monde

Chez ProwebCE, j'ai travaillé dans un premier temps sur le logiciel de gestion de <abbr title="Comité Social et Économique">CSE</abbr> appelé v12 et sur le projet Wonder qui donne accès à des jeux (pronostics de sport, calendrier de l'avent) aux salarié⋅es. Puis j'ai été développeuse sur la refonte du backoffice de Meyclub, le site e-commerce phare de ProwebCE, et suis actuellement sur Meyclub directement, en tant que développeuse principalement front. Je m'occupe également de réaliser le <i lang="en">design system</i> interne de l'entreprise.

---

Note : dans la mesure du possible, les liens mènent vers la version archivée sur web.archive.org, afin de montrer le résultat au moment de la mise en production.
