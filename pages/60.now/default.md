---
title: En ce moment
menu: En ce moment
slug: en-ce-moment
route:
  default: "/en-ce-moment"
id: now
highlight:
  enabled: false
---

===

# En ce moment

<small>Ceci est une [page "now"](https://nownownow.com/about).</small>

Dernière mise à jour : <date date="2024-01-02">mardi 2 janvier 2024</date>.

Quarante ans depuis peu, et : ça va. Je porte peu d'attention à l'âge.

Trois grands amours : l'Amoureux humain officiel, Atrus-le-chat, Silver Blotched Tabby Extraordinaire, et Edora-la-chatte, Bicolore (un peu moins) Accro aux Chaussettes.

J'habite à côté de Grenoble, et c'est parti pour durer, sauf quand, dans dix ans peut-être, on ira s'installer toustes ensemble avec les ami⋅es du groupe de jeu de rôle dans une grande maison avec un jardin gigantesque et un pré aux chèvres.

Projets et lubies du moment :

- des bijoux en résine : mon vieux projet d'encapsuler et monter en pendentif une barrette de RAM précieusement conservée d'un de mes premiers PC portables va peut-être devenir une réalité
- écriture en stand by pour l'instant, ça n'avance pas ; ça reviendra !
- une envie de tout chambouler sur ce site (pas les URL, promis), peut-être passer au site statique généré

Lectures en cours :

- _L'ordre moins le pouvoir_, Normand Baillargeon
- _La maison des feuilles_, Mark Z. Danielewski

Dernières découvertes musicales :

- [_Leaves are falling_ du Garçon de l'Automne](https://legarcondelautomne.bandcamp.com/album/leaves-are-falling) (ya de la vielle à roue, j'ai pas pu résister)
- la comédie musicale _Goosebumps_ (adaptation d'une histoire des romans de la série du même nom, _Chair de poule_ en français)

---

<script type="text/javascript" src="https://encemoment.site/script.js"></script>

<now-webring><a href="https://encemoment.site">Découvrez le Webring “En ce moment”</a></now-webring>
